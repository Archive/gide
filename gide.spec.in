Name:		gide
Summary:	GNOME Integrated Development Environment
Version:	@VERSION@
Release:	1
License:	GPL
Group:		Development/Tools
Source:		http://gide.pn.org/download/%{name}-%{version}.tar.gz
BuildRoot:	/var/tmp/%{name}-%{version}-root
URL:		http://gide.pn.org

Requires:	gnome-libs >= @GNOME_LIBS_REQUIRED@
Requires:	bonobo >= @BONOBO_REQUIRED@
Requires:	oaf >= @OAF_REQUIRED@
Requires:	libglade >= @LIBGLADE_REQUIRED@
Requires:	gal >= @GAL_REQUIRED@
Requires:	gnome-vfs >= @GNOME_VFS_REQUIRED@
Requires:	gdl >= @GDL_REQUIRED@
Requires:	gnome-build >= @GNOME_BUILD_REQUIRED@
Requires:	gnome-debug >= @GNOME_DEBUG_REQUIRED@
BuildRequires:	gnome-libs-devel >= @GNOME_LIBS_REQUIRED@
BuildRequires:	bonobo-devel >= @BONOBO_REQUIRED@
BuildRequires:	oaf-devel >= @OAF_REQUIRED@
BuildRequires:	libglade-devel >= @LIBGLADE_REQUIRED@
BuildRequires:	gal-devel >= @GAL_REQUIRED@
BuildRequires:	gnome-vfs-devel >= @GNOME_VFS_REQUIRED@
BuildRequires:	gdl-devel >= @GDL_REQUIRED@
BuildRequires:	gnome-build-devel >= @GNOME_BUILD_REQUIRED@
BuildRequires:	gnome-debug-devel >= @GNOME_DEBUG_REQUIRED@

%description
Anjuta is part of the GNOME project and is an integrated design environment.

%package devel
Summary:	Libraries and include files gide.
Group:		Development/Tools
Requires:	%{name} = %{version}

%description devel
Libraries and header files if you want to develop your own plug-ins for gide.

%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%{_prefix} \
    --bindir=%{_bindir} --mandir=%{_mandir} \
    --localstatedir=%{_localstatedir} --libdir=%{_libdir} \
    --datadir=%{_datadir} --includedir=%{_includedir} \
    --sysconfdir=%{_sysconfdir}
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{_prefix} \
    --bindir=%{_bindir} --mandir=%{_mandir} \
    --localstatedir=%{_localstatedir} --libdir=%{_libdir} \
    --datadir=%{_datadir} --includedir=%{_includedir} \
    --sysconfdir=%{_sysconfdir}
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{_prefix} bindir=$RPM_BUILD_ROOT%{_bindir} \
    mandir=$RPM_BUILD_ROOT%{_mandir} libdir=$RPM_BUILD_ROOT%{_libdir} \
    localstatedir=$RPM_BUILD_ROOT%{_localstatedir} \
    datadir=$RPM_BUILD_ROOT%{_datadir} \
    includedir=$RPM_BUILD_ROOT%{_includedir} \
    sysconfdir=$RPM_BUILD_ROOT%{_sysconfdir} install

strip $RPM_BUILD_ROOT%{prefix}/bin/* || :

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_datadir}/locale/*
%{_datadir}/gide
%{_datadir}/oaf/*.oaf
%{_datadir}/gnome/ui/*.xml
%{_datadir}/gnome/apps/Development/Anjuta.desktop
%{_datadir}/pixmaps/*.png
%{_mandir}/man1/gide.1*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/libgide/
%{_datadir}/idl/*.idl
%{_libdir}/*a
%{_libdir}/*so

%changelog
* Tue Aug 14 2001 Jens Finke <jens@gnome.org>
- Consider possible gzipped man pages

* Wed Aug 08 2001 Jens Finke <jens@gnome.org>
- removed hardcoded prefix path
- use rpm variables in file section
- added devel package
- updated file section
- updated [Build]Requirements section
- and some other things
