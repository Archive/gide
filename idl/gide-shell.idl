/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * 
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef __ANJUTA_SHELL_IDL__
#define __ANJUTA_SHELL_IDL__

module GNOME {
module Development {
module Environment {
	interface Shell : Bonobo::Unknown {
		enum WindowLocation {
			LOC_TOP,
			LOC_BOTTOM,
			LOC_RIGHT,
			LOC_LEFT,
            LOC_CENTER,
			LOC_FLOATING,
			LOC_LAST
		};
		
		exception DoesntExist { };

		void addControl (in Bonobo::Control ctrl, 
				 in string name,
				 in string title,
				 in WindowLocation loc);
		void addPropertyPage (in Bonobo::Control ctrl,
				      in string name,
				      in string title);
		void addObject (in Bonobo::Unknown obj, in string name);
		void removeObject (in string name);
		Bonobo::Unknown getObject (in string name) raises (DoesntExist);

		void addData (in any value, in string name);
		void removeData (in string name);
        any getData (in string name);

		Bonobo::UIContainer getUIContainer ();
	};
};
};
};
#endif
