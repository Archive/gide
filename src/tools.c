#include <config.h>

#include <gdl/gdl.h>
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>

#include "tools.h"
#include "shell.h"
#include "e-splash.h"
#include "gal/widgets/e-cursors.h"
#include "gal/e-table/e-table-simple.h"
#include "gal/e-table/e-table.h"

#define TOOLS_SPEC "<ETableSpecification cursor-mode=\"line\" selection-mode=\"browse\" draw-focus=\"true\">                    	       \
  <ETableColumn model_col=\"0\" _title=\"Plugin name\" expansion=\"1.0\" minimum_width=\"100\" resizable=\"true\" cell=\"string\"      compare=\"string\"/> \
  <ETableColumn model_col=\"1\" _title=\"Load\"   expansion=\"1.0\" minimum_width=\"30\" resizable=\"true\" cell=\"checkbox\" compare=\"integer\"/> \
        <ETableState> \
	        <column source=\"0\"/> \
	        <column source=\"1\"/> \
	        <grouping> <leaf column=\"0\" ascending=\"true\"/> </grouping>    \
        </ETableState> \
</ETableSpecification>"

#define NAME_COLUMN 0
#define PLUGIN_CONFIG_LOCATION "/anjuta2/Plugins/"

gchar** table_data = NULL;
guint table_rowcount = 0;
gint* table_load = NULL;

typedef struct 
{
	char *name;
	Bonobo_PropertyBag tool;
} InstalledTool;

static char *
get_icon_path_for_component_info (const OAF_ServerInfo *info)
{
        OAF_Property *property;
        const char *shell_component_icon_value;

        /* FIXME: liboaf is not const-safe.  */
        property = oaf_server_info_prop_find ((OAF_ServerInfo *) info,
                                              "anjuta:shell_component_icon");
        if (property == NULL || property->v._d != OAF_P_STRING)
		return NULL;
	
        shell_component_icon_value = property->v._u.value_string;
	
        if (g_path_is_absolute (shell_component_icon_value))
                return g_strdup (shell_component_icon_value);
	
        else
                return g_concat_dir_and_file (ANJUTA_IMAGES, shell_component_icon_value);
}

static InstalledTool *
initialize_tool (AnjutaShell *shell, OAF_ServerInfo *s)
{
	Bonobo_PropertyBag tool = CORBA_OBJECT_NIL;
	
	CORBA_Environment ev;
	InstalledTool *ret;

	CORBA_exception_init (&ev);

	tool = oaf_activate_from_id (s->iid, 0, NULL, &ev);
	if (ev._major == CORBA_NO_EXCEPTION && !CORBA_Object_is_nil (tool,
								     &ev)) {
		bonobo_property_bag_client_set_value_string (tool, 
							     "anjuta-shell",
							     shell->id,
							     &ev);
	} else {
		if (ev._major != CORBA_NO_EXCEPTION) {
			g_print ("Exception activating %s: %s\n",  
				 oaf_server_info_prop_lookup (s, "name", NULL),
				 ev._repo_id);
		} else if (!CORBA_Object_is_nil (tool, &ev)) {
			g_print ("Null object while activating %s\n", 
				 oaf_server_info_prop_lookup (s, "name", NULL));
		}
		
		
	}
	
	if (!CORBA_Object_is_nil (tool, &ev)) {
		ret = g_new0 (InstalledTool, 1);
		ret->name = g_strdup (oaf_server_info_prop_lookup (s, "name", NULL));
		ret->tool = tool;
	} else {
		ret = NULL;
	}

	CORBA_exception_free (&ev);


	return ret;
}

static int
plugin_col_count (ETableModel *etc, void *data)
{
	return 2;
}

static int
plugin_row_count (ETableModel *etc, void *data)
{
	return table_rowcount;
}

static void *
plugin_value_at (ETableModel *etc, int col, int row, void *data)
{
	if(col == NAME_COLUMN)
	{
		return (void *) table_data [row];
	}
	else
	{
		/* this crashes sorting on the column! */
		return (void *) table_load[row];
	}
}

static void
plugin_set_value_at (ETableModel *etc, int col, int row, const void *val,
	void *data)
{
	if(col == NAME_COLUMN)
	{
		g_free (table_data [row]);
		table_data [row] = g_strdup (val);
	}
	else
	{
		table_load[row] = (gint)val;
	}
}

static gboolean
plugin_is_cell_editable (ETableModel *etc, int col, int row, void *data)
{
	switch(col)
	{
	case 0:
		return FALSE;
	case 1:
		return TRUE;
	default:
		g_warning("wrong col value in is_cell_editable\n");
		return FALSE;
	}
}

static void *
plugin_duplicate_value (ETableModel *etc, int col, const void *value,
	void *data)
{
	if(col == NAME_COLUMN)
	{
		return g_strdup (value);
	}
	else
	{
// TODO: fix this *&^%&
		return (void*)1;
	}
}

static void
plugin_free_value (ETableModel *etc, int col, void *value, void *data)
{
	if(col == NAME_COLUMN)
	{
		g_free (value);
	}
}

static void *
plugin_initialize_value (ETableModel *etc, int col, void *data)
{
	if(col == NAME_COLUMN)
	{
		return g_strdup ("");
	}
	else
	{
		return 0;
	}
}

static gboolean
plugin_value_is_empty (ETableModel *etc, int col, const void *value, void *data)
{
	if(col == NAME_COLUMN)
	{
		return !(value && *(char *)value);
	}
	else
	{
		return *(gint*)value;
	}
}

static char *
plugin_value_to_string (ETableModel *etc, int col, const void *value,
	void *data)
{
	if(col == NAME_COLUMN)
	{
		return g_strdup(value);
	}
	else
	{
		return g_strdup_printf("%d", *(gint*)value);
	}
}

static gint
plugin_name_search (gconstpointer a, gconstpointer b)
{
	return strcmp ((char *)a, (char *)b);
}

static gint
tool_name_search (gconstpointer a, gconstpointer b)
{
	return strcmp (((InstalledTool *)a)->name, (char *)b);
}

static GSList*
update_plugins (GSList *installed_tools, AnjutaShell *shell)
{
	gint i;
	CORBA_Environment ev;
	InstalledTool *tool;
	OAF_ServerInfo *ret;

	CORBA_exception_init (&ev);

	for (i = 0; i < table_rowcount; i++)
	{
		GSList *res;

		res = g_slist_find_custom (installed_tools,
			table_data[i], tool_name_search);
		if(res != NULL)
		{
			if(table_load[i])
			{
				continue;
			}

			tool = res->data;


/*			GNOME_Development_Environment_Tool_cleanup (
				tool->tool, &ev);*/
			bonobo_property_bag_client_set_value_string (tool->tool, 
								     "anjuta-shell",
								     "", &ev);

			installed_tools = g_slist_remove (installed_tools,
				tool);
			g_free (tool->name);
			CORBA_Object_release (tool->tool, &ev);
			g_free (tool);
		}
		else
		{
			gchar *query;
			OAF_ServerInfoList *oaf_result;
			int j;

			if(!table_load[i])
			{
				continue;
			}

/*			query = "repo_ids.has ('IDL:GNOME/Development/"
				"Environment/Tool:1.0')";*/
			query = "repo_ids.has ('IDL:Bonobo/PropertyBag:1.0') AND anjuta:shell_component == 'true'";
			oaf_result = oaf_query (query, NULL, &ev);

			if (ev._major == CORBA_NO_EXCEPTION &&
				oaf_result != NULL)
			{
				for (j = 0; j < oaf_result->_length; j++)
				{
					OAF_ServerInfo *s = &oaf_result->
						_buffer[j];

					if( !strcmp (
						oaf_server_info_prop_lookup (
						s, "name", NULL),
						table_data[i]))
					{
						ret = s;
						break;
					}
				}
			}
			tool = initialize_tool (shell, ret);

			if (tool)
			{
				installed_tools = g_slist_append (
					installed_tools, tool);
			}
		}
	}

	CORBA_exception_free (&ev);

	return installed_tools;
}

static GSList *
show_plugin_dialog (GSList *tools, GSList *installed_tools, AnjutaShell *shell)
{
	GtkWidget *e_table;
	ETableModel *e_table_model = NULL;
	GSList *l;
	GnomeDialog *dlg;
	gint button;
	GtkWidget *scrolled_window;
	int i;

	/* allocate room for all plugins that were found, name and status */
	table_data = g_malloc(sizeof(gchar*) * g_slist_length (tools));
	table_load = g_malloc(sizeof(gint) * g_slist_length (tools));

	/* find out what the status of the checkbox should be */
	gnome_config_push_prefix (PLUGIN_CONFIG_LOCATION);
	for (l = tools, table_rowcount = 0; l != NULL; l = l->next,
		table_rowcount++)
	{
		OAF_ServerInfo *s = l->data;
		gchar *query;

		table_data[table_rowcount] = g_strdup (
			oaf_server_info_prop_lookup (s, "name", NULL));
		query = g_strdup_printf ("%s=TRUE",
			table_data[table_rowcount]);
		table_load[table_rowcount] = gnome_config_get_bool (query);
		g_free (query);
	}
	gnome_config_pop_prefix ();

	/* create our table model */
	e_table_model = 
		e_table_simple_new (plugin_col_count, plugin_row_count,
				    NULL, plugin_value_at,
				    plugin_set_value_at,
				    plugin_is_cell_editable,
				    NULL, NULL, 
				    plugin_duplicate_value, plugin_free_value,
				    plugin_initialize_value,
				    plugin_value_is_empty,
				    plugin_value_to_string, NULL);
	
	/* create a dialog to show the table */
	dlg = GNOME_DIALOG (gnome_dialog_new (_("Anjuta plugin manager"),
		GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL));
	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	e_table = e_table_new (e_table_model, NULL, TOOLS_SPEC, NULL);
	/* e-table has a sucky default size */
	gtk_widget_set_usize (e_table, 200, 200);
	gtk_container_add (GTK_CONTAINER (scrolled_window), e_table);
	gtk_widget_show_all (scrolled_window);
	gtk_box_pack_start (GTK_BOX (dlg->vbox), scrolled_window, TRUE, TRUE,
		GNOME_PAD);

	/* run the dialog */
	button = gnome_dialog_run_and_close (dlg);
	switch (button)
	{
	case 0:
		/* on OK, save the plugin state */
		gnome_config_push_prefix (PLUGIN_CONFIG_LOCATION);
		for (i = 0; i < table_rowcount; i++)
		{
			gnome_config_set_bool (table_data[i], table_load[i]);
		}
		gnome_config_pop_prefix ();
		gnome_config_sync ();

		installed_tools = update_plugins(installed_tools, shell);
		break;
	default:
		break;
	}

	/* clean up */
	for (i = 0; i < table_rowcount; i++)
	{
		g_free (table_data[i]);
	}
	table_rowcount = 0;
	g_free (table_data);
	table_data = NULL;
	g_free (table_load);
	table_load = NULL;

	return installed_tools;
}

void
tools_plugin_manager (GtkWidget *widget, gpointer data)
{
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	GSList *tools = NULL;
	GSList *installed_tools = NULL;
	GSList *l = NULL;
	AnjutaShell *shell;

	/* create a list of all plugins */
	CORBA_exception_init (&ev);
	query = "repo_ids.has ('IDL:Bonobo/PropertyBag:1.0') AND anjuta:shell_component == 'true'";
	oaf_result = oaf_query (query, NULL, &ev);

	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL)
	{
		int i;

		for (i = 0; i < oaf_result->_length; i++)
		{
			OAF_ServerInfo *s = &oaf_result->_buffer[i];

			tools = g_slist_prepend (tools, s);
		}
	}

	installed_tools = (GSList*)(gtk_object_get_data (GTK_OBJECT (data),
							 "InstalledTools"));
	if(installed_tools)
	{
		for(l = installed_tools; l != NULL; l = l->next)
		{
			InstalledTool *t = l->data;
			g_print("Found tool %s\n", t->name);
		}
	}

	shell = gtk_object_get_data (GTK_OBJECT (data), "AnjutaShell");

	/* put the list in a dialog and let the user (de)activate plugins */
	installed_tools = show_plugin_dialog (tools, installed_tools, shell);
	gtk_object_set_data (GTK_OBJECT (data), "InstalledTools",
		installed_tools);

	g_slist_free(tools);
	CORBA_exception_free (&ev);
}

void
tools_init (AnjutaWindow *win, ESplash *splash)
{
	AnjutaShell *shell;
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	GSList *icon_tools = NULL;
	GSList *no_icon_tools = NULL;
	GSList *l;
	
	GList *tools = NULL;
	GSList *ignored_tools = NULL;
	void *iterator;
	char *name;
	char *value;

	shell = anjuta_shell_new (win);

	gtk_object_set_data (GTK_OBJECT (win), "AnjutaShell", shell);
	
	/* find out which plugins to remove */
	iterator = gnome_config_init_iterator (PLUGIN_CONFIG_LOCATION);
	while ((iterator = gnome_config_iterator_next (iterator,
		&name, &value)) != NULL)
	{
		if(!g_strcasecmp(value, "false"))
		{
			ignored_tools = g_slist_append (ignored_tools,
				g_strdup (name));
		}
		g_free (name);
		g_free (value);
	}

	CORBA_exception_init (&ev);

	query = "repo_ids.has ('IDL:Bonobo/PropertyBag:1.0') AND anjuta:shell_component == 'true'";

	oaf_result = oaf_query (query, NULL, &ev);

	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		int i;
		
		if (splash) {
			for (i = 0; i < oaf_result->_length; i++) {
				OAF_ServerInfo *s = &oaf_result->_buffer[i];
				GdkPixbuf *icon_pixbuf;
				char *icon_path;
				GSList *res;
				
				res = g_slist_find_custom (ignored_tools,
					oaf_server_info_prop_lookup (s, "name",
					NULL), plugin_name_search);
				if(res != NULL)
				{
					continue;
				}
				
				icon_path = get_icon_path_for_component_info (s);
				if (icon_path) {
					icon_pixbuf = gdk_pixbuf_new_from_file (icon_path);
					e_splash_add_icon (splash, icon_pixbuf);
					gdk_pixbuf_unref (icon_pixbuf);
					g_free (icon_path);
					icon_tools = 
						g_slist_prepend (icon_tools,
								 s);
				} else {
					no_icon_tools = 
						g_slist_prepend (no_icon_tools,
								 s);
				}
				
			}
		}
		if (no_icon_tools && splash) {
			GdkPixbuf *icon_pixbuf = gdk_pixbuf_new_from_file (ANJUTA_IMAGES "/unknown-components.png");
			e_splash_add_icon (splash, icon_pixbuf);
			gdk_pixbuf_unref (icon_pixbuf);
		}
			
		while (gtk_events_pending ())
			gtk_main_iteration ();
		
		i = 0;
		for (l = icon_tools, i = 0; l != NULL; 
		     l = l->next, i++) {			
			OAF_ServerInfo *s = l->data;
			InstalledTool *t;

			t = initialize_tool (shell, s);
			if (t) 
				tools = g_list_prepend (tools, t);
			
			if (splash) {
				e_splash_set_icon_highlight (splash, i, TRUE);
			}
			
			while (gtk_events_pending ())
				gtk_main_iteration ();
		}
		/* i now points at the "all the other tools" icon if one
		 * exists. */
		if (no_icon_tools) {
			for (l = no_icon_tools; l != NULL; l = l->next) {
				OAF_ServerInfo *s = l->data;
				InstalledTool *t;
				
				t = initialize_tool (shell, s);
				if (t)
					tools = g_list_prepend (tools, t);
				
				while (gtk_events_pending ())
					gtk_main_iteration ();
			}
			if (splash) {
				e_splash_set_icon_highlight (splash, i, TRUE);
			}
			
			while (gtk_events_pending ())
				gtk_main_iteration ();
		}
		
		CORBA_free (oaf_result);
	} else if (ev._major == CORBA_USER_EXCEPTION) {
		g_warning ("Error querying oaf.");
	} else {
		g_warning ("No plugins found.");
	}
	
	gtk_object_set_data (GTK_OBJECT (win), "InstalledTools", tools);
}

void 
tools_cleanup (AnjutaWindow *win)
{
	CORBA_Environment ev;
	AnjutaShell *shell = gtk_object_get_data (GTK_OBJECT (win), "AnjutaShell");
	GList *tools = gtk_object_get_data (GTK_OBJECT (win), "InstalledTools");
	GList *i;
	
	CORBA_exception_init (&ev);
	for (i = tools; i != NULL; i = i->next) {
		InstalledTool *tool = i->data;

		bonobo_property_bag_client_set_value_string (tool->tool, 
							     "anjuta-shell",
							     "", &ev);

		g_free (tool->name);
		CORBA_Object_release (tool->tool, &ev);
		g_free (tool);
	}	
	CORBA_exception_free (&ev);

	g_list_free (tools);
	
	bonobo_object_unref (BONOBO_OBJECT (shell));
}
