/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gide-windows.h"
#include "gI_window.h"
#include "tools.h"

static GList *window_list;

static void anjuta_window_delete (AnjutaWindow *window, gpointer data);
static void anjuta_window_destroy (AnjutaWindow *window, gpointer data);

AnjutaWindow *
anjuta_new_window (ESplash *splash)
{
	AnjutaWindow *window;

	/* Open Main Window */
	window = ANJUTA_WINDOW(anjuta_window_new());
	window_list = g_list_append(window_list, (gpointer) window);

	tools_init (window, splash);

	anjuta_window_load_layout (window);

	gtk_widget_show (GTK_WIDGET (window));
	
	gtk_signal_connect( GTK_OBJECT(window), "delete_event",
			    GTK_SIGNAL_FUNC(anjuta_window_delete), NULL );

	gtk_signal_connect( GTK_OBJECT(window), "destroy",
			    GTK_SIGNAL_FUNC(anjuta_window_destroy), NULL );

	return window;
}

static void
anjuta_window_delete (AnjutaWindow *window, gpointer data)
{
	gtk_object_destroy (GTK_OBJECT (window));
	
	gtk_main_quit ();
}

static void
anjuta_window_destroy (AnjutaWindow *window, gpointer data)
{
	g_list_remove (window_list, window);
	tools_cleanup (window);
}

