/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#include <sys/stat.h>
#include <libgnomeui/gnome-window-icon.h>

#include "shell.h"
#include "gI_window.h"
#include "gide-windows.h"
#include "moniker.h"

#include <liboaf/liboaf.h>
#include <bonobo.h>

/* Main window */
AnjutaWindow *main_window;

static const struct poptOption anjuta_popt_options [] = {
	{ NULL, '\0', 0, NULL, 0 }
};

int 
main (int argc, char *argv[]) 
{
	CORBA_ORB orb;
	poptContext ctx;
	gint nofargs = 0;
	GtkWidget *splash;
	CORBA_Environment ev;
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document document;
	const char **startup_files;

	/* Internationalization */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	
	/* Parse options */
	gnomelib_register_popt_table (oaf_popt_options, _("Oaf options"));
	gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv, 
				    anjuta_popt_options, 0, &ctx);
				    
	gnome_window_icon_set_default_from_file (GNOME_ICONDIR "/gide.png");

	/* Initialize Bonobo */
	orb = oaf_init (argc, argv);

	if (!bonobo_init (orb, NULL, NULL))
		g_error ("Failure starting Bonobo");

	bonobo_activate ();

	/* Initialize glade */	
	glade_gnome_init ();

	anjuta_moniker_init ();

	/* Display splashscreen */
	splash = e_splash_new ();
	gtk_widget_show (splash);
	gtk_object_ref (GTK_OBJECT (splash));
	while (gtk_events_pending ())
		gtk_main_iteration ();

	/* Create main window */
	main_window = anjuta_new_window (E_SPLASH (splash));
	
	/* Open files from the command line */
	CORBA_exception_init (&ev);
	docman = GNOME_Development_Environment_Shell_getObject 
		(bonobo_object_corba_objref 
		 (gtk_object_get_data (GTK_OBJECT (main_window), "AnjutaShell")),
		 "DocumentManager", &ev);
	
	/* open the files */
	startup_files = poptGetArgs (ctx);
	nofargs = 0;
	if (startup_files && docman != CORBA_OBJECT_NIL) {
		gchar *curdir = g_get_current_dir ();
		
		while (startup_files [nofargs]) {
			gchar *filename = (gchar *) startup_files [nofargs];
			
			/* openFile needs a full path for the file */
			if (g_path_is_absolute (filename))
				filename = g_strdup (filename);
			else
				filename = g_strdup_printf ("%s/%s", 
							    curdir, filename);

			document = GNOME_Development_Environment_DocumentManager_openFile (docman, filename, &ev);
			bonobo_object_release_unref (document, &ev);
			nofargs++;
			g_free (filename);
		}
		g_free (curdir);
	};

	bonobo_object_release_unref (docman, &ev);

#if 0
	if (nofargs == 0) {
		/* no files given on the commandline, open files from last session */
		open_history (HISTORY);
	}
#endif

	poptFreeContext (ctx);

	gtk_widget_unref (splash);
	gtk_widget_destroy (splash);

	/* GTK main loop */
	gtk_main ();

#if 0
	/* Destroy the main_window */
	gtk_object_destroy (GTK_OBJECT (main_window));
#endif

	/* All�s great, return EXIT_SUCCESS */
	return (EXIT_SUCCESS);
}
