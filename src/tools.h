#ifndef PACKAGES_H
#define PACKAGES_H

#include "shell.h"
#include "e-splash.h"

void tools_init (AnjutaWindow *win, ESplash *splash);
void tools_cleanup (AnjutaWindow *win);
void tools_plugin_manager (GtkWidget *widget, gpointer data);

#endif
