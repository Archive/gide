/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *               2001 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <liboaf/liboaf.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-listener.h>
#include "gI_window.h"
#include "tools.h"
#include <gdl/gdl-dock.h>

#include <libgide/libgide.h>

#include <gnome-xml/parser.h>

static void
tmp_exit (GtkWidget *w, AnjutaWindow *window)
{
	gtk_object_destroy (GTK_OBJECT (window));
	gtk_main_quit ();
}

static void
about_cmd_destroy (GtkWidget *w, gpointer data)
{
	GtkWidget **about = (GtkWidget **)data;
	*about = NULL;
}

static void
about_cmd (GtkWidget *w, AnjutaWindow *window)
{
	static const gchar *authors[] = {
		"JP Rosevear",
		"Dave Camp",
		"Dirk Vangestel",
		"",
		"Original Author: Steffen Kern",
		NULL};
	
	static GtkWidget *about = NULL;
	if (!about) {
		about = gnome_about_new ( _("Anjuta DevStudio"), 
			VERSION,
			_("Copyright Various Authors (C) 1998-2001"),
			authors,
			"A Gnome based IDE", 
			NULL);
		gtk_signal_connect (GTK_OBJECT (about), "destroy",
				    about_cmd_destroy, &about);
		gtk_widget_show (about);
	} else
		gdk_window_raise (GTK_WIDGET (about)->window);
}

static void
view_fullscreen_cmd (BonoboUIComponent           *component,
		     const char                  *path,
		     Bonobo_UIComponent_EventType type,
		     const char                  *state,
		     AnjutaWindow                  *window)
{
	GdkWindow *gdk_window = GTK_WIDGET (window)->window;
	gint client_x, client_y, root_x, root_y;
	gint width, height;
	gboolean fullscreen = atoi (state);
	GtkAllocation *alloc;

	if (fullscreen) /* Get to the top */		
		gnome_win_hints_set_layer (GTK_WIDGET (window), 
					   WIN_LAYER_ABOVE_DOCK);
	else /* Down boy! */		
		gnome_win_hints_set_layer (GTK_WIDGET (window), 
					   WIN_LAYER_NORMAL);

	/* Make sure it's raised */
	gdk_window_raise (gdk_window);
	
	/* Window possize & position data */
	alloc = gtk_object_get_data (GTK_OBJECT (window), "windowpos");
	if (!alloc) {
		alloc = g_new0 (GtkAllocation, 1);
		gtk_object_set_data (GTK_OBJECT (window), "windowpos", alloc);
	}
	
	if (fullscreen) { /* Go to fullscreen */
		gdk_window_get_origin (gdk_window, &root_x, &root_y);
		gdk_window_get_geometry (gdk_window, &client_x, &client_y,
					 &width, &height, NULL);

		gdk_window_move_resize (gdk_window, -client_x, -client_y,
					gdk_screen_width () + 1,
					gdk_screen_height () + 1);
		
		alloc->x = root_x - client_x;
		alloc->y = root_y - client_y;
		alloc->width = width;
		alloc->height = height;
	} else { /* Restore window size & position */
		gdk_window_move_resize (gdk_window, 
					alloc->x, alloc->y,
					alloc->width, 
					alloc->height);
	}
}

/* Menu verbs */
static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileExit", tmp_exit),
	BONOBO_UI_UNSAFE_VERB ("SettingsPlugins", tools_plugin_manager),
#if 0
	BONOBO_UI_UNSAFE_VERB ("SettingsSettings", dialog_prefs_cb),
#endif
	BONOBO_UI_UNSAFE_VERB ("HelpAbout", about_cmd),
	
	BONOBO_UI_VERB_END
};

/* Prototypes */
static void anjuta_window_class_init (AnjutaWindowClass *class);
static void anjuta_window_init (AnjutaWindow *tool);
static void anjuta_window_drag_recv(GtkWidget* widget, GdkDragContext* context,
				  gint x, gint y, GtkSelectionData* seldata,
				  guint info, guint time, gpointer data);
static void anjuta_window_destroy (GtkWidget *widget, gpointer data);

guint
anjuta_window_get_type()
{
	static guint window_type = 0;
	
	if (!window_type) {
		GtkTypeInfo window_info = {
               "AnjutaWindow",
               sizeof (AnjutaWindow),
               sizeof (AnjutaWindowClass),
               (GtkClassInitFunc) anjuta_window_class_init,
               (GtkObjectInitFunc) anjuta_window_init,
               (GtkArgSetFunc) NULL,
               (GtkArgGetFunc) NULL
             };
             window_type = gtk_type_unique (bonobo_window_get_type (),
					    &window_info);
           }
		
         return window_type;
}

static void
anjuta_window_class_init (AnjutaWindowClass *class)
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass*) class;
}

static gint
save_layout_idle_cb (gpointer data)
{
	anjuta_window_save_layout (ANJUTA_WINDOW (data));
	return TRUE;
}

static void
layout_changed_cb (GdlDock *dock, gpointer data)
{
	gtk_idle_add (save_layout_idle_cb, data);
}

static void
anjuta_window_init (AnjutaWindow *window)
{
	BonoboUIContainer *ui_container;
	GtkTargetEntry dragtypes[] = {{"text/uri-list", 0, 0}};

	bonobo_window_construct (BONOBO_WINDOW (window), "anjuta2", 
				 "Anjuta DevStudio");

	gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, TRUE);


	window->dock = gdl_dock_new ();

	gtk_signal_connect (GTK_WIDGET (window->dock), "layout_changed",
			    layout_changed_cb, window);

	bonobo_window_set_contents (BONOBO_WINDOW (window),
				    window->dock);
	gtk_widget_show (window->dock);

	gtk_window_set_default_size (GTK_WINDOW (window), 800, 550);
	gtk_widget_realize (GTK_WIDGET(window));
	
	/* drag'n'drop */
	gtk_drag_dest_set (GTK_WIDGET(window),
			   GTK_DEST_DEFAULT_ALL, dragtypes,
			   sizeof(dragtypes) / sizeof(dragtypes[0]),
			   GDK_ACTION_COPY);
	gtk_signal_connect (GTK_OBJECT(window), "drag_data_received",
			    GTK_SIGNAL_FUNC(anjuta_window_drag_recv),
			    (gpointer)window);

	window->uic = bonobo_ui_component_new_default ();

	ui_container = bonobo_ui_container_new ();
	bonobo_ui_container_set_win (ui_container, BONOBO_WINDOW (window));
	
	bonobo_ui_component_set_container (window->uic, 
					   BONOBO_OBJREF (ui_container));

	bonobo_ui_component_add_verb_list_with_data (window->uic, 
						     verbs, window);

	bonobo_ui_util_set_ui (window->uic, GNOME_DATADIR, "gide.xml", "anjuta");
	
	bonobo_ui_component_add_listener (window->uic, "ViewFullscreen", 
					  view_fullscreen_cmd, window);
	
	window->ui_container = ui_container;
	
	bonobo_ui_engine_config_set_path (bonobo_window_get_ui_engine (BONOBO_WINDOW (window)), "/anjuta2/UIConf/kvps");
	
	/* autosave */
	window->timeout_id = 0;
	
	gtk_signal_connect (GTK_OBJECT(window), "destroy",
			    GTK_SIGNAL_FUNC(anjuta_window_destroy), 
			    NULL);

	gtk_widget_queue_draw (GTK_WIDGET(window));
	gtk_widget_queue_resize (GTK_WIDGET(window));
}

GtkWidget *
anjuta_window_new (void)
{
	AnjutaWindow *window;
	
	window = ANJUTA_WINDOW (gtk_type_new (anjuta_window_get_type ()));

	/* create our menus */	

	return GTK_WIDGET(window);
}

void 
anjuta_window_add_widget (AnjutaWindow *window, GtkWidget *w, 
			const char *id,
			const char *name,
			GNOME_Development_Environment_Shell_WindowLocation loc)
{
	GtkWidget *item;
	item = gdl_dock_item_new (id, name, GDL_DOCK_ITEM_BEH_NORMAL);
	gtk_container_add (GTK_CONTAINER (item), w);
	gdl_dock_add_item (GDL_DOCK (window->dock), 
			   GDL_DOCK_ITEM (item), GDL_DOCK_LEFT);

	gtk_object_set_data (GTK_OBJECT (w), "dockitem", item);

	gtk_widget_show_all (item);	
}

void
anjuta_window_remove_widget (AnjutaWindow *window, GtkWidget *w)
{
	GtkWidget *item;
	item = gtk_object_get_data (GTK_OBJECT (w), "dockitem");
	gtk_container_remove (item->parent, item);
}

void
anjuta_window_save_layout (AnjutaWindow *window)
{
	char *dir;
	char *filename;

	xmlDocPtr doc;
	xmlNodePtr node;

	FILE *f;

	doc = xmlNewDoc ("1.0");
	doc->root = xmlNewDocNode (doc, NULL, "dock-layout", NULL);
	node = xmlNewChild (doc->root, NULL, "layout", NULL);
	xmlSetProp (node, "name", "Default");

	gdl_dock_layout_save (GDL_DOCK (window->dock), node);
	
	dir = gnome_util_prepend_user_home (".anjuta2");
	
	if (!g_file_test (dir, G_FILE_TEST_ISDIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_ISDIR)) {
			anjuta_error_dialog ("Could not create .anjuta2 directory.");
			return;
		}
	}
	g_free (dir);

	filename = gnome_util_prepend_user_home (".anjuta2/layout.xml");
	f = fopen (filename, "w");
	if (f) {
		xmlDocDump (f, doc);
		fclose (f);
	} else {
		anjuta_error_dialog ("Could not save layout.");
	}
	xmlFreeDoc (doc);
}

static xmlDocPtr
load_layout_from_file (AnjutaWindow *window, const char *filename)
{
	xmlDocPtr ret;
	if (g_file_test (filename, G_FILE_TEST_ISFILE)) {
		ret = xmlParseFile (filename);
		if (ret) {
			if (ret->root  
			    && ret->root->childs) {
				return ret;
			} else {
				xmlFreeDoc (ret);
				ret = NULL;
			}		
		}
	}
	return NULL;
}

void
anjuta_window_load_layout (AnjutaWindow *window)
{
	char *filename;	
	xmlDocPtr doc;
	xmlNodePtr node;

	filename = gnome_util_prepend_user_home (".anjuta2/layout.xml");
	if (!(doc = load_layout_from_file (window, filename))) {
		g_free (filename);
		filename = g_concat_dir_and_file (GNOME_DATADIR "/anjuta2",
						  "layout.xml");
		if (!(doc = load_layout_from_file (window, filename))) {
		}
	}
	g_free (filename);

	if (doc) {
		gdl_dock_layout_load (GDL_DOCK (window->dock), doc->root->childs->childs);
		xmlFreeDoc (doc);
	}
}


static void
anjuta_window_drag_recv(GtkWidget* widget, GdkDragContext* context,
		    gint x, gint y, GtkSelectionData* seldata,
		    guint info, guint time, gpointer data)
{
	GList* files;
	GList* fnp;
	gint count;
	gchar* fname;
	CORBA_Environment ev;
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document document;

	g_assert(IS_ANJUTA_WINDOW(widget));

	/* get filenames */
	files = gnome_uri_list_extract_filenames((gchar*)seldata->data);
	count = g_list_length(files);

	CORBA_exception_init (&ev);
	docman = GNOME_Development_Environment_Shell_getObject 
		(bonobo_object_corba_objref 
		 (gtk_object_get_data (GTK_OBJECT (widget), "AnjutaShell")),
		 "DocumentManager", &ev);
	
	/* open files */
	if(count > 0 && docman != CORBA_OBJECT_NIL)
	{
		gchar *curdir = g_get_current_dir ();
		
		fnp = g_list_first(files);
		while(fnp)
		{
			fname = (gchar*)fnp->data;
			
			/* openFile needs a full path for the file */
			if (g_path_is_absolute (fname))
				fname = g_strdup (fname);
			else
				fname = g_strdup_printf ("%s/%s", curdir, fname);

			document = GNOME_Development_Environment_DocumentManager_openFile (docman, fname, &ev);
			bonobo_object_release_unref (document, &ev);
			g_free (fname);

			fnp = g_list_next(fnp);
		}
		g_free (curdir);
	}

	gnome_uri_list_free_strings(files);
}

static void
anjuta_window_destroy (GtkWidget *widget, gpointer data)
{
	AnjutaWindow *window;
	GtkAllocation *alloc;

	g_assert (IS_ANJUTA_WINDOW (widget));

	window = ANJUTA_WINDOW (widget);
	
	alloc = gtk_object_get_data (GTK_OBJECT (window), "windowpos");
	g_free (alloc);
}
