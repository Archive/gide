/* Anjuta
 * Copyright (C) 2001 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <bonobo.h>
#include <libgide/libgide.h>
#include <gnome-xml/tree.h>

#define ANJUTA_WINDOW(o)          (GTK_CHECK_CAST ((o), anjuta_window_get_type(), AnjutaWindow))
#define ANJUTA_WINDOW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), anjuta_window_get_type(), AnjutaWindowClass))
#define IS_ANJUTA_WINDOW(o)       (GTK_CHECK_TYPE ((o), anjuta_window_get_type()))

typedef struct _AnjutaWindowClass         AnjutaWindowClass;

struct _AnjutaWindowClass {
	BonoboWindowClass   parent_class;
};

typedef struct _AnjutaWindow		AnjutaWindow;

struct _AnjutaWindow {
	BonoboWindow app;
	
	BonoboUIComponent *uic;
	BonoboUIContainer *ui_container;

	GtkWidget *dock;
 	
	gint timeout_id;
};

/*
 * Prototypes for 'gI_window.c'
 */
guint anjuta_window_get_type (void);
GtkWidget *anjuta_window_new (void);

void anjuta_window_add_widget (AnjutaWindow *window, 
			     GtkWidget *w, 
			     const char *name,
			     const char *title,
			     GNOME_Development_Environment_Shell_WindowLocation loc);
void anjuta_window_remove_widget (AnjutaWindow *window,
				GtkWidget *w);

void anjuta_window_save_layout (AnjutaWindow *window);
void anjuta_window_load_layout (AnjutaWindow *window);

#endif

