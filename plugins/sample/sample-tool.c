/*
 * Anjuta sample plugin
 *
 * Demonstrates the basics of how to write a plugin for Anjuta.
 */

#include <config.h>

#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <gdl/GDL.h>
#include <bonobo.h>

#define SAMPLE_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:sample"

/*
 * Insert the text "Hello, World!" in the current document
 */
static void
hello_world (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);

	anjuta_insert_text_at_cursor (tool, "Hello, world!\n");
}

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("HelloWorld", hello_world),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (SAMPLE_COMPONENT_IID, "Anjuta Sample Tool", 
			  "anjuta-sample-tool", ANJUTA_DATADIR, 
			  "gide-sample-plugin.xml", verbs, NULL)
