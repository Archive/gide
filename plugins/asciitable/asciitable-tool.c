/*
 * Anjuta asciitable plugin
 *
 * Shows an ASCII table
 */

#include <config.h>

#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>

#define ASCIITABLE_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:asciitable"
#define PLUGIN_NAME			"anjuta-asciitable-plugin"
#define PLUGIN_XML			"gide-asciitable-plugin.xml"

static void
asciitable_select(
	GtkWidget*			clist,
	gint				row,
	gint				column,
	GdkEventButton*			bevent,
	gpointer			data
)
{
	AnjutaTool*			tool = ANJUTA_TOOL(data);

	if(!bevent)
	{
		return;
	}

	if(bevent->type == GDK_2BUTTON_PRESS)
	{
		gchar*			ch;

		gtk_clist_get_text(GTK_CLIST(clist), row, 0, &ch);
		ch += 2;

		anjuta_insert_text_at_cursor(tool, ch);
	}
}

static void
asciitable(
	GtkWidget*			widget,
	gpointer			data
)
{
	GtkWidget*			window;
	GtkWidget*			scrwindow;
	GtkWidget*			clist;
	gchar*				list_titles[] = {
		"Char", "Dec#", "Hex#", "Oct#" };
	gint				i;
	gchar				ch[10];
	gchar				dec[10];
	gchar				hex[10];
	gchar				oct[10];
	gchar*				items[4];

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
		GTK_SIGNAL_FUNC(gtk_widget_destroyed), NULL);
	gtk_window_set_title(GTK_WINDOW(window), _("ASCII Table"));
	gtk_widget_set_usize(window, 210, 400);

	scrwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(window), scrwindow);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrwindow), 
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	clist = gtk_clist_new_with_titles(4, list_titles);
	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
		GTK_SIGNAL_FUNC(asciitable_select), data);
	gtk_container_add(GTK_CONTAINER(scrwindow), clist);
	gtk_clist_column_titles_passive(GTK_CLIST(clist));
	gtk_clist_set_column_width(GTK_CLIST(clist), 0, 40);
	gtk_clist_set_column_width(GTK_CLIST(clist), 1, 40);
	gtk_clist_set_column_width(GTK_CLIST(clist), 2, 40);
	gtk_clist_set_column_width(GTK_CLIST(clist), 3, 40);

	for(i = 0; i < 256; i++)
	{
		sprintf(ch, "%3c", i);
		sprintf(dec, "%3d", i);
		sprintf(hex, "0x%2.2X", i);
		sprintf(oct, "%3.3o", i);

		items[0] = ch;
		items[1] = dec;
		items[2] = hex;
		items[3] = oct;

		gtk_clist_append(GTK_CLIST(clist), items);
	}

	gtk_widget_show_all(window);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("AsciiTable", asciitable),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (ASCIITABLE_COMPONENT_IID, "Anjuta Asciitable Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
