/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <libgide/libgide.h>
#include "tree.h"
#include "parser.h"

#define MAXFUNC				1024
#define MAXLEN				4096
#define TEXT_SPACING			5

/* ignore-list */
/* uh oh: no overflow checking anywhere! (shouldn't be an array anyway) */
static gchar*				functionarray[MAXFUNC];

/* globals */
GtkWidget*				tree_window = NULL;
static GtkWidget*			root_tree;
static GtkCTreeNode*			root_item;
static GtkCTreeNode*			sub_tree;
static gchar				org_line[MAXLEN];
static glong				fuc = -1;
static glong				fic = 0;
static gchar*				filearray[MAXLEN];
static glong				maxrec = 0;
static GList* 				selected_node;

/* Prototypes for local functions */
static void tree_window_destroy(GtkWidget* widget, gpointer data);
static glong gen_tree(gchar* file);
static glong c_parse_line(gchar* buf, glong line, glong recursive);
static glong get_function_name(gchar* key, c_status* c_status, glong modus,
	glong recursive, glong line, glong prototype);
static glong strip_shit_1(gchar* dest, gchar* source);
static glong strip_shit_2(gchar* dest, gchar* source);
static glong strip_shit_3(gchar* dest, gchar* source);
static glong follow_function(gchar* function);
static glong find_function(FILE* file, gchar* function, glong line);
static void select_function(GtkWidget* widget, gint row, gint column,
	GdkEventButton* event, AnjutaTool* tool);
static void select_function_tree(GtkCTree *ctree, GList *node, gint column,
	gpointer user_data);


/* local functions */
static void
tree_window_destroy(
	GtkWidget*			widget,
	gpointer			data
)
{
	gtk_widget_destroy(tree_window);
	tree_window = NULL;
}

static void
select_function(
	GtkWidget*			widget,
	gint				row,
	gint				column,
	GdkEventButton*		       	event,
	AnjutaTool*			tool
)
{
	gchar*				selected_file;
	gchar*				selected_line;
	gchar* 				ptr;
	GtkCTreeRow*			tree_row;
	GtkCTreeNode*			parent;

	if(!event)
	{
		return;
	}

	if(event->type == GDK_2BUTTON_PRESS && selected_node)
	{
		tree_row = GTK_CTREE_ROW(GTK_CTREE_NODE(selected_node));
		selected_line = g_strdup(GTK_CELL_TEXT(tree_row->row.cell[0])->
			text);

		parent = tree_row->parent;
		if(tree_row->is_leaf)
		{
			parent =  GTK_CTREE_ROW(GTK_CTREE_NODE(parent))->parent;
		}

		tree_row = GTK_CTREE_ROW(GTK_CTREE_NODE(parent));
		selected_file = g_strdup(GTK_CELL_TEXT(tree_row->row.cell[0])->
			text);

		anjuta_show_file(tool, selected_file);
		ptr = strchr(selected_line, '[');
		++ptr;
		anjuta_set_line_num(tool, atol(ptr));
	}
}

static void
select_function_tree(
	GtkCTree*			ctree,
	GList*				node,
	gint				column,
	gpointer			user_data
)
{
	selected_node = node;
}

static glong
gen_tree(
	gchar*				infile
)
{
	gchar				buf[MAXLEN];
	glong				i;
	glong				line = 0;
	c_status			c_status;
	FILE*				file;
	gchar*				tmp_array[1];

	selected_node = NULL;

	/* root item */
	tmp_array[0] = infile;
	root_item = gtk_ctree_insert_node(GTK_CTREE(root_tree), NULL, NULL, 
		tmp_array, TEXT_SPACING, NULL, NULL, NULL, NULL, FALSE, FALSE);

	if(maxrec > 0)
	{
		g_print("Max. Depth Level: %-2ld\n", maxrec);
	}

	file = fopen(infile, "r");
	if(file == NULL)
	{
		g_print("ERROR: can't open file %s\n", infile);
		return -1;
	}

	init_cstatus(&c_status);

	while(!feof(file))
	{
		fgets(buf, sizeof(buf), file);
		if(feof(file))
		{
			break;
		}
		for(i = 0; i <= strlen(buf); i++)
		{
			c_parse_special(buf, i, &c_status);
		}
	}

	if(c_status.comment)
	{
		anjuta_error_dialog("ERROR: base parsing failed, unfinished "
			"comment");
		return -1;
	}

	if(c_status.curly_brackets || c_status.round_brackets ||
		c_status.square_brackets)
	{
		g_print("\nERROR: base parsing failed, brackets don't match: "
			"%ld, %ld, %ld\n", c_status.curly_brackets,
			c_status.round_brackets, c_status.square_brackets);
		return -1;
	}

	/* Re-Init */
	rewind(file);
	init_cstatus(&c_status);

	while(!feof(file))
	{
		fgets(buf, sizeof(buf), file);
		line++;
		if(feof(file))
		{
			break;
		}
		c_parse_line(buf, line, 0);
	}

	return 1;
}

static glong
c_parse_line(
	gchar*				buf,
	glong				line,
	glong				recursive
)
{
	gchar				key[MAXLEN];
	glong				kc = 0;
	glong				i;
	static c_status			c_status;

	/* backup the complete original line */
	strcpy(org_line, buf);

	/* parse... */
	for(i = 0; i < strlen(buf); i++)
	{
		c_parse_special(buf, i, &c_status);

		if(c_status.comment || c_status.hyph || c_status.dhyph)
		{
			continue;
		}

		/* abort parsing, if in recursive mode and we left the
		   function */
		if(recursive > 0 && !c_status.curly_brackets)
		{
			return 2;
		}

		switch(buf[i])
		{
		case '(':
			if(kc != 0)
			{
				key[kc] = '\0';
				kc = 0;

				if(!recursive)
				{
					if(c_status.curly_brackets)
					{
						get_function_name(key,
							&c_status, 1, 0, line,
							-1);
					}
					else
					{
						get_function_name(key,
							&c_status, 0, 0, line,
							-1);
					}
				}
				else
				{
					get_function_name(key, &c_status, 1,
						recursive, line, -1);
				}

			}
			break;

		case ')':
		case ';':
		case '=':
		case '/':
		case '*':
		case '+':
		case '-':
			kc = 0;
			break;

		default:
			key[kc] = buf[i];
			kc++;
			break;
		}
	}

	return 1;
}


static glong
get_function_name(
	gchar*				key,
	c_status*			c_status,
	glong				modus,
	glong				recursive,
	glong				line,
	glong				prototype
)
{
	gchar*				wild_ptr;
	glong				hit = 0;
	glong				len = 0;
	glong				i;
	gchar				function_from_array[MAXLEN];
	gchar				function_from_source[MAXLEN];
	GtkCTreeNode*			item;
	gchar				str[MAXLEN];
	gchar*				tmp_array[1];

	/* clean-up */
	strip_shit_1(key, key);
	strip_shit_2(key, key);
	strip_shit_3(key, key);

	/* skip functions that are in the array */
	for(i = 0; i <= fuc; i++)
	{
		strcpy(function_from_array, functionarray[i]);
		wild_ptr = strchr(function_from_array, '%');
		if(wild_ptr)
		{
			*wild_ptr = '\0';
			len = strlen(function_from_array);
			strcpy(function_from_source, key);
			function_from_source[len] = '\0';
			if(!strcmp(function_from_source, function_from_array))
			{
				hit = 1;
			}
		}
		else
		{
			if(!strcmp(key, functionarray[i]))
			{
				hit = 1;
			}
		}
	}

	/* show functions, defines & prototypes */
	if(!hit && !isempty(key))
	{
		if(modus == 1)
		{
			if(recursive > 0)
			{
				if(strstr(org_line, "#define"))
				{
				}
				else
				{
					if(recursive > 1)
					{
						for(i = 0; i < recursive; i++)
						{
							printf("  ");
						}
					}

					g_snprintf(str, MAXLEN, "%s [%ld]", key,
						line);
					tmp_array[0] = str;
					item = gtk_ctree_insert_node(
						GTK_CTREE(root_tree), sub_tree,
						NULL, tmp_array, TEXT_SPACING,
						NULL, NULL, NULL, NULL, TRUE,
						FALSE);

					if(recursive >= maxrec)
					{
						recursive = -1;
					}
					if(recursive != -1)
					{
						follow_function(key);
					}
				}
			}
			else
			{
				if(strstr(org_line, "#define"))
				{
				}
				else
				{
					g_snprintf(str, MAXLEN, "%s [%ld]", key,
						line);
					tmp_array[0] = str;
					item = gtk_ctree_insert_node(
						GTK_CTREE(root_tree), sub_tree,
						NULL, tmp_array, TEXT_SPACING,
						NULL, NULL, NULL, NULL, TRUE,
						FALSE);
					if(recursive >= maxrec)
					{
						recursive = -1;
					}
					if(recursive != -1)
					{
						follow_function(key);
					}
				}
			}
		}
		else
		{
			/* arschbloede prototype erkennung */
			if(strstr(org_line, ");"))
			{
/*				strcpy(buf, bak); */
				return 0;
			}
			else
			{
				g_snprintf(str, MAXLEN, "%s [%ld]", key, line);
				tmp_array[0] = str;
				sub_tree = gtk_ctree_insert_node(
					GTK_CTREE(root_tree), root_item, NULL,
					tmp_array, TEXT_SPACING, NULL, NULL,
					NULL, NULL, FALSE, FALSE);
				return 0;
			}
		}
	}

	return 1;
}

static glong
strip_shit_1(
	gchar*				dest,
	gchar*				source
)
{
	while(*source)
	{
		switch(*source)
		{
		case '!':
			source++;
			break;

		case '*':
			source++;
			break;

		case '&':
			source++;
			break;

		default:
			*dest = *source;
			dest++;
			source++;
		}
	}

	*dest = '\0';

	return 1;
}


static glong
strip_shit_2(
	gchar*				dest,
	gchar*				source
)
{
	gchar*				ptr;
	gchar*				ptr2;
	gchar*				ptr3;
	gchar*				ptr4;
	gchar*				ptr5;
	gchar*				ptr6;

	ptr = strrchr(source, ',');
	if(ptr)
	{
		ptr++;
		strcpy(dest, ptr++);
	}
	else
	{
		strcpy(dest, source);
	}

	ptr2 = strchr(dest, '<');
	if(ptr2)
	{
		ptr2++;
		strcpy(dest, ptr2++);
	}

	ptr3 = strchr(dest, '>');
	if(ptr3)
	{
		ptr3++;
		strcpy(dest, ptr3++);
	}

	ptr4 = strrchr(dest, '{');
	if(ptr4)
	{
		ptr4++;
		strcpy(dest, ptr4++);
	}

	ptr5 = strrchr(dest, '}');
	if(ptr5)
	{
		ptr5++;
		strcpy(dest, ptr5++);
	}

	ptr6 = strrchr(dest, '[');
	if(ptr6)
	{
		ptr6++;
		strcpy(dest, ptr6++);
	}

	return 1;
}


static glong
strip_shit_3(
	gchar*				dest,
	gchar*				source
)
{
	gchar*				wordstart;

	while(*source && (*source == '\t' || *source == ' '))
	{
		source++;
	}

	wordstart = source;

	while(*source)
	{
		wordstart = source;

		while(*source && (*source != '\t' && *source != ' '))
		{
			source++;
		}

		while(*source && (*source == '\t' || *source == ' '))
		{	
			source++;
		}
	}

	source = wordstart;

	while(*source && (*source != '\t' && *source != ' '))
	{
		*dest = *source;
		source++;
		dest++;
	}

	*dest = '\0';

	return 1;
}


static glong
follow_function(
	gchar*				function
)
{
	glong				i;
	glong				found = 0;
	FILE*				file = NULL;
	glong				line = 0;
	gchar				buf[1024];
	glong				recursive = 0;

	for(i = 0; i < fic; i++)
	{
		file = fopen(filearray[i], "r");
		if(!file)
		{
			continue;
		}

		found = find_function(file, function, line);
		if(found)
		{
			break;
		}
	}

	if(found)
	{
		recursive++;
		while(!feof(file))
		{
			fgets(buf, sizeof(buf), file);
			if(feof(file))
			{
				break;
			}

			line++;
			if(c_parse_line(buf, line, recursive) != 1)
			{
				return 0;
			}
		}
	}

	fclose(file);

	return 1;
}


static glong
find_function(
	FILE*				file,
	gchar*				function,
	glong				line
)
{
	gchar				key[MAXLEN];
	gchar				buf[MAXLEN];
	gchar				line_backup[MAXLEN];
	glong				kc = 0;
	glong				i;
	c_status			c_status_find;

	while(!feof(file))
	{
		fgets(buf, sizeof(buf), file);
		if(feof(file))
		{
			break;
		}

		strcpy(line_backup, buf);

		line++;

		for(i = 0; i < strlen(buf); i++)
		{
			c_parse_special(buf, i, &c_status_find);

			if(c_status_find.comment || c_status_find.hyph ||
				c_status_find.dhyph)
			{
				continue;
			}

			switch(buf[i])
			{
			case '(':
				if(kc != 0)
				{
					key[kc] = '\0';
					kc = 0;

					if(!c_status_find.curly_brackets)
					{
						strip_shit_1(key, key);
						strip_shit_2(key, key);
						if(!strcmp(key, function))
						{
							while(!strchr(buf, '{'))
							{
								fgets(buf,
									sizeof(
									buf),
									file);
							}
							return 1;
						}
					}
				}
				break;

			case ')':
			case ';':
			case '=':
			case ' ':
			case '\t':
			case '/':
			case '*':
			case '+':
			case '-':
				kc = 0;
				break;

			default:
				key[kc] = buf[i];
				kc++;
				break;
			}
		}
	}

	return 0;
}

/* public functions */
void
show_tree(
	AnjutaTool*			tool
)
{
	GtkWidget*			vbox;
	GtkWidget*			scrolled;
	glong				i;
//	FILE*				funcfile;
//	gchar				buf[MAXLEN];
//	gchar				funcfilewp[MAXLEN];
//	gchar*				ptr;
	gchar*				tmp_array[1];
	GNOME_Development_Environment_DocumentManager	docman;
	GNOME_Development_Environment_DocumentManager_DocumentList*	doclist;
	GNOME_Development_Environment_Document		doc;
	CORBA_Environment		ev;
	CORBA_char*			filename;

	if(tree_window)
	{
		return;
	}

	tree_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_usize(tree_window, 400, 400);
	gtk_window_set_title(GTK_WINDOW(tree_window), "Functions Tree");
	gtk_signal_connect(GTK_OBJECT(tree_window ), "destroy",
		GTK_SIGNAL_FUNC(tree_window_destroy), NULL);

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(tree_window), vbox);
	gtk_widget_show(vbox);

	scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(vbox), scrolled);
	gtk_widget_show(scrolled);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	tmp_array[0] = g_strdup("tree");
	root_tree = gtk_ctree_new_with_titles(1, 0, tmp_array);
	gtk_clist_set_column_width (GTK_CLIST (root_tree), 0, 350);
	gtk_clist_column_titles_hide (GTK_CLIST (root_tree));
	gtk_container_add (GTK_CONTAINER (scrolled), root_tree);
	gtk_signal_connect(GTK_OBJECT(root_tree), "tree_select_row",
		GTK_SIGNAL_FUNC(select_function_tree), NULL);
	gtk_signal_connect_after(GTK_OBJECT(root_tree), "select_row",
		GTK_SIGNAL_FUNC(select_function), (gpointer)tool);

#if 0
	/* parse function file */
	if(isempty(cfg->ign_file))
	{
		g_snprintf(funcfilewp, MAXLEN, "%s/%s", gide_path,
			FUNC_IGN_FILE);
	}
	else
	{
		g_snprintf(funcfilewp, MAXLEN, "%s/%s", gide_path,
			cfg->ign_file);
	}
	funcfile = fopen(funcfilewp, "r");
	if(funcfile)
	{
		g_print("functions.ignore found\n");
		fuc = -1;
		while(fgets(buf, sizeof(buf), funcfile))
		{
			fuc++;
			functionarray[fuc] = (gchar*)malloc((strlen(buf) + 1) *
				sizeof(gchar));
			ptr = strchr(buf, '\n');
			if(ptr)
			{
				*ptr = '\0';
			}
			strcpy(functionarray[fuc], buf);
		}
		fclose(funcfile);
		g_print("%ld functions read\n", fuc + 1);
	}
#endif

	CORBA_exception_init(&ev);

	docman = GNOME_Development_Environment_Shell_getObject(tool->shell,
		"DocumentManager", &ev);
	doclist = GNOME_Development_Environment_DocumentManager_getOpenFiles(
		docman, &ev);

	for(i = 0; i < doclist->_length; i++)
	{
		doc = doclist->_buffer[i];
		filename = GNOME_Development_Environment_Document_getFilename(
			doc, &ev);

// have to convert to gchar* ?
		gen_tree(filename);

		CORBA_free(filename);

// ????
//		bonobo_object_release_unref(doc, &ev);
	}

// ????
//	bonobo_object_release_unref(doclist, &ev);
	bonobo_object_release_unref(docman, &ev);

	CORBA_exception_free(&ev);

	gtk_ctree_expand(GTK_CTREE(root_tree), root_item);
	gtk_widget_show(root_tree);

	gtk_widget_show(tree_window);
}
