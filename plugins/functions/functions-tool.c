/*
 * Anjuta functions plugin
 *
 * Contains plugins based on functions:
 *  - function list generator
 *  - function tree generator
 *  - prototype generator
 */

#include <config.h>

#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

#include "functions.h"
#include "tree.h"
#include "protogen.h"

#define FUNCTIONS_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:functions"
#define PLUGIN_NAME			"anjuta-functions-plugin"
#define PLUGIN_XML			"gide-functions-plugin.xml"

static void
functionlist(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	show_all_functions(tool);
}

static void
functiontree(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	show_tree(tool);
}

static void
protogen(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	gen_proto(tool);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("FunctionList", functionlist),
	BONOBO_UI_UNSAFE_VERB("FunctionTree", functiontree),
	BONOBO_UI_UNSAFE_VERB("ProtoGen", protogen),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (FUNCTIONS_COMPONENT_IID, "Anjuta Functions Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
