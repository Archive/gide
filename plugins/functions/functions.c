/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libgide/libgide.h>

#include "functions.h"
#include "parser.h"

#define MAXLEN				4096

/* globals */
static gchar				org_line[MAXLEN];
static GtkWidget*			clist;
static GtkWidget*			func_window = NULL;


static void func_window_destroy(GtkWidget* widget, gpointer data);
static void select_function(GtkWidget* widget, gint row, gint column,
	GdkEventButton* bevent, AnjutaTool* tool);
static glong show_functions(AnjutaTool* tool, gchar* file);
static glong c_parse_line(gchar* buf, glong line, gchar* file);


static void
func_window_destroy(
	GtkWidget*			widget,
	gpointer			data
)
{
	gtk_widget_destroy(func_window);
	func_window = NULL;
}

static void
select_function(
	GtkWidget*			widget,
	gint				row,
	gint				column,
	GdkEventButton*			bevent,
	AnjutaTool*			tool
)
{
	gchar*				selected_file;
	gchar*				selected_line;
	glong				line;

	if(!bevent)
	{
		return;
	}

	if(bevent->type == GDK_2BUTTON_PRESS)
	{
		selected_file = (gchar*)gtk_clist_get_row_data(
			GTK_CLIST(clist), row);
		gtk_clist_get_text(GTK_CLIST(clist), row, 1, &selected_line);

		anjuta_show_file(tool, selected_file);
		line = atol(selected_line);
		anjuta_set_line_num(tool, line);
	}
}

void
show_all_functions(
	AnjutaTool*			tool
)
{
	glong				i;
	GNOME_Development_Environment_DocumentManager	docman;
	GNOME_Development_Environment_DocumentManager_DocumentList*	doclist;
	GNOME_Development_Environment_Document		doc;
	CORBA_Environment		ev;
	CORBA_char*			filename;

	if(func_window)
	{
		return;
	}

	CORBA_exception_init(&ev);

	docman = GNOME_Development_Environment_Shell_getObject(tool->shell,
		"DocumentManager", &ev);
	doclist = GNOME_Development_Environment_DocumentManager_getOpenFiles(
		docman, &ev);

	for(i = 0; i < doclist->_length; i++)
	{
		doc = doclist->_buffer[i];
		filename = GNOME_Development_Environment_Document_getFilename(
			doc, &ev);

// have to convert to gchar* ?
		show_functions(tool, filename);

		CORBA_free(filename);

// ????
//		bonobo_object_release_unref(doc, &ev);
	}

// ????
//	bonobo_object_release_unref(doclist, &ev);
	bonobo_object_release_unref(docman, &ev);

	CORBA_exception_free(&ev);
}


static glong
show_functions(
	AnjutaTool*			tool,
	gchar*				file
)
{
	gchar				buf[MAXLEN];
	glong				i;
	glong				line = 0;
	c_status			c_status;
	gchar*				list_titles[] = {
		"File", "Line", "Name" };
	GtkWidget*			func_vbox;
	GtkWidget*			scr_win;
	FILE*				infile;

	if(!func_window)
	{
		func_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_widget_set_usize(func_window, 600, 400);
		gtk_window_set_title(GTK_WINDOW(func_window),
			"Functions List");
		gtk_signal_connect(GTK_OBJECT(func_window), "destroy",
			GTK_SIGNAL_FUNC(func_window_destroy), NULL);

		func_vbox = gtk_vbox_new(FALSE, 0);
		gtk_container_add(GTK_CONTAINER(func_window), func_vbox);
		gtk_widget_show(func_vbox);

		scr_win = gtk_scrolled_window_new(NULL, NULL);
		gtk_box_pack_start(GTK_BOX(func_vbox), scr_win, TRUE, TRUE, 0);
		gtk_widget_show(scr_win);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scr_win),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

		clist = gtk_clist_new_with_titles(3, list_titles);
		gtk_clist_column_titles_passive(GTK_CLIST(clist));
		gtk_clist_set_column_width(GTK_CLIST(clist), 0, 100);
		gtk_clist_set_column_width(GTK_CLIST(clist), 1, 40);
		gtk_container_add(GTK_CONTAINER(scr_win), clist);
		gtk_signal_connect(GTK_OBJECT(clist), "select_row",
			GTK_SIGNAL_FUNC(select_function), tool);
		gtk_widget_show(clist);
	}

	infile = fopen(file, "r");
	if(infile == NULL)
	{
		printf("ERROR: can't open file %s\n", file);
		return -1;
	}

	init_cstatus(&c_status);

	while(!feof(infile))
	{
		fgets(buf, sizeof(buf), infile);
		if(feof(infile))
		{
			break;
		}
		for(i = 0; i <= strlen(buf); i++)
		{
			c_parse_special(buf, i, &c_status);
		}
	}

	if(c_status.comment)
	{
		anjuta_error_dialog("Base parsing failed, unfinished comment");
		g_print("\nERROR: base parsing failed, numbers doesn't match:"
			" %ld; %ld, %ld, %ld\n", c_status.comment,
			c_status.curly_brackets, c_status.round_brackets,
			c_status.square_brackets);
		return -1;
	}

	if(c_status.curly_brackets || c_status.round_brackets ||
		c_status.square_brackets)
	{
		g_print("\nERROR: base parsing failed, numbers doesn't match:"
			" %ld, %ld, %ld\n", c_status.curly_brackets,
			c_status.round_brackets, c_status.square_brackets);
		return -1;
	}

	/* Re-Init */
	rewind(infile);
	init_cstatus(&c_status);

	while(!feof(infile))
	{
		fgets(buf, sizeof(buf), infile);
		line++;
		if(feof(infile))
		{
			break;
		}
		c_parse_line(buf, line, file);
	}

	fclose(infile);

	gtk_widget_show(func_window);

	return 1;
}

static glong
c_parse_line(
	gchar*				buf,
	glong				line,
	gchar*				file
)
{
	gchar				key[MAXLEN];
	gchar*				ptr;
	glong				kc = 0;
	glong				i;
	static c_status			c_status;
	static glong			waiting = 0;
	gchar*				row[3];
	gchar				line_str[32];
	glong				newrow;
	static glong			function_line;

	/* backup the complete original line */
	if(!waiting)
	{
		strcpy(org_line, buf);
	}
	else
	{
		strcat(org_line, buf);
	}

	for(i = 0; i < strlen(buf); i++)
	{
		c_parse_special(buf, i, &c_status);

		if(c_status.comment || c_status.hyph || c_status.dhyph)
		{
			continue;
		}

		if(c_status.curly_brackets && waiting)
		{
			waiting = 0;
			ptr = strchr(org_line, ')');
			if(!ptr)
			{
				g_print("ERROR: incomplete function head.. "
					"this point should never be reached\n");
				exit(-1);
			}
			else
			{
				ptr = strchr(org_line, '{');
				if(!ptr)
				{
					g_print("ERROR: something is going"
						" wrong here....aborting!\n");
					exit(-1);
				}

				ptr--;

				while(*ptr != ')')
				{
					ptr--;
				}

				ptr++;
				*ptr = ';';
				ptr++;
				*ptr = '\0';

				g_snprintf(line_str, 32, "%ld", function_line);
				row[0] = g_basename(file);
				row[1] = line_str;
				row[2] = org_line;
				newrow = gtk_clist_append(GTK_CLIST(clist),
					row);
				gtk_clist_set_row_data(GTK_CLIST(clist),
					newrow, (gpointer)g_strdup(file));
				continue;
			}
		}

		if(buf[i] == ';' && waiting)
		{
			waiting = 0;
		}

		if(c_status.curly_brackets)
		{
			continue;
		}

		switch(buf[i])
		{
		case '(':
			if(kc != 0)
			{
				key[kc] = '\0';
				kc = 0;

				waiting = 1;
				function_line = line;
			}
			break;

		case ')':
		case ';':
		case '=':
		case '/':
		case '*':
		case '+':
		case '-':
			kc = 0;
			break;

		default:
			key[kc] = buf[i];
			kc++;
			break;
		}
	}

	return 1;
}
