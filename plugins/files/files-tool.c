/*
 * Anjuta files plugin
 *
 * Contains plugins based on files:
 *  - file list generator
 *  - file tree generator
 */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <gdl/GDL.h>
#include <dirent.h>
#include <errno.h>
#include <sys/utsname.h>
#include "dir_open.xpm"
#include "dir_close.xpm"
#include "stock_new.xpm"

#define FILES_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:files"
#define PLUGIN_NAME			"anjuta-files-plugin"
#define PLUGIN_XML			"gide-files-plugin.xml"

typedef struct _gI_flist_item_info gI_flist_item_info;
struct _gI_flist_item_info
{
	GtkWidget*			window;
	gchar*				filename;
	glong				isdir;
};

#define ISDIR				"<Directory>"
#define ISLNK				"<Symlink>"

#define TEXT_SPACING	5
#define PIXMAP_SPACING	5

/* globals */
static GtkWidget*			filetree_root_tree = NULL;
static GtkCTreeNode*			filetree_root_item = NULL;
static GtkWidget*			filetree_scrolled_win = NULL;
static GList*				filetree_selected_node = NULL;
static GtkWidget*			filetree_window = NULL;
static GtkWidget*			files_list;
static gchar*				files_list_titles[] = {
	"Name", "Size", "Date/Time" };

/* Pixmaps */
GdkPixmap*				filetree_pixmap_open = NULL;
GdkPixmap*				filetree_pixmap_close = NULL;
GdkPixmap*				filetree_pixmap_file = NULL;
 
/* Masks */
GdkBitmap*				filetree_bitmap_open = NULL;
GdkBitmap*				filetree_bitmap_close = NULL;
GdkBitmap*				filetree_bitmap_file = NULL;

/* Prototypes for local functions */
static void filetree_window_destroy(GtkWidget* widget, gpointer data);
static void filetree_gen_files_tree(gchar* dirname, GtkWidget* window);
static void filetree_select_file(GtkWidget* widget, gint row, gint column,
	GdkEventButton* event, AnjutaTool* tool);
static void filetree_select_file_tree(GtkCTree* ctree, GList* node,
	gint column, gpointer user_data);
static void filetree_load_pixmaps(void);
static void filetree_load_pixmap(char* pix[], GdkPixmap** pixmap,
	GdkBitmap** bitmap);
static void filetree_setup_tree(GtkWidget* window, AnjutaTool* tool);
static void files_list_select(GtkWidget* widget, gint row, gint column,
	GdkEventButton* event, AnjutaTool* tool);
static gchar* flist_file_size(gchar* dname, gchar* fname);
static gchar* flist_time(struct stat* f_stat);
static void gen_files_list(gchar* dirname, GtkWidget* window, AnjutaTool* tool);
static GtkWidget* setup_list_window(void);
static void flist(AnjutaTool* tool);

static void
filetree_window_destroy(
	GtkWidget*			widget,
	gpointer			data
)
{
	gtk_widget_destroy(filetree_window);
	filetree_window = NULL;
}


static void
filetree_load_pixmaps(
	void
)
{
	if(filetree_pixmap_open == NULL)
	{
		filetree_load_pixmap(DIRECTORY_OPEN_XPM,
			&filetree_pixmap_open, &filetree_bitmap_open);
	}

	if(filetree_pixmap_close == NULL)
	{
		filetree_load_pixmap(DIRECTORY_CLOSE_XPM,
			&filetree_pixmap_close, &filetree_bitmap_close);
	}
	if(filetree_pixmap_file == NULL)
	{
		filetree_load_pixmap(STOCK_NEW_XPM,
			&filetree_pixmap_file, &filetree_bitmap_file);
	}
}

static void
filetree_load_pixmap(
	char*				pix[],
	GdkPixmap**			pixmap,
	GdkBitmap**			bitmap
)
{
	GdkImlibImage*			image;

	g_assert(pix);
	g_assert(pixmap);
	g_assert(bitmap);

	image = gdk_imlib_create_image_from_xpm_data(pix);
	gdk_imlib_render(image, image->rgb_width, image->rgb_height);
	*pixmap = gdk_imlib_move_image(image);
	*bitmap = gdk_imlib_move_mask(image);
}

/*
 * Create the actual tree widget
 */
static void
filetree_setup_tree(
	GtkWidget*			window,
	AnjutaTool*			tool
)
{
	struct utsname			uts;
	gchar*				tmp_array[1];

	tmp_array[0] = g_strdup("tree");
	filetree_root_tree = gtk_ctree_new_with_titles(1, 0, tmp_array);
	gtk_clist_set_column_width(GTK_CLIST(filetree_root_tree), 0, 350);
	gtk_clist_column_titles_hide(GTK_CLIST(filetree_root_tree));
	gtk_container_add(GTK_CONTAINER(filetree_scrolled_win),
		filetree_root_tree);

	gtk_signal_connect(GTK_OBJECT(filetree_root_tree), "tree_select_row",
		GTK_SIGNAL_FUNC(filetree_select_file_tree), NULL);
	gtk_signal_connect_after(GTK_OBJECT(filetree_root_tree), "select_row",
		GTK_SIGNAL_FUNC(filetree_select_file), (gpointer)tool);

	gtk_widget_show(filetree_root_tree);

	if(uname(&uts) != 0)
	{
		/* fuck up */
//		g_print("FUCK!");
	}

	tmp_array[0] = uts.nodename;
	filetree_root_item = gtk_ctree_insert_node(GTK_CTREE(
		filetree_root_tree), NULL, NULL, tmp_array, PIXMAP_SPACING,
		filetree_pixmap_close, filetree_bitmap_close,
		filetree_pixmap_open, filetree_bitmap_open, FALSE, FALSE);
}

static void
filetree_select_file(
	GtkWidget*			widget,
	gint				row,
	gint				column,
	GdkEventButton*		       	event,
        AnjutaTool*			tool
)
{
	gchar*				filename;
	gchar*				selected_line;
	gchar*				directory;
	GtkCTreeRow*			tree_row;
	GtkCTreeNode*			parent;
	struct stat 			f_stat;

	if(!event)
	{
		return;
	}

	if(event->type == GDK_2BUTTON_PRESS && filetree_selected_node )
	{

		tree_row = GTK_CTREE_ROW(GTK_CTREE_NODE(
			filetree_selected_node));
		selected_line = g_strdup(GTK_CELL_TEXT(tree_row->row.cell[0])->
			text);

		/* Found a directory */
		if(selected_line[0] == '[')
		{
			selected_line++;
			selected_line[strlen(selected_line) - 1] = '\0';
		}

		parent = tree_row->parent;

		tree_row = GTK_CTREE_ROW(GTK_CTREE_NODE(parent));
		directory = g_strdup(GTK_CELL_TEXT(tree_row->row.cell[0])->
			text);

		filename = g_strconcat(directory, "/", selected_line, NULL);

		if(stat(filename, &f_stat) != 0)
		{
			g_print("%s: %s\n", filename, g_strerror(errno));
		}
		else
		{
			if(S_ISDIR(f_stat.st_mode))
			{
				/* destroy old tree and generate a new one */
				gtk_widget_destroy(filetree_root_tree);
				filetree_root_tree = NULL;
				filetree_setup_tree(filetree_window, tool);
				filetree_gen_files_tree(filename,
					filetree_window);
			}
			else
			{
				anjuta_show_file(tool, filename);
			}
		}
	}
}

static void
filetree_select_file_tree(
	GtkCTree*			ctree,
	GList*				node,
	gint				column,
	gpointer			user_data
)
{
	filetree_selected_node = node;
}

static void
filetree_gen_files_tree(
	gchar*				dirname,
	GtkWidget*			window
)
{
	DIR*				dir;
	struct dirent*			entry;
	struct stat			f_stat;
	gchar*				fwp;
	GtkCTreeNode*			tree_item;
	GtkCTreeNode*			sub_tree_item;
	gchar				cwdbak[STRLEN];
	gchar*				tmp_array[1];

	/* correct dirname, FIXME: does somebody know a better way? */

	/* backup current directory */
	getcwd(cwdbak, STRLEN);

	/* fix dirname */
	if(chdir(dirname) != 0)
	{
		/* abort */
		g_error("FUCK: error changing directory\n");
		return;
	}
	else
	{
		dirname = realloc(dirname, STRLEN);
		getcwd(dirname, STRLEN);
	}

	/* back to old directory */
	chdir(cwdbak);

//	g_print("Processing '%s'...\n", dirname);

	tmp_array[0] = dirname;
	tree_item = gtk_ctree_insert_node(GTK_CTREE(filetree_root_tree),
		filetree_root_item, NULL, tmp_array, PIXMAP_SPACING,
		filetree_pixmap_close, filetree_bitmap_close,
		filetree_pixmap_open, filetree_bitmap_open, FALSE, FALSE);

	dir = opendir(dirname);
	if(!dir)
	{
		g_error("Unable to access this directory!\n");
		/* needs better handling and cleanup */
		return;
	}

	while((entry = readdir(dir)))
	{
		fwp = g_strdup_printf("%s/%s", dirname, entry->d_name);
		if(stat(fwp, &f_stat) != 0)
		{
			g_print("%s: %s\n", fwp, g_strerror(errno));
		}
		else
		{
			if(S_ISDIR(f_stat.st_mode))
			{
				g_free(fwp);
				fwp = g_strdup_printf("[%s]", entry->d_name);
				tmp_array[0] = fwp;
				sub_tree_item = gtk_ctree_insert_node(
					GTK_CTREE(filetree_root_tree),
					tree_item, NULL, tmp_array,
					PIXMAP_SPACING, filetree_pixmap_close,
					filetree_bitmap_close,
					filetree_pixmap_open,
					filetree_bitmap_open, FALSE, FALSE);
			}
			else
			{
				tmp_array[0] = entry->d_name;
				sub_tree_item = gtk_ctree_insert_node(
					GTK_CTREE(filetree_root_tree),
					tree_item, NULL, tmp_array,
					PIXMAP_SPACING, filetree_pixmap_file,
					filetree_bitmap_file, NULL, NULL,
					FALSE, FALSE);
			}
		}
		g_free(fwp);
	}

	gtk_ctree_expand(GTK_CTREE(filetree_root_tree), filetree_root_item);
	gtk_ctree_expand(GTK_CTREE(filetree_root_tree), tree_item);
	
	closedir(dir);
}


/*
 * Create a window containing a scrolled window
 */
static GtkWidget *
filetree_setup_tree_window(
)
{
	static GtkWidget*		window;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Directory/Files Tree");
	gtk_widget_set_usize(window, 400, 400);
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
		GTK_SIGNAL_FUNC(filetree_window_destroy), NULL);
	gtk_widget_show(window);

	filetree_scrolled_win = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(window), filetree_scrolled_win);
	gtk_widget_show(filetree_scrolled_win);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(
		filetree_scrolled_win), GTK_POLICY_AUTOMATIC,
		GTK_POLICY_AUTOMATIC);

	return filetree_scrolled_win;
}

static void
files_list_select(
	GtkWidget*			widget,
	gint				row,
	gint				column,
	GdkEventButton*			event,
	AnjutaTool*			tool
)
{
	gI_flist_item_info*		item_info;

	if(event->type == GDK_2BUTTON_PRESS)
	{
		item_info = (gI_flist_item_info*)gtk_clist_get_row_data(
			GTK_CLIST(files_list), row);
		if(item_info->isdir)
		{
			/* destroy old tree and generate a new one */
			gtk_clist_clear(GTK_CLIST(files_list));
			gen_files_list(item_info->filename, item_info->window,
				tool);
		}
		else
		{
			anjuta_show_file(tool, item_info->filename);
		}
	}
}


static gchar*
flist_file_size(
	gchar*				dname,
	gchar*				fname
)
{
	static gchar			filesize[100];
	gchar*				filename;

	filename = g_strdup_printf("%s/%s", dname, fname);
	sprintf(filesize, "%ld", get_file_size(filename));
	g_free(filename);

	return filesize;
}


static gchar*
flist_time(
	struct stat*			f_stat
)
{
	static gchar			filetime[100];

	strncpy(filetime, ctime(&f_stat->st_mtime), 100);

	return filetime;
}


static void
gen_files_list(
	gchar*				dirname,
	GtkWidget*			window,
	AnjutaTool*			tool
)
{
	DIR*				dir;
	struct dirent*			entry;
	struct stat			f_stat;
	gI_flist_item_info*		item_info;
	gchar				cwdbak[STRLEN];
	gchar*				fwp;
	gchar*				insrow[3];
	int				row;
	GtkWidget*			scrolled_win;

	/* setup clist */
	if(!files_list)
	{
		scrolled_win = gtk_scrolled_window_new(NULL, NULL);
		gtk_container_add(GTK_CONTAINER(window), scrolled_win);
		gtk_widget_show(scrolled_win);
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(
			scrolled_win), GTK_POLICY_AUTOMATIC,
			GTK_POLICY_AUTOMATIC);
		files_list = gtk_clist_new_with_titles(3, files_list_titles);
		gtk_clist_column_titles_passive(GTK_CLIST(files_list));
		gtk_clist_set_column_width(GTK_CLIST(files_list), 0, 250);
		gtk_clist_set_column_width(GTK_CLIST(files_list), 1, 125);
		gtk_container_add(GTK_CONTAINER(scrolled_win), files_list);
		gtk_signal_connect(GTK_OBJECT(files_list), "select_row",
			GTK_SIGNAL_FUNC(files_list_select), (gpointer)tool);
		gtk_widget_show(files_list);
	}

	/* correct dirname, FIXME: does somebody know a better way? */

	/* backup current directory */
	getcwd(cwdbak, STRLEN);

	/* fix dirname */
	if(chdir(dirname) != 0)
	{
		/* abort */
		g_error("filelist-tool: error changing directory\n");
		return;
	}
	else
	{
		dirname = realloc(dirname, STRLEN);
		getcwd(dirname, STRLEN);
	}

	/* back to old directory */
	chdir(cwdbak);

//	g_print("Processing '%s'...\n", dirname);
	gtk_window_set_title(GTK_WINDOW(window), dirname);

	dir = opendir(dirname);

	while((entry = readdir(dir)))
	{
		fwp = g_strdup_printf("%s/%s", dirname, entry->d_name);
		if(stat(fwp, &f_stat) != 0)
		{
			g_print("%s: %s\n", fwp, g_strerror(errno));
		}
		else
		{
			if(S_ISDIR(f_stat.st_mode))
			{
				insrow[0] = entry->d_name;
				insrow[1] = ISDIR;
				/* date/time */
				insrow[2] = flist_time(&f_stat);
				item_info = g_malloc0(
					sizeof(gI_flist_item_info));
				item_info->window = window;
				if(dirname[strlen(dirname)] != '/')
				{
					item_info->filename = g_strconcat(
						dirname, "/", entry->d_name,
						NULL);
				}
				else
				{
					item_info->filename = g_strconcat(
						dirname, entry->d_name, NULL);
				}
				item_info->isdir = TRUE;
				row = gtk_clist_append(GTK_CLIST(files_list),
					insrow);
				gtk_clist_set_row_data(GTK_CLIST(files_list),
					row, (gpointer)item_info);
			}
			if(S_ISLNK(f_stat.st_mode))
			{
				insrow[0] = entry->d_name;
				insrow[1] = ISLNK;
				/* date/time */
				insrow[2] = flist_time(&f_stat);
				item_info = g_malloc0(
					sizeof(gI_flist_item_info));
				item_info->window = window;
				if(dirname[strlen(dirname)] != '/')
				{
					item_info->filename = g_strconcat(
						dirname, "/", entry->d_name,
						NULL);
				}
				else
				{
					item_info->filename = g_strconcat(
						dirname, entry->d_name, NULL);
				}
				item_info->isdir = FALSE;
				row = gtk_clist_append(GTK_CLIST(files_list),
					insrow);
				gtk_clist_set_row_data(GTK_CLIST(files_list),
					row, (gpointer)item_info);
			}
			if(S_ISREG(f_stat.st_mode))
			{
				insrow[0] = entry->d_name;
				insrow[1] = flist_file_size(dirname,
					entry->d_name);
				/* date/time */
				insrow[2] = flist_time(&f_stat);
				item_info = g_malloc0(
					sizeof(gI_flist_item_info));
				item_info->window = window;
				if(dirname[strlen(dirname)] != '/')
				{
					item_info->filename = g_strconcat(
						dirname, "/", entry->d_name,
						NULL);
				}
				else
				{
					item_info->filename = g_strconcat(
						dirname, entry->d_name, NULL);
				}
				item_info->isdir = FALSE;
				row = gtk_clist_append(GTK_CLIST(files_list),
					insrow);
				gtk_clist_set_row_data(GTK_CLIST(files_list),
					row, (gpointer)item_info);
			}
		}
		g_free(fwp);
	}

	closedir(dir);
}


static GtkWidget*
setup_list_window(
)
{
	GtkWidget*			window;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "D/F List");
	gtk_widget_set_usize(window, 600, 400);
	gtk_signal_connect_object(GTK_OBJECT(window), "destroy",
		GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(window));
	gtk_widget_show(window);

	return window;
}


static void
flist(
	AnjutaTool*			tool
)
{
	GtkWidget*			window;

	window = setup_list_window();
	gen_files_list(getcwd(NULL, STRLEN), window, tool);
}

static void
filelist(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	flist(tool);
}

static void
filetree(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	if(filetree_window != NULL)
	{
		return;
	}

	filetree_load_pixmaps();

	filetree_window = filetree_setup_tree_window();
	filetree_setup_tree(filetree_window, tool);
	filetree_gen_files_tree(getcwd(NULL, STRLEN), filetree_window);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("FileList", filelist),
	BONOBO_UI_UNSAFE_VERB("FileTree", filetree),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (FILES_COMPONENT_IID, "Anjuta Files Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
