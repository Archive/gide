/*
 * selection plugin
 *
 * Author:
 *   Dirk Vangestel (dirk.vangestel@advalvas.be)
 */

#include "../../src/gide.h"
#include "../../src/gI_plugin.h"
#include "../../src/gI_tools.h"
#include "../../src/gide-tools.h"
#include "../../src/gI_compile_sets.h"
#include "../../src/gI_common.h"
#include "../../src/gI_file.h"
#include "../../src/gI_text.h"

#define SELECTION_NAME  _("selection")
#define SELECTION_TITLE _("selection Plugin")
#define SELECTION_DESCR _("Tries to open a file which is selected in the editor")

static void
open_selected(
	Tool*				tool,
	ToolState*			state
)
{
	glong				i;
	gchar*				line;
	gchar				filename[MAXLEN];
	gchar*				inc[64];
	gchar*				fwp;
	gI_comp_set*			cset = NULL;
	AnjutaWindow*			win = main_window;
	GideDocument*			doc;

	doc = state->document;

	if(!doc)
	{
		return;
	}

	if(!gI_document_has_selection(doc))
	{
		anjuta_error_dialog(_("No selection in current document!"));
		return;
	}

	line = gI_text_get_selection(doc);

	if(!line)
	{
		return;
	}

	if(!strstr(line, "#include"))
	{
		/* this is wrong: should use path where current file is,
		not working directory */
		fwp = g_strconcat(getcwd(NULL, STRLEN), "/", line, NULL);
		if(!check_file_open(fwp))
		{
			file_open_by_name(win, fwp);
		}
		else
		{
			goto_file(fwp);
		}
		g_free(line);
		g_free(fwp);
		return;
	}

	if(gI_document_get_filename(doc))
	{
		cset = get_comp_set_from_filename(gI_document_get_filename(
			doc));
	}

	if(strchr(line, '<') && strchr(line, '>'))
	{
		strncpy(filename, SK_GetBetween(line, '<', '>'), MAXLEN);

		/* search includes paths, if available */
		if(cset || cfg->incpath)
		{
			if(cset)
			{
				SK_GetFields(cset->incpath, inc, ':');
			}
			else
			{
				if(cfg->incpath)
				{
					SK_GetFields(cfg->incpath, inc, ':');
				}
				else
				{
					anjuta_error_dialog(_("Can't find the "
						"included file in the "
						"specified paths"));
					g_free(line);
					return;
				}
			}

			for(i = 1; i <= atoi(inc[0]); i++)
			{
				if(inc[i] != NULL)
				{
					fwp = g_strconcat(inc[i], "/",
						filename, NULL);
					if(file_exist(fwp))
					{
						if(!check_file_open(fwp))
						{
							file_open_by_name(win,
								fwp);
						}
						else
						{
							goto_file(fwp);
						}
						g_free(fwp);
						g_free(line);
						return;
					}
				}
			}
		}

		anjuta_error_dialog(_("Can't find the included file in the "
			"specified paths"));

		g_free(line);
		return;
	}

	if(strchr(line, '"'))
	{
		/* again, wrong directory */
		strncpy(filename, SK_GetBetween(line, '"', '"' ), MAXLEN);
		fwp = g_strconcat(getcwd(NULL, STRLEN), "/", filename, NULL);
		if(!check_file_open(fwp))
		{
			file_open_by_name(win, fwp);
		}
		else
		{
			goto_file(fwp);
		}
		g_free(line);
		g_free(fwp);
		return;
	}

	g_free(line);
}

static gboolean
selection_sens(
	Tool*				tool,
	ToolState*			state
)
{
	if(state->document)
	{
		if(gI_document_has_selection(state->document))
		{
			return TRUE;
		}
	}

	return FALSE;
}

static int
can_unload(
	PluginData*			pd
)
{
	return 1;
}

static void
cleanup_plugin(
	PluginData*			pd
)
{
	anjuta_tool_remove(SELECTION_NAME);
}

PluginInitResult
init_plugin(
	CommandContext*			context,
	PluginData*			pd
)
{
	GtkObject*			tool;

	if(plugin_version_mismatch (context, pd, VERSION))
	{
		return PLUGIN_QUIET_ERROR;
	}

	tool = gI_tool_new(SELECTION_NAME, (void*)open_selected);
	gI_tool_set_menu_data(TOOL(tool), selection_sens,
		NULL, "Open selected file");
	anjuta_tool_add(TOOL(tool));

	if(plugin_data_init(pd, can_unload, cleanup_plugin,
		SELECTION_TITLE, SELECTION_DESCR))
	{
		return PLUGIN_OK;
	}
	else
	{
		return PLUGIN_ERROR;
	}
}
