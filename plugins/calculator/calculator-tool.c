/*
 * Anjuta calculator plugin
 *
 * Shows a calculator
 */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <gdl/GDL.h>

#define CALCULATOR_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:calculator"
#define PLUGIN_NAME			"anjuta-calculator-plugin"
#define PLUGIN_XML			"gide-calculator-plugin.xml"

static void
calculator(
	GtkWidget*			widget,
	gpointer			data
)
{
	GtkWidget*			window;
	GtkWidget*			calc;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), _("Calculator"));
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
		GTK_SIGNAL_FUNC(gtk_widget_destroyed), NULL);

	calc = gnome_calculator_new();
	gtk_container_add(GTK_CONTAINER(window), calc);
	gtk_window_add_accel_group(GTK_WINDOW(window),
		GNOME_CALCULATOR(calc)->accel);

	gtk_widget_show_all(window);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("Calculator", calculator),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (CALCULATOR_COMPONENT_IID, "Anjuta Calculator Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
