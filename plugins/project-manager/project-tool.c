/* Anjuta Project Manager component, based on gnome-build2 */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <gbf/gbf.h>
#include <gdl/GDL.h>
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>

#define PROJECT_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:project-manager"

static void
open_file (AnjutaTool *tool, GBF_Source *source, int line_num, CORBA_Environment *ev)
{
	CORBA_char *root;
	GbfProjectClient *prj = gtk_object_get_data (GTK_OBJECT (tool), "project");

	if (gbf_project_client_get_project_root (prj, &root) == GBF_PROJECT_CLIENT_OK) {
		char *path = g_strconcat (root, "/", source->path, "/", source->name,NULL);
		gboolean res;

		res = anjuta_show_file (tool, path);
		CORBA_free (root);
		g_free (path);

		if (!res) {
			anjuta_error_dialog(_("Unable to open file."));
			return;
		}
				
		if (line_num != 0)
			anjuta_set_line_num (tool, line_num);
	}
}

static void
set_current_target (AnjutaTool *tool, CORBA_any *any)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	if (any) {
		GNOME_Development_Environment_Shell_addData (tool->shell,
							     any,
							     "CurrentTarget",
							     &ev);
	} else {
		GNOME_Development_Environment_Shell_removeData (tool->shell,
								"CurrentTarget",
								&ev);
	}

	CORBA_exception_free (&ev);
}

static void
event_cb (BonoboListener *listener, const char *event,
	       CORBA_any *any, CORBA_Environment *ev,
	       gpointer user_data)
{
	AnjutaTool *tool = ANJUTA_TOOL (user_data);
	
	if (!strcmp (event, "file-selected")) {
		GBF_Source *source = any->_value;

		open_file (tool, source, 0, ev);
	} else if (!strcmp (event, "target-selected")) {
		set_current_target (tool, any);

	} else if (!strcmp (event, "error-selected")) {
		GBF_BuildError *build_err = any->_value;

		open_file (tool, &build_err->source, build_err->line, ev);
	}
}

static void
set_build (AnjutaTool *tool, const gchar *path)
{
	GbfProjectClientResult res;
	CORBA_Environment ev;
	Bonobo_PropertyBag pb;
	Bonobo_Control ctrl;
	GbfProjectClient *proj;
	char *ior;
	
	g_return_if_fail (tool != NULL);
	g_return_if_fail (ANJUTA_IS_TOOL (tool));
	
	proj = gtk_object_get_data (GTK_OBJECT (tool), "project");

	if (proj) {
		GNOME_Development_Environment_Shell_removeObject (tool->shell,
								  "CurrentProject",
								  &ev);
		
		gtk_object_set_data (GTK_OBJECT (tool), "project", NULL);
	}

	CORBA_exception_init (&ev);
	if (path != NULL) {
		proj = gbf_project_client_new_for_path (path);
		
		if (proj == NULL) {
			return;
		}
		
		res = gbf_project_client_load (proj, path);
		if (res == GBF_PROJECT_CLIENT_DOESNT_EXIST) {
      			anjuta_error_dialog(_("Project does not exist!"));
			gtk_object_unref (GTK_OBJECT (proj));
      			return;
		} else if (res == GBF_PROJECT_CLIENT_MALFORMED) {
      			anjuta_error_dialog(_("Not a proper project!"));
			gtk_object_unref (GTK_OBJECT (proj));
      			return;			
  		} else if (res != GBF_PROJECT_CLIENT_OK) {
      			anjuta_error_dialog(_("Unable to load project!"));
			gtk_object_unref (GTK_OBJECT (proj));
      			return;
      		}
		
		g_assert (proj->objref != CORBA_OBJECT_NIL);
		ior = CORBA_ORB_object_to_string (oaf_orb_get (),
						  proj->objref, &ev);

		GNOME_Development_Environment_Shell_addObject (tool->shell,
							       proj->objref,
							       "CurrentProject",
							       &ev);
		gtk_object_set_data (GTK_OBJECT (tool), "project", proj);
	} else {
		ior = CORBA_string_dup ("");
	}

	ctrl = gtk_object_get_data (GTK_OBJECT (tool), "tree_view");
	pb = Bonobo_Control_getProperties (ctrl, &ev);
	bonobo_property_bag_client_set_value_string (pb, "project-ior", 
						     ior, &ev);
	ctrl = gtk_object_get_data (GTK_OBJECT (tool), "target_view");
	pb = Bonobo_Control_getProperties (ctrl, &ev);
	bonobo_property_bag_client_set_value_string (pb, "project-ior", 
						     ior, &ev);
	ctrl = gtk_object_get_data (GTK_OBJECT (tool), "build_info_view");
	pb = Bonobo_Control_getProperties (ctrl, &ev);
	bonobo_property_bag_client_set_value_string (pb, "project-ior", 
						     ior, &ev);
	CORBA_free (ior);
	
	CORBA_exception_free (&ev);
}

static void 
project_new (GtkWidget *widget, gpointer data)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));
}

static void 
project_open (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GList *file;
	
	file = anjuta_file_sel_new (_("Open Project..."), FALSE, FALSE);
	
	if (file) {
		gchar *filename = file->data;
		int len = strlen (filename);
		struct stat stat_buf;
		
		if (stat (filename, &stat_buf ) == 0 ) {
			if ((S_ISDIR (stat_buf.st_mode)) && (filename[len - 1] != '/')) {
				gchar* tmp = (gchar*)g_malloc (len + 1);
				strcpy (tmp, filename);
				strcat (tmp, "/");
				g_free (filename);
				filename = tmp;
			}
		}

		set_build (tool, g_dirname (filename));
	}


	anjuta_file_sel_free_list (file);
}

static void 
project_close (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	
	set_build (tool, NULL);
}

static void
project_add_source (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GbfProjectClient *prj = gtk_object_get_data (GTK_OBJECT (tool), "project");
	GbfProjectClientResult res;
	char *filename, *fullname, *root;
	
	fullname = anjuta_get_current_filename (tool);
	g_print ("Adding: %s\n", fullname);

	res = gbf_project_client_get_project_root (prj, &root);
	if (res != GBF_PROJECT_CLIENT_OK)
		return;
	
	filename = strstr (fullname, root);
	CORBA_free (root);

	if (filename != NULL) {
		GBF_Target *target;
		GBF_Source source;
		CORBA_any *any;
		CORBA_Environment ev;
		char *d;

		filename += strlen (root) + 1;
		g_print ("Adding: %s\n", filename);

		CORBA_exception_init (&ev);
		any = GNOME_Development_Environment_Shell_getData (tool->shell,
								   "CurrentTarget",
								   &ev);
		target = any->_value;
		CORBA_exception_free (&ev);
		
		source.name = CORBA_string_dup (g_basename (filename));
		d = g_dirname (filename);
		if (strcmp (d, "."))
			source.path = CORBA_string_dup (d);
		else
			source.path = CORBA_string_dup ("");
		g_free (d);
		
		res = gbf_project_client_add_target_source (prj, target, &source);
		if (res != GBF_PROJECT_CLIENT_OK)
			anjuta_error_dialog ("Unable to add source.");
	}
}

static void
project_remove_source (GtkWidget *widget, gpointer data) 
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	GbfProjectClient *prj = gtk_object_get_data (GTK_OBJECT (tool), "project");
	GbfProjectClientResult res;
	char *filename, *fullname, *root;
	
	fullname = anjuta_get_current_filename (tool);
	g_print ("Removing: %s\n", fullname);

	res = gbf_project_client_get_project_root (prj, &root);
	if (res != GBF_PROJECT_CLIENT_OK)
		return;
	
	filename = strstr (fullname, root);
	CORBA_free (root);
	
	if (filename != NULL) {
		GBF_TargetList *tl;
		GBF_Source source;
		char *d;

		filename += strlen (root) + 1;
		g_print ("Removing: %s\n", filename);

		source.name = CORBA_string_dup (g_basename (filename));
		d = g_dirname (filename);
		if (strcmp (d, "."))
			source.path = CORBA_string_dup (d);
		else
			source.path = CORBA_string_dup ("");
		g_free (d);

		res = gbf_project_client_get_targets_of_source (prj, &source, &tl);
		if (res != GBF_PROJECT_CLIENT_OK) {
			anjuta_error_dialog ("Source is not in the project");			
			return;
		}
		
		switch (tl->_length) {
		case 0:
			anjuta_error_dialog ("Source is not in the project");
			break;
		case 1:
			res = gbf_project_client_remove_target_source (prj, &tl->_buffer[0], &source);
			if (res != GBF_PROJECT_CLIENT_OK)
				anjuta_error_dialog ("Unable to remove source.");
			break;
		default:
			anjuta_error_dialog ("Source is in more than one target and"
					 "this is not currently handled.");
		}
	}
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("ProjectNew", project_new),
	BONOBO_UI_UNSAFE_VERB ("ProjectOpen", project_open),
	BONOBO_UI_UNSAFE_VERB ("ProjectClose", project_close),

	BONOBO_UI_UNSAFE_VERB ("ProjectAddSource", project_add_source),
	BONOBO_UI_UNSAFE_VERB ("ProjectRemoveSource", project_remove_source),
	BONOBO_UI_VERB_END
};

static void
add_event_listener (AnjutaTool *tool, Bonobo_Control ctrl)
{
	BonoboListener *listener;
	CORBA_Object source;
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);
	listener = bonobo_listener_new (NULL, NULL);
	gtk_signal_connect (GTK_OBJECT (listener), "event_notify",
			    GTK_SIGNAL_FUNC (event_cb), tool);
	source = Bonobo_Unknown_queryInterface (ctrl, 
						"IDL:Bonobo/EventSource:1.0", 
						&ev);
	if (!CORBA_Object_is_nil (source, &ev) && ev._major == CORBA_NO_EXCEPTION) {
		Bonobo_EventSource_addListener (source, bonobo_object_corba_objref (BONOBO_OBJECT (listener)), &ev);
	} else {
		g_error ("couldn't get event source for project tree widget");
	}
	
	CORBA_exception_free (&ev);	
}

static void
init_project_tree (AnjutaTool *tool, BonoboUIComponent *uic)
{
	CORBA_Environment ev;
	Bonobo_Control ctrl;
	
	CORBA_exception_init (&ev);
	ctrl = oaf_activate_from_id ("OAFIID:GNOME_Development_ProjectTree", 0, NULL, &ev);
	
	if (!CORBA_Object_is_nil (ctrl, &ev)) {
		
		GNOME_Development_Environment_Shell_addControl (tool->shell,
								ctrl,
								"GbfFileTree",
								"Files",
								ANJUTA_WINDOW_LOC_LEFT,
								&ev);
		
		gtk_object_set_data (GTK_OBJECT (tool), "tree_view", ctrl);
		add_event_listener (tool, ctrl);
	} else {
		g_warning ("Could not initialize project manager.");
	}
	
	CORBA_exception_free (&ev);
}

static void
init_target_tree (AnjutaTool *tool, BonoboUIComponent *uic)
{
	CORBA_Environment ev;
	Bonobo_Control ctrl;

	CORBA_exception_init (&ev);
	ctrl = oaf_activate_from_id ("OAFIID:GNOME_Development_TargetTree", 0, NULL, &ev);

	if (!CORBA_Object_is_nil (ctrl, &ev)) {
		
		GNOME_Development_Environment_Shell_addControl (tool->shell,
								ctrl,
								"GbfTargetTree",
								"Targets",
								ANJUTA_WINDOW_LOC_LEFT,
								&ev);
		gtk_object_set_data (GTK_OBJECT (tool), "target_view", ctrl);
		add_event_listener (tool, ctrl);
	} else {
		g_warning ("Could not initialize project manager.");
	}
	CORBA_exception_free (&ev);
}

static void
init_build_info (AnjutaTool *tool, BonoboUIComponent *uic)
{
	CORBA_Environment ev;
	Bonobo_Control ctrl;

	CORBA_exception_init (&ev);
	ctrl = oaf_activate_from_id ("OAFIID:GNOME_Development_BuildInfo", 0, NULL, &ev);

	if (!CORBA_Object_is_nil (ctrl, &ev)) {
		
		GNOME_Development_Environment_Shell_addControl (tool->shell,
								ctrl,
								"GbfBuildInfo",
								"Compilation",
								ANJUTA_WINDOW_LOC_BOTTOM,
								&ev);
		
		gtk_object_set_data (GTK_OBJECT (tool), "build_info_view", ctrl);
		add_event_listener (tool, ctrl);
	} else {
		g_warning ("Could not initialize project manager.");
	}
	CORBA_exception_free (&ev);
}

static gboolean
impl_init (AnjutaTool *tool, 
	   gpointer closure)
{
	BonoboUIComponent *uic;
	CORBA_Environment ev;

	g_return_val_if_fail (tool != NULL, FALSE);
	g_return_val_if_fail (ANJUTA_IS_TOOL (tool), FALSE);
	
	CORBA_exception_init (&ev);

	uic = bonobo_ui_component_new ("gide-project-manager");
	bonobo_ui_component_set_container (uic, tool->ui_container);
	bonobo_ui_util_set_ui (uic, ANJUTA_DATADIR, "gide-project-manager.xml", "gide-project-manager");
	
	bonobo_ui_component_add_verb_list_with_data (uic, verbs, 
						     tool);

	init_project_tree (tool, uic);
	init_target_tree (tool, uic);
	init_build_info (tool, uic);

	gtk_object_set_data(GTK_OBJECT(tool), "ui_component", uic);

	CORBA_exception_free (&ev);
	
	return TRUE;
}

static void
impl_cleanup (AnjutaTool *tool,
	      gpointer closure)
{
	CORBA_Environment ev;
	CORBA_Object obj;

	CORBA_exception_init (&ev);
	
	GNOME_Development_Environment_Shell_removeObject (tool->shell,
							  "GbfFileTree",
							  &ev);
	obj = gtk_object_get_data (tool, "tree_view");
	bonobo_object_release_unref (obj, &ev);
	
	GNOME_Development_Environment_Shell_removeObject (tool->shell,
							  "GbfTargetTree",
							  &ev);
	obj = gtk_object_get_data (tool, "target_view");
	bonobo_object_release_unref (obj, &ev);

	GNOME_Development_Environment_Shell_removeObject (tool->shell,
							  "GbfBuildInfo",
							  &ev);
	GNOME_Development_Environment_Shell_removeObject (tool->shell,
							  "CurrentProject",
							  &ev);
	obj = gtk_object_get_data (tool, "build_info_view");
	bonobo_object_release_unref (obj, &ev);

	bonobo_ui_component_unset_container(gtk_object_get_data(GTK_OBJECT(
		tool), "ui_component"));
	gtk_object_remove_data(GTK_OBJECT(tool), "ui_component");

	CORBA_exception_free (&ev);
}

ANJUTA_SHLIB_TOOL (PROJECT_COMPONENT_IID, "Anjuta Project Manager", 
		   impl_init, impl_cleanup, NULL);
