/* Anjuta Project Manager component, based on gnome-build2 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "file-ops.h"
#include "gide-document-manager.h"
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>

#define DOCMAN_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:document-manager"

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileNew", file_new),
	BONOBO_UI_UNSAFE_VERB ("FileOpen", file_open),
        BONOBO_UI_UNSAFE_VERB ("FileReload", file_reload),
        BONOBO_UI_UNSAFE_VERB ("FileSave", file_save), 
        BONOBO_UI_UNSAFE_VERB ("FileSaveAs", file_save_as),
        BONOBO_UI_UNSAFE_VERB ("FileSaveAll", file_save_all),

	BONOBO_UI_UNSAFE_VERB ("FileClose", file_close),
        BONOBO_UI_UNSAFE_VERB ("FileCloseAll", file_close_all),
	
	BONOBO_UI_VERB_END
};

static void
set_current_document (AnjutaTool *tool, AnjutaDocument *doc)
{
	CORBA_Environment ev;
	CORBA_Object doc_objref;

	CORBA_exception_init (&ev);
	if (doc) {
		doc_objref = BONOBO_OBJREF (doc->docobj);
	
		GNOME_Development_Environment_Shell_addObject (tool->shell,
							       doc_objref,
							       "CurrentDocument",
							       &ev);
	} else {
		GNOME_Development_Environment_Shell_removeObject (tool->shell,
	 							  "CurrentDocument",
								  &ev);
	}

	CORBA_exception_free (&ev);
}

static void 
current_document_changed_cb (AnjutaDocumentManager *docman, 
			     AnjutaDocument *doc,
			     AnjutaTool *tool)
{
	set_current_document (tool, doc);
}

static gboolean
impl_init (AnjutaTool *tool, 
	   gpointer closure)
{
	CORBA_Environment ev;
	GtkWidget *docman;
	AnjutaDocument *doc;

	BonoboControl *ctrl;
	BonoboUIComponent *uic;

	g_return_val_if_fail (tool != NULL, FALSE);
	
	CORBA_exception_init (&ev);
	
	docman = anjuta_document_manager_new (tool->ui_container);
	gtk_signal_connect (GTK_OBJECT (docman), "current_document_changed",
			    GTK_SIGNAL_FUNC (current_document_changed_cb),
			    tool);
	
	gtk_widget_show (docman);

	doc = ANJUTA_DOCUMENT (anjuta_document_new (tool->ui_container));
	anjuta_document_make_temp (doc, "text/plain");
	anjuta_document_manager_add_doc (ANJUTA_DOCUMENT_MANAGER (docman), doc);

	ctrl = bonobo_control_new (docman);
		
	GNOME_Development_Environment_Shell_addControl (tool->shell,
							BONOBO_OBJREF (ctrl),
							"DocumentViewer",
							"Documents",
							ANJUTA_WINDOW_LOC_CENTER,
							&ev);
	Bonobo_Unknown_unref (BONOBO_OBJREF (ctrl), &ev);

	uic = bonobo_ui_component_new ("anjuta-document-manager");
	bonobo_ui_component_set_container (uic, tool->ui_container);
	bonobo_ui_util_set_ui (uic, ANJUTA_DATADIR, "gide-document-manager.xml", "anjuta-document-manager");
	bonobo_ui_component_add_verb_list_with_data (uic, verbs, docman);

	
	gtk_object_set_data (GTK_OBJECT (tool), "BonoboUIComponent", uic);
	gtk_object_set_data (GTK_OBJECT (tool), "DocumentManager", docman);

	GNOME_Development_Environment_Shell_addObject (tool->shell, 
						       bonobo_object_corba_objref (BONOBO_OBJECT (ANJUTA_DOCUMENT_MANAGER (docman)->docman_obj)),
						       "DocumentManager",
						       &ev);
	
	CORBA_exception_free (&ev);
	
	return TRUE;
}

static void 
impl_cleanup (AnjutaTool *tool,
	      gpointer closure)
{
	CORBA_Environment ev;
	BonoboUIComponent *uic;
	AnjutaDocumentManager *docman;

	CORBA_exception_init (&ev);

	docman = gtk_object_get_data (GTK_OBJECT (tool), "DocumentManager");

	gtk_container_remove (GTK_CONTAINER (GTK_WIDGET (docman)->parent), 
			      docman);

	if (docman->current_document) {
		set_current_document (tool, NULL);
	}

	GNOME_Development_Environment_Shell_removeObject (tool->shell,
							  "DocumentViewer",
							  &ev);
	GNOME_Development_Environment_Shell_removeObject (tool->shell, 
							  "DocumentManager",
							  &ev);

	uic = gtk_object_get_data (GTK_OBJECT (tool), "BonoboUIComponent");
	
	bonobo_ui_component_unset_container (uic);

	CORBA_exception_free (&ev);
}

ANJUTA_SHLIB_TOOL (DOCMAN_COMPONENT_IID, "Anjuta Document Manager",
		   impl_init, impl_cleanup, NULL);
