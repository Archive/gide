/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ANJUTA_DOCUMENT_H
#define ANJUTA_DOCUMENT_H

#include <config.h>
#include <bonobo.h>
#include <gdl/gdl.h>

#include "gide-document-obj.h"

#define ANJUTA_DOCUMENT(o)          (GTK_CHECK_CAST ((o), anjuta_document_get_type(), AnjutaDocument))
#define ANJUTA_DOCUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), anjuta_document_get_type(), AnjutaDocumentClass))
#define ANJUTA_IS_DOCUMENT(o)       (GTK_CHECK_TYPE ((o), anjuta_document_get_type()))

typedef struct _AnjutaDocument            AnjutaDocument;
typedef struct _AnjutaDocumentClass       AnjutaDocumentClass;
typedef struct _AnjutaDocumentPrivate     AnjutaDocumentPrivate;

struct _AnjutaDocumentClass {
	GtkVBoxClass vbox;
	
	void (* modified) (AnjutaDocument *document);
	void (* unmodified) (AnjutaDocument *document);
        void (* changed) (AnjutaDocument *document, gint change_type);
	void (* readonly) (AnjutaDocument *document);
	void (* unreadonly) (AnjutaDocument *document);
	void (* source) (AnjutaDocument *document, gchar *filename);
	void (* cursor) (AnjutaDocument* document);	void (* focus) (AnjutaDocument* document);
};

struct _AnjutaDocument {
	GtkVBox parent;
	
	Bonobo_UIContainer ui_container;
	BonoboControlFrame *control_frame;
	GtkWidget *bonobo_widget;
	Bonobo_PersistFile persist_file;
	Bonobo_PersistStream persist_stream;
	
	gboolean file_loaded;
	char *filename;
	char *mime_type;
	
	AnjutaDocumentObj *docobj;

	AnjutaDocumentPrivate *priv;
};

/*
 * Prototypes for 'anjuta_document.c'
 */
/* creation */
guint anjuta_document_get_type(void);
GtkWidget* anjuta_document_new(Bonobo_UIContainer ui_container);

/* file access */
void anjuta_document_make_temp (AnjutaDocument *document, const gchar *mime_type);
void anjuta_document_load_file(AnjutaDocument* document, const gchar* filename);
void anjuta_document_save_file(AnjutaDocument* document, const gchar* filename);

/* Functions to get/set state */
gboolean anjuta_document_is_free(AnjutaDocument* document);
gboolean anjuta_document_is_changed(AnjutaDocument* document);
void anjuta_document_set_changed_state(AnjutaDocument* document, gboolean state);
gboolean anjuta_document_is_readonly(AnjutaDocument* document);
void anjuta_document_set_readonly_state(AnjutaDocument* document, gboolean state);
gboolean anjuta_document_is_busy(AnjutaDocument* document);
void anjuta_document_set_busy_state(AnjutaDocument* document, gboolean state);
gchar* anjuta_document_get_filename(AnjutaDocument* document);
const gchar *anjuta_document_get_mime_type (AnjutaDocument *document);
void anjuta_document_set_last_mod(AnjutaDocument* document, gint last_mod);

void anjuta_document_check_changed(AnjutaDocument* document);
void anjuta_document_set_cfg_values (AnjutaDocument *document);

BonoboObjectClient *anjuta_document_get_control (AnjutaDocument *document);

#endif
