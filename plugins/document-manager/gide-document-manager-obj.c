/* Anjuta
 * Copyright 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <gdl/gdl.h>
#include <libgide/libgide.h>
#include "gide-document-manager-obj.h"
#include "gide-document-manager.h"
#include "file-ops.h"

struct _AnjutaDocumentManagerObjPriv
{
	char dummy;
};

static CORBA_Object create_document_manager_obj          (BonoboObject     *object);
static void         anjuta_document_manager_obj_destroy    (GtkObject        *object);
static void         anjuta_document_manager_obj_class_init (AnjutaDocumentManagerObjClass *class);
static void         anjuta_document_manager_obj_init       (BonoboObject     *object);

static BonoboObjectClass *parent_class;
static POA_GNOME_Development_Environment_DocumentManager__epv docman_epv;
static POA_GNOME_Development_Environment_DocumentManager__vepv docman_vepv;

static inline AnjutaDocumentManagerObj *
docman_from_servant (PortableServer_Servant servant)
{
	return ANJUTA_DOCUMENT_MANAGER_OBJ (bonobo_object_from_servant (servant));
}

/* public routines */

AnjutaDocumentManagerObj *
anjuta_document_manager_obj_new (AnjutaDocumentManager *dm)
{
	AnjutaDocumentManagerObj *docman;
	GNOME_Development_Environment_DocumentManager objref;

	g_return_val_if_fail (dm != NULL, NULL);

	docman = gtk_type_new (anjuta_document_manager_obj_get_type ());
	objref = 
		create_document_manager_obj (BONOBO_OBJECT (docman));
	if (objref == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (docman));
		return NULL;
	}
	
	gtk_object_ref (GTK_OBJECT (docman));
	gtk_object_sink (GTK_OBJECT (docman));
    
	bonobo_object_construct (BONOBO_OBJECT (docman), objref);
    
	docman->dm = dm;

	return docman;
}

GtkType 
anjuta_document_manager_obj_get_type (void)
{
	static GtkType type = 0;
        
	if (!type) {
		GtkTypeInfo info = {
			"IDL:GNOME::Development::Environment::DocumentManager:1.0",
			sizeof (AnjutaDocumentManagerObj),
			sizeof (AnjutaDocumentManagerObjClass),
			(GtkClassInitFunc) anjuta_document_manager_obj_class_init,
			(GtkObjectInitFunc) anjuta_document_manager_obj_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_object_get_type (), &info);
	}

	return type;
}

/* private routines */
CORBA_Object
create_document_manager_obj (BonoboObject *object) 
{
	POA_GNOME_Development_Environment_DocumentManager *servant;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);
	
	servant = 
		(POA_GNOME_Development_Environment_DocumentManager*)g_new0(BonoboObjectServant, 1);
	servant->vepv = &docman_vepv;
	
	POA_GNOME_Development_Environment_DocumentManager__init((PortableServer_Servant) servant, 
								&ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);
	return bonobo_object_activate_servant (object, servant);
}

static void
anjuta_document_manager_obj_destroy (GtkObject *object) 
{
	AnjutaDocumentManagerObj *docman = ANJUTA_DOCUMENT_MANAGER_OBJ (object);
}

static GNOME_Development_Environment_Document
impl_open_file (PortableServer_Servant servant, 
		const CORBA_char *filename,
		CORBA_Environment *ev)
{
	AnjutaDocumentManagerObj *docman = docman_from_servant (servant);
	AnjutaDocument *doc;
	
	GDL_TRACE ();

	doc = file_open_by_name (docman->dm, filename);

	return bonobo_object_dup_ref (BONOBO_OBJREF (doc->docobj), ev);
}

static GNOME_Development_Environment_DocumentManager_DocumentList*
impl_get_open_files (PortableServer_Servant servant,
		     CORBA_Environment *ev)
{
	GNOME_Development_Environment_DocumentManager_DocumentList *ret;
	int i;
	gint numdocs;

	AnjutaDocumentManagerObj *docman = docman_from_servant (servant);

	numdocs = anjuta_document_manager_num_docs(docman->dm);

	ret = GNOME_Development_Environment_DocumentManager_DocumentList__alloc ();

	ret->_length = numdocs;
	ret->_maximum = numdocs;
	ret->_buffer = CORBA_sequence_GNOME_Development_Environment_Document_allocbuf(numdocs);
	CORBA_sequence_set_release(ret, TRUE);

	for(i = 0; i < numdocs; i++)
	{
		AnjutaDocument *doc = anjuta_document_manager_get_nth_doc(docman->dm, i);

// ?????
		ret->_buffer[i] = bonobo_object_dup_ref(BONOBO_OBJREF(doc->docobj), ev);
	}

	return ret;
}

static void
init_docman_corba_class (void) 
{
	/* EPV */
	docman_epv.openFile = impl_open_file;
	docman_epv.getOpenFiles = impl_get_open_files;

	/* VEPV */
	docman_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
	docman_vepv.GNOME_Development_Environment_DocumentManager_epv = &docman_epv;
}
        
static void
anjuta_document_manager_obj_class_init (AnjutaDocumentManagerObjClass *class) 
{
	GtkObjectClass *object_class = (GtkObjectClass*) class;
	parent_class = gtk_type_class (bonobo_object_get_type ());
    
	object_class->destroy = anjuta_document_manager_obj_destroy;
    
	init_docman_corba_class ();
}

static void
anjuta_document_manager_obj_init (BonoboObject *object)
{
	AnjutaDocumentManagerObj *docman = ANJUTA_DOCUMENT_MANAGER_OBJ (object);
	docman->priv = NULL;
}

