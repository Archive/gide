/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000-2001 Dave Camp <dave@ximian.com>
 *               2001 Ximian, Inc.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <bonobo.h>
#include <bonobo/bonobo-stream-memory.h>
#include <fcntl.h>
#include <liboaf/liboaf.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include "gide-document.h"



/* Class signals */
enum {
	DOC_MODIFIED_SIGNAL,
	DOC_UNMODIFIED_SIGNAL,
	DOC_CHANGED_ON_DISK_SIGNAL,
	DOC_READONLY_SIGNAL,
	DOC_UNREADONLY_SIGNAL,
	DOC_SOURCE_SIGNAL,
	DOC_CURSOR_SIGNAL,
	DOC_FOCUS_SIGNAL,
	LAST_SIGNAL
};

static gint doc_signals[LAST_SIGNAL] = { 0 };

typedef struct {
	char *name;
	char *iid;
	guint menu_item;
	gboolean editable;
} AvailableComponent;

struct _AnjutaDocumentPrivate {
	GtkWidget *viewer_option;
	GtkWidget *app_option;
	GList *available_components;
	AvailableComponent *current_component;
	GList *available_apps;
	BonoboStream *saved_stream;
};

/* Prototypes */
static void anjuta_document_class_init (AnjutaDocumentClass *class);
static void anjuta_document_init (AnjutaDocument *document);
static void anjuta_document_destroy( GtkWidget *widget, gpointer data);

static gboolean load_file (AnjutaDocument *doc, const char *filename);
static void unload_file (AnjutaDocument *doc);

static void load_mime (AnjutaDocument *doc, const char *mime);
static void unload_mime (AnjutaDocument *doc);

static void update_viewers (AnjutaDocument *document);
static void unload_viewers (AnjutaDocument *document);

static void activate_component (AnjutaDocument *document, AvailableComponent *c);
static void unload_component (AnjutaDocument *document);

static void change_component (AnjutaDocument *document, AvailableComponent *v);
static void load_file_into_control (AnjutaDocument *document);
static void save_file_from_control (AnjutaDocument *document);

static gboolean create_editor_widget (AnjutaDocument *document, 
				      AvailableComponent *v);
static AvailableComponent *choose_default_component (AnjutaDocument *document);
static void set_default_clicked_cb (GtkWidget *btn, AnjutaDocument *document);

guint
anjuta_document_get_type()
{
	static guint document_type = 0;

	if (!document_type) {
		GtkTypeInfo document_info = {
			"AnjutaDocument",
			sizeof (AnjutaDocument),
			sizeof (AnjutaDocumentClass),
			(GtkClassInitFunc) anjuta_document_class_init,
			(GtkObjectInitFunc) anjuta_document_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};
		
		document_type = gtk_type_unique (gtk_vbox_get_type (),
						 &document_info);
	}
	
	return document_type;
}

static void
anjuta_document_class_init( AnjutaDocumentClass *class )
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass*) class;

	doc_signals[DOC_MODIFIED_SIGNAL]
		= gtk_signal_new ("doc_modified",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     modified),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	doc_signals[DOC_UNMODIFIED_SIGNAL]
		= gtk_signal_new ("doc_unmodified",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     unmodified),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	doc_signals[DOC_CHANGED_ON_DISK_SIGNAL]
		= gtk_signal_new ("doc_changed_on_disk",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     changed),
				  gtk_marshal_NONE__INT,
				  GTK_TYPE_NONE, 1, GTK_TYPE_INT);

	doc_signals[DOC_READONLY_SIGNAL]
		= gtk_signal_new ("doc_readonly",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     readonly),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	doc_signals[DOC_UNREADONLY_SIGNAL]
		= gtk_signal_new ("doc_unreadonly",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     unreadonly),
				  gtk_marshal_NONE__NONE,
				  GTK_TYPE_NONE, 0);

	doc_signals[DOC_SOURCE_SIGNAL]
		= gtk_signal_new ("doc_source",
				  GTK_RUN_FIRST,
				  object_class->type,
				  GTK_SIGNAL_OFFSET (AnjutaDocumentClass,
						     source),
				  gtk_marshal_NONE__POINTER,
				  GTK_TYPE_NONE, 1,
				  GTK_TYPE_POINTER);


	doc_signals[DOC_CURSOR_SIGNAL] = gtk_signal_new("doc_cursor",
		GTK_RUN_FIRST, object_class->type, GTK_SIGNAL_OFFSET(
		AnjutaDocumentClass, cursor), gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	doc_signals[DOC_FOCUS_SIGNAL] = gtk_signal_new("doc_focus",
		GTK_RUN_FIRST, object_class->type, GTK_SIGNAL_OFFSET(
		AnjutaDocumentClass, focus), gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

         gtk_object_class_add_signals (object_class,
				       doc_signals,
				       LAST_SIGNAL);

	 object_class->destroy = (GtkSignalFunc) anjuta_document_destroy;
}

static void
anjuta_document_init( AnjutaDocument *document )
{
	GtkWidget *hbox;
	GtkWidget *btn;

	/* Finish up object construction */
	gtk_object_default_construct( GTK_OBJECT(document) );

	document->priv = g_new0 (AnjutaDocumentPrivate, 1);

	document->file_loaded = FALSE;
	document->filename = NULL;
	document->persist_file = CORBA_OBJECT_NIL;
	document->persist_stream = CORBA_OBJECT_NIL;

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_box_pack_end (GTK_BOX (document), hbox, FALSE, FALSE, 0);

	btn = gtk_button_new_with_label (_("Make Default"));
	gtk_box_pack_end (GTK_BOX (hbox), btn, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (btn), "clicked", 
			    GTK_SIGNAL_FUNC (set_default_clicked_cb), 
			    document);

	document->priv->viewer_option = gtk_option_menu_new ();
	gtk_box_pack_end (GTK_BOX (hbox), document->priv->viewer_option,
			  FALSE, FALSE, 0);

	document->docobj = anjuta_document_obj_new (document);

	gtk_widget_show_all (hbox);
}

GtkWidget *
anjuta_document_new (Bonobo_UIContainer ui_container)
{
	CORBA_Environment ev;
	AnjutaDocument *document;

	document = gtk_type_new( anjuta_document_get_type() );
	document->ui_container = ui_container;

	CORBA_exception_init (&ev);
	Bonobo_Unknown_ref (ui_container, &ev);
	CORBA_exception_free (&ev);

	return GTK_WIDGET( document );
}

void
anjuta_document_make_temp (AnjutaDocument *document, const gchar  *mime_type)
{
	if (document->file_loaded) {
		unload_file (document);
	}

	document->mime_type = g_strdup (mime_type);
	
	update_viewers (document);

	if (document->priv->available_components) {
		activate_component (document, 
				    choose_default_component (document));
	}	
}

void
anjuta_document_load_file( AnjutaDocument *document, const gchar *filename)
{
	g_return_if_fail (document != NULL);
	g_return_if_fail (filename != NULL);
	load_file (document, filename);
}
		
void
anjuta_document_save_file( AnjutaDocument *document, const gchar *filename)
{
	const char *mime_type;
	char *filename_cpy;

	/* There's a pretty good chance the filename passed in was the filename
	 * we are about to free, so we save a copy */
	filename_cpy = g_strdup (filename); 

	if (document->filename) {
		g_free (document->filename);
	}
	document->filename = g_strdup (filename_cpy);

	save_file_from_control (document);

	/* If the file changed mime types, we reload to get the mime stuff
	 * right */
	mime_type = gnome_vfs_get_file_mime_type (filename_cpy, NULL, FALSE);
	if (strcmp (document->mime_type, mime_type)) {
		load_file (document, filename_cpy);
	}


	gtk_signal_emit (GTK_OBJECT (document),
			 doc_signals[DOC_SOURCE_SIGNAL],
			 filename_cpy);

	g_free (filename_cpy);
}

/*
 * this function checks if a document has been changed from
 * outside (i.e. the file mod time has changed)
 * signal: 0 - not changed
 * 	   1 - changed
 *	   2 - does not (longer) exist
 */
void
anjuta_document_check_changed (AnjutaDocument *document)
{
}

gboolean
anjuta_document_is_free( AnjutaDocument *document )
{
	return FALSE;
}

gboolean
anjuta_document_is_changed( AnjutaDocument *document )
{
	return FALSE;
}

void
anjuta_document_set_changed_state( AnjutaDocument *document, gboolean state )
{
}

void
anjuta_document_set_readonly_state( AnjutaDocument *document, gboolean state )
{
}

#if 0
static void
anjuta_document_changed (GtkWidget *widget, gpointer data)
{
	AnjutaDocument *document;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	document = ANJUTA_DOCUMENT(widget);

	anjuta_document_set_changed_state (document, TRUE);	
}
#endif

#if  0
static void
anjuta_document_get_focus (GtkWidget *widget, gpointer data)
{
        AnjutaDocument *document;

	document = ANJUTA_DOCUMENT (widget);

	anjuta_document_check_changed (document);

	gtk_signal_emit( GTK_OBJECT(document),
			 doc_signals[DOC_FOCUS_SIGNAL]);
}

#endif

static void
anjuta_document_destroy (GtkWidget *widget, gpointer data)
{
	AnjutaDocument *document;
	CORBA_Environment ev;
	
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));

	document = ANJUTA_DOCUMENT (widget);

	CORBA_exception_init (&ev);
	
	Bonobo_Unknown_unref (document->ui_container, &ev);

	CORBA_exception_free (&ev);
	
	if (document->file_loaded) {
		unload_file (document);
	} else {
		unload_component (document);
	}

	bonobo_object_unref (document->docobj);
}

gchar*
anjuta_document_get_filename(
	AnjutaDocument*			document
)
{
	return document->filename;
}

const gchar*
anjuta_document_get_mime_type (
	AnjutaDocument*			document
)
{
	return document->mime_type;
}

gboolean
anjuta_document_is_readonly(
	AnjutaDocument*			document
)
{
	return FALSE;
}


gboolean
anjuta_document_is_busy(
	AnjutaDocument*			document
)
{
	return FALSE;
}

void
anjuta_document_set_busy_state(
	AnjutaDocument*			document,
	gboolean			busy
)
{
}

void
anjuta_document_set_last_mod(
	AnjutaDocument*			document,
	gint				last_mod
)
{
}

void 
anjuta_document_set_cfg_values (AnjutaDocument *document)
{
}

BonoboObjectClient *
anjuta_document_get_control (AnjutaDocument *document)
{
	BonoboObjectClient *cli;
	
	cli = bonobo_widget_get_server (BONOBO_WIDGET (document->bonobo_widget));
	return cli;
}


static gboolean
load_file (AnjutaDocument *document, const char *filename)
{
	const char *mime_type;

	if (document->file_loaded)
		unload_file (document);
		
	document->filename = g_strdup (filename);
	
	mime_type = gnome_vfs_get_file_mime_type (filename, NULL, FALSE);

	load_mime (document, mime_type);

	if (document->priv->available_components) {
		activate_component (document, 
				    choose_default_component (document));
		load_file_into_control (document);
	}

	document->file_loaded = TRUE;

	gtk_signal_emit (GTK_OBJECT (document),
			 doc_signals[DOC_SOURCE_SIGNAL],
			 filename);

	return TRUE;
}

static void
unload_file (AnjutaDocument *document)
{
	unload_component (document);

	unload_mime (document);

	if (document->filename) {
		g_free (document->filename);
		document->filename = NULL;
	}

	if (document->priv->saved_stream) {
		bonobo_object_unref (BONOBO_OBJECT (document->priv->saved_stream));
		document->priv->saved_stream = FALSE;
	}
	
	document->file_loaded = FALSE;

}

static void 
load_mime (AnjutaDocument *document, const char *mime_type)
{
	if (document->mime_type) {
		unload_mime (document);
	}
	document->mime_type = g_strdup (mime_type);

	update_viewers (document);	
}

static void
unload_mime (AnjutaDocument *document)
{
	if (document->mime_type) {
		unload_viewers (document);
		g_free (document->mime_type);
		document->mime_type = NULL;
	}
}

static GSList *
get_lang_list (void)
{
        GSList *retval;
        char *lang;
        char * equal_char;

        retval = NULL;

        lang = g_getenv ("LANGUAGE");

        if (!lang) {
                lang = g_getenv ("LANG");
        }


        if (lang) {
                equal_char = strchr (lang, '=');
                if (equal_char != NULL) {
                        lang = equal_char + 1;
                }

                retval = g_slist_prepend (retval, lang);
        }
        
        return retval;
}

static BonoboStream *
get_working_copy (AnjutaDocument *document)
{
	BonoboStream *ret = NULL;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	if (!document->priv->current_component->editable
	    && document->priv->saved_stream) {
		ret = document->priv->saved_stream;
		document->priv->saved_stream = NULL;
	} else if (document->priv->current_component->editable
		   && !CORBA_Object_is_nil (document->persist_stream, &ev)) {
 		ret = bonobo_stream_mem_create (NULL, 0, 
						FALSE, TRUE);
		Bonobo_PersistStream_save (document->persist_stream,
					   bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
					   document->mime_type,
					   &ev);
		if (ev._major != CORBA_NO_EXCEPTION) {
			bonobo_object_unref (BONOBO_OBJECT (ret));
			ret = NULL;
		}
	} 
	
	if (!ret) {
		/* Prompt to save */
 	}

	if (ret) {
		Bonobo_Stream_seek (bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
				    0, Bonobo_Stream_SEEK_SET, &ev);
	}
	
	CORBA_exception_free (&ev);
	return ret;
}

static void
set_working_copy (AnjutaDocument *document, BonoboStream *stream)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	if (stream && !CORBA_Object_is_nil (document->persist_stream, &ev)) {
		/* Both components support Bonobo::PersistStream */
		Bonobo_PersistStream_load (document->persist_stream, 
					   bonobo_object_corba_objref (BONOBO_OBJECT (stream)),
					   document->mime_type,
					   &ev);
	} else if (stream) {
		/* The first component supported Bonobo::PersistStream, 
		 * but the second one does not.
		 * In this case, the user was not prompted to save
		 * because the previous control supported PersistStream and
		 * its contents were stored to memory. */

		/* FIXME: Offer to save the memory stream */
		load_file_into_control (document);
	} else {
		load_file_into_control (document);
	}

	if (!document->priv->current_component->editable && stream) {
		bonobo_object_ref (BONOBO_OBJECT (stream));
		document->priv->saved_stream = stream;
	}

	CORBA_exception_free (&ev);
}

static void
update_option_menu (AnjutaDocument *document, AvailableComponent *component)
{
	gtk_option_menu_set_history (GTK_OPTION_MENU (document->priv->viewer_option),
				     component->menu_item);
}



static void
activate_component (AnjutaDocument *document, AvailableComponent *component) 
{
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);
	if (document->priv->current_component) {
		unload_component (document);
	}
	
	create_editor_widget (document, component);
	
	if (document->bonobo_widget) {
		BonoboObjectClient *cli;
		
		cli = bonobo_widget_get_server (BONOBO_WIDGET (document->bonobo_widget));
		document->persist_file = bonobo_object_client_query_interface (cli, "IDL:Bonobo/PersistFile:1.0", NULL);
		
		document->persist_stream = bonobo_object_client_query_interface (cli, "IDL:Bonobo/PersistStream:1.0", NULL);
		
		document->control_frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (document->bonobo_widget));
			      
		if (!document->control_frame) {
			g_error ("could not get control frame for editor");
		}

		document->priv->current_component = component;
	}

	update_option_menu (document, component);
	
	CORBA_exception_free (&ev);
}

static void
unload_component (AnjutaDocument *document)
{
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);

	gtk_container_remove (GTK_CONTAINER (document), 
			      document->bonobo_widget);

	document->bonobo_widget = NULL;
	document->priv->current_component = NULL;

	if (!CORBA_Object_is_nil (document->persist_file, &ev)) {
		Bonobo_Unknown_unref (document->persist_file, &ev);
		CORBA_Object_release (document->persist_file, &ev);
		document->persist_file = CORBA_OBJECT_NIL;
	}
	if (!CORBA_Object_is_nil (document->persist_stream, &ev)) {
		Bonobo_Unknown_unref (document->persist_stream, &ev);
		CORBA_Object_release (document->persist_stream, &ev);
		document->persist_stream = CORBA_OBJECT_NIL;
	}
	
	CORBA_exception_free (&ev);
}

static void
change_component (AnjutaDocument *document, AvailableComponent *component)
{	
	BonoboStream *tmp = NULL;

	g_return_if_fail (component != NULL);

	if (document->priv->current_component == component) {
		return;
	}
	
	tmp = get_working_copy (document);

	unload_component (document);
	activate_component (document, component);
	
	set_working_copy (document, tmp);
	
	if (tmp) {
		bonobo_object_unref (BONOBO_OBJECT (tmp));
	}
}

static void 
load_file_into_control (AnjutaDocument *document) 
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	
	if (CORBA_Object_is_nil(document->persist_file, &ev)) {
		if (CORBA_Object_is_nil (document->persist_stream, &ev)) {
			g_warning ("The loaded component supports neither Bonobo::PersistFile nor Bonobo::PersistStream");
		} else {
			BonoboStream *stream;
			stream = bonobo_stream_open ("fs", document->filename, Bonobo_Storage_READ, O_RDONLY);
			Bonobo_PersistStream_load (document->persist_stream, bonobo_object_corba_objref (BONOBO_OBJECT (stream)), document->mime_type, &ev);
			bonobo_object_unref (BONOBO_OBJECT (stream));
		}
	} else {
		Bonobo_PersistFile_load (document->persist_file, 
					 document->filename, &ev);
	} 
	CORBA_exception_free (&ev);
}

static void
save_file_from_control (AnjutaDocument *document)
{
	 CORBA_Environment ev;
	 
	 CORBA_exception_init (&ev);
	 
	 if (CORBA_Object_is_nil (document->persist_file, &ev)) {
		  if (CORBA_Object_is_nil (document->persist_stream, 
					   &ev)) {
			   g_warning ("The loaded component supports neither Bonobo::PersistFile nor Bonobo::PersistStream");
		  } else {
			   BonoboStream *stream;
			   stream = bonobo_stream_open ("fs", 
							document->filename,
							Bonobo_Storage_WRITE, 
							O_WRONLY);
			   Bonobo_PersistStream_save (document->persist_stream,
						      bonobo_object_corba_objref (BONOBO_OBJECT (stream)),
						      document->mime_type,
						      &ev);
			   bonobo_object_unref (BONOBO_OBJECT (stream));
		  }
	 } else {
		  Bonobo_PersistFile_save (document->persist_file,
					   document->filename, &ev);
	 }
}
						
static GList *
get_available_components (const char *mime_type)
{
	/* FIXME: This should cache, since we're likely to do a lot of queries
	 * only only a few different file types */

	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	CORBA_char *query;
	GList *ret = NULL;
	char *generic;
	char *p;
	GSList *langs = get_lang_list ();

	generic = g_strdup (mime_type);
	p = strchr (generic, '/');
	g_assert (p);
	*(++p) = '*';
	*(++p) = 0;

	CORBA_exception_init (&ev);
	query = g_strdup_printf ("repo_ids.has ('IDL:Bonobo/Control:1.0') AND (bonobo:supported_mime_types.has ('%s') OR bonobo:supported_mime_types.has ('%s')) AND (repo_ids.has ('IDL:Bonobo/PersistFile:1.0') OR repo_ids.has ('IDL:Bonobo/PersistStream:1.0'))", mime_type, generic);
	
	oaf_result = oaf_query (query, NULL, &ev);

	g_free (generic);	       
	g_free (query);
	
	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL && oaf_result->_length >= 1) {
		int i;

		for (i = 0; i < oaf_result->_length; i++) {
			OAF_Property *prop;
			AvailableComponent *v = g_new (AvailableComponent, 1);
			OAF_ServerInfo *s = &oaf_result->_buffer[i];
			v->name = g_strdup (oaf_server_info_prop_lookup (s, "name",
									 langs));
			v->iid = g_strdup (s->iid);

			prop = oaf_server_info_prop_find (s, 
							  "bonobo:editable");
			v->editable = prop ? prop->v._u.value_boolean : FALSE;

			ret = g_list_prepend (ret, v);
		}
	}
	
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}
	CORBA_exception_free (&ev);

	return ret;
}

static void
destroy_available_components (GList *components)
{
	GList *i;
	for (i  = components; i != NULL; i = i->next) {
		AvailableComponent *c = i->data;
		g_free (c->name);
		g_free (c->iid);
		g_free (c);
	}
	g_list_free (components);
}

static GList *
get_available_apps (const char *mime_type)
{
	return gnome_vfs_mime_get_all_applications (mime_type);
}

static void
destroy_available_apps (GList *apps)
{
	gnome_vfs_mime_application_list_free (apps);
}


static void
launch_command (AnjutaDocument *doc, char *cmd, gboolean run_in_terminal)
{
	char **argv;
	int i = 0;

	if (run_in_terminal) {
		argv = g_new0 (char *, 5);
		argv[i++] = "gnome-terminal";
		argv[i++] = "--command";
		argv[i++] = g_strdup_printf ("%s %s", 
					     cmd, doc->filename);
		
	} else {
		argv = g_new0 (char*, 3);
		argv[i++] = cmd;
		argv[i++] = doc->filename;
	}
	
	gnome_execute_async (g_get_home_dir (), i, argv);
	
	if (run_in_terminal) {
		g_free (argv[2]);
	}
	
	g_free (argv);
}

static void
launch_application (AnjutaDocument *doc, GnomeVFSMimeApplication *app)
{
	launch_command (doc, app->command, app->requires_terminal);
}

static void
view_activated_cb (GtkWidget *widget, AnjutaDocument *document)
{
	AvailableComponent *comp = gtk_object_get_data (GTK_OBJECT (widget), 
							"AvailableComponent");
	if (comp) {
		change_component (document, comp);
	} else {
		GnomeVFSMimeApplication *a = gtk_object_get_data (GTK_OBJECT (widget), 
								  "Application");
		launch_application (document, a);
	}
}

static void
app_activated_cb (GtkWidget *widget, AnjutaDocument *document)
{
	GnomeVFSMimeApplication *a = gtk_object_get_data (GTK_OBJECT (widget), 
							  "Application");
	launch_application (document, a);
}

static void 
browse_cb (GtkWidget *widget, gpointer data)
{
	
}

static void
other_activated_cb (GtkWidget *widget, AnjutaDocument *document)
{
	GtkWidget *dlg;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *needs_terminal;
	GtkWidget *browse;

	dlg = gnome_dialog_new (_("Open With"), GNOME_STOCK_BUTTON_OK,
				GNOME_STOCK_BUTTON_CANCEL, 0);

	hbox = gtk_hbox_new (FALSE, 5);
	label = gtk_label_new (_("Application name:"));
	entry = gnome_entry_new ("open-with");
	browse = gtk_button_new_with_label (_("Browse..."));
	gtk_signal_connect (GTK_OBJECT (browse), "clicked",
			    GTK_SIGNAL_FUNC (browse_cb), 
			    gnome_entry_gtk_entry (GNOME_ENTRY (entry)));

	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 5);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 5);
	gtk_box_pack_start (GTK_BOX (hbox), browse, FALSE, FALSE, 5);
	
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), hbox,
			    TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);

	needs_terminal = gtk_check_button_new_with_label (_("Run in terminal"));
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), 
			    needs_terminal, TRUE, TRUE, 0);
	gtk_widget_show (needs_terminal);
	
	if (gnome_dialog_run (GNOME_DIALOG (dlg)) == 0) {
		char *cmd;
		
		cmd = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1);

		launch_command (document, cmd, 
				gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (needs_terminal)));
	}
	gtk_widget_destroy (dlg);
}

static GtkWidget *
build_component_menu (AnjutaDocument *document)
{
	int index = 0;
	GtkWidget *menu;
	GList *i;
	GtkWidget *item;

	menu = gtk_menu_new ();
	for (i = document->priv->available_components; i != NULL; i = i->next) {
		AvailableComponent *v = i->data;
		char *text;
		text = g_strdup_printf (v->editable ? _("Edit with %s")
					            : _("View with %s"),
					v->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);
		gtk_object_set_data (GTK_OBJECT (item), "AvailableComponent", 
				     v);

		gtk_signal_connect (GTK_OBJECT (item), "activate", 
				    GTK_SIGNAL_FUNC (view_activated_cb), 
				    document);

		gtk_menu_append (GTK_MENU (menu), item);
		v->menu_item = index++;
	}

	item = gtk_menu_item_new ();
	gtk_widget_set_sensitive (GTK_WIDGET (item), FALSE);
	gtk_menu_append (GTK_MENU (menu), item);

	for (i = document->priv->available_apps; i != NULL; i = i->next) {
		GnomeVFSMimeApplication *a = i->data;
		char *text;
		text = g_strdup_printf ("Launch %s", a->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);

		gtk_object_set_data (GTK_OBJECT (item), "AvailableComponent", 
				     NULL);
		gtk_object_set_data (GTK_OBJECT (item), "Application", a);

		gtk_signal_connect (GTK_OBJECT (item), "activate", 
				    GTK_SIGNAL_FUNC (app_activated_cb), 
				    document);

		gtk_menu_append (GTK_MENU (menu), item);
	}	

	item = gtk_menu_item_new ();
	gtk_widget_set_sensitive (GTK_WIDGET (item), FALSE);
	gtk_menu_append (GTK_MENU (menu), item);

	item = gtk_menu_item_new_with_label (_("Other..."));
	gtk_menu_append (GTK_MENU (menu), item);
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			    GTK_SIGNAL_FUNC (other_activated_cb),
			    document);

	return menu;
}

static GtkWidget *
build_app_menu (AnjutaDocument *document)
{
	GtkWidget *menu;
	GList *i;
	GtkWidget *item;

	menu = gtk_menu_new ();

	item = gtk_menu_item_new_with_label (_("Select Application"));
	gtk_menu_append (GTK_MENU (menu), item);

	for (i = document->priv->available_apps; i != NULL; i = i->next) {
		GnomeVFSMimeApplication *a = i->data;
		char *text;
		text = g_strdup_printf ("Launch %s", a->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);

		gtk_object_set_data (GTK_OBJECT (item), "AvailableComponent", 
				     NULL);
		gtk_object_set_data (GTK_OBJECT (item), "Application", a);

		gtk_signal_connect (GTK_OBJECT (item), "activate", 
				    GTK_SIGNAL_FUNC (app_activated_cb), 
				    document);

		gtk_menu_append (GTK_MENU (menu), item);
	}

	item = gtk_menu_item_new_with_label (_("Other..."));
	gtk_menu_append (GTK_MENU (menu), item);
	gtk_signal_connect (GTK_OBJECT (item), "activate",
			    GTK_SIGNAL_FUNC (other_activated_cb),
			    document);

	return menu;
}

static void
update_viewers (AnjutaDocument *document)
{
	GtkWidget *menu;
	
	if (document->priv->available_components) {
		unload_viewers (document);
	}

	document->priv->available_components = 
		get_available_components (document->mime_type);
	document->priv->available_apps = 
		get_available_apps (document->mime_type);
	menu = build_component_menu (document);
	gtk_widget_show_all (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (document->priv->viewer_option),
				  menu);
}

static void
unload_viewers (AnjutaDocument *document)
{
	if (!document->priv->available_components)
		return;

	destroy_available_components (document->priv->available_components);
	document->priv->available_components = NULL;
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (document->priv->viewer_option));

	destroy_available_apps (document->priv->available_apps);
	document->priv->available_apps = NULL;
}

static gboolean
create_editor_widget (AnjutaDocument *document, AvailableComponent *component)
{
	CORBA_Environment ev;
	gboolean ret = FALSE;
	BonoboControlFrame *frame;

	CORBA_exception_init (&ev);

	document->bonobo_widget = bonobo_widget_new_control (component->iid, 
							     document->ui_container);
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (document->bonobo_widget));
	bonobo_control_frame_set_autoactivate (frame, FALSE);

	if (document->bonobo_widget) {				
		gtk_container_add (GTK_CONTAINER (document), 
				   document->bonobo_widget);
		gtk_widget_show (document->bonobo_widget);

		ret = TRUE;
	} else {
		ret = FALSE;
	}
	CORBA_exception_free (&ev);
	return ret;
}

static AvailableComponent *
choose_default_component (AnjutaDocument *document)
{
	OAF_ServerInfo *server_info;
	AvailableComponent *ret = NULL;
	
	g_return_val_if_fail (document->priv->available_components != NULL, 
			      NULL);

	/* First try to match the gnome-vfs default component */
	server_info = gnome_vfs_mime_get_default_component (document->mime_type);
	if (server_info) {
		GList *i;
		for (i = document->priv->available_components; i != NULL; i = i->next) {
			AvailableComponent *component = i->data;
			if (strcmp (component->iid, server_info->iid) == 0) {
				ret = component;
				break;
			}
		}
		CORBA_free (server_info);
	}
	
	/* Then try to find an editable component */
	if (!ret) {
		GList *i;
		for (i = document->priv->available_components; i != NULL; i = i->next) {
			AvailableComponent *component = i->data;
			if (component->editable) {
				ret = component;
				break;
			}
		}
	}

	/* Then just pick the first one */
	if (!ret) {
		ret = document->priv->available_components->data;
	}
	return ret;
}

static void 
set_default_clicked_cb (GtkWidget *btn, AnjutaDocument *document)
{
	char *msg;

	gnome_vfs_mime_set_default_component (document->mime_type,
					      document->priv->current_component->iid);

	msg = g_strdup_printf (_("Set the default component for %s to %s"),
			       document->mime_type,
			       document->priv->current_component->name);
	gnome_ok_dialog (msg);
	g_free (msg);
}
