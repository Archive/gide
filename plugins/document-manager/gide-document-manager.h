/* Anjuta
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef ANJUTA_DOCUMENT_MANAGER_H
#define ANJUTA_DOCUMENT_MANAGER_H

#include <bonobo.h>
#include <gnome.h>
#include "gide-document.h"
#include "gide-document-manager-obj.h"

BEGIN_GNOME_DECLS
 
typedef struct _AnjutaDocumentManager      AnjutaDocumentManager;
typedef struct _AnjutaDocumentManagerClass AnjutaDocumentManagerClass;
typedef struct _AnjutaDocumentManagerPriv  AnjutaDocumentManagerPriv;

#define ANJUTA_DOCUMENT_MANAGER_TYPE        (anjuta_document_manager_get_type ())
#define ANJUTA_DOCUMENT_MANAGER(o)          (GTK_CHECK_CAST ((o), ANJUTA_DOCUMENT_MANAGER_TYPE, AnjutaDocumentManager))
#define ANJUTA_DOCUMENT_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOCUMENT_MANAGER_TYPE, AnjutaDocumentManagerClass))
#define ANJUTA_IS_DOCUMENT_MANAGER(o)       (GTK_CHECK_TYPE ((o), ANJUTA_DOCUMENT_MANAGER_TYPE))
#define ANJUTA_IS_DOCUMENT_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOCUMENT_MANAGER_TYPE))

struct _AnjutaDocumentManager {
	GtkNotebook parent;

	AnjutaDocument *current_document;

	GList *documents;
	Bonobo_UIContainer ui_container;
	AnjutaDocumentManagerObj *docman_obj;

	AnjutaDocumentManagerPriv *priv;
};

struct _AnjutaDocumentManagerClass {
	GtkNotebookClass parent_class;

	void (*current_document_changed)(AnjutaDocumentManager *docman, 
					 AnjutaDocument *document);
};
GtkType       anjuta_document_manager_get_type          (void);
GtkWidget *   anjuta_document_manager_new               (Bonobo_UIContainer ui_container);


/* Document Manipulation */
void anjuta_document_manager_add_doc (AnjutaDocumentManager *docman,
				    AnjutaDocument        *doc);

void anjuta_document_manager_remove_doc (AnjutaDocumentManager *docman, 
				       AnjutaDocument *doc);

void          anjuta_document_manager_goto_doc_by_index (AnjutaDocumentManager *docman,
						       gint                 index);
gint          anjuta_document_manager_num_docs          (AnjutaDocumentManager *docman);
gint anjuta_document_manager_num_changed_docs (AnjutaDocumentManager *docman);
AnjutaDocument *anjuta_document_manager_get_nth_doc       (AnjutaDocumentManager *docman,
						       guint                index);
AnjutaDocument *anjuta_document_manager_get_doc_by_name   (AnjutaDocumentManager *doc,
						       const char *filename);
AnjutaDocument *anjuta_document_manager_get_current_doc   (AnjutaDocumentManager *docman);
gchar * anjuta_document_manager_get_doc_label (AnjutaDocumentManager *docman, 
					     AnjutaDocument *document);

void anjuta_document_manager_set_doc_label (AnjutaDocumentManager *docman,
				     AnjutaDocument *document,
					  const gchar *str);
void
anjuta_document_manager_set_doc_label_style (AnjutaDocumentManager *docman,
					   AnjutaDocument *document,
					   GtkStyle *style);
gboolean anjuta_document_manager_is_open_doc (AnjutaDocumentManager *docman, 
				   const gchar *filename);





/* Navigation */
void          anjuta_document_manager_goto_doc_by_index (AnjutaDocumentManager *docman,
						       gint                 index);
void          anjuta_document_manager_goto_doc_by_file  (AnjutaDocumentManager *docman,
						       const gchar         *filename);






END_GNOME_DECLS
#endif
