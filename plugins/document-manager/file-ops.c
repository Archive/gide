/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <libgide/gide-utils.h>
#include "file-ops.h"
#include "gide-document-manager.h"

/* declared in gtkeditor-regex.h and syntaxtable.h */
#if 0
void            _gtk_editor_destroy_patterns   (GtkEditorHilitePatterns *patterns);
void            _gtk_editor_destroy_stable (GtkEditorSyntaxTable *stable);
#endif

/* Prototypes */
static void file_save_by_name( AnjutaDocumentManager *docman, 
			       AnjutaDocument *document,
			       gchar *filename );
static gint file_close_dialog( AnjutaDocumentManager *docman, 
			       AnjutaDocument *current );

/*
 ---------------------------------------------------------------------
     Function: file_strip_name()
     Desc: strip path from filename (unix-style)
 ---------------------------------------------------------------------
*/
gchar *file_strip_name( gchar *filename )
{
	return g_basename(filename);
}

/*
 ---------------------------------------------------------------------
     Function: file_new()
     Desc: Callback-Function /File/New
 ---------------------------------------------------------------------
*/
void
file_new( GtkWidget *widget, gpointer data )
{
	AnjutaDocument *document;
    
	g_assert (ANJUTA_DOCUMENT_MANAGER (data));

	document = ANJUTA_DOCUMENT( anjuta_document_new(ANJUTA_DOCUMENT_MANAGER(data)->ui_container) );
	anjuta_document_make_temp (document, "text/plain");
	
	anjuta_document_manager_add_doc( ANJUTA_DOCUMENT_MANAGER(data), document );

}


/*
 ---------------------------------------------------------------------
     Function: file_open()
     Desc: Callback-Function /File/Open
 ---------------------------------------------------------------------
*/
AnjutaDocument *
file_open( GtkWidget *widget, gpointer data )
{
	GList *files = anjuta_file_sel_new( _("Open File..."), FALSE, TRUE );
	AnjutaDocument *document = NULL;

	g_assert(ANJUTA_IS_DOCUMENT_MANAGER (data));

	while( files ) {
		gchar *filename = (gchar *) files->data;

		document = file_open_by_name( ANJUTA_DOCUMENT_MANAGER(data), 
					      filename );

		files = g_list_next( files );
	}

	anjuta_file_sel_free_list( files );
	return document;
}


AnjutaDocument *
file_open_by_name( AnjutaDocumentManager *docman, const gchar *filename )
{
    	gchar buf[STRLEN];
    	AnjutaDocument *document = NULL;

    	/* only open the file, if its not already opened */
	if ( anjuta_document_manager_is_open_doc(docman, filename)) {
		anjuta_document_manager_goto_doc_by_file(docman, filename);
		return anjuta_document_manager_get_doc_by_name (docman, 
							      filename);
	}
	
	document = anjuta_document_manager_get_current_doc( docman );
    	if ( !document || !anjuta_document_is_free(document) ) {
		document = ANJUTA_DOCUMENT( anjuta_document_new(docman->ui_container) );
		anjuta_document_load_file( document, filename);
		anjuta_document_manager_add_doc( docman, document );
    	} else {
		anjuta_document_load_file(document, filename);
	}
	return document;
}

/*
 ---------------------------------------------------------------------
     Function: file_reload()
     Desc: Callback-Function /File/Revert
 ---------------------------------------------------------------------
*/
void
file_reload( GtkWidget *widget, gpointer data )
{
	AnjutaDocument *current;
	gchar *filename;
	gchar buf[STRLEN];
	gint pos;

	g_assert(ANJUTA_IS_DOCUMENT_MANAGER(data));

	current = anjuta_document_manager_get_current_doc( ANJUTA_DOCUMENT_MANAGER(data) );
	
	if( !current )
		return;

    	if( !anjuta_document_get_filename(current) )
        	return;

	if ( !anjuta_document_is_changed(current) )
		return;
		
	if( anjuta_ask_dialog( _("The file has been changed,\nDo You want to reload it?")) != 0 )  /* 0 == GNOME_YES */
	{
		return;
	}

#if 0
	pos = gI_document_get_point( current );
#endif	
	filename = g_strdup( anjuta_document_get_filename(current) );
	anjuta_document_load_file(current, filename);
	g_free( filename );
#if 0
    	if( gI_document_get_length( current ) >= pos )
        	gI_document_set_point( current, pos );
#endif
}


/*
 ---------------------------------------------------------------------
     Function: file_save()
     Desc: Callback-Function /File/Save
 ---------------------------------------------------------------------
*/
void
file_save( GtkWidget *widget, gpointer data )
{
	AnjutaDocumentManager *docman;
	AnjutaDocument *document;

	g_assert(ANJUTA_IS_DOCUMENT_MANAGER(data));

	docman = ANJUTA_DOCUMENT_MANAGER(data);
	
	document = anjuta_document_manager_get_current_doc( docman );
	if( !document )
		return;

    	if( !anjuta_document_get_filename(document) ) {
        	file_save_as( widget, data );
        	return;
    	}
    
	anjuta_document_save_file( document,
				 anjuta_document_get_filename(document));
	
//    	gI_window_set_statusbar( window );
}


/*
 ---------------------------------------------------------------------
     Function: file_save_as()
     Desc: Callback-Function /File/Save As
 ---------------------------------------------------------------------
*/
void file_save_as( GtkWidget *widget, gpointer data )
{
	AnjutaDocumentManager *docman;
	AnjutaDocument *document;
	GList *files;

	g_assert(ANJUTA_IS_DOCUMENT_MANAGER(data));

	docman = ANJUTA_DOCUMENT_MANAGER( data );
	document = anjuta_document_manager_get_current_doc( docman );	
	
	files = anjuta_file_sel_new( _("Save As..."), TRUE, FALSE );

	if( files ) {
		gchar *filename = files->data;
		if( !filename )
			return;

	        file_save_by_name( docman, document, filename );
	} else {
		return;
	}	

	anjuta_file_sel_free_list( files );
}

static void
file_save_by_name( AnjutaDocumentManager *docman, AnjutaDocument *document,
		   gchar *filename )
{
	gchar warnmsg[STRLEN];
	gint btn;

	if( file_exist( filename ) != 0 ) {
		/* overwrite dialog */
		sprintf( warnmsg,
			 _("The File %s already exists. Overwrite it?"),
			 filename );
	        btn = anjuta_ask_dialog( warnmsg);
        
        	switch (btn) {
			/* YES */
			case 0:
			    	break;

			/* NO, CANCEL or CLOSE */
	    		case 1:
	    		case 2:
	    		case -1:
				return;
				break;
		}
	}

	anjuta_document_save_file( document, filename);
}

/*
 ---------------------------------------------------------------------
     Function: file_close()
     Desc: Callback-Function /File/Close
 ---------------------------------------------------------------------
*/
void
file_close( GtkWidget *widget, gpointer data)
{
	AnjutaDocumentManager *docman;
	AnjutaDocument *document;

	g_assert(ANJUTA_IS_DOCUMENT_MANAGER(data));
	docman = ANJUTA_DOCUMENT_MANAGER(data);

	document = anjuta_document_manager_get_current_doc( docman );

	if( !document )
		return;

    	if( anjuta_document_is_changed( document ) ) { 
		if( file_close_dialog(docman, document) == 2 )
			return; 
    	}


	anjuta_document_manager_remove_doc (docman, document);
}

/* -------------------------------------*/
/* file_close_dialog()                       */
/* return TRUE if user press NO and file is NOT saved      */
/* Return FALSE if file will save or user press cancel button*/
/* Use this instead of file_close_changed_dialog */
/* --------------------------------------*/
static gint
file_close_dialog( AnjutaDocumentManager *docman, AnjutaDocument *current )
{
	gchar *filename;
	gchar HelpString[STRLEN];
	gint btn;

	if( !anjuta_document_get_filename(current) ) {
		gchar *label;

		label = g_strdup( anjuta_document_manager_get_doc_label(docman, current) );
		
        	filename = g_strdup(file_strip_name( label ));
	} else {
		filename = g_strdup( anjuta_document_get_filename(current) ); 
	}

	g_snprintf( HelpString, STRLEN,
		    _("The file \"%s\" has been modified!\nSave it?\n"),
		    filename );
	g_free(filename);
	
	btn = anjuta_ask_dialog( HelpString );
	if ( btn == 0 ) {
		if( !anjuta_document_get_filename(current) )
			file_save_as( NULL, docman );
		else
			file_save( NULL, docman );
	}

	return btn;
}


static void write_history( AnjutaDocument *document, FILE *history )
{
	if( !document )
        	return;

	if( !anjuta_document_get_filename(document) )
        	return;

	fprintf( history, "%s\n", anjuta_document_get_filename(document) );
}

static void
_file_autosave( AnjutaDocument *document, gpointer data )
{
  /* FIXME: Deal with the component architecture correctly */
#if 0
	FILE *docfile;
	gchar str[STRLEN];
	glong length,i;
	gchar *c;

    	/* don't autosave, if no document is given */
    	if( !document )
        	return;

    	/* don't autosave, if the document has no filename (i.e. "Untitled") */
    	if( !gI_document_get_filename(document) )
        	return;

    	/* don't autosave, if document is unchanged */
    	if( !gI_document_is_changed(document) )
        	return;

    	docfile = fopen( gI_document_get_filename(document), "w" );
    	if( !docfile )
    	{
        	g_snprintf( str, STRLEN, _("Unable to save '%s'!"), gI_document_get_filename(document) );
        	anjuta_error_dialog( str);
        	return;
    	}

    	length = gI_document_get_length( document );
    	for(i=1;i<=length;i++)
	{
        	c = gI_document_get_chars( document, i-1, i );
		fputc( c[0], docfile );
		g_free( c );
	}

    	fclose( docfile );

    	gI_document_set_changed_state( document, FALSE );

	gI_document_set_last_mod(document, get_last_mod( gI_document_get_filename(document) ));

//	gI_window_set_statusbar( main_window );
#endif
}


void file_autosave( GtkWidget *widget, gpointer data )
{
#if 0
	gchar sb_msg[STRLEN];

	/*g_print( "autosave!!\n" );*/
	/* remove timeout */
	gtk_timeout_remove( main_window->timeout_id );

    	g_list_foreach( main_window->documents, (GFunc) _file_autosave, NULL );

	/* re-add timeout */
	main_window->timeout_id = gtk_timeout_add( cfg->autosave_freq * 1000, (GtkFunction) file_autosave, NULL ); 
#endif
}


gint file_check_if_exist( gchar *fname, gint askcreate )
{
	gchar txt[512];
	FILE *file;
	struct stat sta;
	gint e;
        
	e=stat(fname,&sta);
	if( !e && S_ISREG(sta.st_mode) )
	{   	/* file exist and is a regular file */
		return( 1 );	
	}

	if( askcreate && e==-1 )
	{
	    sprintf( txt, _("The file\n'%s'\ndoes not exist!\nDo you want to create the file?"), fname );
	    if( anjuta_ask_dialog( txt ) == 0 ) /*GNOME_YES )*/
	    {
	        file = fopen( fname, "w" );	/* this is not a good way!! */
		if( file ) 
		{
			fclose( file );
		}

		return( 1 );
	    }
	}
	else
	{
		if( !e )
		{ 
		    sprintf( txt, _("The file '%s' is not a regular file!"), fname );
                    anjuta_error_dialog( txt );
                }
	}

	return( 0 );
}

/*
 * Close all open files
 */
void
file_close_all(GtkWidget *widget, gpointer data)
{
	gint i;
	
	AnjutaDocumentManager *docman;
	g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
	docman = ANJUTA_DOCUMENT_MANAGER (data);

	for(i = 0; i < anjuta_document_manager_num_docs(docman); i++)
	{
		gtk_notebook_set_page(GTK_NOTEBOOK(docman), i);
		file_close(NULL, docman);
	}
}


/*
 * Save all files
 */
void
file_save_all(GtkWidget *widget, gpointer data)
{
	gint i;
	AnjutaDocumentManager *docman;
	gint nod;
	AnjutaDocument *document;

	g_return_if_fail(ANJUTA_IS_DOCUMENT_MANAGER(data));

	docman = ANJUTA_DOCUMENT_MANAGER(data);

	nod = anjuta_document_manager_num_changed_docs(docman);

	if(nod > 0) {
		for(i = 0; i < anjuta_document_manager_num_docs(docman); i++) {
			gtk_notebook_set_page(GTK_NOTEBOOK(docman), i);
			document = anjuta_document_manager_get_current_doc(docman);
			if (anjuta_document_is_changed(document)) {
				file_save(NULL, docman);
			}
		}
	}
}







