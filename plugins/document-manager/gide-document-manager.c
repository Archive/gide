/* Anjuta
 * Copyright 1998-2000 Steffen Kern
 *           2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/* This code used to be in Anjuta/src/gI_window.c */

#include <config.h>
#include <gnome.h>

#include "gide-document-manager.h"
#include <libgide/libgide.h>
#include "file-ops.h"
#include "small-close.xpm"

static void docman_destroy    (GtkObject                *object);
static void docman_class_init (AnjutaDocumentManagerClass *class);
static void docman_init       (GtkObject                *obj);

static void docman_switch_notebookpage(GtkWidget* widget,
				       GtkNotebookPage* page,
				       gint page_num,
				       gpointer data);


static void docman_switch_notebookpage(GtkWidget* widget, 
				       GtkNotebookPage* page,
				       gint page_num, gpointer data);
static void docman_doc_modified (GtkWidget *widget, gpointer data);
static void docman_doc_unmodified (GtkWidget *widget, gpointer data);
static void docman_doc_changed (GtkWidget *widget, 
				gint change_type, gpointer data);
static void docman_doc_readonly (GtkWidget *widget, gpointer data);
static void docman_doc_unreadonly (GtkWidget *widget, gpointer data);
static void docman_doc_source (GtkWidget *widget, 
			       gchar *filename, gpointer data);
static void docman_doc_destroy (GtkWidget *widget, gpointer data);
static void set_current_document (AnjutaDocumentManager *docman,
				  AnjutaDocument *doc);

enum {
	CURRENT_DOCUMENT_CHANGED,
	LAST_SIGNAL
};

static gint docman_signals[LAST_SIGNAL];

GtkNotebookClass *parent_class;

/* public routines */

GtkWidget *
anjuta_document_manager_new (Bonobo_UIContainer ui_container)
{
	AnjutaDocumentManager *dm;

	dm = gtk_type_new (anjuta_document_manager_get_type ());

	dm->ui_container = ui_container;

	return GTK_WIDGET (dm);
}

GtkType 
anjuta_document_manager_get_type (void)
{
	static GtkType type = 0;
        
	if (!type) {
		GtkTypeInfo info = {
			"AnjutaDocumentManager",
			sizeof (AnjutaDocumentManager),
			sizeof (AnjutaDocumentManagerClass),
			(GtkClassInitFunc) docman_class_init,
			(GtkObjectInitFunc) docman_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};
		
		type = gtk_type_unique (gtk_notebook_get_type (), &info);
	}
	
	return type;
}

/* Document Manipulation */

static void
close_document_cb (GtkButton *button,
		   AnjutaDocumentManager *docman)
{
	AnjutaDocument *document;

	document = gtk_object_get_data (GTK_OBJECT (button), "document");

    	if( anjuta_document_is_changed( document ) ) { 
		if( file_close_dialog(docman, document) == 2 )
			return; 
    	}


	anjuta_document_manager_remove_doc (docman, document);
}

void
anjuta_document_manager_add_doc (AnjutaDocumentManager *docman,
			       AnjutaDocument *document)
{
	GtkWidget *hbox;
	GtkWidget *tab_hbox;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *pixmap;
	gchar *label_str;
	static glong doc_count = 1;
	char *filename;

	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	
	/* Add the document to the list */
	docman->documents = g_list_append (docman->documents,
					   (gpointer)document);
	
	
	filename = anjuta_document_get_filename (document);
	if (filename)
		label_str = g_strdup (g_basename (filename));
	else
		label_str = g_strdup_printf ("Untitled%ld", doc_count++);

	label = gtk_label_new (label_str);
	g_free (label_str);

	/* Build the tab widget close button */
	tab_hbox = gtk_hbox_new (FALSE, 0);
	pixmap = gnome_pixmap_new_from_xpm_d (small_close);
	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (button), pixmap);
	gtk_object_set_data (GTK_OBJECT (button), "document", 
			     (gpointer) document);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (close_document_cb), docman);

	gtk_box_pack_start (GTK_BOX (tab_hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (tab_hbox), button, FALSE, FALSE, 0);

	gtk_widget_show_all (tab_hbox);

	/* Build an hbox to hold the document */
	hbox = gtk_hbox_new (FALSE, 0);
        gtk_box_pack_start (GTK_BOX(hbox), GTK_WIDGET(document),
			    TRUE, TRUE, 5);
	
        gtk_widget_show (hbox);
	
	/* We'll remember the hbox for tab manipulation */
	gtk_object_set_data (GTK_OBJECT(document),
			     "AnjutaDocumentManager::hbox",
			     (gpointer)hbox);
	/* Add the doc to the notebook */
	gtk_notebook_append_page (GTK_NOTEBOOK (docman),
				  hbox, tab_hbox);


	gtk_signal_connect_after (GTK_OBJECT (docman), "switch_page",
				  GTK_SIGNAL_FUNC (docman_switch_notebookpage),
				  docman);
	
	/* Listen for changes */
	gtk_signal_connect (GTK_OBJECT (document), "doc_modified",
			    GTK_SIGNAL_FUNC (docman_doc_modified),
			    (gpointer) docman);
	gtk_signal_connect (GTK_OBJECT (document), "doc_unmodified",
			    GTK_SIGNAL_FUNC (docman_doc_unmodified),
			    (gpointer) docman);
	gtk_signal_connect (GTK_OBJECT (document), "doc_changed_on_disk",
			    GTK_SIGNAL_FUNC (docman_doc_changed),
			    (gpointer) docman);
	gtk_signal_connect (GTK_OBJECT (document), "doc_readonly",
			    GTK_SIGNAL_FUNC (docman_doc_readonly),
			    (gpointer) docman );
	gtk_signal_connect (GTK_OBJECT(document), "doc_unreadonly",
			    GTK_SIGNAL_FUNC( docman_doc_unreadonly ),
			    (gpointer) docman );
	gtk_signal_connect (GTK_OBJECT(document), "doc_source",
			    GTK_SIGNAL_FUNC( docman_doc_source ),
			    (gpointer) docman );
	gtk_signal_connect (GTK_OBJECT(document), "destroy",
			    GTK_SIGNAL_FUNC( docman_doc_destroy ),
			    (gpointer) docman );

	gtk_widget_show (GTK_WIDGET (document));
	gtk_widget_grab_focus (GTK_WIDGET(document));

	/* Flip to the new page, will set off the signal */
	anjuta_document_manager_goto_doc_by_index (docman, -1);

	/* Well, it won't always set off the signal.  Not the first time. */
	if (!docman->current_document) {
		set_current_document (docman, document);
	}
}

void
anjuta_document_manager_remove_doc (AnjutaDocumentManager *docman, 
				  AnjutaDocument *document)
{
        gint length, i;
        
        length = anjuta_document_manager_num_docs (docman);
        for (i=0; i < length; i++) {
                if ((gpointer)document
                     == g_list_nth_data(docman->documents, i)) {
                        gtk_notebook_remove_page(
                                GTK_NOTEBOOK (docman), i);
                        docman->documents =
                                g_list_remove (docman->documents, document);
                        break;
                }
        }

	if (anjuta_document_manager_num_docs (docman) == 0) {
		set_current_document (docman, NULL);
	}
}

gint
anjuta_document_manager_num_docs (AnjutaDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, -1);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), -1);

	return g_list_length (docman->documents);
}

gint 
anjuta_document_manager_num_changed_docs (AnjutaDocumentManager *docman)
{
	AnjutaDocument *document;
	GList *docs;
	gint cnt = 0;
	
	docs = docman->documents;
	
	while (docs) {
		document = (AnjutaDocument *)docs->data;
		if (!document) {
			docs = docs->next;
			continue;
		}
		
		if (anjuta_document_is_changed (document)) {
			cnt++;
		}
		

		docs = docs->next;
	}

	return cnt;
}

AnjutaDocument *
anjuta_document_manager_get_nth_doc (AnjutaDocumentManager *docman, guint index)
{
	AnjutaDocument *document;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);

	document = g_list_nth_data (docman->documents, index);
	
	return document;
}

AnjutaDocument *
anjuta_document_manager_get_doc_by_name   (AnjutaDocumentManager *docman,
					 const char *filename)
{
	AnjutaDocument *document;
	gint len, i;
	
 	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (filename != NULL, NULL);
	
	len = anjuta_document_manager_num_docs (docman);

	for (i=0; i<len; i++) {
		document = anjuta_document_manager_get_nth_doc (docman, i);
		if (anjuta_document_get_filename (document) &&
		    !strcmp(anjuta_document_get_filename (document), filename)) {
			return anjuta_document_manager_get_nth_doc (docman, i);
		}
	}

	return NULL;
}

AnjutaDocument *
anjuta_document_manager_get_current_doc (AnjutaDocumentManager *docman)
{
	gint pagenum;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);

	pagenum = gtk_notebook_get_current_page (GTK_NOTEBOOK (docman));
	return anjuta_document_manager_get_nth_doc (docman, pagenum);
}

gchar *
anjuta_document_manager_get_doc_label (AnjutaDocumentManager *docman, 
				     AnjutaDocument *document)
{
	GtkWidget *label;
	gpointer hbox;
	gchar *str;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT (document), NULL);
	
	hbox = gtk_object_get_data (GTK_OBJECT (document), 
				    "AnjutaDocumentManager::hbox");
	label = gtk_notebook_get_tab_label (GTK_NOTEBOOK (docman),
					    GTK_WIDGET (hbox));
	gtk_label_get (GTK_LABEL (label), &str);
	
	return str;
}

void
anjuta_document_manager_set_doc_label (AnjutaDocumentManager *docman,
				     AnjutaDocument *document,
				     const gchar *str)
{
	GtkWidget *label;
	gpointer hbox;

	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT (document));
	g_return_if_fail (str != NULL);
	
	hbox = gtk_object_get_data (GTK_OBJECT (document),
				    "AnjutaDocumentManager::hbox");
	label = gtk_notebook_get_tab_label (GTK_NOTEBOOK (docman),
					    GTK_WIDGET (hbox));
	gtk_label_set (GTK_LABEL (label), str);
}	

gboolean
anjuta_document_manager_is_open_doc (AnjutaDocumentManager *docman, 
				   const gchar *filename)
{
	AnjutaDocument *document;
	gint len, i;

	g_return_val_if_fail (docman != NULL, FALSE);
	g_return_val_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	len = anjuta_document_manager_num_docs (docman);
	for (i = 0; i < len; i++) {
		document = anjuta_document_manager_get_nth_doc (docman, i);
		if (anjuta_document_get_filename (document) &&
		    !strcmp (anjuta_document_get_filename (document), filename))
			return TRUE;
	}
	return FALSE;
}					   

/* Navigation */

void
anjuta_document_manager_goto_doc_by_index (AnjutaDocumentManager *docman, 
					 gint index)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (index >= -1);
	
	gtk_notebook_set_page (GTK_NOTEBOOK (docman), index);
}

void
anjuta_document_manager_goto_doc_by_file (AnjutaDocumentManager *docman, 
					const gchar *filename)
{
	AnjutaDocument *document;
	gint len, i;

 	g_return_if_fail (docman != NULL);
	g_return_if_fail (ANJUTA_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (filename != NULL);

	len = anjuta_document_manager_num_docs (docman);

	for (i=0; i<len; i++) {
		document = anjuta_document_manager_get_nth_doc (docman, i);
		if (anjuta_document_get_filename (document) &&
		    !strcmp(anjuta_document_get_filename (document), filename))
			anjuta_document_manager_goto_doc_by_index (docman, i);
	}
}

/* private routines */

static void
docman_destroy (GtkObject *object) 
{
	AnjutaDocumentManager *dm = ANJUTA_DOCUMENT_MANAGER (object);
	
        GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
docman_class_init (AnjutaDocumentManagerClass *class) 
{
	GtkObjectClass *object_class = (GtkObjectClass*) class;
	parent_class = gtk_type_class (gtk_notebook_get_type ());
	
	object_class->destroy = docman_destroy;

	docman_signals[CURRENT_DOCUMENT_CHANGED] =
		gtk_signal_new ("current_document_changed", 
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (AnjutaDocumentManagerClass,
						   current_document_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_STRING);
	gtk_object_class_add_signals (object_class, docman_signals, 
				      LAST_SIGNAL);
}

static void
docman_init (GtkObject *obj)
{
	AnjutaDocumentManager *dm = ANJUTA_DOCUMENT_MANAGER (obj);

	dm->docman_obj = anjuta_document_manager_obj_new (dm);
	gtk_notebook_set_scrollable (GTK_NOTEBOOK (dm), TRUE);
}

static void
docman_switch_notebookpage(GtkWidget* widget, GtkNotebookPage* page,
			   gint page_num, gpointer data)
{
        AnjutaDocument *document;
	AnjutaDocumentManager *docman;

        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        
        g_return_if_fail (docman);
        g_return_if_fail (page_num < anjuta_document_manager_num_docs (docman));

        document = anjuta_document_manager_get_nth_doc (docman, page_num);
        g_return_if_fail (document);

        gtk_widget_grab_focus (GTK_WIDGET(document));

	set_current_document (docman, document);
}

static void
docman_doc_modified (GtkWidget *widget, gpointer data)
{
	AnjutaDocumentManager *docman;
        AnjutaDocument *document;
        GtkStyle *style;
        GdkColor color = {0, 0xffff, 0, 0};
        
        g_return_if_fail(data);
                
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        /* Set red text color style */
        style = gtk_style_new ();
        style->fg[0] = color;
        
        anjuta_document_manager_set_doc_label_style (docman, document, style);
}

static void
docman_doc_unmodified (GtkWidget *widget, gpointer data)
{
        AnjutaDocumentManager *docman;
        AnjutaDocument *document;
        GtkStyle *style;
        GdkColor color = {0, 0, 0, 0};
        
        g_return_if_fail (data);
                
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        /* Set black text color style */
        style = gtk_style_new ();
        style->fg[0] = color;
        
        anjuta_document_manager_set_doc_label_style (docman, document, style);
}

static void
docman_doc_changed (GtkWidget *widget, gint change_type, gpointer data)
{
        AnjutaDocumentManager *docman;
        AnjutaDocument *document;
	gchar *msg = NULL;
        
        g_return_if_fail(data);
                
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        switch (change_type) {
        case 0:
		break;

        case 1:
		if (anjuta_document_is_changed (document)) {
			msg = g_strdup_printf (_("The file\n'%s'\nhas been changed from outside the editor!\nIf you reload, you'll loose the changes you did in Anjuta!\nDo you really want to reload it?"), anjuta_document_get_filename (document));
		} else {
			msg = g_strdup_printf (_("The file\n'%s'\nhas been changed from outside the editor!\nDo you want to reload it?"), anjuta_document_get_filename (document));
		}
		
		if (anjuta_ask_dialog (msg) == 0) /* YES */	{
			file_reload (NULL, docman);
		} else {
			anjuta_document_set_changed_state( document, TRUE );
		}
		g_free (msg);
		
		break;
		
        case 2:
		msg = g_strdup_printf (_("The file\n'%s'\nis not longer available.\nChoose YES to close it, or NO to keep it!"), anjuta_document_get_filename(document));
		
		if (anjuta_ask_dialog (msg) == 0) /* YES */	{
			file_close( NULL, docman );
		} else {
			anjuta_document_set_changed_state (document, TRUE);
		}
		g_free (msg);
		
		break;
        }
}

static void
docman_doc_readonly (GtkWidget *widget, gpointer data)
{
        AnjutaDocumentManager *docman;
        
        g_assert(ANJUTA_IS_DOCUMENT_MANAGER (data));
	
	docman = ANJUTA_DOCUMENT_MANAGER (data);
        
        g_return_if_fail (docman);

}

static void
docman_doc_unreadonly (GtkWidget *widget, gpointer data)
{
        AnjutaDocumentManager *docman;
        
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));

        docman = ANJUTA_DOCUMENT_MANAGER (data);

        g_return_if_fail (docman);
}

static void
docman_doc_source (GtkWidget *widget, gchar *filename, gpointer data)
{
        AnjutaDocumentManager *docman;
        AnjutaDocument *document;
        gpointer hbox;
        gchar *basename;

        g_return_if_fail (data);
        
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);
        hbox = gtk_object_get_data (GTK_OBJECT(widget), 
				    "AnjutaDocumentManager::hbox");

        if (filename) {
                basename = g_basename (filename);

                anjuta_document_manager_set_doc_label (docman, 
						     document, basename);
        }
}

static void
docman_doc_destroy (GtkWidget *widget, gpointer data)
{
        AnjutaDocumentManager *docman;
        AnjutaDocument *document;
        gint length, i;
        
        g_return_if_fail(data);
        
        g_assert (ANJUTA_IS_DOCUMENT_MANAGER (data));
        g_assert (ANJUTA_IS_DOCUMENT (widget));	

        docman = ANJUTA_DOCUMENT_MANAGER (data);
        document = ANJUTA_DOCUMENT (widget);

        length = anjuta_document_manager_num_docs (docman);
        for (i=0; i < length; i++) {
                if ((gpointer)document
                     == g_list_nth_data(docman->documents, i)) {
                        docman->documents =
                                g_list_remove (docman->documents, document);
                        break;
                }
        }

	if (anjuta_document_manager_num_docs (docman) == 0) {
		set_current_document (docman, NULL);
	}
}

static void
set_current_document (AnjutaDocumentManager *docman, AnjutaDocument *doc)
{
	BonoboControlFrame *frame;
	
	if (docman->current_document) {
		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (docman->current_document->bonobo_widget));
		bonobo_control_frame_control_deactivate (frame);
	}

	docman->current_document = doc;

	if (doc) {
		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (doc->bonobo_widget));
		bonobo_control_frame_control_activate (frame);
	}
	
	gtk_signal_emit (GTK_OBJECT (docman), 
			 docman_signals[CURRENT_DOCUMENT_CHANGED],
			 doc);

}

