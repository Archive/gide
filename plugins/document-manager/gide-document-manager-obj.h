/* Anjuta
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef ANJUTA_DOCUMENT_MANAGER_OBJ_H
#define ANJUTA_DOCUMENT_MANAGER_OBJ_H

#include <bonobo.h>

BEGIN_GNOME_DECLS

typedef struct _AnjutaDocumentManagerObj      AnjutaDocumentManagerObj;
typedef struct _AnjutaDocumentManagerObjClass AnjutaDocumentManagerObjClass;
typedef struct _AnjutaDocumentManagerObjPriv  AnjutaDocumentManagerObjPriv;

struct _AnjutaDocumentManager;

#define ANJUTA_DOCUMENT_MANAGER_OBJ_TYPE        (anjuta_document_manager_obj_get_type ())
#define ANJUTA_DOCUMENT_MANAGER_OBJ(o)          (GTK_CHECK_CAST ((o), ANJUTA_DOCUMENT_MANAGER_OBJ_TYPE, AnjutaDocumentManagerObj))
#define ANJUTA_DOCUMENT_MANAGER_OBJ_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_DOCUMENT_MANAGER_OBJ_TYPE, AnjutaDocumentManagerObjClass))
#define ANJUTA_IS_DOCUMENT_MANAGER_OBJ(o)       (GTK_CHECK_TYPE ((o), ANJUTA_DOCUMENT_MANAGER_OBJ_TYPE))
#define ANJUTA_IS_DOCUMENT_MANAGER_OBJ_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_DOCUMENT_MANAGER_OBJ_TYPE))

struct _AnjutaDocumentManagerObj {
	BonoboObject parent;
	
	struct _AnjutaDocumentManager *dm;

	AnjutaDocumentManagerObjPriv *priv;
};

struct _AnjutaDocumentManagerObjClass {
	BonoboObjectClass parent_class;
};
GtkType                 anjuta_document_manager_obj_get_type (void);
AnjutaDocumentManagerObj *anjuta_document_manager_obj_new      (struct _AnjutaDocumentManager *dm);

END_GNOME_DECLS
#endif
