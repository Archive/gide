/* $Header$ */
/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gide.h"
#include "protogen.h"
#include "parser.h"
#include "gI_common.h"

/* global variables */
static GtkWidget*			tools_window = NULL;
static GtkWidget*			cb_append;
static GtkWidget*			e_source;
static GtkWidget*			e_target;
static GtkWidget*			cb_static;
static gchar*				org_line = NULL;
static FILE*				infile;
static FILE*				outfile;

/* local prototypes */
static glong tools_check_entries(void);
static glong tools_parse_prototype(gchar* buf, glong line);
static glong tools_gen_proto_start(void);
static void tools_proto_clicked(GnomeDialog* dlg, int button, gpointer data);

/*
 * Verify: checks the entries of the Prototype Generator Dialog.
 */
static glong
tools_check_entries(
	void
)
{
	gchar*				ptr;

	ptr = gtk_entry_get_text(GTK_ENTRY(e_source));
	if(ptr)
	{
		if(isempty(ptr))
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	ptr = gtk_entry_get_text(GTK_ENTRY(e_target));
	if(ptr)
	{
		if(isempty(ptr))
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	return 1;
}

static glong
tools_parse_prototype(
	gchar*				buf,
	glong				line
)
{
	gchar*				ptr;
	gchar				cmp[MAXLEN];
	glong				i;
	static c_status			c_status;
	static glong			waiting = 0;
	static glong			inside_comment = 0;
	static glong			inside_declaration = 0;

	/* Backup the complete original line */
	if(!waiting)
	{
		g_free(org_line);
		org_line = g_strdup(buf);
	}
	else
	{
		gchar*			str;
		str = g_strconcat(org_line, buf, NULL);
		g_free(org_line);
		org_line = str;
	}

	for(i = 0; i < strlen(buf); i++)
	{
		c_parse_special(buf, i, &c_status);

		/* Needed to skip leading comment */
		if(c_status.comment)
		{
			inside_comment = 1;
		}

		if(c_status.comment || c_status.hyph || c_status.dhyph)
		{
			continue;
		}

		if(inside_declaration && c_status.curly_brackets)
		{
			continue;
		}
		if(inside_declaration && !c_status.curly_brackets)
		{
			inside_declaration = 0;
			g_free(org_line);
			org_line = g_strdup("");
			continue;
		}

		/* Curly brackets means the functions is about to start, so
		we have the entire header */
		if(c_status.curly_brackets && waiting)
		{
			waiting = 0;
			ptr = strchr(org_line, ')');
			if(!ptr)
			{
				/* It's not a function after all (probably a
				   variable declaration) */
				inside_declaration = 1;
				waiting = 1;
				continue;
			}

			ptr = strchr(org_line, '{');
			if(!ptr)
			{
				g_print("ERROR: something is going wrong here"
					"....aborting!\n");
				g_assert_not_reached();
			}

			ptr--;

			while(*ptr != ')')
			{
				ptr--;
			}

			ptr++;
			*ptr = ';';
			ptr++;
			*ptr = '\0';
			if(!GTK_TOGGLE_BUTTON(cb_static)->active)
			{
				fprintf(outfile, "%s\n", org_line);
			}
			else
			{
				if(sscanf(org_line, "%s\n", cmp) != 1)
				{
					fprintf(outfile, "%s\n", org_line);
				}
				else
				{
					if(strcmp(cmp, "static"))
					{
						fprintf(outfile, "%s\n",
							org_line);
					}
				}
			}
			continue;
		}

		if(buf[i] == ';' && waiting)
		{
			waiting = 0;
		}
	}

	/* Strip leading spaces, # lines and comments */
	g_strchug(org_line);
	if(org_line[0] == '#')
	{
		g_free(org_line);
		org_line = g_strdup("");
	}
	if(inside_comment && !c_status.comment)
	{
		inside_comment = 0;
		g_free(org_line);
		org_line = g_strdup("");
	}

	/* Start looking for a function definition */
	if(!waiting && !c_status.comment && !c_status.curly_brackets)
	{
		g_free(org_line);
		org_line = g_strdup("");
		waiting = 1;
	}

	return(1);
}


/*
 * Callback: OK-Button in Prototype Generator
 */
static glong
tools_gen_proto_start(
	void
)
{
	gchar*				ptr;
	gchar*				target;
	glong				i;
	gchar				buf[MAXLEN];
	glong				written;
	glong				line = 0;
	static c_status			c_status;
	gchar*				helpstring = NULL;

	if(!tools_check_entries())
	{
		return(-1);
	}

	ptr = gtk_editable_get_chars(GTK_EDITABLE(e_source), 0, -1);

	infile = fopen( ptr, "r" );
	if( infile == NULL )
	{
		gchar*			msg;
		msg = g_strdup_printf("Can't open source file %s", ptr);
		anjuta_error_dialog(msg);
		g_free(msg);
		g_free(ptr);
		return(-1);
	}
	g_free(ptr);

	init_cstatus(&c_status);

	while(!feof(infile))
	{
		fgets(buf, sizeof(buf), infile);
		if(feof(infile))
		{
			break;
		}
		for(i = 0; i <= strlen(buf); i++)
		{
			c_parse_special(buf, i, &c_status);
		}
	}
/*
	if(!config.ignore)
	{
		if(c_status.comment)
		{
			printf("\nERROR: base parsing failed, unfinished"
				"comment\n");
			return(-1);
		}

		if(c_status.curly_brackets || c_status.round_brackets ||
			c_status.square_brackets)
		{
			printf("\nERROR: base parsing failed, numbers don't"
				" match: %ld, %ld, %ld\n",
				c_status.curly_brackets,
				c_status.round_brackets,
				c_status.square_brackets);
			return(-1);
		}
	}
	else
	{
		if(c_status.comment || c_status.curly_brackets ||
			c_status.round_brackets || c_status.square_brackets)
		{
			printf("ignoring detected errors, continuation"
				" forced...");
		}
	}
*/

	target = gtk_editable_get_chars(GTK_EDITABLE(e_target), 0, -1);

	if(GTK_TOGGLE_BUTTON(cb_append)->active)
	{
		outfile = fopen(target, "a");
	}
	else
	{
		outfile = fopen(target, "w");
	}

	if(outfile == NULL)
	{
		gchar*			msg;
		msg = g_strdup_printf("Can't open destination file %s", target);
		anjuta_error_dialog(msg);
		g_free(msg);
		g_free(target);
		return(-1);
	}

	/* Re-Init */
	rewind(infile);
	init_cstatus(&c_status);

	ptr = g_basename(target);

	fprintf(outfile, "\n/*\n * Prototypes for '%s'\n */\n", ptr);

	/* yuck */
	org_line = g_strdup("");

	while(!feof(infile))
	{
		fgets(buf, sizeof(buf), infile);
		line++;
		if(feof(infile))
		{
			break;
		}
		tools_parse_prototype(buf, line);
	}

	/* yuck */
	g_free(org_line);

	fclose(infile);
	written = ftell(outfile);
	fclose(outfile);

	helpstring = g_strdup_printf(
		"\n    Done!    \n    %ld bytes written to '%s'.    \n",
		written, target);
	anjuta_ok_dialog(helpstring);
	g_free(target);
	g_free(helpstring);

	return 1;
}


/*
 * Button callback for prototype generator
 */
static void
tools_proto_clicked(
	GnomeDialog*			dlg,
	int				button,
	gpointer			data
)
{
	switch(button)
	{
	case 0:
		tools_gen_proto_start();
		break;
	case 1:
		gnome_dialog_close(dlg);
		tools_window = NULL;
		break;
	}
}


/*
 * Dialog: Prototype Generator
 */
void
gen_proto(
	void
)
{
	GtkWidget*			hbox;
	GtkBox*				vbox;

	if(tools_window)
	{
		return;
	}

	tools_window = gnome_dialog_new(_("Tools - Prototype generator"),
		_("Generate"),
		GNOME_STOCK_BUTTON_CLOSE,
		NULL);
	gnome_dialog_set_parent(GNOME_DIALOG(tools_window),
		GTK_WINDOW(main_window));

	vbox = GTK_BOX(GNOME_DIALOG(tools_window)->vbox);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(vbox, hbox, FALSE, TRUE, 10);

	gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("C Source")), FALSE,
		TRUE, 5);

	e_source = gtk_entry_new_with_max_length(255);
	gtk_box_pack_start(GTK_BOX(hbox), e_source, TRUE, TRUE, 5);
	gtk_widget_grab_focus(e_source);
	gnome_dialog_editable_enters(GNOME_DIALOG(tools_window),
		GTK_EDITABLE(e_source));

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(vbox, hbox, FALSE, TRUE, 10);

	gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("Target File")),
		FALSE, TRUE, 5);

	e_target = gtk_entry_new_with_max_length(255);
	gtk_box_pack_start(GTK_BOX(hbox), e_target, TRUE, TRUE, 5);
	gnome_dialog_editable_enters(GNOME_DIALOG(tools_window),
		GTK_EDITABLE(e_target));

	cb_append = gtk_check_button_new_with_label(_("Append to Targetfile"));
	gtk_box_pack_start(vbox, cb_append, FALSE, TRUE, 5);

	cb_static = gtk_check_button_new_with_label(_("Skip Static Declarations"));
	gtk_box_pack_start(vbox, cb_static, FALSE, TRUE, 5);

	gtk_widget_show_all(GTK_WIDGET(vbox));

	gnome_dialog_set_default(GNOME_DIALOG(tools_window), 0);

	gtk_signal_connect(GTK_OBJECT(tools_window), "clicked",
		GTK_SIGNAL_FUNC(tools_proto_clicked), NULL);
	gtk_widget_show(tools_window);
}
