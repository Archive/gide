/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gide.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "gI_common.h"
#include "files.h"
#include "gI_window.h"

/* globals */
static GtkWidget*			files_window = NULL;
static GtkWidget*			clist = NULL;
static glong				files;

/* local prototypes */
static void files_window_destroy(GtkWidget* widget, gpointer data);
static void select_file(GtkWidget* widget, gint row, gint column,
	GdkEventButton* bevent);
static void insert_files(GideDocument* document, gpointer data);


static void
files_window_destroy(
	GtkWidget*			widget,
	gpointer			data
)
{
	gtk_widget_destroy(files_window);
	files_window = NULL;
}


static void
select_file(
	GtkWidget*			widget,
	gint				row,
	gint				column,
	GdkEventButton*			bevent
)
{
	gchar*				no;

	if(bevent->type == GDK_2BUTTON_PRESS)
	{
		gtk_clist_get_text(GTK_CLIST(clist), row, 0, &no);
		gtk_notebook_set_page(GTK_NOTEBOOK(main_window->notebook),
			atoi(no) - 1);
	}
}


static void
insert_files(
	GideDocument*			document,
	gpointer			data
)
{
	gchar*				insrow[2];
	gchar				no_str[16];
	gchar*				filename;

	if(!document)
	{
		return;
	}

	files++;

	filename = gI_document_get_filename(document);
	if(filename != NULL && !isempty(filename))
	{
		sprintf(no_str, "%ld", files);
		insrow[0] = no_str;
		insrow[1] = filename;

		gtk_clist_append(GTK_CLIST(clist), insrow);
	}
}


void
show_all_files(
	void
)
{
	GtkWidget*			vbox;
	GtkWidget*			scrolled_win;
	gchar*				list_titles[2] = { "No.", "File" };

	if(files_window)
	{
		return;
	}

	files_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(files_window), N_("Files"));
	gtk_widget_set_usize(files_window, 400, 250);
	gtk_signal_connect(GTK_OBJECT(files_window), "destroy",
		GTK_SIGNAL_FUNC(files_window_destroy), NULL);

	scrolled_win = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(files_window), scrolled_win);
	gtk_widget_show(scrolled_win);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win), 
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	clist = gtk_clist_new_with_titles(2, list_titles);
	gtk_clist_column_titles_passive(GTK_CLIST(clist));
	gtk_clist_set_column_width(GTK_CLIST(clist), 0, 25);
	gtk_container_add(GTK_CONTAINER(scrolled_win), clist);
	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
		GTK_SIGNAL_FUNC(select_file), NULL);
	gtk_widget_show(clist);


	/* init */
	files = 0;

	/* insert */
	g_list_foreach(main_window->documents, (GFunc)insert_files, NULL);

	gtk_widget_show(files_window);
}
