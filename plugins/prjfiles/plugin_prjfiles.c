/*
 * Filelist plugin
 *
 * Authors:
 *   Steffen Kern (alfi@pn.org)
 *   Dirk Vangestel (dirk.vangestel@advalvas.be)
 */

#include "../../src/gide.h"
#include "../../src/gI_plugin.h"
#include "../../src/gI_tools.h"
#include "../../src/gI_window.h"
#include "../../src/gide-tools.h"
#include "files.h"

#define PRJFILES_NAME			_("Windows - Files")
#define PRJFILES_TITLE			_("Project files Plugin")
#define PRJFILES_DESCR			_("Display a list of open files.")

static void
plugin_prjfiles(
	Tool*				tool,
	ToolState*			state
)
{
	show_all_files();
}

static gboolean
plugin_prjfiles_sens(
	Tool*				tool,
	ToolState*			state
)
{
	return TRUE;
}

static int
can_unload(
	PluginData*			pd
)
{
	return 1;
}

static void
cleanup_plugin(
	PluginData*			pd
)
{
	anjuta_tool_remove(PRJFILES_NAME);
}

PluginInitResult
init_plugin(
	CommandContext*			context,
	PluginData*			pd
)
{
	GtkObject*			tool;

	if(plugin_version_mismatch (context, pd, VERSION))
	{
		return PLUGIN_QUIET_ERROR;
	}

	tool = gI_tool_new(PRJFILES_NAME, (void*)plugin_prjfiles);
	gI_tool_set_menu_data(TOOL(tool), plugin_prjfiles_sens,
		"Windows/Files", NULL);
	anjuta_tool_add(TOOL(tool));

	if(plugin_data_init(pd, can_unload, cleanup_plugin,
		PRJFILES_TITLE, PRJFILES_DESCR))
	{
		return PLUGIN_OK;
	}
	else
	{
		return PLUGIN_ERROR;
	}
}
