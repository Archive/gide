/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#ifndef PLUGIN_TEXT_DELETE_H
#define PLUGIN_TEXT_DELETE_H

void text_delete_to_bof(GtkWidget* widget, gpointer data);
void text_delete_to_eof(GtkWidget* widget, gpointer data);
void text_delete_to_bol(GtkWidget* widget, gpointer data);
void text_delete_to_eol(GtkWidget* widget, gpointer data);

#endif




