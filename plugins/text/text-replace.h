/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#ifndef PLUGIN_TEXT_REPLACE_H
#define PLUGIN_TEXT_REPLACE_H

void text_replace_tab_spaces(GtkWidget* widget, gpointer data);

#endif




