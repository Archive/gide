/*
 * Anjuta text plugin
 *
 * Contains all text based plugins:
 *  - insert date and time
 *  - insert GPL header (C and C++)
 *  - insert file
 *  - delete BOF, EOF, BOL, EOL
 *  - replace tabs with spaces
 */

#include <config.h>

#include <libgide/libgide.h>

#include "text-insert.h"
#include "text-delete.h"
#include "text-replace.h"

#define TEXT_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:text"
#define PLUGIN_NAME			"anjuta-text-plugin"
#define PLUGIN_XML			"gide-text-plugin.xml"


/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("TextInsertDateTime", text_insert_date),
	BONOBO_UI_UNSAFE_VERB("TextInsertGplC", text_insert_gpl_c),
	BONOBO_UI_UNSAFE_VERB("TextInsertGplCpp", text_insert_gpl_cpp),
	BONOBO_UI_UNSAFE_VERB("TextInsertFile", text_insert_file),
	BONOBO_UI_UNSAFE_VERB("TextDeleteBOF", text_delete_to_bof),
	BONOBO_UI_UNSAFE_VERB("TextDeleteEOF", text_delete_to_eof),
	BONOBO_UI_UNSAFE_VERB("TextDeleteBOL", text_delete_to_bol),
	BONOBO_UI_UNSAFE_VERB("TextDeleteEOL", text_delete_to_eol),
	BONOBO_UI_UNSAFE_VERB("TextReplaceTabs", text_replace_tab_spaces),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (TEXT_COMPONENT_IID, "Anjuta Text Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
