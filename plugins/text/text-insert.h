/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#ifndef PLUGIN_TEXT_INSERT_H
#define PLUGIN_TEXT_INSERT_H

void text_insert_date(GtkWidget* widget, gpointer data);
void text_insert_gpl_c(GtkWidget* widget, gpointer data);
void text_insert_gpl_cpp(GtkWidget* widget, gpointer data);
void text_insert_file(GtkWidget* widget, gpointer data);

#endif
