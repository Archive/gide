/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#include <libgide/libgide.h>
#include "text-delete.h"

void
text_delete_to_bof(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	anjuta_delete_text(tool, 0, anjuta_get_cursor_pos(tool));
}

void
text_delete_to_eof(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	anjuta_delete_text(tool, anjuta_get_cursor_pos(tool), anjuta_get_document_length(tool));
}

static glong
get_end_of_line(
	AnjutaTool*			tool,
	glong				point
)
{
	static glong			eol;

	for(eol = point; eol < anjuta_get_document_length(tool); eol++)
	{
		if(anjuta_get_document_chars(tool, eol, eol + 1 )[0] == '\n' )
		{
			break;
		}
	}

	if(eol < 0)
	{
		eol = 0;
	}
	else if(eol > anjuta_get_document_length(tool))
	{
		eol = anjuta_get_document_length(tool);
	}

	return eol;
}

static glong
get_begin_of_line(
	AnjutaTool*			tool,
	glong				point
)
{
	static glong			bol;
	gchar*				ch;

	for(bol = point; bol > 0; bol--)
	{
		ch = anjuta_get_document_chars(tool, bol, bol - 1); 
		if(ch[0] == '\n')
		{
			break;
		}
	}

	if(bol < 0)
	{
		bol = 0;
	}
	else if(bol > anjuta_get_document_length(tool))
	{
		bol = anjuta_get_document_length(tool);
	}

	return bol;
}

void
text_delete_to_bol(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	anjuta_delete_text(tool, anjuta_get_cursor_pos(tool), get_begin_of_line(tool,
		anjuta_get_cursor_pos(tool)));
}

void
text_delete_to_eol(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	anjuta_delete_text(tool, anjuta_get_cursor_pos(tool), get_end_of_line(tool,
		anjuta_get_cursor_pos(tool)));
}
