/*
 * Text manipulation plugin
 *
 * Author:
 *   JP Rosevear (jpr@arcavia.com)
 */

#include <libgide/libgide.h>

#include "text-replace.h"

static void
_edit_repl_tab_spaces(
	gchar*				str,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	gint				i;
	gchar*				c;
	glong				tabsize;
	gint				column = 1;
	gchar*				filler;
	gint				numspaces;

	if(!str || isempty(str))
	{
		return;
	}

	tabsize = atol(str);

	for(i = 1; i <= anjuta_get_document_length(tool); i++)
	{
		c = anjuta_get_document_chars(tool, i - 1, i);
		if(*c == '\n')
		{
			column = 1;
		}
		else if(*c == '\t')
		{
			anjuta_delete_text(tool, i - 1, i);
			numspaces = ((column + tabsize - 1) / tabsize) *
				tabsize + 1 - column;

			filler = g_strnfill(numspaces, ' ');
			anjuta_insert_text_at_pos(tool, i - 1, filler);
			g_free(filler);

			i += numspaces - 1;
			column += numspaces;
		}
		else
		{
			column++;
		}
		g_free(c);
	}
}

void
text_replace_tab_spaces(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	GtkWidget*			dlg;
	gint				spaces;
	gchar*				spacetext;

#if 0
	spaces = gI_text_default_tab_width(state->document);
#else
	spaces = 8;
#endif

	spacetext = g_strdup_printf("%d", spaces);
	dlg = gnome_request_dialog(FALSE, _("Number of spaces per TAB?"),
		spacetext, 3, _edit_repl_tab_spaces, tool, NULL);
	g_free(spacetext);

	gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}
