/*
 * remove plugin
 *
 * Author:
 *   Dirk Vangestel (dirk.vangestel@advalvas.be)
 */

#include "../../src/gide.h"
#include "../../src/gI_plugin.h"
#include "../../src/gI_tools.h"
#include "../../src/gide-tools.h"
#include "../../src/gI_common.h"
#include "../../src/gI_document.h"
#include <errno.h>

#define REMOVE_NAME  _("remove")
#define REMOVE_TITLE _("remove Plugin")
#define REMOVE_DESCR _("Delete active file")

static void
remove_file(
	Tool*				tool,
	ToolState*			state
)
{
	GideDocument*			doc;
	gchar*				filename;
	gchar				errmsg[200];

	doc = state->document;
	filename = gI_document_get_filename(doc);
	if(!doc || !filename)
	{
		return;
	}

	if(remove(filename) < 0)
	{
		g_snprintf(errmsg, sizeof(errmsg), _("File error: %s"),
			strerror(errno));
		anjuta_error_dialog(errmsg);
		return;
	}

	gI_document_check_changed(doc);
}

static gboolean
remove_sens(
	Tool*				tool,
	ToolState*			state
)
{
	if(state->document)
	{
		return TRUE;
	}

	return FALSE;
}

static int
can_unload(
	PluginData*			pd
)
{
	return 1;
}

static void
cleanup_plugin(
	PluginData*			pd
)
{
	anjuta_tool_remove(REMOVE_NAME);
}

PluginInitResult
init_plugin(
	CommandContext*			context,
	PluginData*			pd
)
{
	GtkObject*			tool;

	if(plugin_version_mismatch (context, pd, VERSION))
	{
		return PLUGIN_QUIET_ERROR;
	}

	tool = gI_tool_new(REMOVE_NAME, (void*)remove_file);
	gI_tool_set_menu_data(TOOL(tool), remove_sens,
		NULL, "special/Delete active file");
	anjuta_tool_add(TOOL(tool));

	if(plugin_data_init(pd, can_unload, cleanup_plugin,
		REMOVE_TITLE, REMOVE_DESCR))
	{
		return PLUGIN_OK;
	}
	else
	{
		return PLUGIN_ERROR;
	}
}
