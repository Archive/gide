/*
 * Anjuta swapch plugin
 *
 * Switches besteen .c and .h file
 */

#include <config.h>

#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>

#define SWAPCH_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:swapch"
#define PLUGIN_NAME			"anjuta-swapch-plugin"
#define PLUGIN_XML			"gide-swapch-plugin.xml"

static void
swapch(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	size_t				len;
	gchar*				newfname;
	gchar*				filename;

	filename = anjuta_get_current_filename(tool);
	if(!filename)
	{
		return;
	}

	len = strlen(filename) - 1;
	while(len)
	{
		if(filename[len] == '.')
		{
			break;
		}

		--len;
	}
	if(!len)
	{
		return;
	}

	if(strcasecmp(&filename[len], ".h") == 0)
	{
		len++;
		newfname = g_malloc(len + 4);
		strcpy(newfname, filename);
		strcpy(&newfname[len], "cc");
		if(!file_check_if_exist(newfname, FALSE))
		{
			strcpy(&newfname[len], "cpp");
			if(!file_check_if_exist(newfname, FALSE))
			{
				newfname[len + 1] = 0;
				if(!file_check_if_exist(newfname, TRUE))
				{
					g_free(newfname);
					return;
				}
			}
		}
	}
	else if(strncasecmp(&filename[len], ".c", 2) == 0)
	{
		len++;
		newfname = g_strdup(filename);
		newfname[len] = 'h';
		newfname[len + 1] = 0;
		if(!file_check_if_exist(newfname, TRUE))
		{
			g_free(newfname);
			return;
		}
	}
	else
	{
		return;
	}

	anjuta_show_file(tool, newfname);
	g_free(newfname);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("SwapCH", swapch),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (SWAPCH_COMPONENT_IID, "Anjuta Swap C/H Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
