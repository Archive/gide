/*
 * Anjuta docstat plugin
 *
 * Shows statistics about the current document
 */

#include <config.h>

#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>

#define DOCSTAT_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:docstat"
#define PLUGIN_NAME			"anjuta-docstat-plugin"
#define PLUGIN_XML			"gide-docstat-plugin.xml"

static void
docstat(
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;
	gchar				tmp[255];
	gchar*				c;
	gint				in_word = 0;
	glong				words = 0;
	glong				chars = 0;
	glong				chars_ns = 0;
	GtkWidget*			dlg;
	GtkWidget*			label;
	glong				i;
	glong				length;

	length = anjuta_get_document_length(tool);

	/* go thru document... */
	for(i = 1; i <= length; i++)
	{
		c = anjuta_get_document_chars(tool, i - 1, i);

		/* Character/Word Counters */
		chars++;
		if(!isspace(*c))
		{
			chars_ns++;

			if(*c == ',' || *c == ';' || *c == ':' || *c == '.')
			{
				if(in_word)
				{
					in_word = 0;
				}
			}
			else
			{
				if(!in_word)
				{
					in_word = 1;
					words++;
				}
			}
		}
		else
		{
			if(in_word)
			{
				in_word = 0;
			}
		}

		g_free(c);
	}

	/* Dialog */
	dlg = gnome_dialog_new("Tools - Word Count", GNOME_STOCK_BUTTON_OK,
		NULL);

	g_snprintf(tmp, 255, _("Characters: %ld"), chars);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	g_snprintf(tmp, 255, _("Non-Space Characters: %ld"), chars_ns);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	g_snprintf(tmp, 255, _("Words: %ld"), words);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("DocStat", docstat),
	BONOBO_UI_VERB_END
};



ANJUTA_SHLIB_TOOL_SIMPLE (DOCSTAT_COMPONENT_IID, "Anjuta Docstat Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
