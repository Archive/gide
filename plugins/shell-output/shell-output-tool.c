/* Anjuta
 * 
 * Copyright (C) 2001 Roberto Majadas "telemaco" <phoenix@nova.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */


/*
 * Anjuta Shell Output plugin
 *
 * With this plugin yo can run command like in a shell .
 * An you can use pipes and pipe gide's documents .
 * 
 */

#include <config.h>

#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <bonobo.h>
#include <bonobo/bonobo-stream-memory.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <gdl/GDL.h>
#include <gdl/gdl.h>

#define SHELL_OUTPUT_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:shelloutput"

#define SHELL_OUTPUT_BUFFER_SIZE 1024
#define SHELL_OUTPUT_SH_NAME "sh"
#define SHELL_OUTPUT_SH_PATH "/bin/sh"

typedef struct _shell_output_dialog shell_output_dialog ;
typedef struct _shell_output_info shell_output_info ;

struct _shell_output_dialog {	
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *entry;
	GtkWidget *file_entry1;
	GtkWidget *file_entry2;
	GtkWidget *optionmenu;
	GtkWidget *optionmenu_menu;	
};

struct _shell_output_info {
	GNOME_Development_Environment_DocumentManager_DocumentList *doclist ;
	GNOME_Development_Environment_Document doc ;
	gchar *work_directory;
	gchar *temp_file_name;	
};

static shell_output_dialog *sod ;
static shell_output_info *soi ;
static AnjutaTool* shell_output_tool ;


/* Shell Output prototipes */

static void shell_output (GtkWidget *widget, gpointer data);
static void shell_output_add_dialog_options ();
static void shell_output_create_dialog_table ();
static void shell_output_create_dialog ();
static void shell_output_activate_option (GtkWidget *widget, gpointer data);
static void shell_output_close_dialog (GtkWidget *widget, gpointer data);
static void shell_output_popen (GString *buffer_to_pipe, gchar *command);
GString * shell_output_data_for_pipe_by_editorbuffer ();
GString * shell_output_data_for_pipe_by_persist_stream ();
GString * shell_output_data_for_pipe () ;
static void shell_output_command_run (GtkWidget *widget, gpointer data);
static void shell_output_open_output_file (gchar *file);
gboolean command_out_cb (GIOChannel *chan, GIOCondition cond, gpointer data);


static void
shell_output_add_dialog_options ()
{
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document var_doc ;
	CORBA_Environment ev;
	GtkWidget *optionmenu_menu_item ;
	gchar *moniker;
	gchar *cat_option;
	gchar *filename;
	gint i ;

	soi->doc = NULL ; /* inicialization */
		
	optionmenu_menu_item = gtk_menu_item_new_with_label (_("run command"));
	gtk_widget_show (optionmenu_menu_item);
	gtk_menu_append (GTK_MENU (sod->optionmenu_menu), optionmenu_menu_item);
	gtk_signal_connect (GTK_OBJECT (optionmenu_menu_item), "activate",
					GTK_SIGNAL_FUNC (shell_output_activate_option),NULL);	

	CORBA_exception_init (&ev);
	moniker = g_strdup_printf ("anjuta:%s!DocumentManager", shell_output_tool->shell_id);
	docman = bonobo_get_object (moniker,
						   "IDL:GNOME/Development/Environment/DocumentManager:1.0",
						   &ev);
	g_free (moniker);
	if (ev._major != CORBA_NO_EXCEPTION && !CORBA_Object_is_nil (soi->doc, &ev))
		return ;
   
	soi->doclist = GNOME_Development_Environment_DocumentManager_getOpenFiles (docman,&ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		return ;

	for (i=0 ; i< soi->doclist->_length ; i++){		
		var_doc = soi->doclist->_buffer[i] ;
		filename = g_strdup (GNOME_Development_Environment_Document_getFilename (var_doc,&ev));
		if (ev._major != CORBA_NO_EXCEPTION)
			return ;				
		cat_option = g_strdup_printf ("cat %s |", g_basename(filename));
	    	optionmenu_menu_item = gtk_menu_item_new_with_label (cat_option);
		gtk_widget_show (optionmenu_menu_item);
		gtk_menu_append (GTK_MENU (sod->optionmenu_menu), optionmenu_menu_item);
		gtk_signal_connect (GTK_OBJECT (optionmenu_menu_item), "activate",
						GTK_SIGNAL_FUNC (shell_output_activate_option),
						soi->doclist->_buffer[i]);
		g_free(filename);
		g_free(cat_option);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (sod->optionmenu), sod->optionmenu_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU(sod->optionmenu), 0);

	bonobo_object_release_unref(docman, &ev);

	CORBA_exception_free(&ev);		
}


static void
shell_output_create_dialog_table ()
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *combo_entry;
		
	table = gtk_table_new (3, 2, FALSE);
	gtk_widget_show (table);	
	gtk_box_pack_start (GTK_BOX (sod->vbox), table, TRUE, TRUE, 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);
	label = gtk_label_new (_("Work directory"));
	gtk_widget_show (label);	
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	label = gtk_label_new (_("Temporal output file"));
	gtk_widget_show (label);	
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);	
	sod->optionmenu = gtk_option_menu_new ();
	gtk_widget_show (sod->optionmenu);	
	gtk_table_attach (GTK_TABLE (table), sod->optionmenu, 0, 1, 0, 1,
				   (GtkAttachOptions) (GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	sod->optionmenu_menu = gtk_menu_new (); 
	shell_output_add_dialog_options ();
	sod->entry = gtk_entry_new ();
	gtk_widget_show (sod->entry);
	gtk_table_attach (GTK_TABLE (table), sod->entry, 1, 2, 0, 1,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	sod->file_entry1 = gnome_file_entry_new (NULL, NULL);
	gtk_widget_show (sod->file_entry1);
	gtk_table_attach (GTK_TABLE (table), sod->file_entry1, 1, 2, 1, 2,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gnome_file_entry_set_directory (GNOME_FILE_ENTRY (sod->file_entry1), TRUE);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry1),
								g_get_home_dir ());
	combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (sod->file_entry1));
	gtk_widget_show (combo_entry);
	gtk_entry_set_text (GTK_ENTRY(combo_entry), g_get_home_dir ()); 
	sod->file_entry2 = gnome_file_entry_new (NULL, NULL);
	gtk_widget_show (sod->file_entry2);
	gtk_table_attach (GTK_TABLE (table), sod->file_entry2, 1, 2, 2, 3,
				   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
				   (GtkAttachOptions) (0), 0, 0);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry2),
								g_get_tmp_dir ());
	combo_entry = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (sod->file_entry2));
	gtk_widget_show (combo_entry);
	gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (sod->file_entry2),
								g_get_tmp_dir ());
	gtk_entry_set_text (GTK_ENTRY(combo_entry), tempnam (g_get_tmp_dir (), "gide-"));
}

static void
shell_output_create_dialog ()
{
	GtkWidget *label;
	GtkWidget *dialog_vbox;
	GtkWidget *frame;
	
	sod->dialog = gnome_dialog_new (_("Shell output"), GNOME_STOCK_BUTTON_OK,
							  GNOME_STOCK_BUTTON_CANCEL, NULL);
	gtk_window_set_policy (GTK_WINDOW (sod->dialog), FALSE, FALSE, FALSE);  	
	dialog_vbox = GNOME_DIALOG (sod->dialog)->vbox;
	gtk_widget_show (dialog_vbox);
	sod->vbox = gtk_vbox_new (FALSE, 5);
	gtk_widget_show (sod->vbox);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), sod->vbox, TRUE, TRUE, 0);
	frame = gtk_frame_new (NULL);
 	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (sod->vbox), frame, FALSE, FALSE, 0);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);
	label = gtk_label_new (_("\nPlease , insert command that you want to run\n Remember , you can pipe documents and commands like in a shell \n"));
	gtk_widget_show (label);	
	gtk_container_add (GTK_CONTAINER (frame), label);
	shell_output_create_dialog_table ();

	gnome_dialog_button_connect (GNOME_DIALOG (sod->dialog), 0,
						  GTK_SIGNAL_FUNC(shell_output_command_run), NULL);
	gnome_dialog_button_connect (GNOME_DIALOG (sod->dialog), 1,
						  GTK_SIGNAL_FUNC (shell_output_close_dialog) , NULL);
	gtk_signal_connect (GTK_OBJECT (sod->dialog), "delete_event",
					GTK_SIGNAL_FUNC  (shell_output_close_dialog), NULL);

	gtk_widget_show (sod->dialog);
}

static void
shell_output_activate_option (GtkWidget *widget, gpointer data)
{	
	soi->doc = data ;
}

gboolean
command_out_cb (GIOChannel *chan, GIOCondition cond, gpointer data)
{
	FILE *temp_file;
	gchar c;
	gint read = 1;
			
	temp_file = fopen (soi->temp_file_name, "w");
	
	while (read > 0){
		g_io_channel_read (chan, &c, 1, &read);		
		if (read > 0)			
			fputc (c, temp_file);				
	}
	fclose (temp_file);
	shell_output_open_output_file (soi->temp_file_name);
	g_io_channel_unref (chan);
	g_io_channel_close (chan);
	CORBA_free (soi->doclist) ;	
	g_free (soi->work_directory);
	g_free (soi->temp_file_name);
	g_free (soi);		

	return TRUE;
}

GString *
shell_output_data_for_pipe_by_editorbuffer ()
{
	GNOME_Development_EditorBuffer	buffer;
	GNOME_Development_EditorBuffer_iobuf   *iobuf;
	CORBA_Environment ev;
	glong length;
	GString *result;

	CORBA_exception_init(&ev);
		
	buffer = GNOME_Development_Environment_Document_getEditorInterface(
			soi->doc, "IDL:GNOME/Development/EditorBuffer:1.0", &ev);
	if(CORBA_Object_is_nil(buffer, &ev)){
		bonobo_object_release_unref (buffer, &ev);		
		CORBA_exception_free (&ev);		
		return NULL;
	}
	
	result = g_string_new (NULL);		
	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);
	if (length == 0)
		return result ;
	GNOME_Development_EditorBuffer_getChars (buffer, 0, length, &iobuf, &ev);
	result->str = g_strndup (iobuf->_buffer, iobuf->_length);
	result->len = iobuf->_length ;	
	CORBA_free (iobuf);
	bonobo_object_release_unref (buffer, &ev);
		
	CORBA_exception_free (&ev);
	return result ;
}

GString *
shell_output_data_for_pipe_by_persist_stream ()
{
	Bonobo_PersistStream buffer_ps ;
	BonoboStream *ret = NULL;
	Bonobo_Stream_iobuf *iobufps;
	CORBA_Environment ev;
	glong length;
	GString *result;

	CORBA_exception_init(&ev);
		
	ret = bonobo_stream_mem_create (NULL, 0, FALSE, TRUE);
	buffer_ps = GNOME_Development_Environment_Document_getEditorInterface(
		soi->doc,"IDL:Bonobo/PersistStream:1.0", &ev);
	
	if (CORBA_Object_is_nil(buffer_ps, &ev)){
		bonobo_object_release_unref (buffer_ps, &ev);
		CORBA_exception_free (&ev);
		return NULL;
	}
	result = g_string_new (NULL);	
	Bonobo_PersistStream_save (buffer_ps,
						  BONOBO_OBJREF (BONOBO_OBJECT (ret)),
						  "text/plain",
						  &ev);
	length = bonobo_stream_client_get_length (BONOBO_OBJREF (BONOBO_OBJECT (ret)),
									  &ev);
	if (length == 0)
		return result;

	Bonobo_Stream_seek ( BONOBO_OBJREF (BONOBO_OBJECT (ret)),
					 0, Bonobo_Stream_SEEK_SET, &ev);
	Bonobo_Stream_read (BONOBO_OBJREF (BONOBO_OBJECT (ret)),
					length, &iobufps, &ev);
	result->str = g_strndup (iobufps->_buffer, iobufps->_length);
	result->len = iobufps->_length;
	CORBA_free (iobufps);
	bonobo_object_release_unref (buffer_ps, &ev);
	CORBA_exception_free(&ev);
	
	return result;			
	
}
GString *
shell_output_data_for_pipe ()
{			

    	GString *result ;
	
	if (soi->doc == NULL){
		result = g_string_new (NULL); 
		return result;
	} else {
		result = shell_output_data_for_pipe_by_editorbuffer ();
		if (result==NULL){ 		
			result = shell_output_data_for_pipe_by_persist_stream();
			if (result==NULL){
				g_print ("gide can't read from component\n");
				return NULL;
			} 
		}
		return result ;
	}
}

static void
shell_output_command_run (GtkWidget *widget, gpointer data)
{
	
	gchar    *command_string = NULL;	
	GString  *buffer_to_pipe = NULL;
		
	command_string = gtk_entry_get_text (GTK_ENTRY (sod->entry));
	soi->work_directory = g_strdup (gnome_file_entry_get_full_path
						  (GNOME_FILE_ENTRY (sod->file_entry1),
						   TRUE));
	soi->temp_file_name = g_strdup (gnome_file_entry_get_full_path
							  (GNOME_FILE_ENTRY (sod->file_entry2),
							   FALSE));
    	if (soi->work_directory == NULL){
		gnome_warning_dialog (_("There aren't any work directory"));
		return;
	}
	
	if (soi->temp_file_name == NULL){
		gnome_warning_dialog (_("There aren't any temporal file"));
		return;
	}
	
	if (command_string == NULL || (strlen (command_string) == 0)){
		gnome_warning_dialog (_("There aren't any command"));
		return;
	}
	buffer_to_pipe = shell_output_data_for_pipe ();
	if (buffer_to_pipe==NULL){
		/* If the return is NULL this mean that the component is not
		   soportated */
		gnome_error_dialog (_("Anjuta can't read from this text-component\n"));		
		gnome_dialog_close (GNOME_DIALOG (sod->dialog));
		g_free (sod);
		CORBA_free (soi->doclist) ;	
		g_free (soi->work_directory);
		g_free (soi->temp_file_name);
		g_free (soi);
		return;
	}
	chdir (soi->work_directory);
	shell_output_popen (buffer_to_pipe, command_string);
	gnome_dialog_close (GNOME_DIALOG (sod->dialog));
	g_free (sod);
}

static void
shell_output_popen (GString *buffer_to_pipe, gchar *command)
{
	FILE     *stream ;
	GIOChannel *chan_out;
	gint     fdpipe [2];
	gint     fdpipe2 [2];
	gchar    *arg[4] ;
	gchar    *command_to_run;
	gint     pid;

	if (pipe (fdpipe) == -1)	
		return;
	if (pipe (fdpipe2) == -1)
		return;
	if (buffer_to_pipe->len > 0)
		command_to_run = g_strdup_printf ("cat | %s", command);
	else
		command_to_run = g_strdup_printf (" %s ", command);
	
	pid = fork();
	if (pid == 0){
		dup2(fdpipe[0], 0);
		close (fdpipe[0]);
		close (fdpipe[1]);
		dup2(fdpipe2[1], 1);
		close (fdpipe2[0]);
		close (fdpipe2[1]);

		arg[0]=SHELL_OUTPUT_SH_NAME;
		arg[1]="-c";
		arg[2]=command_to_run;
		arg[3]=NULL;
		
		execvp (SHELL_OUTPUT_SH_PATH, arg);

		g_warning ("A undetermined PIPE problem occurred");
		_exit (1);
	} else {
		close (fdpipe[0]);
		close (fdpipe2[1]);
		
		chan_out = g_io_channel_unix_new (fdpipe2[0]);
		g_io_add_watch (chan_out, G_IO_IN, command_out_cb, NULL);

		if (buffer_to_pipe->len > 0){
			fcntl (fdpipe[1], F_SETFD, FD_CLOEXEC);
			stream = fdopen (fdpipe[1], "w");
			fprintf (stream, buffer_to_pipe->str);
			fclose (stream);			
		}

		g_string_free ( buffer_to_pipe, TRUE ) ;			
	}
}

static void
shell_output_open_output_file (gchar *file)
{
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document var_doc ;
	CORBA_Environment ev;
	gchar *moniker = NULL ;
	
	CORBA_exception_init (&ev);

	moniker = g_strdup_printf ("anjuta:%s!DocumentManager", shell_output_tool->shell_id);
	docman = bonobo_get_object (moniker,
						   "IDL:GNOME/Development/Environment/DocumentManager:1.0",
						   &ev);
	g_free (moniker);
	var_doc = GNOME_Development_Environment_DocumentManager_openFile (docman, file, &ev);
	bonobo_object_release_unref (docman, &ev);

	CORBA_exception_free (&ev);

}

static void
shell_output_close_dialog (GtkWidget *widget, gpointer data)
{
	gnome_dialog_close (GNOME_DIALOG (sod->dialog));
	g_free (sod);
	g_free (soi);
}

static void
shell_output(
	GtkWidget*			widget,
	gpointer			data
)
{
	shell_output_tool = ANJUTA_TOOL(data);	

	sod = g_new (shell_output_dialog, 1);
	soi = g_new (shell_output_info, 1);
		
	shell_output_create_dialog ();
		
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("ShellOutput", shell_output),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (SHELL_OUTPUT_COMPONENT_IID, 
			  "Anjuta Shell Output Plugin",
			  "anjuta-shelloutput-plugin", 
			  ANJUTA_DATADIR, "gide-shelloutput-plugin.xml",
			  verbs, NULL);
