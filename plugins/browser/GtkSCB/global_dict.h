/* ########################################################################

			      global_dict.h

   File: global_dict.h
   Path: /home/fournigault/c/X11/xcoral-2.31/global_dict.h
   Description: 
   Created: Fri Jan 27 11:25:30 MET 1995
   Author: Bruno Pages
   Modified: Fri Jan 27 11:25:31 MET 1995
   Last maintained by: Bruno Pages

   RCS $Revision$ $State$
   

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Bruno Pages

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef GLOBALDICT_H
#define GLOBALDICT_H

/*------------------------------------------------------------------------------
*/

struct GlobalRec {
	char*								_name;
	FileRec*							_impl_file;
	LineNumber							_impl_line;
	int									_static;
	unsigned							_hide;
	struct GlobalRec*					_next;
};
typedef struct GlobalRec GlobalRec;


GlobalRec* create_global(char* global_name, int staticp, char* parsed_file);
GlobalRec* find_global(char* global);
void global_eraze_file(char* file_name);
void garbage_global(void);
StringTable* get_globals_list(char* prefix);
void show_all_globals(void);
void init_global(void);
StringTable* get_globals_list(char* prefix);

#endif
