/* gtk-helpers */

#include "gtk_helpers.h"
#include "classes.xpm"
#include "functions.xpm"
#include "globals.xpm"
#include "struct_and_classes.xpm"
#include "variables.xpm"


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* GLOBALS */
const gchar*							list_item_data_key = "list_item_data";
GtkWidget*								pixmapwid;
GdkPixmap*								pixmap[10];
GdkBitmap*								mask[10];

GtkWidget*
get_widget(
	GtkWidget*							widget,
	gchar*								widget_name
)
{
	GtkWidget*							parent;
	GtkWidget*							found_widget;

	for(;;)
	{
		if(GTK_IS_MENU(widget))
		{
			parent = gtk_menu_get_attach_widget(GTK_MENU(widget));
		}
		else
		{
			parent = widget->parent;
		}
		if(parent == NULL)
		{
			break;
		}
		widget = parent;
	}

	found_widget = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(widget),
		widget_name);
	if(!found_widget)
	{
		g_warning("Widget not found: %s\n", widget_name);
	}
	return found_widget;
}


/*
 * Load the pixmaps for future use
 */
void
LoadPictures(
	GtkWidget*							parent
)
{
	GtkStyle*							style;

	/* Get the style of the button to get the background color */
	style = gtk_widget_get_style(parent);

	/* Now on to the xpm stuff */
/*
	pixmap[0] = gdk_pixmap_create_from_xpm(parent->window, &mask[0],
		&style->bg[GTK_STATE_NORMAL], "globals.xpm");
	pixmap[1] = gdk_pixmap_create_from_xpm(parent->window, &mask[1],
		&style->bg[GTK_STATE_NORMAL], "classes.xpm");
	pixmap[2] = gdk_pixmap_create_from_xpm(parent->window, &mask[2],
		&style->bg[GTK_STATE_NORMAL], "functions.xpm");
	pixmap[3] = gdk_pixmap_create_from_xpm(parent->window, &mask[3],
		&style->bg[GTK_STATE_NORMAL], "variables.xpm");
	pixmap[4] = gdk_pixmap_create_from_xpm(parent->window, &mask[4],
		&style->bg[GTK_STATE_NORMAL], "struct_and_classes.xpm");
*/
	pixmap[0] = gdk_pixmap_create_from_xpm_d(parent->window, &mask[0],
		&style->bg[GTK_STATE_NORMAL], globals_xpm);
	pixmap[1] = gdk_pixmap_create_from_xpm_d(parent->window, &mask[1],
		&style->bg[GTK_STATE_NORMAL], classes_xpm);
	pixmap[2] = gdk_pixmap_create_from_xpm_d(parent->window, &mask[2],
		&style->bg[GTK_STATE_NORMAL], function_xpm);
	pixmap[3] = gdk_pixmap_create_from_xpm_d(parent->window, &mask[3],
		&style->bg[GTK_STATE_NORMAL], variables_xpm);
	pixmap[4] = gdk_pixmap_create_from_xpm_d(parent->window, &mask[4],
		&style->bg[GTK_STATE_NORMAL], struct_and_classes);
}


/*
 * Create a new hbox with an image and a label packed into it
 * and return the box.
 */
GtkWidget*
xpm_label_box(
	gchar*								label_text,
	int									index
)
{
	GtkWidget*							box1;
	GtkWidget*							label;

	/* Create box for xpm and label */
	box1 = gtk_hbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(box1), 2);

	/* Create a label for the button */
	label = gtk_label_new(label_text);

	pixmapwid = gtk_pixmap_new(pixmap[index], mask[index]); 

	/* Pack the pixmap and label into the box */
	gtk_box_pack_start(GTK_BOX(box1), pixmapwid, FALSE, FALSE, 3);

	gtk_box_pack_start(GTK_BOX(box1), label, FALSE, FALSE, 3);

	gtk_widget_show(pixmapwid);
	gtk_widget_show(label);

	return box1;
}

GtkWidget*
gtk_tree_append_with_pic_and_label(
	GtkWidget*							tree,
	gchar*								label,
	int									index
)
{
	GtkWidget*							box1;
	GtkWidget*							tree_item;

	tree_item=gtk_tree_item_new();

	/* This calls our box creating function */
	box1 = xpm_label_box(label,index);

	/* Pack and show all our widgets */
	gtk_widget_show(box1);

	gtk_container_add(GTK_CONTAINER(tree_item), box1);

	/* Add it to the parent tree */
	gtk_tree_append(GTK_TREE(tree), tree_item);

	gtk_widget_show(tree_item);

	gtk_object_set_data(GTK_OBJECT(tree_item), list_item_data_key, label);

	return tree_item;
}
