/* ########################################################################

			      proc_dict.h

   File: proc_dict.h
   Path: /home/fournigault/c/X11/xcoral-2.31/proc_dict.h
   Description: 
   Created: Fri Jan 27 11:25:30 MET 1995
   Author: Dominique Leveque
   Modified: Fri Jan 27 11:25:31 MET 1995
   Last maintained by: Dominique Leveque

   RCS $Revision$ $State$
   

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef PROCDICT_H
#define PROCDICT_H

enum ProcType { UNKNOWN_PROC, C_PROC, CPLUS_PROC };
typedef enum ProcType ProcType;

struct ProcRec {
	char*								_name;
	FileRec*							_impl_file;
	LineNumber							_impl_line;
	int									_decl;
	unsigned							_hide;
	struct ProcRec*						_next;
};
typedef struct ProcRec ProcRec;

#define STATIC_PROC						1
#define INLINE_PROC						2
#define TEMPLATE_PROC					4

/* routines to manipulate the procedures dictionary */
extern ProcRec* create_proc(char* proc_name, int staticp, char* parsed_file);
extern ProcRec* find_proc(char* proc_name);
void proc_eraze_file(char* file_name);
void garbage_proc(void);
StringTable* get_procs_list(char* prefix);
void show_all_procs(void);

/* initialization routine */
extern void init_proc(void);

#endif
