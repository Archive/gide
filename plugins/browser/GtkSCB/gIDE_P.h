#include <gtk/gtk.h>
#include <sys/param.h>

/*
 * Le Mode
 */
typedef struct _mode {
	char*								name;
	char*								suffixes;
	struct _mode*						next;
	char*								pp_path;
	/* Toggle flag pour le pre processeur */
	int									pp_flag;
	char*								pp_options;
} Mode;

/*
 * Le browser.
 */
typedef struct _Browser {
	/* Larguer hauteur du browser */
	int									width;
	int									height;
	/* Les couleurs */
	unsigned long						fg;
	unsigned long						bg;
	unsigned long						text_fg;
	unsigned long						text_bg;
	/* L'objet selectionne */
	char*								title_name;
	/* La hauteur du titre */
	int									title_height;
	/* la hauteur des boutons */
	int									button_height;
	/* La largeur des boutons */
	int									button_width;
	/* Les couleurs pour le 3D */
	unsigned long						ts;
	unsigned long						bs;
	/* Toggle flag pour les methodes */
	int									dec_imp_flag;
	/* Sauvegarde des divers pointeurs */
	char**								attributes_save;
	char**								procs_save;
	char**								globals_save;
	/* Sauvegarde des divers pointeurs */
	char**								methods_save;
	char**								class_save;
	char**								files_save;
	/* Browser mappe ou non */
	int									stat;
	/* Visibilite */
	int									visible;
	/* Le mode */
	Mode*								mode;
	/* La directorie courante */
	char								dir[MAXPATHLEN];
	int									visit_raise;
	/* Recursif ou pas */
	int									parse_flag;
} Browser;

extern int OpVerbose();

/* the file-selection_window */
extern GtkWidget*						filew;
/* the main-browser-window */
extern GtkWidget*						GTKSCB_main_window;
