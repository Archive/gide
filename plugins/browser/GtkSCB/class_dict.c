/* ########################################################################

			      class_dict.c

   File: class_dict.c
   Path: /home/fournigault/c/X11/xcoral-2.31/class_dict.c
   Description: 
   Created: Fri Jan 27 10:53:29 MET 1995
   Author: Dominique Leveque
   Modified: Fri Jan 27 10:53:30 MET 1995
   Last maintained by: Dominique Leveque

   RCS $Revision$ $State$
   

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */

#include "config.h"

#if HAVE_STRING_H
#include <string.h>
#else
#include <strings.h>
#endif
#include <stdlib.h>

#include "result_types.h"
#include "file_dict.h"
#include "class_dict.h"
#include "browser_util.h"


/*
 * Class dictionary
 */

#define CLASS_DICT_SIZE      503

ClassRec* class_dict[CLASS_DICT_SIZE];

int class_count = 0;

typedef long Bits;

#define LAST_BIT  ((sizeof(Bits) * 8) - 1)

static Bits erazed_bits[(CLASS_DICT_SIZE + LAST_BIT) / (LAST_BIT + 1)];

static int hidden_because_child(ClassRec* current_class);

static int
hidden_because_child(
	ClassRec*							current_class
)
{
	ParentRec*							current_parent;

	current_parent = current_class->_parents_list;

	while(current_parent)
	{
		ClassRec* parent = find_class(current_parent->_name);

		if(parent)
		{
			if((parent->_hide & HideChildrenOf) ||
				hidden_because_child(parent))
			{
				return 1;
			}
		}

		current_parent = current_parent->_next;
	}

	return 0;
}


void
show_all_classes(
	void
)
{
	int									index;

	for(index = 0; index < CLASS_DICT_SIZE; index++)
	{
		ClassRec*						current_class = class_dict[index];

		while(current_class)
		{
			MethodRec* current_method = current_class->_methods_list;
			AttributeRec* current_attr = current_class->_attributes_list;

			SHOW(current_class->_name);
			current_class->_hide = 0;

			while(current_method != Null)
			{
				SHOW(current_method->_name);
				current_method->_hide = 0;
				current_method = current_method->_next;
			}
			while(current_attr)
			{
				SHOW(current_attr->_name);
				current_attr->_hide = 0;
				current_attr = current_attr->_next;
			}
			current_class = current_class->_next;
		}
	}
}


StringTable*
get_classes_list(
	void
)
{
	ClassRec*							current_class;
	StringTable*						result;
	char**								current_entry;
	int									index;
	int									x_size;
	char*								infos;

	x_size = sizeof(char*) * (class_count + 1);
	result = (StringTable*)xmalloc(x_size);
	if(result != Null)
	{
		current_entry = (char**)result;
		for(index = 0; index < CLASS_DICT_SIZE; index++)
		{
			current_class = class_dict[index];
			while(current_class != Null)
			{
				(*current_entry) = current_class->_name;
				infos = (*current_entry) - CLASS_PLENGTH + 1;
				infos[0] = (current_class->_decl_file != Null) ? 'd' : '?';
				if(current_class->_decl & TEMPLATE_TYPE)
				{
					infos[1] = 'T';
				}
				if(hidden_because_child(current_class))
				{
					current_class->_hide |= HidenChildren;
					HIDE(current_class->_name);
				}
				else
				{
					current_class->_hide &= ~HidenChildren;
				}
				if((Hide_Bits & HideInternalTypes) &&
					strchr(current_class->_name, ':'))
				{
					current_class->_hide |= HideInternalTypes;
					HIDE(current_class->_name);
				}
				else
				{
					current_class->_hide &= ~HideInternalTypes;
				}
				if(current_class->_hide & UserHide)
				{
					HIDE(current_class->_name);
				}
				else if(!(current_class->_hide & ~HideChildrenOf))
				{
					SHOW(current_class->_name);
				}

				current_entry++;
				current_class = current_class->_next;
			}
		}
		(*current_entry) = Null;
		qsort(result, class_count, sizeof(char*), sort_util);
	}
	return result;
}


/*
 */
StringTable*
get_sons_list(
	char*								class_name
)
{
	int									index;
	int									marked_count;
	ClassRec*							marked_list;
	ClassRec*							current_class;
	ParentRec*							current_parent;
	StringTable*						result;
	char**								current_entry;
	int									x_size;
	char*								infos;

	marked_list = Null;
	marked_count = 0;
	for(index = 0; index < CLASS_DICT_SIZE; index++)
	{
		current_class = class_dict[index];
		while(current_class != Null)
		{
			current_parent = current_class->_parents_list;
			while(current_parent != Null)
			{
				if(strcmp(current_parent->_name, class_name) == 0)
				{
					current_class->_next_marked = marked_list;
					marked_list = current_class;
					marked_count++;
				}
				current_parent = current_parent->_next;
			}
			current_class = current_class->_next;
		}
	}
	x_size = sizeof(char*) * (marked_count + 1);
	result = (StringTable*)xmalloc(x_size);
	if(result != Null)
	{
		current_entry = (char**)result;
		while(marked_list != Null)
		{
			(*current_entry) = marked_list->_name;
			infos = (*current_entry) - CLASS_PLENGTH + 1;
			if(marked_list->_decl_file != Null)
			{
				*infos = 'd';
			}
			else
			{
				*infos = '?';
			}
			current_entry++;
			marked_list = marked_list->_next_marked;
		}
		(*current_entry) = Null;
		qsort(result, marked_count, sizeof(char*), sort_util);
	}
	return(result);
}


/*
 * Add a new class to the dictionary
 */
ClassRec*
create_class(
	char*								class_name,
	unsigned							decl
)
{
	ClassRec**							head;
	ClassRec*							current_class;
	int									x_size;

	get_head_Rec(class_name, class_dict, CLASS_DICT_SIZE, head);
	search_Rec(class_name, ClassRec, head, current_class);
	if(current_class == Null)
	{
		x_size = sizeof(ClassRec) + CLASS_PLENGTH + strlen(class_name) + 1;
		current_class = (ClassRec*)xmalloc(x_size);
		if(current_class != Null)
		{
			create_Rec(class_name, ClassRec, head, current_class, CLASS_PREFIX,
				CLASS_PLENGTH);
			current_class->_decl_file = Null;
			current_class->_decl_line = 0;
			current_class->_parents_list = Null;
			current_class->_parents_count = 0;
			current_class->_methods_list = Null;
			current_class->_attributes_list = Null;
			current_class->_decl = decl;
			current_class->_hide = 0;
			current_class->_next_marked = Null;
			class_count++;
		}
	}
	return(current_class);
}


/*
 * Find the specified class in the dictionary
 */
ClassRec*
find_class(
	char*								class_name
)
{
	ClassRec**							head;
	ClassRec*							current_class;

	get_head_Rec(class_name, class_dict, CLASS_DICT_SIZE, head);
	search_Rec(class_name, ClassRec, head, current_class);
	return(current_class);
}


/*
 * Remove a class from the dictionary
 */
void
class_eraze_file(
	char*								file_name
)
{
	Bits*								erazed_slice;
	FileRec*							current_file;
	ClassRec*							current_class;
	MethodRec*							current_method;
	AttributeRec*						current_attr;
	ParentRec*							current_parent;
	int									index;

	current_file = find_file(file_name);
	if(current_file != Null)
	{
		erazed_slice = erazed_bits;
		for(index = 0; index < CLASS_DICT_SIZE; index++)
		{
			current_class = class_dict[index];
			while(current_class != Null)
			{
				if(current_class->_decl_file == current_file)
				{
					current_class->_decl_file = Null;
					current_class->_decl_line = 0;
					current_class->_parents_count = 0;
					current_parent = current_class->_parents_list;
					while(current_parent != Null)
					{
						current_parent->_scope = UNKNOWN_SCOPE;
						current_parent = current_parent->_next;
					}
					(*erazed_slice) |= ((0x00000001L) << (index & LAST_BIT));
				}
				current_method = current_class->_methods_list;
				while(current_method != Null)
				{
					if(current_method->_decl_file == current_file)
					{
						current_method->_decl_file = Null;
						current_method->_decl_line = 0;
						(*erazed_slice) |= ((0x00000001L) << (index & LAST_BIT));
					}
					if(current_method->_impl_file == current_file)
					{
						current_method->_impl_file = Null;
						current_method->_impl_line = 0;
						(*erazed_slice) |= ((0x00000001L) << (index & LAST_BIT));
					}
					current_method = current_method->_next;
				}
				current_attr = current_class->_attributes_list;
				while(current_attr != Null)
				{
					if(current_attr->_decl_file == current_file)
					{
						current_attr->_decl_file = Null;
						current_attr->_decl_line = 0;
						(*erazed_slice) |= ((0x00000001L) << (index & LAST_BIT));
					}
					current_attr = current_attr->_next;
				}
				current_class = current_class->_next;
			}
			if((index & LAST_BIT) == LAST_BIT)
			{
				erazed_slice++;
			}
		}
	}
}


/*
 * Clean up the dictionary
 */
void
garbage_class(
	void
)
{
	Bits*								erazed_slice;
	ClassRec*							previous_class;
	ClassRec*							current_class;
	ClassRec*							next_class;
	MethodRec*							previous_method;
	MethodRec*							current_method;
	MethodRec*							next_method;
	AttributeRec*						previous_attr;
	AttributeRec*						current_attr;
	AttributeRec*						next_attr;
	ParentRec*							previous_parent;
	ParentRec*							current_parent;
	ParentRec*							next_parent;
	int									index;

	erazed_slice = erazed_bits;
	for(index = 0; index < CLASS_DICT_SIZE; index++)
	{
		if((*erazed_slice) == 0)
		{
			index += LAST_BIT;
			erazed_slice++;
		}
		else
		{
			if(((*erazed_slice) & ((0x00000001L) << (index & LAST_BIT))) != 0)
			{
				previous_class = Null;
				current_class = class_dict[index];
				while(current_class != Null)
				{
					previous_method = Null;
					current_method = current_class->_methods_list;
					while(current_method != Null)
					{
						if((current_method->_decl_file == Null) &&
							(current_method->_impl_file == Null))
						{
							next_method = current_method->_next;
							if(previous_method == Null)
							{
								current_class->_methods_list = next_method;
							}
							else
							{
								previous_method->_next = next_method;
							}
							free(current_method);
							current_method = next_method;
						}
						else
						{
							previous_method = current_method;
							current_method = current_method->_next;
						}
					}
					previous_attr = Null;
					current_attr = current_class->_attributes_list;
					while(current_attr != Null)
					{
						if(current_attr->_decl_file == Null)
						{
							next_attr = current_attr->_next;
							if(previous_attr == Null)
							{
								current_class->_attributes_list = next_attr;
							}
							else
							{
								previous_attr->_next = next_attr;
							}
							free(current_attr);
							current_attr = next_attr;
						}
						else
						{
							previous_attr = current_attr;
							current_attr = current_attr->_next;
						}
					}
					previous_parent = Null;
					current_parent  = current_class->_parents_list;
					while(current_parent != Null)
					{
						if(current_parent->_scope == UNKNOWN_SCOPE)
						{
							next_parent = current_parent->_next;
							if(previous_parent == Null)
							{
								current_class->_parents_list = next_parent;
							}
							else
							{
								previous_parent->_next = next_parent;
							}
							free(current_parent);
							current_parent = next_parent;
						}
						else
						{
							previous_parent = current_parent;
							current_parent = current_parent->_next;
						}
					}
					if((current_class->_methods_list == Null) &&
						(current_class->_attributes_list == Null) &&
						(current_class->_decl_file == Null))
					{
						next_class = current_class->_next;
						if(previous_class == Null)
						{
							class_dict[index] = next_class;
						}
						else
						{
							previous_class->_next = next_class;
						}
						--class_count;
						free(current_class);
						current_class = next_class;
					}
					else
					{
						previous_class = current_class;
						current_class = current_class->_next;
					}
				}
			}
			if((index & LAST_BIT) == LAST_BIT)
			{
				*erazed_slice = 0;
				erazed_slice++;
			}
		}
	}
}


/*
 * Initialize class dictionary
 */
void
init_class(
	void
)
{
	Bits*								erazed_slice;
	int									index;

	erazed_slice = erazed_bits;
	for(index = 0; index < CLASS_DICT_SIZE; index++)
	{
		if((index & LAST_BIT) == LAST_BIT)
		{
			*erazed_slice = 0;
			erazed_slice++;
		}
		class_dict[index] = Null;
	}
}
