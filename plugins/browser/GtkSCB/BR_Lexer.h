/* ########################################################################

			      BR_Lexer.h

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef BR_LEXER_H
#define BR_LEXER_H


struct BR_TokenRec
{
	BR_Int				_code;
	struct BR_TokenRec*		_prefix;
	BR_Str*				_val;
	BR_ULong			_val_len;
	BR_ULong			_bits;
	BR_ULong			_line_num;
	struct BR_TokenRec*		_next;
	struct BR_TokenRec*		_last;
};

typedef struct BR_TokenRec BR_TokenRec;
typedef BR_TokenRec* BR_TokenPt;

#define YYSTYPE				BR_TokenPt

#define yylval				BR_yylval
extern YYSTYPE				BR_yylval;

#define yylex				BR_yylex
extern BR_Int				BR_yylex(void);

extern void BR_lex_init(FILE* main_file, BR_Str* main_name);
extern void BR_lex_empty_frame(void);
extern BR_TokenPt BR_lex_stack_frame(BR_ULong init_bits);
extern BR_TokenPt BR_lex_unstack_frame(void);
extern BR_TokenPt BR_lex_alloc_token(BR_Int code, BR_Str* val, BR_Int val_len);
extern BR_Int BR_lex_get_line_num(void);
extern void BR_lex_ignore_proc_tail(void);
extern void BR_lex_jump_bracket(void);
extern void BR_lex_jump_colon(void);
extern void BR_lex_save_state(void);
extern void BR_lex_synchronize(void);

#endif
