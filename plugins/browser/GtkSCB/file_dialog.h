/* ########################################################################

			      file_dialog.h

   File: file_dialog.h
   Description: 
   Created: Sa Aug 14 1999
   Author: Dominique Leveque
   Modified: Sa Aug 14 1999
   Last maintained by: Oliver Granert

   RCS $Revision$ $State$
   

*/
#include <gtk/gtk.h>

extern GtkWidget* create_FileSelectionDialog(void);
