/* ########################################################################

			     BR_Interface.h

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef BR_INTERFACE_H
#define BR_INTERFACE_H

   /*-----------------------------------------------------------------------*/

#define BR_INFO_EMPTY               0x00000000

#define BR_INFO_INCLUDE             0x00000001
#define BR_INFO_TAG                 0x00000002

#define BR_INFO_TYPEDEF             0x00000004
#define BR_INFO_PROC                0x00000008
#define BR_INFO_DATA                0x00000010

#define BR_INFO_CLASS               0x00000020
#define BR_INFO_STRUCT              0x00000040
#define BR_INFO_UNION               0x00000080

#define BR_INFO_PARENT              0x00000100

#define BR_INFO_PRIVATE             0x00000200
#define BR_INFO_PROTECTED           0x00000400
#define BR_INFO_PUBLIC              0x00000800

#define BR_INFO_EXTERN              0x00001000
#define BR_INFO_STATIC              0x00002000
#define BR_INFO_TEMPLATE            0x00004000

#define BR_INFO_INLINE              0x00008000
#define BR_INFO_CONSTRUCTOR         0x00010000
#define BR_INFO_DESTRUCTOR          0x00020000
#define BR_INFO_OPERATOR            0x00040000
#define BR_INFO_CONST               0x00080000
#define BR_INFO_VIRTUAL             0x00100000
#define BR_INFO_PURE                0x00200000

#define BR_INFO_C                   0x00400000
#define BR_INFO_DEF                 0x00800000

#define BR_INFO_FRIEND              0x01000000

#define BR_INFO_ENUM                0x02000000

#define BR_INFO_ERROR               0x80000000


   /*-----------------------------------------------------------------------*/

typedef FCT(void, (*BR_InfoHandler), (BR_Str*  prefix,
				      BR_Str*  syntax_unit,
				      BR_ULong infos,
				      BR_ULong line_num) );


   /*-------------------------------------------------------------------------
   **                                                                       **
   **   file_name :                                                         **
   **     . the name of the file to parse                                   **
   **     . a null pointer is an error                                      **
   **   info_handler :                                                      **
   **     . a pointer on the information handler                            **
   **     . a null pointer inhibits informations feedback                   **
   **   filter_cmd :                                                        **
   **     . a pointer on a null terminated char* array                      **
   **     . the first element is the filter command name (mandatory)        **
   **     . the other elements are the filter arguments                     **
   **       (parsed file name included)                                     **
   **     . a null pointer is used when there is no filter command          **
   **                                                                       **
   **-----------------------------------------------------------------------*/

extern
FCT(BR_Boolean, BR_parse_file, (BR_Str*        file_name,
				BR_InfoHandler info_handler,
				BR_Str**       filter_cmd) );


#endif  /* BR_INTERFACE_H */
