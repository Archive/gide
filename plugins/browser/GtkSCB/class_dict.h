/* ########################################################################

			      class_dict.h

   File: class_dict.h
   Description: 
   Created: Fri Jan 27 10:53:57 MET 1995
   Author: Dominique Leveque
   Modified: Aug 28 1999
   Last maintained by: Oliver Granert

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef CLASSDICT_H
#define CLASSDICT_H


enum ScopeType { UNKNOWN_SCOPE, PUBLIC_SCOPE, PROTECTED_SCOPE, PRIVATE_SCOPE };
typedef enum ScopeType ScopeType;

struct ParentRec {
	/* name of the class parent */
	char*								_name;
	/* La nature de l'heritage */
	ScopeType							_scope;
	/* La ligne dans le fichier */
	LineNumber							_decl_line;
	/* La classe suivante */
	struct ParentRec*					_next;
};

typedef struct ParentRec ParentRec;

struct ClassRec;

#define METHOD_ISA_CONSTRUCTOR(decl)	(decl & 0x10)
#define METHOD_IS_VIRTUAL(decl)			(decl & 0x100)
#define METHOD_IS_INLINE(decl)			(decl & 0x8)

struct MethodRec {
	/* Le de la methode */
	char*								_name;
	/* Le nom de la classe */
	char*								_class_name;
	/* La portee de la methode */
	ScopeType							_scope;
	int									_decl;
	/* Le fichier de declaration */
	FileRec*							_decl_file;
	/* La ligne dans le fichier precedent */
	LineNumber							_decl_line;
	/* Le fichier d'implementation */
	FileRec*							_impl_file;
	/* La ligne d'implementation */
	LineNumber							_impl_line;
	unsigned							_hide;
	/* Le pointeur de marquage */
	struct MethodRec*					_next_marked;
	/* La methode suivante */
	struct MethodRec*					_next;
};

typedef struct MethodRec MethodRec;

#define ATTRIBUT_IS_STATIC(decl) (decl & 0x8)

struct AttributeRec {
	/* the attributes name */
	char*								_name;
	/* the class_name */
	char*								_class_name;
	/* the scope from the attribute */
	ScopeType							_scope;
	int									_decl;
	/* Le fichier de declaration */
	FileRec*							_decl_file;
	/* La ligne dans le fichier precedent */
	LineNumber							_decl_line;
	unsigned							_hide;
	/* Le pointeur de marquage */
	struct AttributeRec*				_next_marked;
	/* L'attribut suivant */
	struct AttributeRec*				_next;
};

typedef struct AttributeRec AttributeRec;

struct ClassRec {
	/* La nom de la classe */
	char*								_name;
	/* Le fichier de declaration */
	FileRec*							_decl_file;
	/* La ligne de declaration */
	LineNumber							_decl_line;
	/* La liste des parents */
	ParentRec*							_parents_list;
	/* Le nombre de parents */
	int									_parents_count;
	/* La liste des methodes */
	MethodRec*							_methods_list;
	/* La liste des methodes */
	AttributeRec*						_attributes_list;
	unsigned							_decl;
	unsigned							_hide;
	/* Le pointeur de marquage */
	struct ClassRec*					_next_marked;
	/* La classe suivante */
	struct ClassRec*					_next;
};
typedef struct ClassRec ClassRec;

#define TYPEDEF_TYPE					1
#define TEMPLATE_TYPE					2
#define ISA_TYPEDEF(x)					(x & TYPEDEF_TYPE)

/* routines to manipulate the class dictionary */
ClassRec* create_class(char* file_name, unsigned decl);
ClassRec* find_class(char* file_name);
void class_eraze_file(char* file_name);
void garbage_class(void);
void init_class(void);
StringTable* get_classes_list(void);
StringTable* get_sons_list(char* class_name);
void show_all_classes(void);

#endif  /*  CLASSDICT_H  */

