/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  Gtk Source Code Browser
 *  Copyright (C) 1999 Oliver Granert
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <gtk/gtk.h>

extern void new_scann(void);
GtkWidget* create_GtkSCB (void);

gboolean
on_GtkSCB_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean on_GtkSCB_destroy_event       (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_global_list_selection_changed(GtkList *list, gpointer user_data);
void on_member_list_selection_changed(GtkList *list,gpointer user_data);
