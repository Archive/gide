/* ########################################################################

                         BR_Global.h

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */


#ifndef BR_GLOBAL_H
#define BR_GLOBAL_H

#include <config.h>

typedef unsigned char					BR_UChar;
typedef char							BR_Char;
typedef unsigned long					BR_ULong;
typedef int								BR_Int;
typedef BR_Char							BR_Str;
#define BR_Null							(0)
typedef BR_UChar						BR_Boolean;
#define BR_FALSE						((BR_Boolean)0)
#define BR_TRUE							((BR_Boolean)!BR_FALSE)

#define BR_WARNING(msg, arg) \
	fprintf(stderr, msg, arg);

#define BR_FATAL_ERROR(msg, arg) \
{ \
	fprintf(stderr, msg, arg); \
	exit(1); \
}

#endif
