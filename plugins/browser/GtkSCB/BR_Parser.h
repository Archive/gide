#ifndef YYSTYPE
#define YYSTYPE int
#endif
#define	T_ARRAY	258
#define	T_PREFIX	259
#define	T_ELLIPSIS	260
#define	T_OPERATOR	261
#define	T_STRUCT	262
#define	T_UNION	263
#define	T_CLASS	264
#define	T_ENUM	265
#define	T_PUBLIC	266
#define	T_PROTECTED	267
#define	T_PRIVATE	268
#define	T_IDENT	269
#define	T_TEMPLATE	270
#define	T_BASIC	271
#define	T_TEMPLATE_ARG	272
#define	T_FRAME	273
#define	T_CONST	274
#define	T_FRIEND	275
#define	T_LEXER_ERROR	276


extern YYSTYPE BR_yylval;
