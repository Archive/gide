/***************************************************************************

			      file_dialog.c

   File: file_dialog.c
   Description: 
   Created: Sa Aug 14 1999
   Author: Dominique Leveque
   Modified: Sa Aug 14 1999
   Last maintained by: Oliver Granert

   RCS $Revision$ $State$
   

*****************************************************************************/
#include "file_dialog.h"
#include "result_types.h"
#include "browser_eng.h"
#include "gtk_helpers.h"
#include "GtkMainWnd.h"
#include "file_dict.h"
#include "proc_dict.h"
#include "global_dict.h"
#include "class_dict.h"

static GtkWidget* filesel = NULL;

/* A key used to store pointers to the main window. */
static const gchar* MainWindowKey = "MainWindowKey";

/*
 * Update file list when file is selected
 */
static void
update_file_list(
	GtkWidget*							file_list
)
{
	int									index;
	StringTable*						files;
	GtkWidget*							list_item = Null;

	files = get_files_list();

	/* clear list-widget */
	gtk_list_clear_items((GtkList*)file_list, 0, 1000);

	/* fill the file-list */
	for(index = 0; (*files)[index] != NULL; ++index)
	{
		GtkWidget*						label;

		list_item = gtk_list_item_new();

		label = gtk_label_new((*files)[index]);

		/* Pack and show all our widgets */
		gtk_widget_show(label);

		gtk_container_add(GTK_CONTAINER(list_item), label);

		gtk_container_add(GTK_CONTAINER(file_list), list_item);

		gtk_widget_show(list_item);

		gtk_object_set_data(GTK_OBJECT(list_item), list_item_data_key,
			(*files)[index]);
	}
	gtk_widget_reparent(list_item, file_list);
	new_scann();
}


/*
 * Callback for ok button of file selection dialog
 */
static void
file_ok_sel(
	GtkWidget*							button
)
{
	GtkWidget*							filesel;
	GtkWidget*							main_window;
	GtkWidget*							file_list;

	filesel = gtk_widget_get_toplevel(GTK_WIDGET(button));
	main_window = gtk_object_get_data(GTK_OBJECT(filesel), MainWindowKey);
	gtk_widget_hide(filesel);
	g_print("parsing file: %s\n", gtk_file_selection_get_filename(
		GTK_FILE_SELECTION(filesel)));

	parse_file(gtk_file_selection_get_filename(GTK_FILE_SELECTION(filesel)));
	file_list = get_widget(main_window, "file_list");

	update_file_list(file_list);
}


/*
 * Open the real file selection window
 */
static GtkWidget*
create_open_file_selection(
)
{
	GtkWidget*							open_file_selection;
	GtkWidget*							ok_button1;
	GtkWidget*							cancel_button1;

	open_file_selection = gtk_file_selection_new("Open File");
	gtk_object_set_data(GTK_OBJECT(open_file_selection), "open_file_selection",
		open_file_selection);
	gtk_container_border_width(GTK_CONTAINER(open_file_selection), 10);

/*	gtk_signal_connect(GTK_OBJECT(open_file_selection), "delete_event",
		GTK_SIGNAL_FUNC(on_open_filesel_delete_event), NULL);*/
	gtk_window_position(GTK_WINDOW(open_file_selection), GTK_WIN_POS_MOUSE);

	ok_button1 = GTK_FILE_SELECTION(open_file_selection)->ok_button;
	gtk_object_set_data(GTK_OBJECT(open_file_selection), "ok_button1",
		ok_button1);
	gtk_widget_show(ok_button1);
	GTK_WIDGET_SET_FLAGS(ok_button1, GTK_CAN_DEFAULT);
	gtk_signal_connect(GTK_OBJECT(ok_button1), "clicked",
		GTK_SIGNAL_FUNC(file_ok_sel), NULL);

	cancel_button1 = GTK_FILE_SELECTION(open_file_selection)->cancel_button;
	gtk_object_set_data(GTK_OBJECT(open_file_selection), "cancel_button1",
		cancel_button1);
	gtk_widget_show(cancel_button1);
	GTK_WIDGET_SET_FLAGS(cancel_button1, GTK_CAN_DEFAULT);
/*	gtk_signal_connect(GTK_OBJECT(cancel_button1), "clicked",
		GTK_SIGNAL_FUNC(on_open_filesel_cancel_button_clicked), NULL);*/

	return open_file_selection;
}


/*
 * Callback for remove file button
 */
static void
remove_file_clicked(
	GtkWidget*							button,
	GtkWidget*							file_list
)
{
	GtkWidget*							list_item;
	GList*								dlist;
	gchar*								filename;

	/* Fetch the currently selected list item which
	 * will be our file to remove from the list)
	 */
	dlist = GTK_LIST(file_list)->selection;
	if(dlist)
	{
		filename = gtk_object_get_data(GTK_OBJECT(dlist->data),
			list_item_data_key);
		g_print("remove: %s ", filename);

		/* remove the file from the parser-list */

		proc_eraze_file(filename);
		global_eraze_file(filename);
		class_eraze_file(filename);
		remove_file(filename);

		garbage_global();
		garbage_proc();
		garbage_class();

		list_item = GTK_WIDGET(dlist->data);
		gtk_list_unselect_child(GTK_LIST(file_list), list_item);

		gtk_widget_destroy(list_item);
	}
}


/*
 * Callback for add file button
 */
static void
add_file_clicked(
	GtkWidget*							button,
	GtkWidget*							file_list
)
{
	GtkWidget*							main_window;

	main_window = get_widget(GTK_WIDGET(button), "FileSelectionDialog");

	/* We use the same file selection widget each time, so first
	   of all we create it if it hasn't already been created. */
	if(filesel == NULL)
	{
		filesel = create_open_file_selection();
	}

	/* We save a pointer to the main window inside the file selection's
	   data list, so we can get it easily in the callbacks. */
	gtk_object_set_data(GTK_OBJECT(filesel), MainWindowKey, main_window);

	/* Lets set the filename, as if this were a save dialog, and we are giving
	   a default filename */
	gtk_file_selection_set_filename(GTK_FILE_SELECTION(filesel), "test.c");

	gtk_widget_show(filesel);
	gdk_window_raise(filesel->window);
}


/*
 * Callback for ok button
 */
static void
ok_clicked(
	GtkWidget*							this,
	GtkWidget*							window
)
{
	gtk_widget_destroy(window);
}


/*
 * Create a file selection dialog box. Used to select file(s) which we want to
 * browse
 */
GtkWidget*
create_FileSelectionDialog(
)
{
	GtkWidget* FileSelectionDialog;
	GtkWidget* dialog_vbox1;
	GtkWidget* scrolledwindow1;
	GtkWidget* dialog_action_area1;
	GtkWidget* hbuttonbox1;
	GtkWidget* file_list;
	GtkWidget* add_button;
	GtkWidget* remove_button;
	GtkWidget* ok_button;

	FileSelectionDialog = gtk_dialog_new();
	gtk_widget_set_name(FileSelectionDialog, "FileSelectionDialog");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "FileSelectionDialog",
		FileSelectionDialog);
	GTK_WINDOW(FileSelectionDialog)->type = GTK_WINDOW_DIALOG;
	gtk_window_set_title(GTK_WINDOW(FileSelectionDialog),
		"GtkSCB File selection dialog");
	gtk_window_position(GTK_WINDOW(FileSelectionDialog), GTK_WIN_POS_CENTER);
	gtk_window_set_policy(GTK_WINDOW(FileSelectionDialog), TRUE, TRUE, FALSE);

	dialog_vbox1 = GTK_DIALOG(FileSelectionDialog)->vbox;
	gtk_widget_set_name(dialog_vbox1, "dialog_vbox1");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "dialog_vbox1",
		dialog_vbox1);
	gtk_widget_show(dialog_vbox1);

	scrolledwindow1 = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_name(scrolledwindow1, "scrolledwindow1");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "scrolledwindow1",
		scrolledwindow1);
	gtk_widget_show(scrolledwindow1);
	gtk_box_pack_start(GTK_BOX(dialog_vbox1), scrolledwindow1, TRUE, TRUE, 0);

	file_list = gtk_list_new();
	gtk_widget_set_name(file_list, "file_list");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "file_list",
		file_list);
	gtk_widget_show(file_list);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolledwindow1),
		file_list);

	dialog_action_area1 = GTK_DIALOG(FileSelectionDialog)->action_area;
	gtk_widget_set_name(dialog_action_area1, "dialog_action_area1");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "dialog_action_area1",
		dialog_action_area1);
	gtk_widget_show(dialog_action_area1);
	gtk_container_border_width(GTK_CONTAINER(dialog_action_area1), 10);

	hbuttonbox1 = gtk_hbutton_box_new();
	gtk_widget_set_name(hbuttonbox1, "hbuttonbox1");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "hbuttonbox1",
		hbuttonbox1);
	gtk_widget_show(hbuttonbox1);
	gtk_box_pack_start(GTK_BOX(dialog_action_area1), hbuttonbox1, TRUE, TRUE,
		0);

	add_button = gtk_button_new_with_label("add a file ...");
	gtk_widget_set_name(add_button, "add_button");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "add_button",
		add_button);
	gtk_widget_show(add_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), add_button);

	remove_button = gtk_button_new_with_label("remove a file...");
	gtk_widget_set_name(remove_button, "remove_button");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "remove_button",
		remove_button);
	gtk_widget_show(remove_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), remove_button);

	ok_button = gtk_button_new_with_label("OK");
	gtk_widget_set_name(ok_button, "ok_button");
	gtk_object_set_data(GTK_OBJECT(FileSelectionDialog), "ok_button",
		ok_button);
	gtk_widget_show(ok_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), ok_button);

	/* Connect the "clicked" signal of the button to our callback */
	gtk_signal_connect(GTK_OBJECT(ok_button), "clicked",
		GTK_SIGNAL_FUNC(ok_clicked), (gpointer)FileSelectionDialog);

	/* Connect the "clicked" signal of the button to our callback */
	gtk_signal_connect(GTK_OBJECT(add_button), "clicked",
		GTK_SIGNAL_FUNC(add_file_clicked), (gpointer)FileSelectionDialog);

	/* Connect the "clicked" signal of the button to our callback */
	gtk_signal_connect(GTK_OBJECT(remove_button), "clicked",
		GTK_SIGNAL_FUNC(remove_file_clicked), (gpointer)file_list);

	update_file_list(file_list);

	return FileSelectionDialog;
}
