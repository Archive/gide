/* ########################################################################

			      proc_dict.c

   File: proc_dict.c
   Path: /home/fournigault/c/X11/xcoral-2.31/proc_dict.c
   Description: 
   Created: Fri Jan 27 11:25:07 MET 1995
   Author: Dominique Leveque
   Modified: Fri Jan 27 11:25:08 MET 1995
   Last maintained by: Dominique Leveque

   RCS $Revision$ $State$
   

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */

#include "config.h"

#if HAVE_STRING_H
#include <string.h>
#else
#include <strings.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#include "result_types.h"
#include "file_dict.h"
#include "proc_dict.h"
#include "browser_util.h"

/*
 * Procedure dictionary
 */
#define PROC_DICT_SIZE					503

ProcRec*								proc_dict[PROC_DICT_SIZE];

int										proc_count = 0;

typedef long Bits;

#define LAST_BIT  ((sizeof(Bits) * 8) - 1)

static Bits erazed_bits[(PROC_DICT_SIZE + LAST_BIT) / (LAST_BIT + 1)];


void
show_all_procs(
	void
)
{
	int									index;
	ProcRec*							current_proc;

	for(index = 0; index < PROC_DICT_SIZE; index++)
	{
		current_proc = proc_dict[index];
 
		while(current_proc)
		{
			SHOW(current_proc->_name);
			current_proc->_hide = 0;
			current_proc = current_proc->_next;
		}
	}
}

StringTable*
get_procs_list(
	char*								prefix
)
{
	ProcRec*							current_proc;
	StringTable*						result;
	char**								current_entry;
	int									index;
	int									x_size;
	char*								infos;
	unsigned							pfxlen = 0;

	x_size = sizeof(char*) * (proc_count + 1);
	result = (StringTable*)xmalloc(x_size);
	if(result != Null)
	{
		if(prefix)
		{
			pfxlen = strlen(prefix);
		}
		current_entry = (char**)result;
		for(index = 0; index < PROC_DICT_SIZE; index++)
		{
			current_proc = proc_dict[index];
			while(current_proc != Null)
			{
				if(prefix)
				{
					if(strncmp(current_proc->_name, prefix, pfxlen))
					{
						current_proc = current_proc->_next;
						continue;
					}
					(*current_entry) = (char*)current_proc;
				}
				else
				{
					(*current_entry) = current_proc->_name;
				}
				infos = current_proc->_name - PROC_PLENGTH + 1;
				if(current_proc->_decl & STATIC_PROC)
				{
					infos[0] = 's';
				}
				if(current_proc->_decl & INLINE_PROC)
				{
					infos[1] = 'I';
				}
				if(current_proc->_decl & TEMPLATE_PROC)
				{
					infos[2] = 'T';
				}
				if((current_proc->_decl & STATIC_PROC) &&
					(Hide_Bits & HideStaticFunctions))
				{
					current_proc->_hide |= HideStaticFunctions;
					HIDE(current_proc->_name);
				}
				else
				{
					current_proc->_hide &= ~HideStaticFunctions;
				}
				if(current_proc->_hide & UserHide)
				{
					HIDE(current_proc->_name);
				}
				else if(!current_proc->_hide)
				{
					SHOW(current_proc->_name);
				}

				current_entry++;
				current_proc = current_proc->_next;
			}
		}
		(*current_entry) = Null;
		if(prefix)
		{
			if(!*result)
			{
				free(result);
				return 0;
			}
		}
		else
		{
			qsort(result, proc_count, sizeof(char*), sort_util);
		}
	}
	return(result);
}


/*
 * Add a new procedure to the dictionary
 */
ProcRec*
create_proc(
	char*								proc_name,
	int									staticp,
	char*								parsed_file
)
{
	ProcRec**							head;
	ProcRec*							current_proc;
	int									x_size;

	get_head_Rec(proc_name, proc_dict, PROC_DICT_SIZE, head);
	iter_Rec(proc_name, ProcRec, head, current_proc)
	{
		if(current_proc->_impl_file &&
			((!strcmp(parsed_file, current_proc->_impl_file->_name)) ||
			((! staticp) && (!(current_proc->_decl & STATIC_PROC)))))
		{
			return current_proc;
		}
	}
	if(current_proc == Null)
	{
		x_size = sizeof(ProcRec) + PROC_PLENGTH + strlen(proc_name) + 1;
		current_proc = (ProcRec*)xmalloc(x_size);
		if(current_proc != Null)
		{
			create_Rec(proc_name, ProcRec, head, current_proc, PROC_PREFIX,
				PROC_PLENGTH);
			current_proc->_impl_file = Null;
			current_proc->_impl_line = 0;
			current_proc->_decl = 0;
			current_proc->_hide = 0;
			proc_count++;
		}
	}
	return(current_proc);
}


/*
 * Find the specified procedure in the dictionary
 */
ProcRec*
find_proc(
	char*								proc_name
)
{
	ProcRec**							head;
	ProcRec*							current_proc;

	get_head_Rec(proc_name, proc_dict, PROC_DICT_SIZE, head);
	assoc_Rec(proc_name, ProcRec, head, current_proc);
	return(current_proc);
}


/*
 * Remove a procedure from the dictionary
 */
void
proc_eraze_file(
	char*								file_name
)
{
	Bits*								erazed_slice;
	FileRec*							current_file;
	ProcRec*							current_proc;
	int									index;

	current_file = find_file(file_name);
	if(current_file != Null)
	{
		erazed_slice = erazed_bits;
		for(index = 0; index < PROC_DICT_SIZE; index++)
		{
			current_proc = proc_dict[index];
			while(current_proc != Null)
			{
				if(current_proc->_impl_file == current_file)
				{
					current_proc->_impl_file = Null;
					current_proc->_impl_line = 0;
					(*erazed_slice) |= ((0x00000001L) << (index & LAST_BIT));
				}
				current_proc = current_proc->_next;
			}
			if((index & LAST_BIT) == LAST_BIT)
			{
				erazed_slice++;
			}
		}
	}  
}


/*
 * Clean up the dictionary
 */
void
garbage_proc(
	void
)
{
	Bits*								erazed_slice;
	ProcRec*							previous_proc;
	ProcRec*							current_proc;
	ProcRec*							next_proc;
	int									index;

	erazed_slice = erazed_bits;
	for(index = 0; index < PROC_DICT_SIZE; index++)
	{
		if((*erazed_slice) == 0)
		{
			index += LAST_BIT;
			erazed_slice++;
		}
		else
		{
			if(((*erazed_slice) & ((0x00000001L) << (index & LAST_BIT))) != 0)
			{
				previous_proc = Null;
				current_proc = proc_dict[index];
				while(current_proc != Null)
				{
					if(current_proc->_impl_file == Null)
					{
						next_proc = current_proc->_next;
						if(previous_proc == Null)
						{
							proc_dict[index] = next_proc;
						}
						else
						{
							previous_proc->_next = next_proc;
						}
						--proc_count;
						free(current_proc);
						current_proc = next_proc;
					}
					else
					{
						previous_proc = current_proc;
						current_proc = current_proc->_next;
					}
				}
			}
			if((index & LAST_BIT) == LAST_BIT)
			{
				*erazed_slice = 0;
				erazed_slice++;
			}
		}
	}
}


/*
 * Initialize procedure dictionary
 */
void
init_proc(
)
{
	Bits*								erazed_slice;
	int									index;

	erazed_slice = erazed_bits;
	for(index = 0; index < PROC_DICT_SIZE; index++)
	{
		if((index & LAST_BIT) == LAST_BIT)
		{
			*erazed_slice = 0;
			erazed_slice++;
		}
		proc_dict[index] = Null;
	}
}
