#ifndef _gtk_helpers_H
#define _gtk_helpers_H 1

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

/* This is our data identification string to store
 * data in list items
 */
extern const gchar *list_item_data_key;

extern GtkWidget *pixmapwid;
extern GdkPixmap *pixmap[];
extern GdkBitmap *mask[];

extern GtkWidget* get_widget(GtkWidget *widget, gchar *widget_name);
extern void LoadPictures( GtkWidget *parent);
extern GtkWidget *xpm_label_box(gchar *label_text ,int index);
extern GtkWidget *gtk_tree_append_with_pic_and_label(GtkWidget *tree, gchar *label , int index);

#endif