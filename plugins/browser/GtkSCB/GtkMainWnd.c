/*  Gtk Source Code Browser
 *  Copyright (C) 1999 Oliver Granert
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "result_types.h"
#include "file_dict.h"
#include "class_dict.h"
#include "proc_dict.h"
#include "global_dict.h"
#include "GtkMainWnd.h"
#include "gtk_helpers.h"
#include "browser_eng.h"
#include "file_dialog.h"
#include "Anjuta_P.h"

#if 0
static void scann_clicked(GtkWidget* widget, GtkWidget* tree);
#endif

extern void plugin_browser_destroy();

/*
 * Create the main source code browser window
 */
GtkWidget*
create_GtkSCB(
)
{
	GtkWidget*							GtkSCB;
	GtkWidget*							eventbox1;
	GtkWidget*							packer1;
	GtkWidget*							vbox1;
	GtkWidget*							packer5;
	GtkWidget*							Globals_Label;
	GtkWidget*							scrolledwindow5;
	GtkWidget*							tree;
	GtkWidget*							Info_text;
	GtkWidget*							hbuttonbox1;
	GtkWidget*							help_button;
	GtkWidget*							cancel_button;
/*	GtkWidget*							ok_button;*/

	GtkSCB = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "GtkSCB", GtkSCB);
/*	gtk_signal_connect(GTK_OBJECT(GtkSCB), "delete_event",
		GTK_SIGNAL_FUNC(gtk_exit), NULL);*/
	gtk_signal_connect(GTK_OBJECT(GtkSCB), "destroy",
		GTK_SIGNAL_FUNC(plugin_browser_destroy), NULL);

	gtk_window_set_title(GTK_WINDOW(GtkSCB), "GtkSourceCodeBrowser");
	gtk_window_set_policy(GTK_WINDOW(GtkSCB), TRUE, TRUE, FALSE);
	gtk_window_set_wmclass(GTK_WINDOW(GtkSCB), "GtkSourceCodeBrowser",
		"GtkSourceCodeBrowser");

	eventbox1 = gtk_event_box_new();
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "eventbox1", eventbox1);
	gtk_widget_show(eventbox1);
	gtk_container_add(GTK_CONTAINER(GtkSCB), eventbox1);

	packer1 = gtk_packer_new();
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "packer1", packer1);
	gtk_widget_show(packer1);
	gtk_container_add(GTK_CONTAINER(eventbox1), packer1);

	vbox1 = gtk_vbox_new(FALSE, 0);
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "vbox1", vbox1);
	gtk_widget_show(vbox1);
	gtk_packer_add(GTK_PACKER(packer1), vbox1, GTK_SIDE_TOP,
		GTK_ANCHOR_CENTER, GTK_PACK_EXPAND | GTK_FILL_X | GTK_FILL_Y,
		0, 0, 0, 0, 0);

	packer5 = gtk_packer_new();
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "packer5", packer5);
	gtk_widget_show(packer5);
	gtk_box_pack_start(GTK_BOX(vbox1), packer5, TRUE, TRUE, 0);

	Globals_Label = gtk_label_new("classes + functions + variables");
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "Globals_Label", Globals_Label);
	gtk_widget_show(Globals_Label);
	gtk_packer_add(GTK_PACKER(packer5), Globals_Label, GTK_SIDE_TOP,
		GTK_ANCHOR_NORTH_WEST, 0, 0, 0, 0, 0, 0);

	scrolledwindow5 = gtk_scrolled_window_new(NULL, NULL);
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "scrolledwindow5", scrolledwindow5);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwindow5),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show(scrolledwindow5);
	gtk_packer_add(GTK_PACKER(packer5), scrolledwindow5, GTK_SIDE_TOP,
		GTK_ANCHOR_CENTER, GTK_PACK_EXPAND | GTK_FILL_X | GTK_FILL_Y,
		0, 0, 0, 0, 0);

	tree = gtk_tree_new();
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "tree", tree);
	gtk_widget_show(tree);

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolledwindow5),
		tree);

	/* Create the GtkText widget */
	Info_text = gtk_text_new(NULL, NULL);
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "Info_text", Info_text);
	gtk_text_set_editable(GTK_TEXT(Info_text), FALSE);
	gtk_widget_show(Info_text);
	gtk_packer_add(GTK_PACKER(packer5), Info_text, GTK_SIDE_TOP,
		GTK_ANCHOR_NORTH_WEST, GTK_FILL_X, 0, 5, 5, 0, 15);
	gtk_widget_set_usize(Info_text, -2, 50);

	/* Realizing a widget creates a window for it, ready for us to insert some
	   text */
/*	gtk_widget_realize(Info_text);*/

	hbuttonbox1 = gtk_hbutton_box_new();
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "hbuttonbox1", hbuttonbox1);
	gtk_widget_show(hbuttonbox1);
	gtk_packer_add(GTK_PACKER(packer1), hbuttonbox1, GTK_SIDE_TOP,
		GTK_ANCHOR_CENTER, 0, 0, 0, 0, 0, 0);

	help_button = gtk_button_new_with_label("Help");
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "help_button", help_button);
	gtk_widget_show(help_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), help_button);

	cancel_button = gtk_button_new_with_label("Cancel");
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "cancel_button", cancel_button);
	gtk_widget_show(cancel_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), cancel_button);
	gtk_signal_connect_object(GTK_OBJECT(cancel_button), "clicked",
		GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(GtkSCB));
/*
	ok_button = gtk_button_new_with_label("Scanning...");
	gtk_object_set_data(GTK_OBJECT(GtkSCB), "ok_button", ok_button);
	gtk_widget_show(ok_button);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), ok_button);
*/
	/* Connect the "clicked" signal of the button to our callback */
/*
	gtk_signal_connect(GTK_OBJECT(ok_button), "clicked",
		GTK_SIGNAL_FUNC(scann_clicked), (gpointer)tree);
*/
	return GtkSCB;
}


/*
 * Make a text describing the class
 */
static void
make_class_info_text(
	gchar*								class_name,
	GtkWidget*							Info_text
)
{
	ClassRec*							ClassInfoRec;

	ClassInfoRec = find_class(class_name);
	if(ClassInfoRec != NULL)
	{
		char							buffer[100];
		GdkColor						green;
		GdkColor						red;

		green.red = 0;
		green.green = 0xffff;
		green.blue = 0;
		red.red = 0xffff;
		red.green = 0;
		red.blue = 0;
		/* test if the item is a typedef ... */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &green, NULL,
			ISA_TYPEDEF(ClassInfoRec->_decl) ? "typedef " : "", -1);

		/* print the name */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, class_name, -1);

		/* print the file_name */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, "\nfile: " , -1);

		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, ClassInfoRec->_decl_file->_name, -1);

		sprintf(buffer, "\nline: %d\n", ClassInfoRec->_decl_line);
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &red, NULL, buffer, -1);
	}
}


/*
 * Callback for all the GtkItem:: and GtkTreeItem:: signals
 */
static void
cb_class_item_select(
	GtkWidget*							item,
	gchar*								signame
)
{
	gchar*								label;
	GtkWidget*							main_window;
	GtkWidget*							Info_text;

	/* get the main-window-widget to get the Info_text-widget ... */
	/**************************************************************/
	main_window = gtk_widget_get_toplevel(GTK_WIDGET(item));
	Info_text = gtk_object_get_data(GTK_OBJECT(main_window), "Info_text");

	/* get the selected class-name */
	/*******************************/
	label = gtk_object_get_data(GTK_OBJECT(item), list_item_data_key);

	/* output the class-name in the Info_text-widget */
	/*************************************************/
	/* Freeze the text widget */
	gtk_text_freeze(GTK_TEXT(Info_text));

	/* delete the text */
	gtk_text_backward_delete(GTK_TEXT(Info_text),
		gtk_text_get_length(GTK_TEXT(Info_text)));

	make_class_info_text(label, Info_text);

	/* Thaw the text widget, allowing the updates to become visible */
	gtk_text_thaw(GTK_TEXT(Info_text));
}


/*
 * Create a text describing the function
 */
static void
make_function_info_text(
	gchar*								function_name,
	GtkWidget*							Info_text
)
{
	ProcRec*							ProcInfoRec;

	ProcInfoRec = find_proc(function_name);
	if(ProcInfoRec != NULL)
	{
		char							buffer[100];
		GdkColor						green;
		GdkColor						red;

		green.red = 0;
		green.green = 0xffff;
		green.blue = 0;
		red.red = 0xffff;
		red.green = 0;
		red.blue = 0;

		/* print the infos */
		/*******************/
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, "function ", -1);

		/* test if the item is a static, inline, template function... */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &green, NULL,
			(ProcInfoRec->_decl & STATIC_PROC) ? "<static> " : "", -1);
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &green, NULL,
			(ProcInfoRec->_decl & INLINE_PROC) ? "<inline> " : "", -1);
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &green, NULL,
			(ProcInfoRec->_decl & TEMPLATE_PROC) ? "<template> " : "", -1);

		/* print the name */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, function_name, -1);

		/* print the file_name */
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, "\nfile: ", -1);

		gtk_text_insert(GTK_TEXT(Info_text), NULL, &Info_text->style->black,
			NULL, ProcInfoRec->_impl_file->_name, -1);

		sprintf(buffer, "\nline: %d\n", ProcInfoRec->_impl_line);
		gtk_text_insert(GTK_TEXT(Info_text), NULL, &red, NULL, buffer, -1);
	}
}


/*
 * Callback for all the GtkItem:: and GtkTreeItem:: signals
 */
static void
cb_function_item_select(
	GtkWidget*							item,
	gchar*								signame
)
{
	gchar*								label;
	GtkWidget*							main_window;
	GtkWidget*							Info_text;

	/* get the main-window-widget to get the Info_text-widget ... */
	/**************************************************************/
	main_window = gtk_widget_get_toplevel(GTK_WIDGET(item));
	Info_text = gtk_object_get_data(GTK_OBJECT(main_window), "Info_text");

	/* get the selected class-name */
	/*******************************/
	label = gtk_object_get_data(GTK_OBJECT(item), list_item_data_key);

	/* output the class-name in the Info_text-widget */
	/*************************************************/
	/* Freeze the text widget */
	gtk_text_freeze(GTK_TEXT(Info_text));

	/* delete the text */
	gtk_text_backward_delete(GTK_TEXT(Info_text),
		gtk_text_get_length(GTK_TEXT(Info_text)));

	make_function_info_text(label, Info_text);

	/* Thaw the text widget, allowing the updates to become visible */
	gtk_text_thaw(GTK_TEXT(Info_text));
}


#if 0
/*
 * Callback for the scanning button on the main window
 */
static void
scann_clicked(
	GtkWidget*							widget,
	GtkWidget*							tree
)
{
	GtkWidget*							file_dialog;

	file_dialog = create_FileSelectionDialog();
	gtk_widget_show(file_dialog);
}
#endif

/*
 * Create a new tree
 */
void
new_scann(
)
{
	int									index;
	StringTable*						procs;
	StringTable*						variables;
	StringTable*						classes;
	StringTable*						attributes;

	GtkWidget*							tree;
	GtkWidget*							tree_item_classes;
	GtkWidget*							tree_item_globals;

	GtkWidget*							subtree_globals;
	GtkWidget*							subtree_classes;

	GtkWidget*							tree_item_globals_functions;
	GtkWidget*							tree_item_globals_variables;

	GtkWidget*							subtree_globals_functions;
	GtkWidget*							subtree_globals_variables;

	GtkWidget*							subtree_classes_attributes;
	GtkWidget*							subtree_item;

	tree = gtk_object_get_data(GTK_OBJECT(GTKSCB_main_window), "tree");
	gtk_tree_clear_items(GTK_TREE(tree), 0, 2);

	/* entry: Classes and Globals */
	tree_item_classes = gtk_tree_append_with_pic_and_label(tree,
		"class/struct", 1);
	tree_item_globals = gtk_tree_append_with_pic_and_label(tree,
		"globals", 0);

	subtree_globals = gtk_tree_new();
	gtk_tree_item_set_subtree(GTK_TREE_ITEM(tree_item_globals),
		subtree_globals);

	/* sub entry: Funktions and Variables */
	tree_item_globals_functions = gtk_tree_append_with_pic_and_label(
		subtree_globals, "functions", 2);
	tree_item_globals_variables = gtk_tree_append_with_pic_and_label(
		subtree_globals, "variables", 3);

	classes = get_classes_list();
	if(classes != NULL)
	{
		/* sub-sub entry: the class-list */
		subtree_classes = gtk_tree_new();

		gtk_tree_item_set_subtree(GTK_TREE_ITEM(tree_item_classes),
			subtree_classes);

		gtk_tree_set_selection_mode(GTK_TREE(subtree_classes),
			GTK_SELECTION_SINGLE);

		for(index = 0; (*classes)[index] != NULL; ++index)
		{
			int							index2;

			subtree_item = gtk_tree_append_with_pic_and_label(subtree_classes,
				(*classes)[index], 4);
			gtk_signal_connect(GTK_OBJECT(subtree_item), "select",
				GTK_SIGNAL_FUNC(cb_class_item_select), "select");

			/* get all attributes to the current class */
			/*******************************************/
			attributes = get_attributes_list((*classes)[index], 1);
			if(attributes != NULL)
			{
				/* sub-sub-sub entry : the attribute-list */
				subtree_classes_attributes = gtk_tree_new();
				gtk_tree_item_set_subtree(GTK_TREE_ITEM(subtree_item),
					subtree_classes_attributes);
				/* add all attributes to the class tree */
				for(index2 = 0; (*attributes)[index2] != NULL; ++index2)
				{
					subtree_item = gtk_tree_append_with_pic_and_label(
						subtree_classes_attributes, (*attributes)[index2], 3);
				} /* end-for attribute-list */
				free(attributes);
			} /* end if */
		} /* end-for class-list */
		free(classes);
	} /* end if */

	procs = get_procs_list(NULL);
	if(procs != NULL)
	{
		/* sub-sub entry: the function-list */
		subtree_globals_functions = gtk_tree_new();
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(tree_item_globals_functions),
			subtree_globals_functions);

		for(index = 0; (*procs)[index] != NULL; ++index)
		{
			subtree_item = gtk_tree_append_with_pic_and_label(
				subtree_globals_functions, (*procs)[index], 2);
			gtk_signal_connect(GTK_OBJECT(subtree_item), "select",
				GTK_SIGNAL_FUNC(cb_function_item_select), "select");

		} /* end-for function-list */
		free(procs);
	} /* end if */

	variables = get_globals_list(NULL);
	if(variables != NULL)
	{
		/* sub-sub entry: the variable and struct - list */
		subtree_globals_variables = gtk_tree_new();
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(tree_item_globals_variables),
			subtree_globals_variables);

		for(index = 0; (*variables)[index] != NULL; ++index)
		{
			subtree_item = gtk_tree_append_with_pic_and_label(
				subtree_globals_variables, (*variables)[index], 3);
		} /* end-for variables-list */
		free(variables);
	} /* end if */
}
