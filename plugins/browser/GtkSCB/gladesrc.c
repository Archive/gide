/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  Gtk SourceCode Browser
 *  Copyright (C) 1999 Oliver Granert
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "gladesig.h"
#include "gladesrc.h"

static GList* pixmaps_directories = NULL;

GtkWidget*
get_widget(
	GtkWidget*							widget,
	gchar*								widget_name
)
{
	GtkWidget*							parent;
	GtkWidget*							found_widget;

	for(;;)
	{
		if(GTK_IS_MENU(widget))
		{
			parent = gtk_menu_get_attach_widget(GTK_MENU(widget));
		}
		else
		{
			parent = widget->parent;
		}
		if(parent == NULL)
		{
			break;
		}
		widget = parent;
	}

	found_widget = (GtkWidget*)gtk_object_get_data(GTK_OBJECT(widget),
		widget_name);
	if(!found_widget)
	{
		g_warning("Widget not found: %s", widget_name);
	}
	return found_widget;
}


/* This is an internally used function to set notebook tab widgets. */
void
set_notebook_tab(
	GtkWidget*							notebook,
	gint								page_num,
	GtkWidget*							widget
)
{
	GtkNotebookPage*					page;
	GtkWidget*							notebook_page;

	page = (GtkNotebookPage*)g_list_nth(GTK_NOTEBOOK(notebook)->children,
		page_num)->data;
	notebook_page = page->child;
	gtk_widget_ref(notebook_page);
	gtk_notebook_remove_page(GTK_NOTEBOOK(notebook), page_num);
	gtk_notebook_insert_page(GTK_NOTEBOOK(notebook), notebook_page,
		widget, page_num);
	gtk_widget_unref(notebook_page);
}


/* Use this function to set the directory containing installed pixmaps. */
void
add_pixmap_directory(
	gchar*								directory
)
{
	pixmaps_directories = g_list_prepend(pixmaps_directories,
		g_strdup(directory));
}


/* This is an internally used function to check if a pixmap file exists. */
#ifndef G_DIR_SEPARATOR_S
#define G_DIR_SEPARATOR_S "/"
#endif
gchar*
check_file_exists(
	gchar*								directory,
	gchar*								filename
)
{
	gchar*								full_filename;
	struct stat							s;
	gint								status;

	full_filename = g_malloc(strlen(directory) + 1 + strlen(filename) + 1);
	strcpy(full_filename, directory);
	strcat(full_filename, G_DIR_SEPARATOR_S);
	strcat(full_filename, filename);

	status = stat(full_filename, &s);
	if(status == 0 && S_ISREG(s.st_mode))
	{
		return full_filename;
	}
	g_free(full_filename);
	return NULL;
}


/* This is an internally used function to create pixmaps. */
GtkWidget*
create_pixmap(
	GtkWidget*							widget,
	gchar*								filename
)
{
	gchar*								found_filename = NULL;
	GdkColormap*						colormap;
	GdkPixmap*							gdkpixmap;
	GdkBitmap*							mask;
	GtkWidget*							pixmap;
	GList*								elem;

	/* We first try any pixmaps directories set by the application. */
	elem = pixmaps_directories;
	while(elem)
	{
		found_filename = check_file_exists((gchar*)elem->data, filename);
		if(found_filename)
		{
			break;
		}
		elem = elem->next;
	}

	/* If we haven't found the pixmap, try the source directory. */
	if(!found_filename)
	{
		found_filename = check_file_exists("../pixmap", filename);
	}

	if(!found_filename)
	{
		g_print("Couldn't find pixmap file: %s", filename);
		return NULL;
	}

	colormap = gtk_widget_get_colormap(widget);
	gdkpixmap = gdk_pixmap_colormap_create_from_xpm(NULL, colormap, &mask,
		NULL, found_filename);
	g_free(found_filename);
	if(gdkpixmap == NULL)
	{
		return NULL;
	}
	pixmap = gtk_pixmap_new(gdkpixmap, mask);
	gdk_pixmap_unref(gdkpixmap);
	gdk_bitmap_unref(mask);
	return pixmap;
}
