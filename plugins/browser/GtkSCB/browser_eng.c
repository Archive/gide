/* ########################################################################

   browser_eng.c

   File: browser_eng.c
   Path: /home/fournigault/c/X11/xcoral-2.31/browser_eng.c
   Description: 
   Created: Fri Jan 27 10:44:37 MET 1995
   Author: Dominique Leveque
   Modified: Fri Jan 27 10:44:38 MET 1995
   Last maintained by: Dominique Leveque

   RCS $Revision$ $State$
   

   ########################################################################

   Note: 

   ########################################################################

   Copyright (c) : Dominique Leveque

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   ######################################################################## */

#include "config.h"

#if HAVE_STRING_H
#include <string.h>
#else
#include <strings.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#include "proto_decl.h"

#include "result_types.h"
#include "file_dict.h"
#include "proc_dict.h"
#include "global_dict.h"
#include "class_dict.h"
#include "browser_eng.h"
#include "browser_pars.h"
#include "browser_util.h"

#include "BR_main.h"

MethodRec*								marked_list;
int										marked_count;
AttributeRec*							marked_attr_list;
int										marked_attr_count;
/*
unsigned								Hide_Bits;
*/

/* Local prototypes */
static int sort_method(const void* i, const void* j);
static void post_compute(StringTable* result);
static void get_inherit_list(ClassRec* current_class, ScopeType max_scope,
	int depth);
static void get_inherit_attr_list(ClassRec* current_class, ScopeType max_scope,
	int depth);
static int sort_attribute(const void* i, const void* j);
/* used in result_types.h */
char* get_method_class(char* method_name);
char* get_attribute_class(char* attr_name);


/* not used? */
StringTable*
get_parents_list(
	char*								class_name,
	int									obj
)
{
	StringTable*						result;
	char**								current_entry;
	ClassRec*							current_class;
	ClassRec*							parent_class;
	ParentRec*							current_parent;
	int									x_size;
	char*								infos;

	result = Null;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		x_size = sizeof(char*) * (current_class->_parents_count + 1);
		result = (StringTable*)xmalloc(x_size);
		if(result != Null)
		{
			current_entry = (char**)result;
			current_parent = current_class->_parents_list;
			while(current_parent != Null)
			{
				if(obj)
				{
					(*current_entry) = (char*)current_parent;
				}
				else
				{
					(*current_entry) = current_parent->_name;
				}
				infos = current_parent->_name - CLASS_PLENGTH + 1;
				parent_class = find_class(*current_entry);
				if((parent_class != Null) &&
					(parent_class->_decl_file != Null))
				{
					*infos = 'd';
				}
				else
				{
					*infos = '?';
				}
				current_entry++;
				current_parent = current_parent->_next;
			}
			(*current_entry) = Null;
			if(!obj)
			{
				qsort(result, current_class->_parents_count, sizeof(char*),
					sort_util);
			}
		}
	}
	return(result);
}


/*
 */
static int
sort_method(
	const void*							_i,
	const void*							_j
)
{
	char**								i = (char**)_i;
	char**								j = (char**)_j;
	char*								ti = *i;
	char*								tj = *j;
	char*								par_posit;
	int									result;

	if((**i) == '~')
	{
		ti = (*i) + 1;
	}
	if((**j) == '~')
	{
		tj = (*j) + 1;
	}
	par_posit = strchr(ti, '(');
	result = strncmp(ti, tj, (par_posit - ti));
	if(result == 0)
	{
		if((**i) == '~')
		{
			result = 1;
		}
		else if((**j) == '~')
		{
			result = -1;
		}
		else
		{
			result = strcmp(*i, *j);
			if(result == 0)
			{
				return(*((*i) - METHOD_PLENGTH + 2) > *((*j) - METHOD_PLENGTH +
					2)) ? 1 : -1;
			}
		}
	}
	return(result);
}


static void
post_compute(
	StringTable*						result
)
{
	char**								current_entry;
	char*								current_depth;
	char*								infos;
	char*								virtual_method_name = "";
	int									virtual_flag;

	virtual_flag = False;
	current_entry = (char**)result;
	while((*current_entry) != Null)
	{
		infos = (*current_entry) - METHOD_PLENGTH + 1;
		if((*infos) == 'V')
		{
			if(strcmp(virtual_method_name, (*current_entry)) == 0)
			{
				(*infos) = 'v';
			}
			else
			{
				virtual_flag = True;
				virtual_method_name = (*current_entry);
			}
		}
		else if(virtual_flag == True)
		{
			if(strcmp(virtual_method_name, (*current_entry)) == 0)
			{
				(*infos) = 'v';
			}
			else
			{
				virtual_flag = False;
			}
		}
		current_depth = infos + 1;
		if((*current_depth) == 0)
		{
			(*current_depth) = 'L';
		}
		else
		{
			(*current_depth) = '0' + (*current_depth);
			(*current_depth) = (((*current_depth) <= '9') ? (*current_depth) :
				'+');
			if(Hide_Bits & HideInherited)
			{
				HIDE(*current_entry);
			}
		}
		current_entry++;
	}
}


static void
get_inherit_list(
	ClassRec*							current_class,
	ScopeType							max_scope,
	int									depth
)
{
	MethodRec*							current_method;
	ParentRec*							current_parent;

	current_method = current_class->_methods_list;
	while(current_method != Null)
	{
		if((current_method->_scope <= max_scope) &&
			(current_method->_next_marked == Null))
		{
			*(current_method->_name - METHOD_PLENGTH + 2) = depth;
			current_method->_next_marked = marked_list;
			marked_list = current_method;
			marked_count++;
		}
		current_method = current_method->_next;
	}
	current_parent = current_class->_parents_list;
	if(depth < 255)
	{
		depth++;
	}
	while(current_parent != Null)
	{
		if(current_parent->_scope < PRIVATE_SCOPE)
		{
			current_class = find_class(current_parent->_name);
			if(current_class != Null)
			{
				get_inherit_list(current_class, PROTECTED_SCOPE, depth);
			}
		}
		current_parent = current_parent->_next;
	}
}


#define END_MARKED_LIST ((MethodRec*)0x00000001)

/* not used? */
StringTable*
get_methods_list(
	char*								class_name,
	int									cut
)
{
	ClassRec*							current_class;
	MethodRec*							current_method;
	StringTable*						result;
	char**								current_entry;
	int									x_size;
	char*								infos;

	result = Null;
	marked_list = END_MARKED_LIST;
	marked_count = 0;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		get_inherit_list(current_class, PRIVATE_SCOPE, 0);
		if(marked_list != END_MARKED_LIST)
		{
			x_size = sizeof(char*) * (marked_count + 1);
			result = (StringTable*)xmalloc(x_size);
			if(result != Null)
			{
				current_entry  = (char**)result;
				current_method = marked_list;
				while(current_method != END_MARKED_LIST)
				{
					(*current_entry) = current_method->_name;
					infos = (*current_entry) - METHOD_PLENGTH + 1;
					if(current_method->_decl_file != Null)
					{
						*(infos + 2) = 'd';
					}
					else
					{
						*(infos + 2) = '?';
					}
					if(current_method->_impl_file != Null)
					{
						*(infos + 3) =
							(METHOD_IS_INLINE(current_method->_decl)) ? 'I' :
							'i';
					}
					else
					{
						*(infos + 3) = '?';
					}
					if(METHOD_ISA_CONSTRUCTOR(current_method->_decl))
					{
						*infos = 'c';
					}
					else if((*(current_method->_name)) == '~')
					{
						*infos = 'd';
					}
					else if(METHOD_IS_VIRTUAL(current_method->_decl))
					{
						*infos = 'V';
					}
					else
					{
						*infos = ' ';
					}
					if(Hide_Bits & HideProtected &&
						(current_method->_scope >= PROTECTED_SCOPE))
					{
						current_method->_hide |= HideProtected;
						HIDE(current_method->_name);
					}
					else
					{
						current_method->_hide &= ~HideProtected;
					}
					if(Hide_Bits & HidePrivate &&
						(current_method->_scope == PRIVATE_SCOPE))
					{
						current_method->_hide |= HidePrivate;
						HIDE(current_method->_name);
					}
					else
					{
						current_method->_hide &= ~HidePrivate;
						if(!current_method->_hide)
						{
							SHOW(current_method->_name);
						}
					}
					current_entry++;
					if(!cut)
					{
						current_method = current_method->_next_marked;
					}
					else
					{
						marked_list = marked_list->_next_marked;
						current_method->_next_marked = Null;
						current_method = marked_list;
					}
				}
				(*current_entry) = Null;
				if(cut)
				{
					qsort(result, marked_count, sizeof(char*), sort_method);
				}
				post_compute(result);
			}
		}
	}
	return(result);
}

#undef END_MARKED_LIST


#define END_MARKED_LIST ((AttributeRec*) 0x00000001)

static void
get_inherit_attr_list(
	ClassRec*							current_class,
	ScopeType							max_scope,
	int									depth
)
{
	AttributeRec*						current_attr;
	ParentRec*							current_parent;

	current_attr = current_class->_attributes_list;
	while(current_attr != Null)
	{
		if((current_attr->_scope <= max_scope) &&
			(current_attr->_next_marked == Null))
		{
			*(current_attr->_name - ATTRIBUTE_PLENGTH + 1) = depth;
			current_attr->_next_marked = marked_attr_list;
			marked_attr_list = current_attr;
			marked_attr_count++;
		}
		current_attr = current_attr->_next;
	}
	current_parent = current_class->_parents_list;
	if(depth < 255)
	{
		depth++;
	}
	while(current_parent != Null)
	{
		if(current_parent->_scope < PRIVATE_SCOPE)
		{
			current_class = find_class(current_parent->_name);
			if(current_class != Null)
			{
				get_inherit_attr_list(current_class, PROTECTED_SCOPE, depth);
			}
		}
		current_parent = current_parent->_next;
	}
}


static int
sort_attribute(
	const void*							_i,
	const void*							_j
)
{
	char**								i = (char**)_i;
	char**								j = (char**)_j;
	int									result = strcmp(*i, *j);

	if(result)
	{
		return result;
	}

	if(*((*i) - ATTRIBUTE_PLENGTH + 2) == 'L')
	{
		return -1;
	}

	if(*((*j) - ATTRIBUTE_PLENGTH + 2) == 'L')
	{
		return 1;
	}

	return(*((*i) - ATTRIBUTE_PLENGTH + 2) > *((*j) - ATTRIBUTE_PLENGTH + 2)) ?
		1 : -1;
}


StringTable*
get_attributes_list(
	char*								class_name,
	int									cut
)
{
	ClassRec*							current_class;
	AttributeRec*						current_attr;
	StringTable*						result;
	char**								current_entry;
	int									x_size;
	char*								infos;

	result = Null;
	marked_attr_list = END_MARKED_LIST;
	marked_attr_count = 0;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		get_inherit_attr_list(current_class, PRIVATE_SCOPE, 0);
		if(marked_attr_list != END_MARKED_LIST)
		{
			x_size = sizeof(char*) * (marked_attr_count + 1);
			result = (StringTable*)xmalloc(x_size);
			if(result != Null)
			{
				current_entry = (char**)result;
				current_attr = marked_attr_list;
				while(current_attr != END_MARKED_LIST)
				{
					(*current_entry) = current_attr->_name;
					infos = (*current_entry) - ATTRIBUTE_PLENGTH + 1;
					if(Hide_Bits & HideProtected &&
						(current_attr->_scope >= PROTECTED_SCOPE))
					{
						current_attr->_hide |= HideProtected;
						HIDE(current_attr->_name);
					}
					else
					{
						current_attr->_hide &= ~HideProtected;
					}
					if(Hide_Bits & HidePrivate &&
						(current_attr->_scope == PRIVATE_SCOPE))
					{
						current_attr->_hide |= HidePrivate;
						HIDE(current_attr->_name);
					}
					else
					{
						current_attr->_hide &= ~HidePrivate;
					}
					if(*infos)
					{
						if(Hide_Bits & HideInherited)
						{
							HIDE(*current_entry);
						}
						else if(!current_attr->_hide)
						{
							SHOW(*current_entry);
						}
						*infos += '0';
						*infos = (((*infos) <= '9') ? (*infos) : '+');
					}
					else
					{
						*infos = 'L';
						if(!current_attr->_hide)
						{
							SHOW(*current_entry);
						}
					}
					*(infos + 1) = (ATTRIBUT_IS_STATIC(current_attr->_decl)) ?
						's' : ' ';
					current_entry++;
					if(!cut)
					{
						current_attr = current_attr->_next_marked;
					}
					else
					{
						marked_attr_list = marked_attr_list->_next_marked;
						current_attr->_next_marked = Null;
						current_attr = marked_attr_list;
					}
				}
				(*current_entry) = Null;
				if(cut)
				{
					qsort(result, marked_attr_count, sizeof(char*),
						sort_attribute);
				}
			}
		}
	}
	return(result);
}


/* not used? */
Position*
get_class_decl(
	char*								class_name
)
{
	Position*							result;
	ClassRec*							current_class;
	int									x_size;

	result = Null;
	current_class = find_class(class_name);
	x_size = sizeof(Position);
	if((current_class != Null) &&
		(current_class->_decl_file != Null) &&
		((result = (Position*)xmalloc(x_size)) != Null))
	{
		result->file_name = current_class->_decl_file->_name;
		result->line_number = current_class->_decl_line;
	}
	return(result);
}


char*
get_method_class(
	char*								method_name
)
{
	MethodRec*							current_method;
	int									x_size;

	x_size = sizeof(MethodRec);
	current_method = (MethodRec*)(method_name - x_size - METHOD_PLENGTH);
	return(current_method->_class_name);
}


/* not used? */
Position*
get_method_decl(
	char*								class_name,
	char*								method_name
)
{
	Position*							result;
	ClassRec*							current_class;
	MethodRec*							current_method;
	int									x_size;

	result = Null;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		current_method = current_class->_methods_list;
		while(current_method != Null)
		{
			if(strcmp(current_method->_name, method_name) == 0)
			{
				break;
			}
			current_method = current_method->_next;
		}
		x_size = sizeof(Position);
		if((current_method != Null) && (current_method->_decl_file != Null) &&
			((result = (Position*)xmalloc(x_size)) != Null))
		{
			result->file_name = current_method->_decl_file->_name;
			result->line_number = current_method->_decl_line;
		}
	}
	return(result);
}


/* not used? */
Position*
get_method_impl(
	char*								class_name,
	char*								method_name
)
{
	Position*							result;
	ClassRec*							current_class;
	MethodRec*							current_method;
	int									x_size;

	result = Null;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		current_method = current_class->_methods_list;
		while(current_method != Null)
		{
			if(strcmp(current_method->_name, method_name) == 0)
			{
				break;
			}
			current_method = current_method->_next;
		}
		x_size = sizeof(Position);
		if((current_method != Null) && (current_method->_impl_file != Null) &&
			((result = (Position*)xmalloc(x_size)) != Null))
		{
			result->file_name = current_method->_impl_file->_name;
			result->line_number = current_method->_impl_line;
		}
	}
	return(result);
}


char*
get_attribute_class(
	char*								attr_name
)
{
	AttributeRec*						current_attr;
	int									x_size;

	x_size = sizeof(AttributeRec);
	current_attr = (AttributeRec*)(attr_name - x_size - ATTRIBUTE_PLENGTH);
	return(current_attr->_class_name);
}


/* not used? */
Position*
get_attribute_decl(
	char*								class_name,
	char*								attr_name
)
{
	Position*							result;
	ClassRec*							current_class;
	AttributeRec*						current_attr;
	int									x_size;

	result = Null;
	current_class = find_class(class_name);
	if(current_class != Null)
	{
		current_attr = current_class->_attributes_list;
		while(current_attr != Null)
		{
			if(strcmp(current_attr->_name, attr_name) == 0)
			{
				break;
			}
			current_attr = current_attr->_next;
		}
		x_size = sizeof(Position);
		if((current_attr != Null) && (current_attr->_decl_file != Null) &&
			((result = (Position*)xmalloc(x_size)) != Null))
		{
			result->file_name = current_attr->_decl_file->_name;
			result->line_number = current_attr->_decl_line;
		}
	}
	return(result);
}


/* not used? */
Position*
get_proc_impl(
	char*								proc_name
)
{
	Position*							result;
	ProcRec*							current_proc;
	int									x_size;

	result = Null;
	current_proc = find_proc(proc_name);
	x_size = sizeof(Position);
	if((current_proc != Null) && (current_proc->_impl_file != Null) &&
		((result = (Position*)xmalloc(x_size)) != Null))
	{
		result->file_name = current_proc->_impl_file->_name;
		result->line_number = current_proc->_impl_line;
	}
	return(result);
}


/* not used? */
Position*
get_global_impl(
	char*								global_name
)
{
	Position*							result;
	GlobalRec*							current_global;
	int									x_size;

	result = Null;
	current_global = find_global(global_name);
	x_size = sizeof(Position);
	if((current_global != Null) && (current_global->_impl_file != Null) &&
		((result = (Position*)xmalloc(x_size)) != Null))
	{
		result->file_name = current_global->_impl_file->_name;
		result->line_number = current_global->_impl_line;
	}
	return(result);
}


/*
 * Parse a file and get all functions and all definitions...
 */
void
parse_file(
	char*								file_name
)
{
	parsed_file = find_file(file_name);

	if(parsed_file != Null)
	{
/*		printf("File %s found!\n", file_name);*/
		proc_eraze_file(file_name);
		global_eraze_file(file_name);
		class_eraze_file(file_name);
	}
	else
	{ 
/*		printf("File %s not found!\n", file_name);*/
		parsed_file = create_file(file_name);
	}

	if(parsed_file != Null)
	{
/*		printf("Start parsing...\n");*/
		browser_yyparse(file_name);
		garbage_global();
		garbage_proc();
		garbage_class();
	}
	parsed_file = Null;
	class_cache = Null;
}


void
delete_file(
	char*								file_name
)
{
	FileRec*							current_file;

	current_file = find_file(file_name);

	if(current_file != Null)
	{
		proc_eraze_file(file_name);
		class_eraze_file(file_name);
		global_eraze_file(file_name);
		garbage_proc();
		garbage_class();
		garbage_global();
	}
	remove_file(file_name);
}


void
browser_show_all(
	void
)
{
	Hide_Bits = 0;
	show_all_classes();
	show_all_procs();
	show_all_globals();
}


void
BrowserHide(
	char*								selected_class_name
)
{
/*	FCT(void, ClearListBox, ());
	FCT(void, FillList, (char* str));
	FCT(char*, SelectFromListBox, (char* msg));

	static char* msgs[] = {
		"show all",
		"show protected and private members",
		"hide protected and private members",
		"hide private members",
		"show inherited members",
		"hide inherited members",
		"show static functions",
		"hide static functions",
		"show static globals",
		"hide static globals",
		"show internal types",
		"hide internal types",
		"show children of selected class",
		"hide children of selected class"
	};
	char*								str;
	ClassRec* parent = (selected_class_name) ?
		find_class(selected_class_name + CLASS_PLENGTH) : 0;

	ClearListBox();
	FillList(msgs[0]);
	if(Hide_Bits & HideProtected)
	{
		FillList(msgs[1]);
	}
	else
	{
		FillList(msgs[2]);
		if(Hide_Bits & HidePrivate)
		{
			FillList(msgs[1]);
		}
		else
		{
			FillList(msgs[3]);
		}
	}
	FillList((Hide_Bits & HideInherited) ? msgs[4] : msgs[5]);
	FillList((Hide_Bits & HideInternalTypes) ? msgs[10] : msgs[11]);
	FillList((Hide_Bits & HideStaticFunctions) ? msgs[6] : msgs[7]);
	FillList((Hide_Bits & HideStaticGlobals) ? msgs[8] : msgs[9]);
	if(parent)
	{
		FillList((parent->_hide & HideChildrenOf) ? msgs[12] : msgs[13]);
	}

	if(!(str = SelectFromListBox("Browser objects visibility")))
	{
		return;
	}
	if(!strcmp(str, msgs[0]))
	{
		browser_show_all();
	}
	else if(!strcmp(str, msgs[1]))
	{
		Hide_Bits &= ~(HidePrivate | HideProtected);
	}
	else if(!strcmp(str, msgs[2]))
	{
		Hide_Bits |= HidePrivate | HideProtected;
	}
	else if(!strcmp(str, msgs[3]))
	{
		Hide_Bits |= HidePrivate;
	}
	else if(!strcmp(str, msgs[4]))
	{
		Hide_Bits &= ~HideInherited;
	}
	else if(!strcmp(str, msgs[5]))
	{
		Hide_Bits |= HideInherited;
	}
	else if(!strcmp(str, msgs[6]))
	{
		Hide_Bits &= ~HideStaticFunctions;
	}
	else if(!strcmp(str, msgs[7]))
	{
		Hide_Bits |= HideStaticFunctions;
	}
	else if(!strcmp(str, msgs[8]))
	{
		Hide_Bits &= ~HideStaticGlobals;
	}
	else if(!strcmp(str, msgs[9]))
	{
		Hide_Bits |= HideStaticGlobals;
	}
	else if(!strcmp(str, msgs[10]))
	{
		Hide_Bits &= ~HideInternalTypes;
	}
	else if(!strcmp(str, msgs[11]))
	{
		Hide_Bits |= HideInternalTypes;
	}
	else if(!strcmp(str, msgs[12]))
	{
		parent->_hide &= ~HideChildrenOf;
	}
	else if(!strcmp(str, msgs[13]))
	{
		parent->_hide = HideChildrenOf;
	}

	RefreshBrowserInfos();
*/
}


/*
 * Initializing the browser
 */
void
init_browser(
	void
)
{
	init_file();
	init_proc();
	init_class();
}
