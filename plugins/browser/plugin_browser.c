/*
 * Source Code Browser plugin
 */

#include "gide.h"
#include "gI_plugin.h"
#include "gI_tools.h"
#include "gide-tools.h"
#include "GtkSCB/result_types.h"
#include "GtkSCB/browser_eng.h"
#include "GtkSCB/file_dialog.h"
#include "GtkSCB/GtkMainWnd.h"
#include "GtkSCB/gtk_helpers.h"
#include "GtkSCB/Anjuta_P.h"

#define BROWSER_TITLE			_("Browser plugin")
#define BROWSER_DESCR			_("Source Code Browser")
#define BROWSER_NAME			_("Browser - Open source code browser")
#define BROWSER_MENU			_("Windows/Source Code Browser")

unsigned				Hide_Bits = 0;
GtkWidget*				filew;
GtkWidget*				GTKSCB_main_window;
Browser					br;
Mode					m;

/* should be in some header file */
int OpVerbose(void);

static void plugin_browser_create(Tool* tool, ToolState* state);
void plugin_browser_destroy(void);

static void
plugin_browser_create(
	Tool*				tool,
	ToolState*			state
)
{
/*  	gI_project*			project; */
/*  	gint				i; */
/*  	gint				j; */

/*  	init_browser(); */
/*  	m.pp_path = "//usr//bin//"; */
/*  	m.pp_flag = 0; */
/*  	br.mode = &m; */

/*  	GTKSCB_main_window = create_GtkSCB(); */
/*  	gtk_widget_show(GTKSCB_main_window); */
/*  	LoadPictures(GTKSCB_main_window); */

/*  	project = gI_project_get_current(); */
/*  	if(!project) */
/*  	{ */
/*  		return; */
/*  	} */
/*  	for(i = 0; i < project->targets_no; i++) */
/*  	{ */
/*  		for(j = 0; j < project->targets[i]->sources_no; j++) */
/*  		{ */
/*  			gchar*		src; */
/*  			if(project->targets[i]->sources[j][0] != '/') */
/*  			{ */
/*  				src = g_strdup_printf("%s/%s", project->prjroot, */
/*  					project->targets[i]->sources[j]); */
/*  			} */
/*  			else */
/*  			{ */
/*  				src = g_strdup_printf("%s%s", project->prjroot, */
/*  					project->targets[i]->sources[j]); */
/*  			} */
/*  			parse_file(src); */
/*  			g_free(src); */
/*  		} */
/*  	} */

/*  	new_scann(); */
}


void
plugin_browser_destroy(
	void
)
{
	if(GTKSCB_main_window)
	{
		gtk_widget_destroy(GTKSCB_main_window);
		GTKSCB_main_window = NULL;
	}
}


/* needed for SCB, don't know why yet */
int
OpVerbose (void)
{
	return 1;
}


static gboolean
plugin_browser_sens (Tool *tool, ToolState *state)
{
/*  	if(state->project) */
/*  		return TRUE; */

	return FALSE;
}


static void
plugin_browser_init (void)
{
	GtkObject*			tool;

	tool = gI_tool_new(BROWSER_NAME, (void*)plugin_browser_create);
	gI_tool_set_menu_data(TOOL(tool), plugin_browser_sens,
		BROWSER_MENU, NULL);
	anjuta_tool_add(TOOL(tool));
}


static int
can_unload (PluginData *pd)
{
	/* should be fixed to only allow closing when not visible */
	return 1;
}


static void
cleanup_plugin (PluginData *pd)
{
	plugin_browser_destroy();
	anjuta_tool_remove(BROWSER_NAME);
}


PluginInitResult
init_plugin (CommandContext *context, PluginData *pd)
{
	if (plugin_version_mismatch (context, pd, VERSION))
		return PLUGIN_QUIET_ERROR;

	plugin_browser_init();

	if (!plugin_data_init (pd, can_unload, cleanup_plugin, 
			       BROWSER_TITLE, BROWSER_DESCR)) {
		return PLUGIN_ERROR;
	}

	return PLUGIN_OK;
}
