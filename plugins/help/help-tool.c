/*
 * Anjuta functiontree plugin
 *
 * Shows a tree of functions in the active documents
 */

#include <config.h>

#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>
#include <libgide/libgide.h>
#include <unistd.h>
#include <sys/stat.h>
#include <gdl/GDL.h>
#include <dirent.h>
#include <errno.h>

#include "help.h"

#define HELP_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:help"
#define PLUGIN_NAME			"anjuta-help-plugin"
#define PLUGIN_XML			"gide-help-plugin.xml"

static gboolean
is_program_in_path (const char *program)
{
        static char **paths = NULL;
        char **p;
        
        if (paths == NULL)
                paths = g_strsplit(g_getenv("PATH"), ":", -1);
        
        for (p = paths; *p != NULL; p++){
                char *f = g_strconcat (*p, "/", program, NULL);
                if (access (f, X_OK) == 0) {
                        g_free (f);
			return 1;
		}
		
                g_free (f);
        }
        return 0;
}

static void
show_man_browser (
	GtkWidget*			widget,
	gpointer			data
)
{
	AnjutaTool*			tool = (AnjutaTool*)data;

	help(tool);
}

static void
show_devhelp (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	
	if (is_program_in_path ("devhelp")) {
		char *argv[] = { "devhelp" };
		gnome_execute_async (g_get_home_dir (), 1, argv);
	} else {
		anjuta_error_dialog (_("The devhelp program is not installed.  You must install devhelp to browse developer documentation."));
	}
}

static void
show_devhelp_function (GtkWidget *widget, gpointer data)
{
	AnjutaTool *tool = ANJUTA_TOOL (data);
	
	if (is_program_in_path ("devhelp")) {
		char *curword;
		curword = anjuta_get_current_word (tool);
		if (curword) {
			char *argv[] = { "devhelp", "--use-factory", "-s", NULL };
			argv[3] = curword;
			gnome_execute_async (g_get_home_dir (), 4, argv);
			g_free (curword);
		}
	} else {
		anjuta_error_dialog (_("The devhelp program is not installed.  You must install devhelp to browse developer documentation."));
	}
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("Man", show_man_browser),
	BONOBO_UI_UNSAFE_VERB("Devhelp", show_devhelp),
	BONOBO_UI_UNSAFE_VERB("DevhelpFunction", show_devhelp_function),
	BONOBO_UI_VERB_END
};

ANJUTA_SHLIB_TOOL_SIMPLE (HELP_COMPONENT_IID, "Anjuta Help Plugin",
			  PLUGIN_NAME, ANJUTA_DATADIR, PLUGIN_XML,
			  verbs, NULL);
