# Danish translation of gIDE.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Keld Simonsen <keld@dkuug.dk>, 2001.
# Kjartan Maraas <kmaraas@gnome.org>, 2001.
# 
msgid ""
msgstr ""
"Project-Id-Version: gIDE 0.0\n"
"POT-Creation-Date: 2001-10-17 07:35-0400\n"
"PO-Revision-Date: 2001-10-19 00:23+0200\n"
"Last-Translator: Keld Simonsen <keld@dkuug.dk>\n"
"Language-Team: Danish <dansk@klid.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: libgide/gide-utils.c:506
msgid "Retry"
msgstr "Pr�v igen"

#: libgide/gide-utils.c:775 plugins/document-manager/file-ops.c:430
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"does not exist!\n"
"Do you want to create the file?"
msgstr ""

#: libgide/gide-utils.c:791 plugins/document-manager/file-ops.c:446
#, c-format
msgid "The file '%s' is not a regular file!"
msgstr ""

#: plugins/asciitable/asciitable-tool.c:65
msgid "ASCII Table"
msgstr ""

#: plugins/asciitable/gide-asciitable-plugin.xml.h:1
msgid "ASCII table"
msgstr ""

#: plugins/asciitable/gide-asciitable-plugin.xml.h:2
msgid "Show an ASCII table"
msgstr ""

#: plugins/calculator/calculator-tool.c:31
#: plugins/calculator/gide-calculator-plugin.xml.h:1
msgid "Calculator"
msgstr "Kalkulator"

#: plugins/calculator/gide-calculator-plugin.xml.h:2
msgid "Open a calculator"
msgstr ""

#: plugins/debugger/debugger-tool.c:347
msgid "Unable to start a debugging server for this target."
msgstr ""

#: plugins/debugger/debugger-tool.c:361
msgid "Could not load file into the debugger."
msgstr ""

#: plugins/debugger/debugger-tool.c:606
#, c-format
msgid "Could not find file %s"
msgstr "Kunne ikke finde filen %s"

#: plugins/debugger/debugger-tool.c:615
msgid "Couldn't get breakpoint info for a breakpoint that was just set."
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:1
msgid "Debug"
msgstr "Fejls�g"

#: plugins/debugger/gide-debugger.xml.h:2
msgid "Run under the debugger"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:3
msgid "Set a breakpoint."
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:4
msgid "Set breakpoint"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:5
msgid "Step Into"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:6
msgid "Step Out"
msgstr "Tr�d ud"

#: plugins/debugger/gide-debugger.xml.h:7
msgid "Step Over"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:8
msgid "Stop"
msgstr "Stop"

#: plugins/debugger/gide-debugger.xml.h:9
msgid "Stop the program"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:10
msgid "_Debug"
msgstr "_Fejls�g"

#: plugins/debugger/gide-debugger.xml.h:11
msgid "_Set Breakpoint"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:12
msgid "_Step Into"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:13
msgid "_Step Out"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:14
msgid "_Step Over"
msgstr ""

#: plugins/debugger/gide-debugger.xml.h:15
msgid "_Stop"
msgstr "_Stop"

#: plugins/docstat/docstat-tool.c:80
#, c-format
msgid "Characters: %ld"
msgstr ""

#: plugins/docstat/docstat-tool.c:86
#, c-format
msgid "Non-Space Characters: %ld"
msgstr ""

#: plugins/docstat/docstat-tool.c:92
#, c-format
msgid "Words: %ld"
msgstr ""

#: plugins/docstat/gide-docstat-plugin.xml.h:1
msgid "Document statistics"
msgstr ""

#: plugins/docstat/gide-docstat-plugin.xml.h:2
msgid "Give an overview of the current document"
msgstr ""

#: plugins/document-manager/file-ops.c:86
msgid "Open File..."
msgstr "�bn fil..."

#: plugins/document-manager/file-ops.c:156
msgid ""
"The file has been changed,\n"
"Do You want to reload it?"
msgstr ""

#: plugins/document-manager/file-ops.c:223
msgid "Save As..."
msgstr "Gem som..."

#: plugins/document-manager/file-ops.c:248
#, c-format
msgid "The File %s already exists. Overwrite it?"
msgstr ""

#: plugins/document-manager/file-ops.c:322
#, c-format
msgid ""
"The file \"%s\" has been modified!\n"
"Save it?\n"
msgstr ""

#: plugins/document-manager/file-ops.c:374
#, c-format
msgid "Unable to save '%s'!"
msgstr ""

#: plugins/document-manager/gide-document-manager.c:580
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"has been changed from outside the editor!\n"
"If you reload, you'll loose the changes you did in gIDE!\n"
"Do you really want to reload it?"
msgstr ""

#: plugins/document-manager/gide-document-manager.c:582
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"has been changed from outside the editor!\n"
"Do you want to reload it?"
msgstr ""

#: plugins/document-manager/gide-document-manager.c:595
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"is not longer available.\n"
"Choose YES to close it, or NO to keep it!"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:1
msgid "Close"
msgstr "Luk"

#: plugins/document-manager/gide-document-manager.xml.h:2
msgid "Close All"
msgstr "Luk alle"

#: plugins/document-manager/gide-document-manager.xml.h:3
msgid "Close all open files"
msgstr "Luk alle �bne filer"

#: plugins/document-manager/gide-document-manager.xml.h:4
msgid "Close the current file"
msgstr "Luk aktiv fil"

#: plugins/document-manager/gide-document-manager.xml.h:5
msgid "Create a new file"
msgstr "Opret en ny fil"

#: plugins/document-manager/gide-document-manager.xml.h:6
msgid "Find"
msgstr "Find"

#: plugins/document-manager/gide-document-manager.xml.h:7
msgid "Find Again"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:8
msgid "Find In Files"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:9
msgid "Find again"
msgstr "Find igen"

#: plugins/document-manager/gide-document-manager.xml.h:10
msgid "Find in files"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:11
msgid "Goto Line..."
msgstr "G� til linje"

#: plugins/document-manager/gide-document-manager.xml.h:12
msgid "Goto line number"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:13
msgid "Open a file"
msgstr "�bn en fil"

#: plugins/document-manager/gide-document-manager.xml.h:14
msgid "Print"
msgstr "Udskriv"

#: plugins/document-manager/gide-document-manager.xml.h:15
msgid "Print Preview"
msgstr "Forh�ndsvisning af udskrift"

#: plugins/document-manager/gide-document-manager.xml.h:16
msgid "Print Setup"
msgstr "Skriverops�tning"

#: plugins/document-manager/gide-document-manager.xml.h:17
msgid "Print preview"
msgstr "Forh�ndsvisning af udskrift"

#: plugins/document-manager/gide-document-manager.xml.h:18
msgid "Print the current file"
msgstr "Udskriv aktiv fil"

#: plugins/document-manager/gide-document-manager.xml.h:19
msgid "Read Only"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:20
msgid "Redo"
msgstr "Genopret"

#: plugins/document-manager/gide-document-manager.xml.h:21
msgid "Redo the undone action"
msgstr "Genopret den fortrudte handling"

#: plugins/document-manager/gide-document-manager.xml.h:22
msgid "Replace in files"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:23
msgid "Revert"
msgstr "Omg�r"

#: plugins/document-manager/gide-document-manager.xml.h:24
msgid "Revert to last saved version"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:25
msgid "Save"
msgstr "Gem"

#: plugins/document-manager/gide-document-manager.xml.h:26
msgid "Save All"
msgstr "Gem alle"

#: plugins/document-manager/gide-document-manager.xml.h:27
msgid "Save As"
msgstr "Gem som"

#: plugins/document-manager/gide-document-manager.xml.h:28
msgid "Save all current files"
msgstr ""

#: plugins/document-manager/gide-document-manager.xml.h:29
msgid "Save the current file"
msgstr "Gem aktiv fil"

#: plugins/document-manager/gide-document-manager.xml.h:30
msgid "Save the current file with a different name"
msgstr "Gem filen med et nyt navn"

#: plugins/document-manager/gide-document-manager.xml.h:31
msgid "Setup the page settings for your current printer"
msgstr "Indstil sideops�tning for aktiv skriver"

#: plugins/document-manager/gide-document-manager.xml.h:32
msgid "Undo"
msgstr "Fortryd"

#: plugins/document-manager/gide-document-manager.xml.h:33
msgid "Undo the last action"
msgstr "Angre forrige handling"

#: plugins/document-manager/gide-document-manager.xml.h:34 src/gide.xml.h:12
msgid "_Edit"
msgstr "R_edig�r"

#: plugins/document-manager/gide-document-manager.xml.h:35 src/gide.xml.h:13
msgid "_File"
msgstr "_Fil"

#: plugins/document-manager/gide-document-manager.xml.h:36
msgid "_New"
msgstr "_Ny"

#: plugins/document-manager/gide-document-manager.xml.h:37
msgid "_Open"
msgstr "_�bn"

#: plugins/document-manager/gide-document.c:216
msgid "Make Default"
msgstr ""

#: plugins/document-manager/gide-document.c:928
msgid "Open With"
msgstr "�bn med"

#: plugins/document-manager/gide-document.c:932
msgid "Application name:"
msgstr ""

#: plugins/document-manager/gide-document.c:934
msgid "Browse..."
msgstr "Bladr igennem..."

#: plugins/document-manager/gide-document.c:948
msgid "Run in terminal"
msgstr "K�r i terminalvindue"

#: plugins/document-manager/gide-document.c:976
#, c-format
msgid "Edit with %s"
msgstr ""

#: plugins/document-manager/gide-document.c:977
#, c-format
msgid "View with %s"
msgstr ""

#: plugins/document-manager/gide-document.c:1018
#: plugins/document-manager/gide-document.c:1057
msgid "Other..."
msgstr "Andet..."

#: plugins/document-manager/gide-document.c:1036
msgid "Select Application"
msgstr ""

#: plugins/document-manager/gide-document.c:1176
#, c-format
msgid "Set the default component for %s to %s"
msgstr ""

#: plugins/evo-mail/gide-mail-plugin.xml.h:1
#: plugins/mail/gide-mail-plugin.xml.h:1 plugins/mail/mail-tool.c:222
msgid "Mail document"
msgstr ""

#: plugins/evo-mail/gide-mail-plugin.xml.h:2
#: plugins/mail/gide-mail-plugin.xml.h:2
msgid "Send the current document via mail"
msgstr ""

#: plugins/evo-mail/mail-tool.c:63
msgid "No document to send."
msgstr ""

#. FIXME: Should support files too
#: plugins/evo-mail/mail-tool.c:73
msgid "Document doesn't support the Bonobo::PersistStream interface."
msgstr ""

#: plugins/evo-mail/mail-tool.c:144
msgid ""
"Evolution is not running.  Evolution must be \n"
"running to send mail with this plugin."
msgstr ""

#: plugins/files/gide-files-plugin.xml.h:1
msgid "Display a list of files"
msgstr ""

#: plugins/files/gide-files-plugin.xml.h:2
msgid "Display a tree of files"
msgstr ""

#: plugins/files/gide-files-plugin.xml.h:3
msgid "File list"
msgstr ""

#: plugins/files/gide-files-plugin.xml.h:4
msgid "File tree"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:1
msgid "Function list"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:2
msgid "Function tree"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:3
msgid "Generate prototypes for a c-file"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:4
msgid "Prototype generator"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:5
msgid "Show a list of the functions in the active documents"
msgstr ""

#: plugins/functions/gide-functions-plugin.xml.h:6
msgid "Show a tree of the functions in the active documents"
msgstr ""

#: plugins/functions/protogen.c:409
msgid "Tools - Prototype generator"
msgstr ""

#: plugins/functions/protogen.c:410
msgid "Generate"
msgstr ""

#: plugins/functions/protogen.c:421
msgid "C Source"
msgstr ""

#: plugins/functions/protogen.c:433
msgid "Target File"
msgstr ""

#: plugins/functions/protogen.c:441
msgid "Append to Targetfile"
msgstr ""

#: plugins/functions/protogen.c:444
msgid "Skip Static Declarations"
msgstr ""

#: plugins/help/gide-help-plugin.xml.h:1
msgid "Browse developer documentation"
msgstr ""

#: plugins/help/gide-help-plugin.xml.h:2
msgid "Browse manual pages"
msgstr ""

#: plugins/help/gide-help-plugin.xml.h:3
msgid "Get help on the function under the cursor."
msgstr ""

#: plugins/help/gide-help-plugin.xml.h:4
msgid "Help on function"
msgstr ""

#: plugins/help/help.c:119
msgid "You need to select a section first"
msgstr ""

#: plugins/mail/mail-tool.c:120
msgid "Connect error: "
msgstr ""

#: plugins/mail/mail-tool.c:202
msgid "Mail sent."
msgstr ""

#: plugins/mail/mail-tool.c:230
msgid "Send to:"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:1
msgid "Add Source"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:2
msgid "Add source to project"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:3
msgid "Close Project"
msgstr "Luk projekt"

#: plugins/project-manager/gide-project-manager.xml.h:4
msgid "Close project"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:5
msgid "Create a new project"
msgstr "Opret et nyt projekt"

#: plugins/project-manager/gide-project-manager.xml.h:6
msgid "New Project"
msgstr "Nyt projekt"

#: plugins/project-manager/gide-project-manager.xml.h:7
msgid "Open Project"
msgstr "�bne projekt"

#: plugins/project-manager/gide-project-manager.xml.h:8
msgid "Open a project"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:9
msgid "Remove Source"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:10
msgid "Remove source from project"
msgstr ""

#: plugins/project-manager/gide-project-manager.xml.h:11
msgid "_Project"
msgstr ""

#: plugins/project-manager/project-tool.c:31
msgid "Unable to open file."
msgstr ""

#: plugins/project-manager/project-tool.c:115
msgid "Project does not exist!"
msgstr ""

#: plugins/project-manager/project-tool.c:119
msgid "Not a proper project!"
msgstr ""

#: plugins/project-manager/project-tool.c:123
msgid "Unable to load project!"
msgstr ""

#: plugins/project-manager/project-tool.c:171
msgid "Open Project..."
msgstr ""

#: plugins/sample/gide-sample-plugin.xml.h:1
msgid "Hello, World"
msgstr ""

#: plugins/sample/gide-sample-plugin.xml.h:2
msgid "Insert a message into the current text"
msgstr ""

#: plugins/swapch/gide-swapch-plugin.xml.h:1
msgid "Swap .c/.h file"
msgstr ""

#: plugins/swapch/gide-swapch-plugin.xml.h:2
msgid "Switch from .c to .h file and vice versa"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:1
msgid "Delete all text until the beginning of the current line"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:2
msgid "Delete all text until the beginning of the document"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:3
msgid "Delete all text until the end of the current line"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:4
msgid "Delete all text until the end of the document"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:5
msgid "Delete to begin of file"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:6
msgid "Delete to begin of line"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:7
msgid "Delete to end of file"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:8
msgid "Delete to end of line"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:9
msgid "Insert GPL header (C)"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:10
msgid "Insert GPL header (C++)"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:11
msgid "Insert a file"
msgstr "S�t ind en fil"

#: plugins/text/gide-text-plugin.xml.h:12
msgid "Insert a file in the current document"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:13
msgid "Insert date and time"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:14
msgid "Insert the standard GPL header in a C file"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:15
msgid "Insert the standard GPL header in a C++ file"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:16
msgid "Insert the time and date in the document"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:17
msgid "Replace all tabs in the document with spaces"
msgstr ""

#: plugins/text/gide-text-plugin.xml.h:18
msgid "Replace tabs with spaces"
msgstr "Erstat tabulatorer med mellemrum"

#: plugins/text/gide-text-plugin.xml.h:19
msgid "_Delete"
msgstr "S_let"

#: plugins/text/gide-text-plugin.xml.h:20
msgid "_Insert"
msgstr "_Inds�t"

#: plugins/text/text-insert.c:105
msgid "Insert File..."
msgstr "Inds�t fil..."

#: plugins/text/text-insert.c:113
#, c-format
msgid "File Error: %s"
msgstr ""

#: plugins/text/text-replace.c:79
msgid "Number of spaces per TAB?"
msgstr ""

#: src/gI_window.c:60
msgid "gIDE"
msgstr ""

#: src/gI_window.c:62
msgid "Copyright Various Authors (C) 1998-2001"
msgstr ""

#. Parse options
#: src/gide.c:84
msgid "Oaf options"
msgstr "Oaf-alternativer"

#: src/gide.xml.h:1
msgid "About this application"
msgstr "Om denne applikation"

#: src/gide.xml.h:2
msgid "About..."
msgstr "Om..."

#: src/gide.xml.h:3
msgid "Customi_ze..."
msgstr ""

#: src/gide.xml.h:4
msgid "Customize"
msgstr "Tilpas"

#: src/gide.xml.h:5
msgid "Customize toolbars"
msgstr "Tilpas v�rkt�jslinjer"

#: src/gide.xml.h:6
msgid "Exit"
msgstr "Afslut"

#: src/gide.xml.h:7
msgid "Exit the program"
msgstr "Afslut programmet"

#: src/gide.xml.h:8
msgid "Manage available plugin modules"
msgstr "H�ndt�r tilg�ngelige till�gsmoduler"

#: src/gide.xml.h:9
msgid "Plug-ins..."
msgstr ""

#: src/gide.xml.h:10
msgid "Set application preferences"
msgstr ""

#: src/gide.xml.h:11
msgid "View the IDE in fullscreen mode"
msgstr ""

#: src/gide.xml.h:14
msgid "_Full screen"
msgstr ""

#: src/gide.xml.h:15
msgid "_Help"
msgstr "_Hj�lp"

#: src/gide.xml.h:16
msgid "_Settings"
msgstr "_Indstillinger"

#: src/gide.xml.h:17
msgid "_Settings..."
msgstr "_Indstillinger..."

#: src/gide.xml.h:18
msgid "_Tools"
msgstr "V�rk_t�jer"

#: src/gide.xml.h:19
msgid "_View"
msgstr "_Vis"
