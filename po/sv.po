# Swedish messages for gIDE.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Christian Rose <menthos@menthos.com>, 2001.
#
# $Id$
#
msgid ""
msgstr ""
"Project-Id-Version: gide\n"
"POT-Creation-Date: 2001-10-19 12:26+0200\n"
"PO-Revision-Date: 2001-10-21 21:10+0200\n"
"Last-Translator: Christian Rose <menthos@menthos.com>\n"
"Language-Team: Swedish <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: libgide/gide-utils.c:506
msgid "Retry"
msgstr "F�rs�k igen"

#: libgide/gide-utils.c:775 plugins/document-manager/file-ops.c:430
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"does not exist!\n"
"Do you want to create the file?"
msgstr ""
"Filen\n"
"\"%s\"\n"
"finns inte!\n"
"Vill du skapa denna fil?"

#: libgide/gide-utils.c:791 plugins/document-manager/file-ops.c:446
#, c-format
msgid "The file '%s' is not a regular file!"
msgstr "Filen \"%s\" �r inte en vanlig fil!"

#: plugins/asciitable/asciitable-tool.c:65
msgid "ASCII Table"
msgstr "ASCII-tabell"

#: plugins/asciitable/gide-asciitable-plugin.xml.h:1
msgid "ASCII table"
msgstr "ASCII-tabell"

#: plugins/asciitable/gide-asciitable-plugin.xml.h:2
msgid "Show an ASCII table"
msgstr "Visa en ASCII-tabell"

#: plugins/calculator/calculator-tool.c:31 plugins/calculator/gide-calculator-plugin.xml.h:1
msgid "Calculator"
msgstr "Minir�knare"

#: plugins/calculator/gide-calculator-plugin.xml.h:2
msgid "Open a calculator"
msgstr "�ppna en minir�knare"

#: plugins/debugger/debugger-tool.c:347
msgid "Unable to start a debugging server for this target."
msgstr "Kan inte starta en fels�kningsserver f�r detta m�l."

#: plugins/debugger/debugger-tool.c:361
msgid "Could not load file into the debugger."
msgstr "Kunde inte l�sa in filen i fels�karen."

#: plugins/debugger/debugger-tool.c:606
#, c-format
msgid "Could not find file %s"
msgstr "Kunde inte hitta filen %s"

#: plugins/debugger/debugger-tool.c:615
msgid "Couldn't get breakpoint info for a breakpoint that was just set."
msgstr ""
"Kunde inte f� tag i brytpunktsinformation f�r en brytpunkt som just sattes."

#: plugins/debugger/gide-debugger.xml.h:1
msgid "Debug"
msgstr "Fels�k"

#: plugins/debugger/gide-debugger.xml.h:2
msgid "Run under the debugger"
msgstr "K�r under fels�karen"

#: plugins/debugger/gide-debugger.xml.h:3
msgid "Set a breakpoint."
msgstr "S�tt en brytpunkt."

#: plugins/debugger/gide-debugger.xml.h:4
msgid "Set breakpoint"
msgstr "S�tt brytpunkt"

#: plugins/debugger/gide-debugger.xml.h:5
msgid "Step Into"
msgstr "Stega in"

#: plugins/debugger/gide-debugger.xml.h:6
msgid "Step Out"
msgstr "Stega ut"

#: plugins/debugger/gide-debugger.xml.h:7
msgid "Step Over"
msgstr "Stega �ver"

#: plugins/debugger/gide-debugger.xml.h:8
msgid "Stop"
msgstr "Stoppa"

#: plugins/debugger/gide-debugger.xml.h:9
msgid "Stop the program"
msgstr "Stoppa programmet"

#: plugins/debugger/gide-debugger.xml.h:10
msgid "_Debug"
msgstr "Fels_�k"

#: plugins/debugger/gide-debugger.xml.h:11
msgid "_Set Breakpoint"
msgstr "_S�tt brytpunkt"

#: plugins/debugger/gide-debugger.xml.h:12
msgid "_Step Into"
msgstr "Stega _in"

#: plugins/debugger/gide-debugger.xml.h:13
msgid "_Step Out"
msgstr "Stega _ut"

#: plugins/debugger/gide-debugger.xml.h:14
msgid "_Step Over"
msgstr "Stega _�ver"

#: plugins/debugger/gide-debugger.xml.h:15
msgid "_Stop"
msgstr "_Stoppa"

#: plugins/docstat/docstat-tool.c:80
#, c-format
msgid "Characters: %ld"
msgstr "Tecken: %ld"

#: plugins/docstat/docstat-tool.c:86
#, c-format
msgid "Non-Space Characters: %ld"
msgstr "Ickeblanka tecken: %ld"

#: plugins/docstat/docstat-tool.c:92
#, c-format
msgid "Words: %ld"
msgstr "Ord: %ld"

#: plugins/docstat/gide-docstat-plugin.xml.h:1
msgid "Document statistics"
msgstr "Dokumentstatistik"

#: plugins/docstat/gide-docstat-plugin.xml.h:2
msgid "Give an overview of the current document"
msgstr "Visa en �versikt av det aktuella dokumentet"

#: plugins/document-manager/file-ops.c:86
msgid "Open File..."
msgstr "�ppna fil..."

#: plugins/document-manager/file-ops.c:156
msgid ""
"The file has been changed,\n"
"Do You want to reload it?"
msgstr ""
"Filen har �ndrats,\n"
"vill du l�sa om den?"

#: plugins/document-manager/file-ops.c:223
msgid "Save As..."
msgstr "Spara som..."

#: plugins/document-manager/file-ops.c:248
#, c-format
msgid "The File %s already exists. Overwrite it?"
msgstr "Filen %s finns redan redan. Skriva �ver den?"

#: plugins/document-manager/file-ops.c:322
#, c-format
msgid ""
"The file \"%s\" has been modified!\n"
"Save it?\n"
msgstr ""
"Filen \"%s\" har �ndrats!\n"
"Spara den?\n"

#: plugins/document-manager/file-ops.c:374
#, c-format
msgid "Unable to save '%s'!"
msgstr "Kan inte spara \"%s\"!"

#: plugins/document-manager/gide-document-manager.c:580
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"has been changed from outside the editor!\n"
"If you reload, you'll loose the changes you did in gIDE!\n"
"Do you really want to reload it?"
msgstr ""
"Filen\n"
"\"%s\"\n"
"har �ndrats utanf�r redigeraren!\n"
"Om du l�ser om den kommer du att f�rlora de �ndringar du har gjort i gIDE!\n"
"Vill du verkligen l�sa om den?"

#: plugins/document-manager/gide-document-manager.c:582
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"has been changed from outside the editor!\n"
"Do you want to reload it?"
msgstr ""
"Filen\n"
"\"%s\"\n"
"har �ndrats utanf�r redigeraren!\n"
"Vill du l�sa om den?"

#: plugins/document-manager/gide-document-manager.c:595
#, c-format
msgid ""
"The file\n"
"'%s'\n"
"is not longer available.\n"
"Choose YES to close it, or NO to keep it!"
msgstr ""
"Filen\n"
"\"%s\"\n"
"�r inte l�ngre tillg�nglig.\n"
"V�lj JA f�r att st�nga den, eller NEJ f�r att beh�lla den!"

#: plugins/document-manager/gide-document-manager.xml.h:1
msgid "Close"
msgstr "St�ng"

#: plugins/document-manager/gide-document-manager.xml.h:2
msgid "Close All"
msgstr "St�ng alla"

#: plugins/document-manager/gide-document-manager.xml.h:3
msgid "Close all open files"
msgstr "St�ng alla �ppna filer"

#: plugins/document-manager/gide-document-manager.xml.h:4
msgid "Close the current file"
msgstr "St�ng aktuell fil"

#: plugins/document-manager/gide-document-manager.xml.h:5
msgid "Create a new file"
msgstr "Skapa en ny fil"

# src/dialogs.c:473 src/menus.c:358
#: plugins/document-manager/gide-document-manager.xml.h:6
msgid "Find"
msgstr "S�k"

#: plugins/document-manager/gide-document-manager.xml.h:7
msgid "Find Again"
msgstr "S�k igen"

#: plugins/document-manager/gide-document-manager.xml.h:8
msgid "Find In Files"
msgstr "S�k i filer"

#: plugins/document-manager/gide-document-manager.xml.h:9
msgid "Find again"
msgstr "S�k igen"

#: plugins/document-manager/gide-document-manager.xml.h:10
msgid "Find in files"
msgstr "S�k i filer"

#: plugins/document-manager/gide-document-manager.xml.h:11
msgid "Goto Line..."
msgstr "G� till rad..."

#: plugins/document-manager/gide-document-manager.xml.h:12
msgid "Goto line number"
msgstr "G� till radnummer"

#: plugins/document-manager/gide-document-manager.xml.h:13
msgid "Open a file"
msgstr "�ppna en fil"

#: plugins/document-manager/gide-document-manager.xml.h:14
msgid "Print"
msgstr "Skriv ut"

#: plugins/document-manager/gide-document-manager.xml.h:15
msgid "Print Preview"
msgstr "F�rhandsgranska"

#: plugins/document-manager/gide-document-manager.xml.h:16
msgid "Print Setup"
msgstr "Skrivarinst�llningar"

#: plugins/document-manager/gide-document-manager.xml.h:17
msgid "Print preview"
msgstr "F�rhandsgranska"

#: plugins/document-manager/gide-document-manager.xml.h:18
msgid "Print the current file"
msgstr "Skriv ut den aktuella filen"

#: plugins/document-manager/gide-document-manager.xml.h:19
msgid "Read Only"
msgstr "Skrivskyddad"

# src/menus.c:306
#: plugins/document-manager/gide-document-manager.xml.h:20
msgid "Redo"
msgstr "G�r om"

#: plugins/document-manager/gide-document-manager.xml.h:21
msgid "Redo the undone action"
msgstr "G�r om den �ngrade �tg�rden"

#: plugins/document-manager/gide-document-manager.xml.h:22
msgid "Replace in files"
msgstr "Ers�tt i filer"

#: plugins/document-manager/gide-document-manager.xml.h:23
msgid "Revert"
msgstr "�terg�"

#: plugins/document-manager/gide-document-manager.xml.h:24
msgid "Revert to last saved version"
msgstr "�terg� till den senast sparade versionen"

# src/menus.c:280
#: plugins/document-manager/gide-document-manager.xml.h:25
msgid "Save"
msgstr "Spara"

#: plugins/document-manager/gide-document-manager.xml.h:26
msgid "Save All"
msgstr "Spara alla"

#: plugins/document-manager/gide-document-manager.xml.h:27
msgid "Save As"
msgstr "Spara som"

#: plugins/document-manager/gide-document-manager.xml.h:28
msgid "Save all current files"
msgstr "Spara alla aktuella fil"

#: plugins/document-manager/gide-document-manager.xml.h:29
msgid "Save the current file"
msgstr "Spara den aktuella filen"

#: plugins/document-manager/gide-document-manager.xml.h:30
msgid "Save the current file with a different name"
msgstr "Spara den aktuella filen med ett annat namn"

#: plugins/document-manager/gide-document-manager.xml.h:31
msgid "Setup the page settings for your current printer"
msgstr "St�ll in sidinst�llningarna f�r din nuvarande skrivare"

# src/menus.c:302
#: plugins/document-manager/gide-document-manager.xml.h:32
msgid "Undo"
msgstr "�ngra"

#: plugins/document-manager/gide-document-manager.xml.h:33
msgid "Undo the last action"
msgstr "�ngra den senaste �tg�rden"

#: plugins/document-manager/gide-document-manager.xml.h:34 src/gide.xml.h:12
msgid "_Edit"
msgstr "_Redigera"

# Arkiv Arkiv Arkiv Arkiv Arkiv Arkiv Arkiv och inget annat �n Arkiv!
#: plugins/document-manager/gide-document-manager.xml.h:35 src/gide.xml.h:13
msgid "_File"
msgstr "_Arkiv"

#: plugins/document-manager/gide-document-manager.xml.h:36
msgid "_New"
msgstr "_Ny"

#: plugins/document-manager/gide-document-manager.xml.h:37
msgid "_Open"
msgstr "_�ppna"

#: plugins/document-manager/gide-document.c:216
msgid "Make Default"
msgstr "G�r till standardval"

#: plugins/document-manager/gide-document.c:928
msgid "Open With"
msgstr "�ppna med"

#: plugins/document-manager/gide-document.c:932
msgid "Application name:"
msgstr "Programnamn:"

#: plugins/document-manager/gide-document.c:934
msgid "Browse..."
msgstr "Bl�ddra..."

#: plugins/document-manager/gide-document.c:948
msgid "Run in terminal"
msgstr "K�r i terminal"

#: plugins/document-manager/gide-document.c:976
#, c-format
msgid "Edit with %s"
msgstr "Redigera med %s"

#: plugins/document-manager/gide-document.c:977
#, c-format
msgid "View with %s"
msgstr "Visa med %s"

#: plugins/document-manager/gide-document.c:1018 plugins/document-manager/gide-document.c:1057
msgid "Other..."
msgstr "Annan..."

#: plugins/document-manager/gide-document.c:1036
msgid "Select Application"
msgstr "V�lj program"

#: plugins/document-manager/gide-document.c:1176
#, c-format
msgid "Set the default component for %s to %s"
msgstr "St�ll in standardkomponenten f�r %s till %s"

#: plugins/evo-mail/gide-mail-plugin.xml.h:1 plugins/mail/gide-mail-plugin.xml.h:1 plugins/mail/mail-tool.c:222
msgid "Mail document"
msgstr "Skicka dokument med e-post"

#: plugins/evo-mail/gide-mail-plugin.xml.h:2 plugins/mail/gide-mail-plugin.xml.h:2
msgid "Send the current document via mail"
msgstr "Skicka det aktuella dokumentet med e-post"

#: plugins/evo-mail/mail-tool.c:63
msgid "No document to send."
msgstr "Inget dokument att skicka."

#. FIXME: Should support files too
#: plugins/evo-mail/mail-tool.c:73
msgid "Document doesn't support the Bonobo::PersistStream interface."
msgstr "Dokumentet st�der inte gr�nssnittet Bonobo::PersistStream."

#: plugins/evo-mail/mail-tool.c:144
msgid ""
"Evolution is not running.  Evolution must be \n"
"running to send mail with this plugin."
msgstr ""
"Evolution k�r inte. Evolution m�ste k�ra f�r att\n"
"e-post ska kunna skickas med denna insticksmodul."

#: plugins/files/gide-files-plugin.xml.h:1
msgid "Display a list of files"
msgstr "Visa en lista med filer"

#: plugins/files/gide-files-plugin.xml.h:2
msgid "Display a tree of files"
msgstr "Visa ett tr�d med filer"

#: plugins/files/gide-files-plugin.xml.h:3
msgid "File list"
msgstr "Fillista"

#: plugins/files/gide-files-plugin.xml.h:4
msgid "File tree"
msgstr "Filtr�d"

#: plugins/functions/gide-functions-plugin.xml.h:1
msgid "Function list"
msgstr "Funktionslista"

#: plugins/functions/gide-functions-plugin.xml.h:2
msgid "Function tree"
msgstr "Funktionstr�d"

#: plugins/functions/gide-functions-plugin.xml.h:3
msgid "Generate prototypes for a c-file"
msgstr "Generera prototyper f�r en c-fil"

#: plugins/functions/gide-functions-plugin.xml.h:4
msgid "Prototype generator"
msgstr "Prototypgenerator"

#: plugins/functions/gide-functions-plugin.xml.h:5
msgid "Show a list of the functions in the active documents"
msgstr "Visa en lista med funktionerna i de aktiva dokumenten"

#: plugins/functions/gide-functions-plugin.xml.h:6
msgid "Show a tree of the functions in the active documents"
msgstr "Visa ett tr�d med funktionerna i de aktiva dokumenten"

#: plugins/functions/protogen.c:409
msgid "Tools - Prototype generator"
msgstr "Verktyg - Prototypgenerator"

#: plugins/functions/protogen.c:410
msgid "Generate"
msgstr "Generera"

#: plugins/functions/protogen.c:421
msgid "C Source"
msgstr "C-k�llkod"

#: plugins/functions/protogen.c:433
msgid "Target File"
msgstr "M�lfil"

#: plugins/functions/protogen.c:441
msgid "Append to Targetfile"
msgstr "L�gg till i m�lfil"

#: plugins/functions/protogen.c:444
msgid "Skip Static Declarations"
msgstr "Hoppa �ver statiska deklarationer"

#: plugins/help/gide-help-plugin.xml.h:1
msgid "Browse developer documentation"
msgstr "Bl�ddra i utvecklardokumentation"

#: plugins/help/gide-help-plugin.xml.h:2
msgid "Browse manual pages"
msgstr "Bl�ddra i manualsidor"

#: plugins/help/gide-help-plugin.xml.h:3
msgid "Get help on the function under the cursor."
msgstr "F� hj�lp med funktionen under mark�ren."

#: plugins/help/gide-help-plugin.xml.h:4
msgid "Help on function"
msgstr "Hj�lp med funktion"

#: plugins/help/help.c:119
msgid "You need to select a section first"
msgstr "Du m�ste v�lja en sektion f�rst"

#: plugins/mail/mail-tool.c:120
msgid "Connect error: "
msgstr "Anslutningsfel: "

#: plugins/mail/mail-tool.c:202
msgid "Mail sent."
msgstr "E-posten skickad."

#: plugins/mail/mail-tool.c:230
msgid "Send to:"
msgstr "Skicka till:"

#: plugins/project-manager/gide-project-manager.xml.h:1
msgid "Add Source"
msgstr "L�gg till k�llkod"

#: plugins/project-manager/gide-project-manager.xml.h:2
msgid "Add source to project"
msgstr "L�gg till k�llkod i projektet"

#: plugins/project-manager/gide-project-manager.xml.h:3
msgid "Close Project"
msgstr "St�ng projekt"

#: plugins/project-manager/gide-project-manager.xml.h:4
msgid "Close project"
msgstr "St�ng projekt"

#: plugins/project-manager/gide-project-manager.xml.h:5
msgid "Create a new project"
msgstr "Skapa ett nytt projekt"

#: plugins/project-manager/gide-project-manager.xml.h:6
msgid "New Project"
msgstr "Nytt projekt"

#: plugins/project-manager/gide-project-manager.xml.h:7
msgid "Open Project"
msgstr "�ppna projekt"

#: plugins/project-manager/gide-project-manager.xml.h:8
msgid "Open a project"
msgstr "�ppna ett projekt"

#: plugins/project-manager/gide-project-manager.xml.h:9
msgid "Remove Source"
msgstr "Ta bort k�llkod"

#: plugins/project-manager/gide-project-manager.xml.h:10
msgid "Remove source from project"
msgstr "Ta bort k�llkod fr�n projektet"

#: plugins/project-manager/gide-project-manager.xml.h:11
msgid "_Project"
msgstr "_Projekt"

#: plugins/project-manager/project-tool.c:31
msgid "Unable to open file."
msgstr "Kan inte �ppna filen."

#: plugins/project-manager/project-tool.c:115
msgid "Project does not exist!"
msgstr "Projektet finns inte!"

#: plugins/project-manager/project-tool.c:119
msgid "Not a proper project!"
msgstr "Inte ett giltigt projekt!"

#: plugins/project-manager/project-tool.c:123
msgid "Unable to load project!"
msgstr "Kan inte l�sa in projekt!"

#: plugins/project-manager/project-tool.c:171
msgid "Open Project..."
msgstr "�ppna projekt..."

#: plugins/sample/gide-sample-plugin.xml.h:1
msgid "Hello, World"
msgstr "Hej v�rlden"

#: plugins/sample/gide-sample-plugin.xml.h:2
msgid "Insert a message into the current text"
msgstr "Infoga ett meddelande i den aktuella texten"

#: plugins/swapch/gide-swapch-plugin.xml.h:1
msgid "Swap .c/.h file"
msgstr "Byt .c/.h-fil"

#: plugins/swapch/gide-swapch-plugin.xml.h:2
msgid "Switch from .c to .h file and vice versa"
msgstr "Byt fr�n .c- till .h-fil och tv�rtom"

#: plugins/text/gide-text-plugin.xml.h:1
msgid "Delete all text until the beginning of the current line"
msgstr "Ta bort all text till b�rjan p� den aktuella raden"

#: plugins/text/gide-text-plugin.xml.h:2
msgid "Delete all text until the beginning of the document"
msgstr "Ta bort all text till b�rjan p� dokumentet"

#: plugins/text/gide-text-plugin.xml.h:3
msgid "Delete all text until the end of the current line"
msgstr "Ta bort all text till slutet p� den aktuella raden"

#: plugins/text/gide-text-plugin.xml.h:4
msgid "Delete all text until the end of the document"
msgstr "Ta bort all text till slutet p� dokumentet"

#: plugins/text/gide-text-plugin.xml.h:5
msgid "Delete to begin of file"
msgstr "Ta bort till b�rjan p� filen"

#: plugins/text/gide-text-plugin.xml.h:6
msgid "Delete to begin of line"
msgstr "Ta bort till slutet p� raden"

#: plugins/text/gide-text-plugin.xml.h:7
msgid "Delete to end of file"
msgstr "Ta bort till slutet p� filen"

#: plugins/text/gide-text-plugin.xml.h:8
msgid "Delete to end of line"
msgstr "Ta bort till slutet p� raden"

#: plugins/text/gide-text-plugin.xml.h:9
msgid "Insert GPL header (C)"
msgstr "Infoga GPL-sidhuvud (C)"

#: plugins/text/gide-text-plugin.xml.h:10
msgid "Insert GPL header (C++)"
msgstr "Infoga GPL-sidhuvud (C++)"

#: plugins/text/gide-text-plugin.xml.h:11
msgid "Insert a file"
msgstr "Infoga en fil"

#: plugins/text/gide-text-plugin.xml.h:12
msgid "Insert a file in the current document"
msgstr "Infoga en fil i det aktuella dokumentet"

#: plugins/text/gide-text-plugin.xml.h:13
msgid "Insert date and time"
msgstr "Infoga datum och tid"

#: plugins/text/gide-text-plugin.xml.h:14
msgid "Insert the standard GPL header in a C file"
msgstr "Infoga ett GPL-standardsidhuvud i en C-fil"

#: plugins/text/gide-text-plugin.xml.h:15
msgid "Insert the standard GPL header in a C++ file"
msgstr "Infoga ett GPL-standardsidhuvud i en C++-fil"

#: plugins/text/gide-text-plugin.xml.h:16
msgid "Insert the time and date in the document"
msgstr "Infoga datumet och tiden i dokumentet"

#: plugins/text/gide-text-plugin.xml.h:17
msgid "Replace all tabs in the document with spaces"
msgstr "Ers�tt alla tabulatorsteg i dokumentet med blanksteg"

#: plugins/text/gide-text-plugin.xml.h:18
msgid "Replace tabs with spaces"
msgstr "Ers�tt tabulatorsteg med blanksteg"

#: plugins/text/gide-text-plugin.xml.h:19
msgid "_Delete"
msgstr "_Ta bort"

#: plugins/text/gide-text-plugin.xml.h:20
msgid "_Insert"
msgstr "_Infoga"

#: plugins/text/text-insert.c:105
msgid "Insert File..."
msgstr "Infoga fil..."

#: plugins/text/text-insert.c:113
#, c-format
msgid "File Error: %s"
msgstr "Filfel: %s"

#: plugins/text/text-replace.c:79
msgid "Number of spaces per TAB?"
msgstr "Antal blanksteg per TABULATORSTEG?"

#: src/gI_window.c:60
msgid "gIDE"
msgstr "gIDE"

#: src/gI_window.c:62
msgid "Copyright Various Authors (C) 1998-2001"
msgstr "Copyright diverse f�rfattare � 1998-2001"

#. Parse options
#: src/gide.c:84
msgid "Oaf options"
msgstr "Oaf-alternativ"

#: src/gide.xml.h:1
msgid "About this application"
msgstr "Om detta program"

#: src/gide.xml.h:2
msgid "About..."
msgstr "Om..."

#: src/gide.xml.h:3
msgid "Customi_ze..."
msgstr "_Anpassa..."

#: src/gide.xml.h:4
msgid "Customize"
msgstr "Anpassa"

#: src/gide.xml.h:5
msgid "Customize toolbars"
msgstr "Anpassa verktygsrader"

# src/menus.c:315 src/menus.c:316
#: src/gide.xml.h:6
msgid "Exit"
msgstr "Avsluta"

#: src/gide.xml.h:7
msgid "Exit the program"
msgstr "Avsluta programmet"

#: src/gide.xml.h:8
msgid "Manage available plugin modules"
msgstr "Hantera tillg�ngliga insticksmoduler"

#: src/gide.xml.h:9
msgid "Plug-ins..."
msgstr "Insticksmoduler..."

#: src/gide.xml.h:10
msgid "Set application preferences"
msgstr "St�ll in programinst�llningar"

#: src/gide.xml.h:11
msgid "View the IDE in fullscreen mode"
msgstr "Visa utvecklingsmilj�n i helsk�rmsl�ge"

#: src/gide.xml.h:14
msgid "_Full screen"
msgstr "_Helsk�rm"

#: src/gide.xml.h:15
msgid "_Help"
msgstr "_Hj�lp"

#: src/gide.xml.h:16
msgid "_Settings"
msgstr "_Inst�llningar"

#: src/gide.xml.h:17
msgid "_Settings..."
msgstr "_Inst�llningar..."

#: src/gide.xml.h:18
msgid "_Tools"
msgstr "_Verktyg"

#: src/gide.xml.h:19
msgid "_View"
msgstr "_Visa"
