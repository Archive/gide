/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Copied here from Anjuta/src/gI_util.c */
/* FIXME: Because of the fact that this is for components, the dialog
 * parenting stuff is removed.  This needs to be resolved */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <grp.h>
#include <sys/types.h>
#include <pwd.h>

#include "gide-utils.h"

/* local prototypes */
static void modal_dialog_callback(GtkWidget *w,gpointer );

gchar *get_path( gchar *filename )
{
	gchar *str;

	str = g_dirname(filename);
	if (!strcmp(str, ".")) {
		free(str);
		str = g_strdup("");
	}

	return str;
}


glong isempty( gchar *s )
{
	if( !s )
		return( 1 );
	if( strlen( s ) == 0 )
		return( 1 );

	while(*s)
	{
		if( !isspace(*s) )
			return( 0 );
		s++;
	}

	return( 1 );
}


/*
 ---------------------------------------------------------------------
     Function: anjuta_ask_dialog( gchar *msg)
     Desc: Display a yes/no/cancel dialog with msg string
     Return: 0 = ok, 1 = no, 2 = cancel, -1 = close
 ---------------------------------------------------------------------
*/
gint anjuta_ask_dialog( gchar *msg )
{
	GtkWidget *dlg;

	dlg = gnome_message_box_new( msg,
	                             GNOME_MESSAGE_BOX_QUESTION,
	                             GNOME_STOCK_BUTTON_YES,
	                             GNOME_STOCK_BUTTON_NO,
	                             GNOME_STOCK_BUTTON_CANCEL,
	                             NULL );

	gtk_window_set_modal( GTK_WINDOW( dlg ), TRUE );
#if 0
	gnome_dialog_set_parent( GNOME_DIALOG( dlg ),
	                         GTK_WINDOW( main_window ) );
#endif

	return( gnome_dialog_run_and_close( GNOME_DIALOG( dlg ) ) );
}


/*
 ---------------------------------------------------------------------
     Function: anjuta_error_dialog( gchar *msg)
     Desc: Display a nice error dialog
     Return: nothing
 ---------------------------------------------------------------------
*/
void anjuta_error_dialog( gchar *msg)
{
	GtkWidget *dlg;
#if 0
	dlg = gnome_error_dialog_parented( msg,
	                                   GTK_WINDOW( main_window ) );
#else
	dlg = gnome_error_dialog (msg);
#endif

	gnome_dialog_run_and_close( GNOME_DIALOG( dlg ) );
}


/*
 ---------------------------------------------------------------------
     Function: anjuta_ok_dialog( gchar *msg)
     Desc: Display a nice ok dialog, mostly for info
     Return: nothing
 ---------------------------------------------------------------------
*/
void anjuta_ok_dialog( gchar *msg)
{
	GtkWidget *dlg;
#if 0
	dlg = gnome_ok_dialog_parented( msg,
	                                GTK_WINDOW( main_window ) );
#else
	dlg = gnome_ok_dialog (msg);
#endif	

	gnome_dialog_run_and_close( GNOME_DIALOG( dlg ) );
}


/*
 ---------------------------------------------------------------------
     Function: file_exist()
     Desc: checks if a given file exists (1 = yep, it does, 0 = nope,
                                          it doesnt, -1 = error)
 ---------------------------------------------------------------------
*/

gint file_exist( gchar *filename )
{
	struct stat s;
	glong ret;

	if( !filename )
	{
		g_warning( "Filename is NULL-Pointer!\n" );
		return( -1 );
	}

	if( strlen( filename ) == 0 )
	{
		g_warning( "Filename is 0 gchars glong!\n" );
		return( -1 );
	}

	ret = stat( filename, &s );
	if( (ret < 0) /*&& (errno == ENOENT)*/ )
	{
		return( 0 );
	}
	else
	{
		return( 1 );
	}
}


glong get_file_size( gchar *filename )
{
	struct stat f_stat;

	if( stat( filename, &f_stat ) != 0 )
	{
		g_print( "%s: %s\n", filename, g_strerror( errno ) );
		return( -1 );
	}

	return( (glong) f_stat.st_size );
}


gchar *SK_GetBetween( gchar *str, gchar start, gchar stop )
{
	gchar *ptr = NULL;

	while( *str )
	{
		if( *str == start && !ptr )
		{
			str++;
			ptr = str;
		}
		if( *str == stop && ptr )
		{
			*str = '\0';
			break;
		}
		str++;
	}

	if( !ptr )
		return( str );
	else
		return( ptr );
}


/*
(--------------------------------------------------------------------------)
(                                                                          )
(    Function Name: SK_GetFields                                           )
(    Prototype: int SK_GetFields( gchar *string, gchar *retarray[],          )
(                                 gchar separator )                         )
(    Purpose: Liefert die einzelnen, durch das Trennzeichen seperator      )
(             getrennten Felder, im Array retarray zurueck.                )
(    Return: Bei Fehler 0, andernfalls die Anzahl der zurueckgelieferten   )
(            Felder.                                                       )
(    Author: Steffen Kern                                                  )
(    Date: 05.04.1998 (Last Update: 07.04.1998)                            )
(                                                                          )
(--------------------------------------------------------------------------)
*/

gint SK_GetFields( gchar string[], gchar *retarray[], gchar separator )
{
	gint  HelpCount;
	gint  strpos;
	gint  ArrayCount;
	gchar *HelpString = (gchar *) malloc( (strlen( string ) + 1) * sizeof( gchar ) );
	gchar HelpChar;

	/* set start values */
	HelpCount = 0;
	strpos = 0;
	ArrayCount = 1;

	if( strlen( string ) == 0 )
	{
		free(HelpString);
		return(0);
	}

	while( strpos <= strlen( string ) )
	{
		HelpChar = string[strpos];
		if( HelpChar == '\n' ||
		        HelpChar == separator ||
		        strpos == strlen( string ) )
		{
			HelpString[ HelpCount ] = '\0';
			retarray[ArrayCount] = (gchar *)malloc( (HelpCount+1) * sizeof( gchar ) );
			strcpy( retarray[ArrayCount], HelpString );
			ArrayCount++;
			strpos++;
			HelpCount=0;
		}
		else
		{
			HelpString[ HelpCount ] = HelpChar;
			HelpCount++;
			strpos++;
		}
	}

	/* free allocated memory */
	free( HelpString );

	/* allocate memory for retarray[0] */
	retarray[0] = (gchar *)malloc( 10 );
	sprintf( retarray[0], "%d", ArrayCount );

	return( ArrayCount );
}  /* SK_GetFields */


GtkWidget *entry_dialog( gchar *label, gchar *title, void *ok_func )
{
	GtkWidget *button, *wlabel;
	static GtkWidget *dialog_window;
	static GtkWidget *entry;
	GtkWidget *hbox;

	dialog_window = gtk_dialog_new();
	gtk_signal_connect( GTK_OBJECT( dialog_window ), "destroy",
	                    GTK_SIGNAL_FUNC( gtk_widget_destroyed ),
	                    &dialog_window );
	gtk_container_set_border_width( GTK_CONTAINER( dialog_window ), 10 );
	gtk_window_set_title( GTK_WINDOW( dialog_window ), title );
	gtk_window_set_position( GTK_WINDOW( dialog_window ), GTK_WIN_POS_MOUSE );

	hbox = gtk_hbox_new( FALSE, 0 );
	gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog_window )->vbox ),
	                    hbox, FALSE, TRUE, 5 );
	gtk_widget_show( hbox );

	wlabel = gtk_label_new( label );
	gtk_box_pack_start( GTK_BOX( hbox ), wlabel, FALSE, TRUE, 5 );
	gtk_widget_show( wlabel );

	entry = gtk_entry_new();
	gtk_box_pack_start( GTK_BOX( hbox ), entry, TRUE, TRUE, 0 );
	gtk_signal_connect( GTK_OBJECT( entry ), "activate",
	                    GTK_SIGNAL_FUNC( ok_func ), (gpointer) entry );
	gtk_signal_connect_object( GTK_OBJECT( entry ), "activate",
	                           GTK_SIGNAL_FUNC( gtk_widget_destroy ),
	                           GTK_OBJECT( dialog_window ) );
	gtk_widget_grab_focus( entry );
	gtk_widget_show( entry );

	button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog_window )->action_area),
	                    button, TRUE, TRUE, 5 );
	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( ok_func ), (gpointer) entry );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked",
	                           GTK_SIGNAL_FUNC( gtk_widget_destroy ),
	                           GTK_OBJECT( dialog_window ) );
	GTK_WIDGET_SET_FLAGS( button, GTK_CAN_DEFAULT );
	gtk_widget_grab_default( button );
	gtk_widget_show( button );

	button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog_window )->action_area),
	                    button, TRUE, TRUE, 5 );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked",
	                           GTK_SIGNAL_FUNC( gtk_widget_destroy ),
	                           GTK_OBJECT( dialog_window ) );
	gtk_widget_show( button );

	gtk_widget_show( dialog_window );

	return( entry );
}


glong get_last_mod( gchar const *filename )
{
	struct stat f_stat;

	if( stat( filename, &f_stat ) != 0 )
	{
		/*g_print( "%s: %s\n", filename, g_strerror( errno ) );*/
		return( -1 );
	}

	return( (glong) f_stat.st_mtime );
}


glong get_write_stat( gchar *filename )
{
	struct stat f_stat;
	struct group *f_group;
	struct passwd *f_passwd;
	glong mnr;

	if( access( filename, W_OK ) == 0 )
	{
		return( 1 );
	}
	else
	{
		return( 0 );
	}

	/** old **/

	if( stat( filename, &f_stat ) != 0 )
	{
		g_print( "%s: %s\n", filename, g_strerror( errno ) );
		return( -1 );
	}

	/* others */
	if( (f_stat.st_mode & S_IRWXU) || (f_stat.st_mode & S_IWUSR) )
		return( 1 );

	/* user */
	if( (getuid() == f_stat.st_uid) || (geteuid() == f_stat.st_uid) )
	{
		if( (f_stat.st_mode & S_IWUSR) || (f_stat.st_mode & S_IRWXU) )
			return( 1 );
	}

	/* group */
	if( (getgid() == f_stat.st_gid) || (getegid() == f_stat.st_gid) )
	{
		if( (f_stat.st_mode & S_IWGRP) || (f_stat.st_mode & S_IRWXG) )
			return( 1 );
	}

	f_group = getgrgid( f_stat.st_gid );
	if( !f_group )
		return( 0 );

	mnr = 0;
	while( f_group->gr_mem[mnr] != NULL )
	{
		f_passwd = getpwnam( f_group->gr_mem[mnr] );
		if( !f_passwd )
			return( 0 );

		if( (f_passwd->pw_uid == getuid()) || (f_passwd->pw_uid == geteuid()) )
		{
			if( (f_stat.st_mode | S_IWGRP) || (f_stat.st_mode | S_IRWXG) )
				return( 1 );
		}
		mnr++;
	}

	return( 0 );
}


/*
--------------------------------------------------
gint modal_dialog(gchar *msg,gchar *title,gint type);

	type can be ~ (bitwise) with MB_OK MB_YESNO MB_CANCEL MB_RETRY
	return values are IDOK IDYES IDNO IDRETRY IDCANCEL
----------------------------------------------------
*/
gint modal_dialog(gchar *msg,gchar *title,gint type)
{
	gint *returnvalue;
	gint retvalue;
	GtkWidget *window;
	GtkWidget *buttonok;
	GtkWidget *buttoncancel;
	GtkWidget *buttonyes;
	GtkWidget *buttonno;
	GtkWidget *buttonretry;
	GtkWidget *vbox,*hbox;
	GtkWidget *label;
	GtkWidget *separator;

	returnvalue=g_malloc(sizeof(gint));
	*returnvalue=0;

	window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_CENTER);
	gtk_window_set_policy(GTK_WINDOW(window),FALSE,FALSE,FALSE);
	gtk_window_set_title(GTK_WINDOW(window),title);

	gtk_signal_connect_object(GTK_OBJECT(window),"destroy",
	                          GTK_SIGNAL_FUNC(gtk_widget_destroy),GTK_OBJECT(window));

	/*gtk_widget_set_usize(GTK_WIDGET(window),280,150);*/
	vbox=gtk_vbox_new(FALSE,0);
	gtk_container_add(GTK_CONTAINER(window),vbox);
	gtk_container_set_border_width(GTK_CONTAINER(vbox),10);

	separator=gtk_hseparator_new();
	label=gtk_label_new(msg);
	gtk_box_pack_start(GTK_BOX(vbox),label,TRUE,TRUE,0);
	hbox=gtk_hbox_new(FALSE,0);
	gtk_container_set_border_width(GTK_CONTAINER(window),10);
	gtk_box_pack_start(GTK_BOX(vbox),separator,FALSE,FALSE,10);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,10);

	gtk_signal_connect(GTK_OBJECT(window),"delete_event",
	                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);

	if(type & MB_OK) {
		buttonok = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
		gtk_box_pack_start(GTK_BOX(hbox),buttonok,TRUE,FALSE,0);

		if(!(type & MB_CANCEL))
			gtk_object_set_data(GTK_OBJECT(window),"ButtonID",(gpointer)IDOK);

		gtk_object_set_data(GTK_OBJECT(buttonok),"ButtonID",(gpointer)IDOK);
		gtk_signal_connect(GTK_OBJECT(buttonok),"clicked",
		                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);
	}
	else if(type & MB_YESNO)	{
		buttonyes = gnome_stock_button (GNOME_STOCK_BUTTON_YES);
		buttonno = gnome_stock_button (GNOME_STOCK_BUTTON_NO);

		gtk_box_pack_start(GTK_BOX(hbox),buttonyes,TRUE,FALSE,0);
		gtk_box_pack_start(GTK_BOX(hbox),buttonno,TRUE,FALSE,0);
		gtk_signal_connect(GTK_OBJECT(buttonyes),"clicked",
		                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);
		gtk_signal_connect(GTK_OBJECT(buttonno),"clicked",
		                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);

		if(!(type & MB_CANCEL))     /* if no CANCEL button then default is no if  */
			gtk_object_set_data(GTK_OBJECT(window),"ButtonID",(gpointer)IDNO);

		gtk_object_set_data(GTK_OBJECT(buttonyes),"ButtonID",(gpointer)IDYES);
		gtk_object_set_data(GTK_OBJECT(buttonno),"ButtonID",(gpointer)IDNO);
	}
	if(type & MB_RETRY)
	{
		buttonretry=gtk_button_new_with_label( _("Retry") );
		gtk_widget_set_usize(GTK_WIDGET(buttonretry),70,24);

		gtk_box_pack_start(GTK_BOX(hbox),buttonretry,TRUE,FALSE,0);

		gtk_object_set_data(GTK_OBJECT(buttonretry),"ButtonID",(gpointer)IDRETRY);
		gtk_signal_connect(GTK_OBJECT(buttonretry),"clicked",
		                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);

	}
	if(type & MB_CANCEL)
	{
		buttoncancel = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
		gtk_box_pack_start(GTK_BOX(hbox),buttoncancel,TRUE,FALSE,0);
		gtk_object_set_data(GTK_OBJECT(buttoncancel),"ButtonID",(gpointer)IDCANCEL);

		gtk_signal_connect(GTK_OBJECT(buttoncancel),"clicked",
		                   GTK_SIGNAL_FUNC(modal_dialog_callback),returnvalue);

		gtk_object_set_data(GTK_OBJECT(window),"ButtonID",(gpointer)IDCANCEL);
	}

	gtk_widget_show_all(window);

	gtk_grab_add(window);
	while (!*returnvalue)
	{	/* loop until callback return something */
		gtk_main_iteration_do(TRUE);
	};
	gtk_grab_remove(window);
	gtk_widget_destroy(window);

	retvalue = *returnvalue;
	g_free(returnvalue);

	return retvalue;
}

static void modal_dialog_callback(GtkWidget *w,gpointer ret)
{
	gint *r=(gint *)ret;
	*r=(gint)gtk_object_get_data(GTK_OBJECT(w),"ButtonID");
}


GtkWidget *entry_dialog_data( gchar *label, gchar *title, void *ok_func, gpointer data )
{
	GtkWidget *button, *wlabel;
	static GtkWidget *dialog_window;
	static GtkWidget *entry;
	GtkWidget *hbox;
	entry_data *ed;

	dialog_window = gtk_dialog_new();
	gtk_signal_connect( GTK_OBJECT( dialog_window ), "destroy",
	                    GTK_SIGNAL_FUNC( gtk_widget_destroyed ),
	                    &dialog_window );
	gtk_container_set_border_width( GTK_CONTAINER( dialog_window ), 10 );
	gtk_window_set_title( GTK_WINDOW( dialog_window ), title );
	gtk_window_set_position( GTK_WINDOW( dialog_window ), GTK_WIN_POS_MOUSE );
	gtk_widget_show( dialog_window );

	hbox = gtk_hbox_new( FALSE, 0 );
	gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog_window )->vbox ),
	                    hbox, FALSE, TRUE, 5 );
	gtk_widget_show( hbox );

	wlabel = gtk_label_new( label );
	gtk_box_pack_start( GTK_BOX( hbox ), wlabel, FALSE, TRUE, 5 );
	gtk_widget_show( wlabel );

	ed = (entry_data *) g_malloc0( sizeof( entry_data ) );
	ed->data = data;
	ed->dialog = dialog_window;

	entry = gtk_entry_new();
	ed->entry = entry;
	gtk_box_pack_start( GTK_BOX( hbox ), entry, TRUE, TRUE, 0 );
	gtk_signal_connect( GTK_OBJECT( entry ), "activate",
	                    GTK_SIGNAL_FUNC( ok_func ), ed );
	gtk_widget_grab_focus( entry );
	gtk_widget_show( entry );

	button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_box_pack_start( GTK_BOX( GTK_DIALOG( dialog_window )->action_area),
	                    button, TRUE, TRUE, 0 );
	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( ok_func ), ed );
	GTK_WIDGET_SET_FLAGS( button, GTK_CAN_DEFAULT );
	gtk_widget_grab_default( button );
	gtk_widget_show( button );

	return( entry );
}


static void anjuta_file_sel_button_clicked( GtkWidget *widget, glong *selected )
{
	*selected = (glong) gtk_object_get_data( GTK_OBJECT( widget ), "button" );
}


GList *anjuta_file_sel_new( const gchar *title, glong file_ops, glong multi )
{
	GtkWidget *filesel = NULL;
	glong *selected;

	selected = (glong *) malloc( sizeof( glong ) );

	*selected = 0;

	filesel = gtk_file_selection_new( title );

	gtk_object_set_data( GTK_OBJECT( GTK_FILE_SELECTION( filesel )->ok_button ),
	                     "button",
	                     (gpointer) 1 );
	gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( filesel )->ok_button ),
	                    "clicked",
	                    GTK_SIGNAL_FUNC( anjuta_file_sel_button_clicked ),
	                    (gpointer) selected );
	gtk_object_set_data( GTK_OBJECT( GTK_FILE_SELECTION( filesel )->cancel_button ),
	                     "button",
	                     (gpointer) 2 );
	gtk_signal_connect( GTK_OBJECT( GTK_FILE_SELECTION( filesel )->cancel_button ),
	                    "clicked",
	                    GTK_SIGNAL_FUNC( anjuta_file_sel_button_clicked ),
	                    (gpointer) selected );
	gtk_widget_show( filesel );
	gtk_grab_add( filesel );

	gtk_window_set_title( &(GTK_FILE_SELECTION( filesel )->window), title );
	if( file_ops )
	{
		gtk_file_selection_show_fileop_buttons( GTK_FILE_SELECTION( filesel ) );
	}
	else
	{
		gtk_file_selection_hide_fileop_buttons( GTK_FILE_SELECTION( filesel ) );
	}
	if( multi )
	{
		gtk_clist_set_selection_mode( GTK_CLIST( GTK_FILE_SELECTION( filesel )->file_list ), GTK_SELECTION_EXTENDED );
	}
	else
	{
		gtk_clist_set_selection_mode( GTK_CLIST( GTK_FILE_SELECTION( filesel )->file_list ), GTK_SELECTION_SINGLE );
	}

	while( !*selected )
	{
		gtk_main_iteration_do( TRUE );
	}

	gtk_grab_remove( filesel );

	if( *selected == 1 )	/* OK-Button */
	{
		gchar *sel_filename = g_strdup( gtk_file_selection_get_filename( GTK_FILE_SELECTION( filesel ) ) );
		GList *row = GTK_CLIST( GTK_FILE_SELECTION( filesel )->file_list )->row_list;
		glong rownum = 0;
		GList *filenames = NULL;
		gchar *filename = NULL;

		if( !multi )
		{
			filenames = g_list_append( filenames, sel_filename );
			goto after_multi;
		}

		while( row )
		{
			if( GTK_CLIST_ROW( row )->state == GTK_STATE_SELECTED )
			{
				if( gtk_clist_get_cell_type( GTK_CLIST( GTK_FILE_SELECTION( filesel )->file_list ), rownum, 0 ) == GTK_CELL_TEXT )
				{
					gtk_clist_get_text( GTK_CLIST( GTK_FILE_SELECTION( filesel )->file_list ), rownum, 0, &filename );
					if( filename != NULL )
					{
						gchar *fnwp = g_strconcat( get_path( sel_filename ), "/", filename, NULL );
						filenames = g_list_append( filenames, fnwp );
					}
				}
			}

			rownum++;
			row = g_list_next( row );
		}

after_multi:;

		gtk_widget_destroy( filesel );
		filesel = NULL;

		g_free( selected );

		filenames = g_list_first( filenames );

		return( filenames );
	}
	else					/* Cancel-Button */
	{
		gtk_widget_destroy( filesel );
		filesel = NULL;

		g_free( selected );

		return( NULL );
	}
}


/* ok, mikael, if you feel better now
   (but i still think that it doesnt matter) */
static void _anjuta_file_sel_free_list( gchar *data, gpointer doesnotmatter )
{
	if( data )
	{
		g_free( data );
	}
}

void anjuta_file_sel_free_list( GList *filenames )
{
	if( !filenames )
		return;

	g_list_foreach( filenames, (GFunc) _anjuta_file_sel_free_list, NULL );
	g_list_free( filenames );
}

gint
strcase_equal (gconstpointer v, gconstpointer v2)
{
        return strcasecmp ((const gchar*) v, (const gchar*)v2) == 0;
}

guint
strcase_hash (gconstpointer v)
{ 
        const unsigned char *s = (const unsigned char *)v;
        const unsigned char *p;
        guint h = 0, g; 
   
        for(p = s; *p != '\0'; p += 1) {
                h = ( h << 4 ) + tolower (*p);
                if ( ( g = h & 0xf0000000 ) ) {  
                        h = h ^ (g >> 24);
                        h = h ^ g;
                }
        }
 
        return h /* % M */;
}

gint file_check_if_exist( gchar *fname, gint askcreate )
{
	gchar txt[512];
	FILE *file;
	struct stat sta;
	gint e;
        
	e=stat(fname,&sta);
	if( !e && S_ISREG(sta.st_mode) )
	{   	/* file exist and is a regular file */
		return( 1 );	
	}

	if( askcreate && e==-1 )
	{
	    sprintf( txt, _("The file\n'%s'\ndoes not exist!\nDo you want to create the file?"), fname );
	    if( anjuta_ask_dialog( txt ) == 0 ) /*GNOME_YES )*/
	    {
	        file = fopen( fname, "w" );	/* this is not a good way!! */
		if( file ) 
		{
			fclose( file );
		}

		return( 1 );
	    }
	}
	else
	{
		if( !e )
		{ 
		    sprintf( txt, _("The file '%s' is not a regular file!"), fname );
                    anjuta_error_dialog( txt );
                }
	}

	return( 0 );
}
