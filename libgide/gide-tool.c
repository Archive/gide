/* Anjuta
 * Copyright 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <libgide/libgide.h>
#include <gdl/gdl.h>

struct _AnjutaToolPriv 
{
	AnjutaToolInitFn init_fn;
	AnjutaToolCleanupFn cleanup_fn;
};

enum {
	ARG_NONE,
	ARG_SHELL,
	ARG_LAST
};

static void anjuta_tool_destroy    (GtkObject         *object);
static void anjuta_tool_class_init (AnjutaToolClass     *class);
static void anjuta_tool_init       (GtkObject         *object);
static void set_prop             (BonoboPropertyBag *bag,
				  const BonoboArg   *arg,
				  guint              arg_id,
				  CORBA_Environment *ev,
				  gpointer           user_data);
static void get_prop             (BonoboPropertyBag *bag,
				  BonoboArg         *arg,
				  guint              arg_id,
				  CORBA_Environment *ev,
				  gpointer           user_data);

static GtkObjectClass *parent_class;


/* public routines */

AnjutaTool *
anjuta_tool_new (AnjutaToolInitFn init_fn, 
	       AnjutaToolCleanupFn cleanup_fn,
	       gpointer data)
{
	AnjutaTool *tool;

	g_return_val_if_fail (init_fn != NULL, NULL);

	tool = gtk_type_new (anjuta_tool_get_type ());	
	gtk_object_ref (GTK_OBJECT (tool));
	gtk_object_sink (GTK_OBJECT (tool));
    
	tool->props = bonobo_property_bag_new (get_prop, 
					       set_prop, tool);
	bonobo_property_bag_add (tool->props, "anjuta-shell", ARG_SHELL,
				 BONOBO_ARG_STRING, NULL, "The id of the shell that owns the plugin.",
				 BONOBO_PROPERTY_WRITEABLE);

	tool->priv->init_fn = init_fn;
	tool->priv->cleanup_fn = cleanup_fn;
	tool->data = data;

	return tool;
}

GtkType 
anjuta_tool_get_type (void)
{
	static GtkType type = 0;
        
	if (!type) {
		GtkTypeInfo info = {
			"AnjutaTool",
			sizeof (AnjutaTool),
			sizeof (AnjutaToolClass),
			(GtkClassInitFunc) anjuta_tool_class_init,
			(GtkObjectInitFunc) anjuta_tool_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}

/* private routines */

static void
anjuta_tool_destroy (GtkObject *object) 
{
	CORBA_Environment ev;
	AnjutaTool *tool = ANJUTA_TOOL (object);
	
	g_free (tool->priv);
	
	CORBA_exception_init (&ev);
	Bonobo_Unknown_unref (tool->shell, &ev);
	Bonobo_Unknown_unref (tool->ui_container, &ev);
	CORBA_exception_free (&ev);
	
}
        
static void
anjuta_tool_class_init (AnjutaToolClass *class) 
{
	GtkObjectClass *object_class = (GtkObjectClass*) class;
	parent_class = gtk_type_class (gtk_object_get_type ());
    
	object_class->destroy = anjuta_tool_destroy;
}

static void
anjuta_tool_init (GtkObject *object)
{
	AnjutaTool *tool = ANJUTA_TOOL (object);
	tool->priv = g_new0 (AnjutaToolPriv, 1);
}

static void 
set_prop (BonoboPropertyBag *bag, 
	  const BonoboArg *arg,
	  guint arg_id,
	  CORBA_Environment *ev,
	  gpointer user_data)
{
	AnjutaTool *tool = ANJUTA_TOOL (user_data);

	switch (arg_id) {
	case ARG_SHELL :
		{
		char *val = BONOBO_ARG_GET_STRING (arg);

		if (val[0]) {
			char *val = BONOBO_ARG_GET_STRING (arg);
			
			char *moniker_string = g_strdup_printf ("anjuta:%s", 
								val);
			tool->shell = bonobo_get_object (moniker_string, 
							 "IDL:GNOME/Development/Environment/Shell:1.0",
							 ev);
			tool->shell_id = g_strdup (val);
			tool->ui_container = GNOME_Development_Environment_Shell_getUIContainer (tool->shell, ev);
			
			tool->priv->init_fn (tool, tool->data);
		} else {
			tool->priv->cleanup_fn (tool, tool->data);
			
			g_free (tool->shell_id);
			bonobo_object_release_unref (tool->shell, ev);
			bonobo_object_release_unref (tool->ui_container, ev);
		}
		break;
		}
	default :
		g_assert_not_reached ();
	}
}

static void
get_prop (BonoboPropertyBag *bag, 
	  BonoboArg *arg,
	  guint arg_id,
	  CORBA_Environment *ev,
	  gpointer user_data)
{

}

