/* Anjuta
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef ANJUTA_DOC_H
#define ANJUTA_DOC_H

#include <gdl/GDL.h>

GNOME_Development_EditorBuffer anjuta_get_editor_buffer (AnjutaTool *tool);

gboolean anjuta_show_file (AnjutaTool* tool, gchar* path);
gchar* anjuta_get_current_filename (AnjutaTool* tool);
char *anjuta_get_current_word (AnjutaTool *tool);
glong anjuta_get_document_length (AnjutaTool* tool);
gchar* anjuta_get_document_chars (AnjutaTool* tool, glong start_pos, glong end_pos);

glong anjuta_get_line_num (AnjutaTool* tool);
void anjuta_set_line_num (AnjutaTool* tool, glong line);

glong anjuta_get_cursor_pos (AnjutaTool* tool);
void anjuta_set_cursor_pos (AnjutaTool* tool, glong pos);

void anjuta_insert_text_at_pos (AnjutaTool *tool, glong pos, gchar *text);
void anjuta_insert_text_at_cursor (AnjutaTool *tool, gchar *text);
void anjuta_delete_text (AnjutaTool *tool, glong startpos, glong endpos);

void anjuta_add_marker (AnjutaTool *tool, const char *path, 
		      int line, const char *type);
void anjuta_remove_marker (AnjutaTool *tool, const char *path, 
			 int line, const char *type);

#endif
