/* Anjuta
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef ANJUTA_TOOL_H
#define ANJUTA_TOOL_H

#include <bonobo.h>
#include <bonobo/bonobo-shlib-factory.h>

BEGIN_GNOME_DECLS

typedef struct _AnjutaTool      AnjutaTool;
typedef struct _AnjutaToolClass AnjutaToolClass;
typedef struct _AnjutaToolPriv  AnjutaToolPriv;

#define ANJUTA_TOOL_TYPE        (anjuta_tool_get_type ())
#define ANJUTA_TOOL(o)          (GTK_CHECK_CAST ((o), ANJUTA_TOOL_TYPE, AnjutaTool))
#define ANJUTA_TOOL_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), ANJUTA_TOOL_TYPE, AnjutaToolClass))
#define ANJUTA_IS_TOOL(o)       (GTK_CHECK_TYPE ((o), ANJUTA_TOOL_TYPE))
#define ANJUTA_IS_TOOL_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), ANJUTA_TOOL_TYPE))

typedef gboolean (*AnjutaToolInitFn) (AnjutaTool *tool,
				    gpointer closure);
typedef void     (*AnjutaToolCleanupFn) (AnjutaTool *tool,
				       gpointer closure);

struct _AnjutaTool {
	GtkObject parent;

	GNOME_Development_Environment_Shell shell;
	char *shell_id;
	Bonobo_UIContainer ui_container;	

	AnjutaToolPriv *priv;
	gpointer data;

	BonoboPropertyBag *props;
};

struct _AnjutaToolClass {
	BonoboObjectClass parent_class;
};

GtkType   anjuta_tool_get_type (void);
AnjutaTool *anjuta_tool_new      (AnjutaToolInitFn    init_fn,
			      AnjutaToolCleanupFn cleanup_fn,
			      gpointer          data);



#define ANJUTA_SHLIB_TOOL(oafiid, descr, init_fn, cleanup_fn, data)     \
static CORBA_Object                                                     \
make_tool (PortableServer_POA poa, const char *id,                      \
           gpointer impl_ptr, CORBA_Environment *ev)                    \
{                                                                       \
	AnjutaTool *tool;                                               \
	tool = anjuta_tool_new (init_fn, cleanup_fn, data);             \
        oaf_plugin_use (poa, impl_ptr);                                 \
	return CORBA_Object_duplicate (BONOBO_OBJREF (tool->props), ev);\
}                                                                       \
static const OAFPluginObject plugin_list[] = {{oafiid, make_tool}, {NULL}}; \
const OAFPlugin OAF_Plugin_info = { plugin_list, descr };


#define ANJUTA_SHLIB_TOOL_SIMPLE(oafiid, descr, name, datadir, xmlfile, verbvar, data) \
static gboolean \
impl_init (AnjutaTool *tool, gpointer closure) \
{ \
	BonoboUIComponent *uic; \
	CORBA_Environment ev; \
	g_return_val_if_fail (tool != NULL, FALSE); \
	g_return_val_if_fail (ANJUTA_IS_TOOL (tool), FALSE); \
	CORBA_exception_init (&ev); \
	uic = bonobo_ui_component_new (name); \
	bonobo_ui_component_set_container (uic, tool->ui_container); \
	bonobo_ui_util_set_ui (uic, datadir, xmlfile, name); \
	bonobo_ui_component_add_verb_list_with_data (uic, verbvar, tool); \
	gtk_object_set_data(GTK_OBJECT(tool), "ui_component", uic); \
	CORBA_exception_free(&ev); \
	return TRUE; \
} \
static void \
impl_cleanup (AnjutaTool *tool, gpointer closure)\
{ \
	bonobo_ui_component_unset_container(gtk_object_get_data(GTK_OBJECT( \
		tool), "ui_component")); \
	gtk_object_remove_data(GTK_OBJECT(tool), "ui_component"); \
} \
ANJUTA_SHLIB_TOOL(oafiid, descr, impl_init, impl_cleanup, data)

END_GNOME_DECLS
#endif
