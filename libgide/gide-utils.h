/* Anjuta
 * Copyright (C) 1998-2000 Steffen Kern
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Copied here from Anjuta/src/gI_common.h */

#ifndef GI_TOOLS_H
#define GI_TOOLS_H

#include <gnome.h>

/*
 * Prototypes for 'gI_common.c'
 */
gchar *get_path( gchar *filename );
glong isempty( gchar *s );
gint anjuta_ask_dialog( gchar *msg );
void anjuta_error_dialog( gchar *msg);
void anjuta_ok_dialog( gchar *msg);
gint file_exist( gchar *filename );
glong get_file_size( gchar *filename );
gchar *SK_GetBetween( gchar *str, gchar start, gchar stop );
gint SK_GetFields( gchar string[], gchar *retarray[], gchar separator );
GtkWidget *entry_dialog( gchar *label, gchar *title, void *ok_func );
glong get_last_mod( gchar const *filename );
glong get_write_stat( gchar *filename );
gint modal_dialog(gchar *msg,gchar *title,gint type);
guint strcase_hash  (gconstpointer v);
gint  strcase_equal (gconstpointer v, gconstpointer v2);
gint file_check_if_exist( gchar *fname, gint askcreate );

#ifndef STRLEN
#  define STRLEN 1024
#endif

/* defines for modal_dialog */
#define MB_YESNO 0x0002
#define MB_CANCEL 0x0004 
#define MB_OK 0x0008
#define MB_RETRY 0x0010
#define IDYES 1
#define IDNO 2
#define IDCANCEL 3
#define IDRETRY 4
#define IDOK 5

GtkWidget *entry_dialog_data( gchar *label, gchar *title, void *ok_func, gpointer data );

typedef struct _entry_data entry_data;
struct _entry_data
{
	GtkWidget *dialog;
        GtkWidget *entry;
        gpointer data;
};

GList *anjuta_file_sel_new( const gchar *title, glong file_ops, glong multi );
void anjuta_file_sel_free_list( GList *filenames );

/* useful macros */
/* number of items in an array */
#define anjuta_number(arr) ((gint)(sizeof(arr) / sizeof(arr[0])))

#endif

