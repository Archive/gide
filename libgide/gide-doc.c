/* Anjuta
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

/*
 * Document related functions for anjuta plugins
 */

/* TODO: unref, add checks, check exceptions */

#include <config.h>

#include <ctype.h>
#include <libgide/libgide.h>
#include <gdl/GDL.h>

GNOME_Development_EditorBuffer
anjuta_get_editor_buffer (AnjutaTool *tool)
{
	GNOME_Development_EditorBuffer	buffer;
	Bonobo_Unknown			obj;
	CORBA_Environment		ev;
	char *moniker_string;
	
	
	CORBA_exception_init(&ev);

	moniker_string = g_strdup_printf ("anjuta:%s!CurrentDocument", tool->shell_id);
	obj = bonobo_get_object (moniker_string, "IDL:GNOME/Development/Environment/Document:1.0", &ev);
	g_free (moniker_string);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print("no document open\n");
		CORBA_exception_free(&ev);
		return NULL;
	}

	/* Get the EditorBuffer interface */
	buffer = GNOME_Development_Environment_Document_getEditorInterface(
		obj, "IDL:GNOME/Development/EditorBuffer:1.0", &ev);
	if(CORBA_Object_is_nil(buffer, &ev))
	{
		g_print("not an editor buffer\n");
		CORBA_exception_free(&ev);
		return NULL;
	}

	bonobo_object_release_unref (obj, &ev);

	CORBA_exception_free(&ev);
	return buffer;
}

static GNOME_Development_EditorGutter
get_editor_gutter (AnjutaTool *tool, const char *path)
{
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document doc;
	GNOME_Development_EditorGutter gutter;
	CORBA_Environment ev;
	
	CORBA_exception_init(&ev);

	docman = GNOME_Development_Environment_Shell_getObject (
		tool->shell, "DocumentManager", &ev);
	doc = GNOME_Development_Environment_DocumentManager_openFile (docman,
								      path,
								      &ev);
	gutter = GNOME_Development_Environment_Document_getEditorInterface(
		doc, "IDL:GNOME/Development/EditorGutter:1.0", &ev);
	
	if(CORBA_Object_is_nil(gutter, &ev)) {
		g_print("no editor gutter\n");
		CORBA_exception_free(&ev);
		return CORBA_OBJECT_NIL;
	}
	bonobo_object_release_unref (doc, &ev);
	bonobo_object_release_unref (docman, &ev);
	
	CORBA_exception_free (&ev);

	return gutter;
}


gboolean
anjuta_show_file (AnjutaTool *tool, gchar *path)
{
	GNOME_Development_Environment_DocumentManager docman;
	GNOME_Development_Environment_Document doc;
	CORBA_Environment ev;
	char *moniker;
	
	CORBA_exception_init (&ev);

	moniker = g_strdup_printf ("anjuta:%s!DocumentManager", tool->shell_id);
	docman = bonobo_get_object (moniker, "IDL:GNOME/Development/Environment/DocumentManager:1.0", &ev);
	g_free (moniker);

	if (ev._major != CORBA_NO_EXCEPTION && !CORBA_Object_is_nil (docman, &ev))
		return FALSE;
	
	doc = GNOME_Development_Environment_DocumentManager_openFile (docman, path, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		return FALSE;

	bonobo_object_release_unref(doc, &ev);
	bonobo_object_release_unref(docman, &ev);

	CORBA_exception_free(&ev);

	return TRUE;
}

glong
anjuta_get_cursor_pos (AnjutaTool *tool)
{
	glong				pos = 0;
	Bonobo_Control			ctrl;
	GList*				props;
	GList*				l;
	Bonobo_PropertyBag		bag;
	CORBA_Environment		ev;
	Bonobo_Unknown			obj;

	CORBA_exception_init(&ev);

	/* Get the current document */
	obj = GNOME_Development_Environment_Shell_getObject (tool->shell,
							     "CurrentDocument",
							     &ev);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print ("no document open\n");
		CORBA_exception_free(&ev);
		return 0;
	}

	ctrl = GNOME_Development_Environment_Document_getEditor (obj, &ev);

	bag = Bonobo_Control_getProperties(ctrl, &ev);

	props = bonobo_property_bag_client_get_property_names(bag, &ev);
	for(l = props; l != NULL; l = l->next) {
		CORBA_TypeCode tc;
		char *name = l->data;

		tc = bonobo_property_bag_client_get_property_type(bag, name,
			NULL);

		if(!strcmp(name, "position") && tc->kind == CORBA_tk_long) {
			pos = bonobo_property_bag_client_get_value_glong(bag,
				name, NULL);
		}
	}
	g_list_free(props);

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free(&ev);
	return pos;
}

void
anjuta_set_cursor_pos (AnjutaTool *tool, glong pos)
{
	Bonobo_Control			ctrl;
	GList*				props;
	GList*				l;
	Bonobo_PropertyBag		bag;
	CORBA_Environment		ev;
	Bonobo_Unknown			obj;

	CORBA_exception_init(&ev);

	/* Get the current document */
	obj = GNOME_Development_Environment_Shell_getObject (tool->shell,
							     "CurrentDocument",
							     &ev);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print ("no document open\n");
		CORBA_exception_free (&ev);
		return;
	}

	ctrl = GNOME_Development_Environment_Document_getEditor (obj, &ev);

	bag = Bonobo_Control_getProperties (ctrl, &ev);

	props = bonobo_property_bag_client_get_property_names (bag, &ev);
	for(l = props; l != NULL; l = l->next) {
		CORBA_TypeCode tc;
		char *name = l->data;

		tc = bonobo_property_bag_client_get_property_type (bag, name, NULL);

		if (!strcmp (name, "position") && tc->kind == CORBA_tk_long) {
			bonobo_property_bag_client_set_value_glong (bag, name,
								    pos, NULL);
		}
	}

	bonobo_object_release_unref (obj, &ev);
	bonobo_object_release_unref (bag, &ev);

	g_list_free (props);

	CORBA_exception_free (&ev);
}


static int 
anjuta_find_word_begin (AnjutaTool *tool, 
			GNOME_Development_EditorBuffer buffer,
			int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	
	CORBA_exception_init (&ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);
	while (!BONOBO_EX (&ev) && 
	       iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
	       pos >= 0) {
		
		pos--;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}
	
	if (!BONOBO_EX (&ev)) {
		CORBA_free (iobuf);
	}
	CORBA_exception_free (&ev);
	return pos;
}

static int
anjuta_find_word_end (AnjutaTool *tool, 
		      GNOME_Development_EditorBuffer buffer,
		      int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	long size;

	CORBA_exception_init (&ev);
	size = GNOME_Development_EditorBuffer_getLength (buffer, &ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);
	while (!BONOBO_EX (&ev) && 
	       iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
		pos < size) {
		
		pos++;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}
	
	if (!BONOBO_EX (&ev)) {
		CORBA_free (iobuf);
	}
	CORBA_exception_free (&ev);
	return pos;
}

char *
anjuta_get_current_word (AnjutaTool *tool)
{
	char *str = NULL;
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer buffer;

	CORBA_exception_init (&ev);
	buffer = anjuta_get_editor_buffer (tool);
	/* FIXME: This could be a lot lot more efficient than it is. */
	if (!CORBA_Object_is_nil (buffer, &ev)) {
		int begin, end, pos;
 		GNOME_Development_EditorBuffer_iobuf *iobuf;

		pos = anjuta_get_cursor_pos (tool);

		begin = anjuta_find_word_begin (tool, buffer, pos);
		end = anjuta_find_word_end (tool, buffer, pos);

		if (end - begin > 1) {
			GNOME_Development_EditorBuffer_getChars (buffer, 
								 begin,
								 end - begin, 
								 &iobuf,
								 &ev);
			if (!BONOBO_EX (&ev)) {
				if (iobuf->_length > 0) {
					str = g_strndup (iobuf->_buffer, 
							 iobuf->_length);
				}
				CORBA_free (iobuf);
			}
		}
	}
	
	CORBA_exception_free (&ev);
	return str;
}

// almost the same as get_cursor_pos: should write a common function
glong
anjuta_get_line_num (AnjutaTool *tool)
{
	glong pos = 0;
	Bonobo_Control ctrl;
	GList *props;
	GList *l;
	Bonobo_PropertyBag bag;
	CORBA_Environment ev;
	Bonobo_Unknown obj;

	CORBA_exception_init (&ev);

	/* Get the current document */
	obj = GNOME_Development_Environment_Shell_getObject (tool->shell,
							    "CurrentDocument",
							     &ev);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print ("no document open\n");
		CORBA_exception_free(&ev);
		return 0;
	}

	ctrl = GNOME_Development_Environment_Document_getEditor (obj, &ev);

	bag = Bonobo_Control_getProperties (ctrl, &ev);

	props = bonobo_property_bag_client_get_property_names (bag, &ev);
	for (l = props; l != NULL; l = l->next) {
		CORBA_TypeCode tc;
		char *name = l->data;

		tc = bonobo_property_bag_client_get_property_type(bag, name,
								  NULL);

		if (!strcmp (name, "line_num") && tc->kind == CORBA_tk_long) {
			pos = bonobo_property_bag_client_get_value_glong (bag,
									  name,
									  NULL);
		}
	}
	g_list_free (props);

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free (&ev);
	return pos;
}

void
anjuta_set_line_num (AnjutaTool *tool, glong pos)
{
	Bonobo_Control ctrl;
	GList *props;
	GList *l;
	Bonobo_PropertyBag bag;
	CORBA_Environment ev;
	Bonobo_Unknown obj;

	CORBA_exception_init (&ev);

	/* Get the current document */
	obj = GNOME_Development_Environment_Shell_getObject (tool->shell,
							     "CurrentDocument",
							     &ev);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print ("no document open\n");
		CORBA_exception_free (&ev);
		return;
	}

	ctrl = GNOME_Development_Environment_Document_getEditor (obj, &ev);

	bag = Bonobo_Control_getProperties (ctrl, &ev);

	props = bonobo_property_bag_client_get_property_names (bag, &ev);
	for (l = props; l != NULL; l = l->next) {
		CORBA_TypeCode tc;
		char *name = l->data;

		tc = bonobo_property_bag_client_get_property_type (bag, name,
								   NULL);

		if (!strcmp (name, "line_num") && tc->kind == CORBA_tk_long) {
			bonobo_property_bag_client_set_value_glong (bag, name,
								    pos, NULL);
		}
	}
	g_list_free (props);

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free (&ev);
}

void
anjuta_insert_text_at_pos (AnjutaTool *tool, glong pos, gchar *text)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free(&ev);
		return;
	}

	/* Insert text */
	GNOME_Development_EditorBuffer_insert (buffer, pos, text, &ev);

	anjuta_set_cursor_pos (tool, pos + strlen (text));

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);
}

void
anjuta_insert_text_at_cursor (AnjutaTool *tool, gchar *text)
{
	glong pos;

	pos = anjuta_get_cursor_pos (tool);

	anjuta_insert_text_at_pos (tool, pos, text);
}

void
anjuta_delete_text (AnjutaTool *tool, glong startpos, glong endpos)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	glong temp;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free (&ev);
		return;
	}

	if (startpos > endpos) {
		temp = startpos;
		startpos = endpos;
		endpos = temp;
	}

	GNOME_Development_EditorBuffer_delete (buffer, startpos, endpos - startpos, &ev);

	bonobo_object_release_unref (buffer, &ev);
	CORBA_exception_free(&ev);
}

glong
anjuta_get_document_length (AnjutaTool *tool)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	glong length;

	CORBA_exception_init (&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer)
		return 0;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);

	return length;
}

gchar *
anjuta_get_document_chars (AnjutaTool *tool, glong start_pos, glong end_pos)
{
	GNOME_Development_EditorBuffer	buffer;
	CORBA_Environment		ev;
	glong				length;
	glong				temp;
	GNOME_Development_EditorBuffer_iobuf*	buf;
	gchar*				result;

	if (start_pos > end_pos) {
		temp = start_pos;
		start_pos = end_pos;
		end_pos = temp;
	}

	CORBA_exception_init(&ev);

	buffer = anjuta_get_editor_buffer (tool);
	if (!buffer)
		return NULL;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	if (start_pos > length)
		return NULL;

	if (end_pos > length)
		end_pos = length;

	GNOME_Development_EditorBuffer_getChars (buffer, start_pos,
						 end_pos - start_pos, 
						 &buf, &ev);

	result = g_strndup (buf->_buffer, buf->_length);

	CORBA_free (buf);

	bonobo_object_release_unref (buffer, &ev);
	
	CORBA_exception_free (&ev);

	return result;
}

gchar *
anjuta_get_current_filename (AnjutaTool *tool)
{
	CORBA_Environment ev;
	Bonobo_Unknown obj;
	CORBA_char *filename;
	gchar *ret;

	CORBA_exception_init(&ev);

	/* Get the current document */
	obj = GNOME_Development_Environment_Shell_getObject (tool->shell,
							     "CurrentDocument",
							     &ev);

	if (CORBA_Object_is_nil (obj, &ev)) {
		g_print ("no document open\n");
		CORBA_exception_free (&ev);
		return NULL;
	}

	filename = GNOME_Development_Environment_Document_getFilename (obj, &ev);

	/* need conversion? */
	ret = g_strdup (filename);

	CORBA_free (filename);

	bonobo_object_release_unref (obj, &ev);

	CORBA_exception_free(&ev);

	return ret;
}

void
anjuta_add_marker (AnjutaTool *tool, const char *filename, 
		 int line, const char *marker)
{
	CORBA_Environment ev;
	GNOME_Development_EditorGutter gutter;
	
	CORBA_exception_init (&ev);
	gutter = get_editor_gutter (tool, filename);

	GNOME_Development_EditorGutter_addMarker (gutter, line, marker, &ev);
	
	bonobo_object_release_unref (gutter, &ev);
	CORBA_exception_free (&ev);
}

void
anjuta_remove_marker (AnjutaTool *tool, const char *filename, 
		    int line, const char *marker)
{
	CORBA_Environment ev;
	GNOME_Development_EditorGutter gutter;
	
	CORBA_exception_init (&ev);
	gutter = get_editor_gutter (tool, filename);

	GNOME_Development_EditorGutter_removeMarker (gutter, line, 
						     marker, &ev);
	bonobo_object_release_unref (gutter, &ev);
	
	CORBA_exception_free (&ev);
}

